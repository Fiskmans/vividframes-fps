

struct VertexInput
{
	float4 myPosition		:	POSITION;
	float4 myNormal			:	NORMAL;
	float4 myTangent		:	TANGENT;
	float4 myBiTangent		:	BITANGENT;
#ifdef VERTEXCOLOR
	float4 myColor			:	COLOR;
#endif
	float2 myUV				:	UV0;
#ifdef MULTIPLE_UV
	float2 myUV1			:	UV1;
	float2 myUV2			:	UV2;
#endif
#ifdef HAS_BONES
	uint myBones[BONESPERVERTEX]			:	BONES;
	float myBoneWeights[BONESPERVERTEX]	:	BONEWEIGHTS;
#if BONESPERVERTEX == 0
#error "uhmm model has no bones"
#endif // BONESPERVERTEX == 0
#endif
};

struct VertexToPixel
{
	float4 myPosition	:	SV_POSITION;
	float4 myWorldPos	:	POSITION;
	float4 myNormal		:	NORMAL;
	float4 myTangent	:	TANGENT;
	float4 myBiTangent	:	BITANGENT;
#ifdef VERTEXCOLOR
	float4 myColor		:	COLOR;
#endif
	float2 myUV			:	UV0;
#ifdef MULTIPLE_UV
	float2 myUV1		:	UV1;
	float2 myUV2		:	UV2;
#endif
};

struct PixelOutput
{
	float4 myColor		:	SV_TARGET;
};


cbuffer frameData : register(b0)
{
	float4x4 worldToCameraMatrix;
	float4x4 cameraToProjectionMatrix;
	float4 environmentLightDirection;
	float3 environmentLightColor;
	float enviromentLightIntensity;
	float3 cameraPosition;
	float totalTime;
};

cbuffer objectData : register(b1)
{
	struct PointLight
	{
		float3 position;
		float intensity;
		float3 color;
		float range;
	} pointLights[NUMBEROFPOINTLIGHTS];
	float4x4 modelToWorldMatrix;
	float4 tint;
	uint myBoneOffsetIndex;
	uint numOfUsedPointLights;
	float objectLifeTime;
	float objectExpectedLifeTime;
};

cbuffer BoneBuffer : register(b2)
{
	float4x4 myBoneTransforms[NUMBEROFANIMATIONBONES];
}


Texture2D AlbedoMap						: register(t0);
Texture2D NormalMap						: register(t1);
Texture2D MaterialMap					: register(t2);
TextureCube SkyBox						: register(t3);
TextureCube EnvironmentLight			: register(t4);
Texture2D BoneTexture					: register(t5);

Texture2D PerlinNoise					: register(t7);