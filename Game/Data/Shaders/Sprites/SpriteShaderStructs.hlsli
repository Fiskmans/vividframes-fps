struct VertexInput
{
    float4 myPosition : POSITION;
    float2 myUV : UV;
};

struct VertexToPixel
{
    float4 myPosition : SV_POSITION;
    float2 myUV : UV;
};

struct PixelOutput
{
    float4 myColor : SV_TARGET;
};

cbuffer FrameBuffer : register(b0)
{
	float2 screenSize;
	float2 junk;
}

cbuffer ObjectBuffer : register(b1)
{
    float4x4 toWorld;
	float4 color;
	float2 UVMin;
	float2 UVMax;
	float4 pivot;
}

Texture2D albedoTexture : register(t0);
SamplerState defaultSampler : register(s0);