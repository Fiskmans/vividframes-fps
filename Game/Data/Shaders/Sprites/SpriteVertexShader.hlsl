#include "SpriteShaderStructs.hlsli"

VertexToPixel vertexShader(VertexInput input)
{
    VertexToPixel returnValue;
	float4 pos = input.myPosition;

	pos.x -= pivot.x;
	pos.y += pivot.y;

    returnValue.myPosition = mul(toWorld, pos);

	returnValue.myUV.x = (input.myUV.x + UVMin.x) * (UVMax.x - UVMin.x);
	returnValue.myUV.y = (input.myUV.y + UVMin.y) * (UVMax.y - UVMin.y);

    return returnValue;
}