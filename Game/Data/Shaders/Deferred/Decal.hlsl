#include "../Fullscreen/FullscreenShaderStructs.hlsli"
#include "../PBRFunctions.hlsli"
struct GBufferOutput
{
	float4 myWorldPosition		:	SV_TARGET0;
	float4 myAlbedo				:	SV_TARGET1;
	float4 myNormal				:	SV_TARGET2;
	float4 myVertexNormal		:	SV_TARGET3;
	float myMetalness			:	SV_TARGET4;
	float myRoughness			:	SV_TARGET5;
	float myAmbientOcclusion	:	SV_TARGET6;
	float myEmissive			:	SV_TARGET7;
};


cbuffer LightBuffer : register(b0)
{
	float4x4 toCamera;
	float4x4 toProj;

	float3 Position;
	float Intensity;
	float3 CameraPosition;
	float Range;
};

TextureCube EnvironmentLight : register(t8);

Texture2D Depth : register(t15);

GBufferOutput pixelShader(VertexToPixel input)
{
	GBufferOutput output;
	float bias = 0.01;

	float4 worldPosition = resource1.Sample(defaultSampler, input.myUV).rgba;
	float4 albedo = resource2.Sample(defaultSampler, input.myUV).rgba;
	float3 Normal = resource3.Sample(defaultSampler, input.myUV).rgb;
	float3 VertexNormal = resource4.Sample(defaultSampler, input.myUV).rgb;
	float metalness = resource5.Sample(defaultSampler, input.myUV).r;
	float roughness = resource6.Sample(defaultSampler, input.myUV).r;
	float AO = resource7.Sample(defaultSampler, input.myUV).r;
	float emissive = resource8.Sample(defaultSampler, input.myUV).r;


	output.myWorldPosition = worldPosition;
	output.myAlbedo.rgba = albedo;
	output.myNormal = float4(Normal, 1);
	output.myVertexNormal = float4(VertexNormal, 1);
    output.myMetalness = 1; //metalness;
    output.myRoughness = 1; //roughness;
    output.myAmbientOcclusion = 0;// AO;
    output.myEmissive = 0;//emissive;



	float3 toEye = normalize(CameraPosition - worldPosition.xyz);
	float3 toLight = worldPosition.xyz - Position;

	float align = dot(normalize(toLight), Normal);
	if (align > 0)
	{
		discard;
	}
	 
	float4 camPos = mul(toCamera, float4(worldPosition.xyz, 1));
	float4 projPos = mul(toProj,camPos);
    //bias /= 1/projPos.z;
	projPos /= projPos.w;
	if (projPos.x < -1 || projPos.x > 1 || projPos.y < -1 || projPos.y > 1 || projPos.z < -1 || projPos.z > 1)
	{
		discard;
	}
	float2 projuv = (projPos.xy + float2(1, 1)) * float2(0.5, -0.5);
	

	float4 spotData = resource10.Sample(defaultSampler, projuv);
	
	
    float depth = resource9.Sample(defaultSampler, projuv).r;
    if (abs(depth - projPos.z) > bias)
	{
		discard;
	}
	float val = 1.0;
	if (projuv.y > -0.5)
	{
		val = depth;
	}
	else
	{
		val = projPos.z;
	}

    output.myAlbedo.rgba = float4(lerp(albedo.rgb, spotData.rgb, spotData.a), spotData.a);
    //output.myAlbedo.rgba = lerp(float4(1, 0, 0, 1), float4(0, 1, 0, 1), val);
	return output;
}