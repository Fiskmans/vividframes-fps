#include "../Fullscreen/FullscreenShaderStructs.hlsli"
#include "../PBRFunctions.hlsli"

cbuffer LightBuffer : register(b0)
{
	float4x4 toCamera[6];
	float4x4 toProj[6];

	float3 LightColor;
	float Intensity;
	float3 Position;
	float Range;
	float3 CameraPosition;
	float trash;
};

TextureCube EnvironmentLight : register(t8);

Texture2D Depth : register(t15);

unsigned int MajorAxis(float3 aVector)
{
	float x = abs(aVector.x);
	float y = abs(aVector.y);
	float z = abs(aVector.z);


	unsigned int axisIndex = 2 * (y > x&& y > z) + 4 * (z > x&& z > y);
	axisIndex += (axisIndex == 0 && aVector.x < 0);
	axisIndex += (axisIndex == 2 && aVector.y < 0);
	axisIndex += (axisIndex == 4 && aVector.z < 0);
	return axisIndex;
}

PixelOutput pixelShader(VertexToPixel input)
{
	float bias = 0.001;
	if (Depth.Sample(defaultSampler, input.myUV).r == 1)
	{
		discard;
	}

	PixelOutput returnValue;
	float3 worldPosition = resource1.Sample(defaultSampler, input.myUV).rgb;
	float3 albedo = resource2.Sample(defaultSampler, input.myUV).rgb;
	float3 normal = resource3.Sample(defaultSampler, input.myUV).rgb;
	//float3 worldNormal = resource4.Sample(defaultSampler, input.myUV).rgb;
	float metalness = resource5.Sample(defaultSampler, input.myUV).r;
	float roughness = resource6.Sample(defaultSampler, input.myUV).r;
	//float ambientOcclusion = resource7.Sample(defaultSampler, input.myUV).r;
	//float emissive = resource8.Sample(defaultSampler, input.myUV).r;

	float3 toEye = normalize(CameraPosition - worldPosition);

	float3 specColor = lerp((float3) 0.04, albedo, metalness);
	float3 diffColor = lerp((float3) 0.00, albedo, 1 - metalness);

	


	float3 toLight = worldPosition - Position;



	unsigned int cameraIndex = MajorAxis(toLight);

	unsigned int reMapping[6] = 
	{
		5,
		4,
		2,
		3,
		0,
		1
	};
	cameraIndex = reMapping[cameraIndex];
	 
	//worldPosition.z *= -1;
	float4 camPos = mul(toCamera[cameraIndex], float4(worldPosition, 1));
	float4 projPos = mul(toProj[cameraIndex],camPos);
	projPos /= projPos.w;
	if (projPos.x < -1 || projPos.x > 1 || projPos.y < -1 || projPos.y > 1 || projPos.z < -1 || projPos.z > 1)
	{
		discard;
	}
	float2 projuv = (projPos.xy - float2(1, -1)) * float2(0.5/6, -0.5) +float2(1.0/6.0* (cameraIndex+1), 0);
	

	float depth = resource9.Sample(defaultSampler, projuv).r;

	if (depth < projPos.z - bias)
	{
		discard;
	}

	float sampleRad = 0.001;

	//float occlusion = 0;
	//occlusion += resource9.Sample(defaultSampler, projuv + float2(1, 0) * sampleRad).r > depth - bias;
	//occlusion += resource9.Sample(defaultSampler, projuv + float2(-1, 0) * sampleRad).r > depth - bias;
	//occlusion += resource9.Sample(defaultSampler, projuv + float2(0, 1) * sampleRad).r > depth - bias;
	//occlusion += resource9.Sample(defaultSampler, projuv + float2(0, -1) * sampleRad).r > depth - bias;
	//
	//occlusion /= 4;

	//returnValue.myColor.rgba = pow(resource9.Sample(defaultSampler, projuv), 12) * occlusion;
	//returnValue.myColor.rgba = colors[cameraIndex];
	
	returnValue.myColor.rgba = pow(abs(EvaluatePointLight_Alpha(diffColor, specColor, normal, roughness, LightColor, Intensity, Range, Position, worldPosition, toEye)), 1.0 / 2.2);// *occlusion;
	//returnValue.myColor.gb = float2(1, 1);
	//returnValue.myColor.a = 1;
	return returnValue;
}