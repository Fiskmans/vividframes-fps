#include "Data/Shaders/ShaderStructs.hlsli"
SamplerState defaultSampler : register(s0);

#ifndef MULTIPLE_UV
#error "model missing data"
#endif



SamplerState MeshTextureSampler
{
	Filter = MIN_MAG_MIP_LINEAR;
	AddressU = Wrap;
	AddressV = Wrap;
};

PixelOutput pixelShader(VertexToPixel input)
{
	//Panner for weapon slashes
	PixelOutput returnValue;

	float clampValueGreen = clamp(floor(objectLifeTime * 1) * 0.1, 0,0.9);
	float clampValueRed = clamp((objectLifeTime * 1) * 0.1, 0, 0.9);

	float4 materialTextureRed = MaterialMap.Sample(defaultSampler, float2(input.myUV.x * 4, (input.myUV.y * -1) + 0.1));
	float4 materialTextureGreen = MaterialMap.Sample(defaultSampler, float2(input.myUV.x * 4, input.myUV.y));
	
	float4 albedoTexture = AlbedoMap.Sample(defaultSampler, float2(input.myUV.x * 4, input.myUV.y));

	float redMask = materialTextureRed.r;
	float greenMask = materialTextureGreen.g;





	returnValue.myColor = albedoTexture;
	returnValue.myColor.a = (redMask + greenMask) * 1;

	return returnValue;
}