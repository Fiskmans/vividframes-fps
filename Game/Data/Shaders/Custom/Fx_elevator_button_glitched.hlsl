#include "Data/Shaders/ShaderStructs.hlsli"
SamplerState defaultSampler : register(s0);

#ifndef MULTIPLE_UV
#error "model missing data"
#endif



SamplerState MeshTextureSampler
{
	Filter = MIN_MAG_MIP_LINEAR;
	AddressU = Wrap;
	AddressV = Wrap;
};

PixelOutput pixelShader(VertexToPixel input)
{
	//Panner for weapon slashes
	PixelOutput returnValue;


	//float redChannelMask = MaterialMap.Sample(defaultSampler, input.myUV).r;
	//float greenChannelMask = MaterialMap.Sample(defaultSampler, input.myUV).g;
	float blueChannelMask = MaterialMap.Sample(defaultSampler, input.myUV).b;

	float pulseValue;

	pulseValue = lerp(15, 0.5, sin(totalTime));
	returnValue.myColor.r = blueChannelMask * 10;
	returnValue.myColor.g = 0;
	returnValue.myColor.b = 0;


	//returnValue.myColor = redChannelMask, redChannelMask, redChannelMask;

	returnValue.myColor.a = blueChannelMask * pulseValue;


	return returnValue;
}