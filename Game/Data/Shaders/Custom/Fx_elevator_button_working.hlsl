#include "Data/Shaders/ShaderStructs.hlsli"
SamplerState defaultSampler : register(s0);

#ifndef MULTIPLE_UV
#error "model missing data"
#endif



SamplerState MeshTextureSampler
{
	Filter = MIN_MAG_MIP_LINEAR;
	AddressU = Wrap;
	AddressV = Wrap;
};

PixelOutput pixelShader(VertexToPixel input)
{
	//Panner for weapon slashes
	PixelOutput returnValue;

	float blueChannelMask = MaterialMap.Sample(defaultSampler, input.myUV).b;

	float pulseValue;

	if (objectLifeTime < 5)
	{
		pulseValue = lerp(15, 1, sin(totalTime));
		returnValue.myColor.r = 0;
		returnValue.myColor.g = blueChannelMask * 10;
		returnValue.myColor.b = 0.1;
	}

	else
	{
		pulseValue = lerp(5, 1, sin(totalTime));
		returnValue.myColor.r = blueChannelMask * 10;
		returnValue.myColor.g = blueChannelMask * 10;
		returnValue.myColor.b = 0.1;
	}

	//returnValue.myColor = redChannelMask, redChannelMask, redChannelMask;

	returnValue.myColor.a = blueChannelMask * pulseValue;


	return returnValue;
}