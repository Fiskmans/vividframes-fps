#include "pch.h"
#include "MainMenuState.h"
#include "GraphicEngine.h"
#include "VideoState.h"
#include "DirectX11Framework.h"
#include "SpriteInstance.h"
#include "video.h"
#include <Xinput.h>
#include "OptionState.h"
#include "LevelSelectState.h"

#include <rapidjson/document.h>
#include <rapidjson/filereadstream.h>


template<typename>
struct array_size;
template<typename T, size_t N>
struct array_size<std::array<T, N> > {
	static size_t const size = N;
};
#define SIZEOFARRAY(arg) array_size<decltype(arg)>::size


MainMenuState::MainMenuState(bool aShouldDeleteOnPop) :
	BaseState(aShouldDeleteOnPop),
	myStateInitData{ nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr }
{
	SetUpdateThroughEnabled(false);
	SetDrawThroughEnabled(false);
	myIntroHasPlayed = false;
	myVideo = nullptr;
	myGameTitleImage = nullptr;
	myMousePointer = nullptr;
	myShouldRemoveVideo = true;
}

MainMenuState::~MainMenuState()
{
	Deactivate();

	delete myMousePointer;
	delete myGameTitleImage;

	WIPE(*this);
}

void MainMenuState::Update(const float aDeltaTime)
{
#if DIRECTTOGAME
	static bool first = true;

	if (first)
	{
		myPlayButton.TriggerOnPressed();
		first = false;
	}
#endif

	if (myVideo != nullptr)
	{
		myVideo->Update(myStateInitData.myFrameWork->GetContext(), aDeltaTime);
	}
}

void MainMenuState::RecieveMessage(const Message& aMessage)
{
	if (aMessage.myMessageType == MessageType::InputMouseMoved)
	{
		myMousePointer->SetPosition(CommonUtilities::Vector2<float>(aMessage.myFloatValue, aMessage.myFloatValue2));
	}
}

bool MainMenuState::Init(InputManager* aInputManager, ModelLoader* aModelLoader, SpriteFactory* aSpritefactory,
	LightLoader* aLightLoader, WindowHandler* aWindowHandler, DirectX11Framework* aFramework, AudioManager* aAudioManager, SpriteRenderer* aSpriteRenderer)
{
	myIsMain = true;

	if (!myMousePointer)
	{
		myMousePointer = aSpritefactory->CreateSprite("Data/UI/mouse.dds");
	}

	myStateInitData.myFrameWork = aFramework;
	myStateInitData.myInputManager = aInputManager;
	myStateInitData.myLightLoader = aLightLoader;
	myStateInitData.myModelLoader = aModelLoader;
	myStateInitData.mySpriteFactory = aSpritefactory;
	myStateInitData.myWindowHandler = aWindowHandler;
	myStateInitData.myAudioManager = aAudioManager;
	myStateInitData.mySpriteRenderer = aSpriteRenderer;

	InitLayout(aSpritefactory);

	return true;
}

void MainMenuState::Render(CGraphicsEngine* aGraphicsEngine)
{
	std::vector<SpriteInstance*> sprites;

	if (myVideo != nullptr)
	{
		sprites.push_back(myVideo->GetSpriteInstance());

	}
	sprites.push_back(myGameTitleImage);

	sprites.push_back(myPlayButton.GetCurrentSprite());
	sprites.push_back(myExitButton.GetCurrentSprite());
	sprites.push_back(myCreditButton.GetCurrentSprite());
	sprites.push_back(myOptionsButton.GetCurrentSprite());
	sprites.push_back(myLevelSelectButton.GetCurrentSprite());
	
	sprites.push_back(myMousePointer);

	aGraphicsEngine->RenderMovie(sprites);
}

void MainMenuState::Activate()
{
	PostMaster::GetInstance()->Subscribe(MessageType::InputMouseMoved, this);
	myExitButton.Subscribe();
	myPlayButton.Subscribe();
	myCreditButton.Subscribe();
	myOptionsButton.Subscribe();
	myLevelSelectButton.Subscribe();

	StartVideo();
	myIsActive = true;
	myShouldRemoveVideo = true;

	Message message;
	message.myMessageType = MessageType::MainMenuStateActivated;
	SendMessages(message);
}

void MainMenuState::Deactivate()
{
	PostMaster::GetInstance()->UnSubscribe(MessageType::InputMouseMoved, this);
	if (myVideo != nullptr && myShouldRemoveVideo)
	{
		myVideo->Stop();
		myVideo->Destroy();
		delete myVideo;
		myVideo = nullptr;
	}
	myExitButton.Unsubscribe();
	myPlayButton.Unsubscribe();
	myCreditButton.Unsubscribe();
	myOptionsButton.Unsubscribe();
	myLevelSelectButton.Unsubscribe();
	myIsActive = false;
}

void MainMenuState::Unload()
{
}

void MainMenuState::InitLayout(SpriteFactory* aSpritefactory)
{
	rapidjson::Document mainMenuDoc;

#pragma warning(suppress : 4996)
	FILE* fp = fopen("Data\\UI\\UIButtons\\MainMenuLayout.json", "rb");
	char readBuffer[4096];
	rapidjson::FileReadStream is(fp, readBuffer, sizeof(readBuffer));
	mainMenuDoc.ParseStream(is);
	fclose(fp);

	std::string imagesPath = mainMenuDoc["ImagesPath"].GetString();

	myGameTitleImage = aSpritefactory->CreateSprite(imagesPath + "\\" + mainMenuDoc["GameTitleImage"]["name"].GetString());
	myGameTitleImage->SetPosition({ mainMenuDoc["GameTitleImage"]["PosX"].GetFloat(),  mainMenuDoc["GameTitleImage"]["PosY"].GetFloat() });

	myPlayButton.Init(imagesPath, mainMenuDoc["StartButton"]["name"].GetString(), { mainMenuDoc["StartButton"]["PosX"].GetFloat(),  mainMenuDoc["StartButton"]["PosY"].GetFloat() }, V2F(0.545f, 1.f), aSpritefactory);
	myExitButton.Init(imagesPath, mainMenuDoc["ExitButton"]["name"].GetString(), { mainMenuDoc["ExitButton"]["PosX"].GetFloat(),  mainMenuDoc["ExitButton"]["PosY"].GetFloat() }, V2F(0.545f, 1.f), aSpritefactory);
	myCreditButton.Init(imagesPath, mainMenuDoc["CreditsButton"]["name"].GetString(), { mainMenuDoc["CreditsButton"]["PosX"].GetFloat(),  mainMenuDoc["CreditsButton"]["PosY"].GetFloat() }, V2F(0.545f, 1.f), aSpritefactory);
	myOptionsButton.Init(imagesPath, mainMenuDoc["OptionsButton"]["name"].GetString(), { mainMenuDoc["OptionsButton"]["PosX"].GetFloat(),  mainMenuDoc["OptionsButton"]["PosY"].GetFloat() }, V2F(0.545f, 1.f), aSpritefactory);
	myLevelSelectButton.Init(imagesPath, mainMenuDoc["LevelSelectButton"]["name"].GetString(), { mainMenuDoc["LevelSelectButton"]["PosX"].GetFloat(),  mainMenuDoc["LevelSelectButton"]["PosY"].GetFloat() }, V2F(0.545f, 1.f), aSpritefactory);

	myPlayButton.SetOnPressedFunction([this]
	{
		Message message;
		message.myMessageType = MessageType::PushState;
		message.myData = CreateGameState(0);

		Publisher::SendMessages(message);
#if PLAYINTRO

		message.myMessageType = MessageType::PushState;
		VideoState* video = new VideoState();
		if (video->Init(myStateInitData.myModelLoader, myStateInitData.mySpriteFactory, "Data\\Cinematics\\Intro.mp4", false, myStateInitData.myFrameWork->GetContext()) == false)
		{
			//TODO: PROPER DELETE OF DATA
			delete video;
			return;
		}
		video->SetMain(true);
		message.myData = video;
		Publisher::SendMessages(message);

		myIntroHasPlayed = true;

#endif

		return;
	});

	myExitButton.SetOnPressedFunction([this]
	{
		Message message;
		message.myMessageType = MessageType::PopState;
		message.myBool = true;
		Publisher::SendMessages(message);
	});

	myCreditButton.SetOnPressedFunction([this]
	{
		Message message;
		message.myMessageType = MessageType::PushState;
		VideoState* video = new VideoState();
		if (video->Init(myStateInitData.myModelLoader, myStateInitData.mySpriteFactory, "Data\\Cinematics\\Credits.mp4", false, myStateInitData.myFrameWork->GetContext()) == false)
		{
			//TODO: PROPER DELETE OF DATA
			delete video;
			return;
		}
		video->SetMain(true);
		message.myData = video;
		Publisher::SendMessages(message);
	}
	);

	myOptionsButton.SetOnPressedFunction([this]
	{
		Message message;
		message.myMessageType = MessageType::PushState;
		OptionState* state = new OptionState();

		if (!state->Init(myStateInitData.mySpriteFactory, myStateInitData.myFrameWork, myStateInitData.mySpriteRenderer, myVideo, myMousePointer))
		{
			delete state;
			return;
		}

		myShouldRemoveVideo = false;

		message.myData = state;
		SendMessages(message);
		
	});

	myLevelSelectButton.SetOnPressedFunction([this]
	{
		Message message;
		message.myMessageType = MessageType::PushState;
		LevelSelectState* state = new LevelSelectState();

		if (!state->Init(myStateInitData.myInputManager, myStateInitData.myModelLoader, myStateInitData.mySpriteFactory, myStateInitData.myLightLoader,
			myStateInitData.myWindowHandler, myStateInitData.myFrameWork, myStateInitData.myAudioManager, myStateInitData.mySpriteRenderer, &myVideo, myMousePointer))
		{
			delete state;
			return;
		}

		myShouldRemoveVideo = false;

		message.myData = state;
		SendMessages(message);
	});

}

GameState* MainMenuState::CreateGameState(const int& aStartLevel)
{
	GameState* state = new GameState();

	if (state->Init(myStateInitData.myWindowHandler, myStateInitData.myInputManager, myStateInitData.myModelLoader,
		myStateInitData.mySpriteFactory, myStateInitData.myLightLoader, myStateInitData.myFrameWork, myStateInitData.myAudioManager, myStateInitData.mySpriteRenderer) == false)
	{
		//TODO: PROPER DELETE OF DATA
		delete state;
		return nullptr;
	}
	state->SetMain(true);
	state->LoadLevel(aStartLevel);

	return state;
}


void MainMenuState::StartVideo()
{
	if (myVideo == nullptr)
	{
		myVideo = new Video();
		myVideo->Init("Data\\Cinematics\\menu_video.mp4", myStateInitData.myModelLoader->myDevice, myStateInitData.mySpriteFactory);
		myVideo->Play(true);
	}
}