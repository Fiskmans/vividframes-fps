#pragma once
#include "AIBaseState.h"
#include "Vector3.hpp"

class AIPollingStation;

class SeekTargetState : public AIBaseState
{
public:
	SeekTargetState(Entity* aEntity, AIPollingStation* aPollingStation);
	~SeekTargetState();

	bool CheckCondition(AIStates& aState) override;

	void Update(float aDeltaTime) override;
	void OnEnter() override;
	void OnExit() override;

private:
	Entity* myEntity;
	AIPollingStation* myPollingStation;

	V3F myTargetPos;

	float myUpdatePathfindingTimer;
};
