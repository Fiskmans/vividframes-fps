#include "pch.h"
#include "SeekTargetState.h"
#include "AIPollingStation.h"
#include "../Entity.h"
#include "../Movement3D.h"
#include "../AIController.h"

#include <iostream>
#include "GBPhysXKinematicComponent.h"


SeekTargetState::SeekTargetState(Entity* aEntity, AIPollingStation* aPollingStation):
	myEntity(aEntity),
	myPollingStation(aPollingStation)
{
	myUpdatePathfindingTimer = 0;
}

SeekTargetState::~SeekTargetState()
{
}

bool SeekTargetState::CheckCondition(AIStates& aState)
{

	V3F enemyDir = myEntity->GetPosition() - myPollingStation->GetPlayer()->GetPosition();
	float distanceSqr = enemyDir.LengthSqr();

	if (distanceSqr < 30000.f)
	{
		aState = AIStates::Attack;
		return true;
	}


	aState = AIStates::None;
	return false;
}

void SeekTargetState::Update(float aDeltaTime)
{
	myUpdatePathfindingTimer -= aDeltaTime;

	Movement3D* move = myEntity->GetComponent<Movement3D>();
	if (move && myUpdatePathfindingTimer <= 0)
	{
		myUpdatePathfindingTimer = 0.5f;

		myTargetPos = myPollingStation->GetPlayer()->GetPosition();

		move->SetTargetPositionOnNavmeshAtPosition(myTargetPos);
		myEntity->GetComponent<GBPhysXKinematicComponent>()->SetTargetDirection((myTargetPos - myEntity->GetPosition()).GetNormalized());
		//Check if path is colliding with other enemies and if so change path
		V3F direction = move->GetDirection();

		float threshold = 20000.f;
		float decayCoefficient = 100000000;
		float maxAcceleration = 200000.f;

		float strength = 0;

		bool couldntMoveForward = false;



		for (int i = 0; i < myPollingStation->GetSeekingEnemies().size(); ++i)
		{
			Entity* enemy = myPollingStation->GetSeekingEnemies()[i];

			if (enemy == myEntity)
			{
				continue;
			}

			V3F enemyDir = myEntity->GetPosition() - enemy->GetPosition();
			float distanceSqr = enemyDir.LengthSqr();

			strength = 0;
			if (distanceSqr < threshold)
			{
				strength = min(decayCoefficient / (distanceSqr * distanceSqr), maxAcceleration);
				couldntMoveForward = true;
			}

			if (enemyDir.Length() > 0)
			{
				enemyDir.Normalize();
			}
			direction += enemyDir * strength;
		}

	
		V3F enemyDir = myEntity->GetPosition() - myTargetPos;
		float distanceSqr = enemyDir.LengthSqr();

		strength = 0;
		if (distanceSqr < threshold)
		{
			strength = min(decayCoefficient / (distanceSqr * distanceSqr), maxAcceleration);
			couldntMoveForward = true;
		}

		if (enemyDir.Length() > 0)
		{
			enemyDir.Normalize();
		}
		direction += enemyDir * strength;

		if (couldntMoveForward)
		{
			myTargetPos = myEntity->GetPosition() + direction.GetNormalized() * 100.f;
			move->SetTargetPositionOnNavmeshAtPosition(myTargetPos);
			myEntity->GetComponent<GBPhysXKinematicComponent>()->SetTargetDirection((myTargetPos - myEntity->GetPosition()).GetNormalized());
		}
	}
}

void SeekTargetState::OnEnter()
{
	myPollingStation->AddSeekingEnemy(myEntity);
}

void SeekTargetState::OnExit()
{
	myEntity->GetComponent<Movement3D>()->ClearMovementTargetPoints();
}