#pragma once
#include "Observer.hpp"
#include "Vector2.hpp"
#include "Publisher.hpp"
#include <vector>
#include "ProgressBar.h"

class SpriteFactory;
class TextFactory;

class UIManager : public Observer, public Publisher
{
public:

	UIManager();
	~UIManager();

	void Init(Entity* aPlayer, Scene& aScene, SpriteFactory& aSpriteFactory, TextFactory& aTextFactory, Camera* aCamera);
	void Update(const float aDeltaTime);
	void AddUIToScene(Scene& aScene);
	void FadeIn();
	void FadeOut();
	void RecieveMessage(const Message& aMessage) override;

	void ReAddMouseToScene();

private:
	SpriteInstance* CreateAndAddSpriteToScene(const std::string& aPath, const V2F& aPosition, const V2F aPivot = V2F(0, 0));
	void RemoveSpriteFromSceneAndDelete(SpriteInstance*& aSprite);

	Camera* myCamera;
	Scene* myScene;
	SpriteFactory* mySpriteFactory;
	TextFactory* myTextFactory;
	Entity* myPlayerPtr;

	SpriteInstance* myFadeSprite;
	SpriteInstance* myVignette;
	SpriteInstance* myCrosshair;

	TextInstance* myAmmoCounter;

	float myUIFadeTime;
	float myUIFadeTimer;
	float myUIFadeDirection;

	int myCurrentLevel;

	bool myIsFadingOut;
	bool myIsFadingIn;
	bool myShouldKillPlayerAfterFadeOut;
	bool myShouldSwapLevelAfterFade;

	bool myFirstFadeInDone;

	void Fade(const float aDeltaTime);

	std::vector<SpriteInstance*> mySprites;

	ProgressBar healthBar;
};

