#pragma once

enum class AudioEvent
{
	None,

	Walking,
	Attacking,

	Count
};