#pragma once
#include "pch.h"
#include "GameState.h"
#include <GraphicEngine.h>
#include <Scene.h>
#include <ModelLoader.h>
#include <SpriteFactory.h>
#include "ForwardRenderer.h"
#include "SpriteRenderer.h"
#include "InputManager.h"
#include "PostMaster.hpp"
#include <assert.h>
#include <fstream>
#include <Skybox.h>
#include <imgui.h>

#include "AudioManager.h"
#include "AudioInstance.h"

#include "DataStructs.h"
#include <StringManipulation.h>
#include <DebugDrawer.h>

//TODO Move to a better place
#include "ModelInstance.h"
#include "Model.h"
#include "CameraFactory.h"
#include "Camera.h"
#include "LightLoader.h"
#include "Environmentlight.h"
#include "PointLight.h"
#include "VideoState.h"
#include <DirectX11Framework.h>
#include <Random.h>
#include "VideoState.h"
#include "NodePollingStation.h"
#include "CNodeInstance.h"
#include "CinematicState.h"
#include <functional>
#include <DirectX11Framework.h>

#include <PathFinder.h>
#include "PathFinderData.h"

#include <GBPhysX.h>

#if USEIMGUI
#include <experimental/filesystem>
#include "ImGuiPackage.h"
#include <WindowControl.h>
#endif // _DEBUG

#ifdef _DEBUG
#include "DebugTools.h"
#endif
#if USEFILEWATHCER
#include <ShaderCompiler.h>
#endif

#if ENABLESCREEPS
#include "ScreepsState.h"
#endif


GameState::GameState(bool aShouldDeleteOnPop) :
	BaseState(aShouldDeleteOnPop),
	myScene(nullptr),
	myModelLoader(nullptr),
	mySpriteFactory(nullptr),
	mySkybox(nullptr),
	myLightLoader(nullptr)
{
	myGameWorld = new GameWorld();
	SetUpdateThroughEnabled(false);
	SetDrawThroughEnabled(false);
	myInputManager = nullptr;
	PostMaster::GetInstance()->Subscribe(MessageType::FadeOutComplete, this);
	PostMaster::GetInstance()->Subscribe(MessageType::FinnishGame, this);
	PostMaster::GetInstance()->Subscribe(MessageType::PlayerDied, this);
	PostMaster::GetInstance()->Subscribe(MessageType::EnableScreeps, this);
	PostMaster::GetInstance()->Subscribe(MessageType::ChangeLevel, this);
	PostMaster::GetInstance()->Subscribe(MessageType::NextLevel, this);
	PostMaster::GetInstance()->Subscribe(MessageType::BulletHit, this);
	PostMaster::GetInstance()->Subscribe(MessageType::LoadLevel, this);
	myCurrentLevel = "";
	myIsMain = true;
	myCurrentLevelindex = 0;
	myGraphManager.Load();
	myNodePollingStation = nullptr;
	myFinnishGameAfterFadeOut = false;
	myQueuedPartialLevel.myIsComplete = false;
}

GameState::~GameState()
{
	PostMaster::GetInstance()->UnSubscribe(MessageType::FadeOutComplete, this);
	PostMaster::GetInstance()->UnSubscribe(MessageType::FinnishGame, this);
	PostMaster::GetInstance()->UnSubscribe(MessageType::PlayerDied, this);
	PostMaster::GetInstance()->UnSubscribe(MessageType::EnableScreeps, this);
	PostMaster::GetInstance()->UnSubscribe(MessageType::ChangeLevel, this);
	PostMaster::GetInstance()->UnSubscribe(MessageType::NextLevel, this);
	PostMaster::GetInstance()->UnSubscribe(MessageType::BulletHit, this);
	PostMaster::GetInstance()->UnSubscribe(MessageType::LoadLevel, this);
	
#if USEFILEWATHCER
	myWatcher.UnRegister(myMetricHandle);
#endif // !_RETAIL
#ifdef _DEBUG
	DebugTools::FileList = nullptr;
#endif
	SAFE_DELETE(myGameWorld);
	SAFE_DELETE(myScene);
	if (myGBPhysX)
	{
		myGBPhysX->GBCleanUpPhysics();
	}
	SAFE_DELETE(myGBPhysX);
}

bool GameState::Init(WindowHandler* aWindowHandler, InputManager* aInputManager, ModelLoader* aModelLoader, SpriteFactory* aSpritefactory, LightLoader* aLightLoader, DirectX11Framework* aFramework, AudioManager* aAudioManager, SpriteRenderer* aSpriteRenderer)
{
	myIsGameWon = false;
	myIsMain = true;

	myContext = aFramework->GetContext();
	myModelLoader = aModelLoader;
	mySpriteFactory = aSpritefactory;
	myLightLoader = aLightLoader;
	myInputManager = aInputManager;
	myAudioManager = aAudioManager;
	myWindowHandler = aWindowHandler;
	myGraphManager.LoadTreeFromFile();
	myGBPhysX = new GBPhysX();

	if (!myGBPhysX->GetGBPhysXActive())
	{
		myGBPhysX->GBInitPhysics(false);
	}
	else
	{
		SYSERROR("Tried to Init GBPhysX when it was already running", "");
		throw std::exception("Tried to Init GBPhysX when it was already running");
	}

	CreateWorld(aWindowHandler, aFramework, aAudioManager, aSpriteRenderer);

	mySpotlightFactory.Init(aFramework->GetDevice());
	myDecalFactory.Init(aFramework->GetDevice());
	myTestSpotlight = mySpotlightFactory.LoadSpotlight("Data/Textures/Spotlights/PlayerSpotlight.dds", 30, 500, 10000, V3F(), V3F(-2528, 200, -3330));
	myScene->AddToScene(myTestSpotlight);

#ifdef _DEBUG
	DebugTools::myLightLoader = myLightLoader;

#endif // _DEBUG


#if !BAKEVALUES
	MetricHandler::Load(METRICFILE);
#if USEFILEWATHCER
	myMetricHandle = myWatcher.RegisterCallback(METRICFILE, MetricHandler::Load);
#endif // !_RETAIL
#endif
	return !!aModelLoader;
}

void GameState::Render(CGraphicsEngine* aGraphicsEngine)
{
	if (!myIsGameWon)
	{
		PathFinder::GetInstance().DrawDebug();

		aGraphicsEngine->RenderFrame(myScene);
	}
}

void GameState::LoadLevel(const int& aLevel)
{
	myCurrentLevelindex = aLevel;

#if DEMOSCENE
	LoadLevel("");
	return;
#endif
	//TODO: --AIUPPGIFT--
	if (myLevelSequence.empty())
	{
		EnvironmentLight* light = myLightLoader->LoadLight("Data/Textures/Skyboxes/skyBox_Level2_light.dds");
		if (light)
		{
			light->myDirection = V3F(1, 1, 1);
			light->myIntensity = 1;
			light->myColor = V3F(1, 1, 1);

			EnvironmentLight* oldLight = myScene->GetEnvironmentLight();
			myScene->SetEnvironmentLight(light);
			SAFE_DELETE(oldLight);
		}

		return;
	}

	if (myCurrentLevelindex >= myLevelSequence.size() || myCurrentLevelindex < 0)
	{
		SYSERROR("Level doesnt exist", std::to_string(myCurrentLevelindex));
		return;
	}

	std::string levelToLoad = myLevelSequence[myCurrentLevelindex];
	//myLevelSequence.pop();
	if (!LoadLevel(levelToLoad))
	{
		SYSERROR("Failed to Load level: ", levelToLoad);
	}

	Message levelNumberMessage;
	levelNumberMessage.myMessageType = MessageType::CurrentLevel;
	levelNumberMessage.myIntValue = myCurrentLevelindex;
	SendMessages(levelNumberMessage);

}


void GameState::RunImGui(float aDeltatime)
{
#if  USEIMGUI
	PathFinder::GetInstance().Imgui();
	// ImGui
	ModelInstance* toRemove = nullptr;


	static bool ShowGizmo = false;
	static bool editGraphs = false;
	static bool drawFrustums = false;
	if (drawFrustums)
	{
		for (auto i : myScene->GetDecals())
		{
			DebugDrawer::GetInstance().DrawFrustum(i->myCamera->GenerateFrustum());
		}
	}
	if (GetAsyncKeyState('O'))
	{
		myScene->GetDecals().clear();
	}

	static V2F GizmoPosition = { 0.f,16.f };
#ifdef _DEBUG
	DebugTools::FileList = &myFoundFiles;
#endif // _DEBUG
	{
		PERFORMANCETAG("Gamestate imgui");
		WindowControl::Window("Game", [&]()
			{

				ImGui::Checkbox("Visualize decals", &drawFrustums);
				ImGui::Checkbox("Edit graphs", &editGraphs);
				if (editGraphs)
				{

					myGraphManager.PreFrame(aDeltatime);
					myGraphManager.ConstructEditorTreeAndConnectLinks();
					myGraphManager.PostFrame();
				}
				ImGui::Checkbox("Show Gizmo", &ShowGizmo);
				if (ShowGizmo)
				{
					ImGui::SameLine();
					static bool moveGizmo = false;
					ImGui::Checkbox("Move Gizmo", &moveGizmo);
					if (moveGizmo)
					{
						if (ImGui::Begin("Gizmo", &moveGizmo, ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_NoCollapse))
						{
							ImGui::DragFloat2("Position", &GizmoPosition.x, 0.1f, -70.f, 70.f);
						}
						ImGui::End();
					}
				}



				ImGui::Separator();

				EnvironmentLight* light = myScene->GetEnvironmentLight();
				if (light)
				{
					ImGui::ColorEdit3("Light Color", &light->myColor.x);
					ImGui::DragFloat3("Light Direction", &light->myDirection.x, 0.01f);
					ImGui::DragFloat("Light Intensity", &light->myIntensity, 0.01f);
				}
				static float fov = 81;
				static float fov2 = 65;
				static float lastFov;
				static float lastFov2;
				ImGui::DragFloat("Camera FOV", &fov, 0.1f, 0.f, 170.f);
				ImGui::DragFloat("Camera Weapon FOV", &fov2, 0.1f, 0.f, 190.f);
				if (fov != lastFov)
				{
					myScene->GetMainCamera()->SetFov(fov);
					lastFov = fov;
				}
				if (fov2 != lastFov2)
				{
					myScene->GetMainCamera()->SetFov(fov2, true);
					lastFov2 = fov2;
				}

				if (ImGui::CollapsingHeader("Scene"))
				{
					if (ImGui::Button("Remove all"))
					{
						myScene->RemoveAll();
					}
					if (ImGui::TreeNode("Meshes"))
					{
						for (ModelInstance* i : *myScene)
						{
							if (ImGui::TreeNode(i->GetFriendlyName().c_str()))
							{
								if (i->ImGuiNode(myFoundFiles, myScene->GetMainCamera()))
								{
									toRemove = i;
								}
								ImGui::TreePop();
							}
						}
						ImGui::TreePop();
					}
					if (ImGui::TreeNode("Lights"))
					{
						int counter = 0;
						for (PointLight* i : myScene->GetPointLights())
						{
							ImGui::PushID(counter);
							ImGui::ColorEdit3("Color", reinterpret_cast<float*>(&i->color));
							ImGui::DragFloat3("Position", reinterpret_cast<float*>(&i->position));
							ImGui::DragFloat("Intensity", &i->intensity);
							ImGui::DragFloat("Range", &i->range);
							ImGui::Separator();
							ImGui::PopID();
							counter++;
						}
						ImGui::TreePop();
					}
				}

				ImGuiNodePackage pack;
				pack.myFileList = &myFoundFiles;
				pack.myMainCamera = myScene->GetMainCamera();
				myGameWorld->ImGuiNode(pack);
				ImGui::PushID("header");
				if (ImGui::CollapsingHeader("Models"))
				{
					for (auto& i : *myModelLoader)
					{

						ImGui::PushID(i.second->GetFriendlyName().c_str());
						if (ImGui::TreeNode(i.second->GetFriendlyName().c_str()))
						{
							i.second->ImGuiNode(myFoundFiles);

							ImGui::Separator();
							ImGui::TreePop();
						}
						ImGui::PopID();
					}
				}
				ImGui::PopID();
				if (ImGui::CollapsingHeader("Tools"))
				{
					if (ImGui::Button("Clear log"))
					{
						system("cls");
					}

					static bool randomizeRotation;
					if (ImGui::Button("Create Cube"))
					{
						CommonUtilities::Vector4<float> pos = { 0,0,3,1 };
						pos = pos * myScene->GetMainCamera()->GetTransform();
						myScene->AddToScene(myModelLoader->InstanciateCube(pos, randomizeRotation));
					}
					ImGui::SameLine();
					ImGui::Checkbox("Randomize rotation", &randomizeRotation);
					if (ImGui::Button("Reset Camera"))
					{
						myScene->GetMainCamera()->SetPosition({ 0,0,0 });
						myScene->GetMainCamera()->SetRotation(CommonUtilities::Matrix3x3<float>());
					}

					if (ImGui::Button("Reload lists"))
					{
						SearchForFiles();
					}
					if (ImGui::TreeNode("Models"))
					{
						if (ImGui::Button("Blast it, give me one of each"))
						{
							for (auto& i : myFoundFiles[".fbx"])
							{
								ModelInstance* model = myModelLoader->InstantiateModel(i);
								if (model)
								{
									myScene->AddToScene(model);
								}
							}
						}
						ImGui::Separator();
						for (auto& i : myFoundFiles[".fbx"])
						{
							if (ImGui::Button(("Create " + i).c_str()))
							{
								ModelInstance* model = myModelLoader->InstantiateModel(i);
								if (model)
								{
									myScene->AddToScene(model);
								}
							}
						}
						ImGui::TreePop();
					}
					if (ImGui::TreeNode("Levels"))
					{
						static bool removeOnReload = true;
						static bool resetOnReload = true;
						ImGui::Checkbox("Remove all on load", &removeOnReload);
						ImGui::Checkbox("Reset camera on load", &resetOnReload);

						for (auto& i : myFoundFiles[".lvl"])
						{

							ImGui::PushID(i.c_str());
							if (ImGui::Button("Set as default"))
							{
								SetDefaultLevel(i);
							}
							ImGui::SameLine();
							if (ImGui::Button("Load"))
							{
								if (removeOnReload)
								{
									myGameWorld->ClearWorld();
									myScene->RemoveAll();
								}
								if (resetOnReload)
								{
									myScene->GetMainCamera()->SetPosition({ 0,0,0 });
									myScene->GetMainCamera()->SetRotation(CommonUtilities::Matrix3x3<float>());
								}
								LoadLevel(i);
							}
							ImGui::SameLine();
							ImGui::Text(i.c_str());
							ImGui::PopID();

						}
						ImGui::TreePop();
					}
				}
				if (ImGui::CollapsingHeader("Info"))
				{
					CommonUtilities::Vector3<float> pos = myScene->GetMainCamera()->GetPosition();
					ImGui::BulletText("Camera position X:%.2f Y:%.2f Z:%.2f", pos.x, pos.y, pos.z);
					ImGui::BulletText("Mouse screen position X:%.2f Y:%.2f", myInputManager->GetMouseNomalizedX(), myInputManager->GetMouseNomalizedY());
#if _DEBUG
					V3F mpWorld = PathFinder::GetInstance().FindPoint(*DebugTools::LastKnownMouseRay);
					if (mpWorld == V3F(0, 0, 0))
					{
						ImGui::BulletText("Mouse world position Outside of navmesh");
					}
					else
					{
						ImGui::BulletText("Mouse world position X:%.2f Y:%.2f Z:%.2f", mpWorld.x, mpWorld.y, mpWorld.z);
					}
#endif // _DEBUG
				}
			});
		Logger::RapportWindow();
	}

	//GAMEWORLD UPDATE --------------------------------------

	{
		PERFORMANCETAG("Gameworld update")
			myGameWorld->Update(*myInputManager->GetInputHandler(), aDeltatime);
	}

	WindowControl::Window("Components", []()
		{
			for (auto& i : GetComponentsInUse())
			{
				ImGui::Text(i.first + 6); // skip 'class ' part of c string
				ImGui::SameLine();
				ImGui::Förloppsindikator(float(i.second.first) / i.second.second, ImVec2(-1, 0), std::string(std::to_string(i.second.first) + "/" + std::to_string(i.second.second)).c_str());
			}
		});


	// Gizmo ------------------------------------------------

	if (ShowGizmo)
	{
		Camera* camera = myScene->GetMainCamera();
		V3F pos = camera->GetPosition() + camera->GetForward() * 40.f + camera->GetUp() * GizmoPosition.y + camera->GetRight() * GizmoPosition.x;
		DebugDrawer::GetInstance().DrawGizmo(pos, 2);
	}

	// Model Remove -----------------------------------------

	if (toRemove)
	{
		myScene->RemoveModel(toRemove);
	}

	// Flush Model Changes ----------------------------------

	Model::FlushChanges();


#endif // !_RETAIL
}

void GameState::SetDefaultLevel(const std::string& aLevel)
{
	std::ofstream outFile;
	outFile.open("defaultLevel.unversioned");
	outFile << aLevel;
}

void GameState::LoadDefaultLevel()
{
#ifdef _DEBUG
	std::ifstream inFile;
	inFile.open("defaultLevel.unversioned");
	if (inFile)
	{
		std::string levelFile;
		std::getline(inFile, levelFile);
		LoadLevel(levelFile);
		return;
	}
#endif
	LoadLevel("Data/Levels/LVL1_A_Harbour.lvl");
}

bool GameState::LoadLevel(const std::string& aFilePath)
{
	if (myCurrentLevel != "")
	{
		UnloadCurrentLevel();
	}

	return MergeLevel(aFilePath);
}

void GameState::MergeNextLevel()
{
	++myCurrentLevelindex;
	if (myCurrentLevelindex >= myLevelSequence.size())
	{
		return;
	}

	std::string nextLevel = myLevelSequence[myCurrentLevelindex];
	MergeLevel(nextLevel);
}

bool GameState::MergeLevel(const std::string& aFilePath)
{
	if (!myAsyncPartialLevel.valid())
	{
		myAsyncPartialLevel = std::async(&GameState::ParseLevelFile,this,aFilePath);
		Message startLoading;
		startLoading.myText = aFilePath;
		startLoading.myMessageType = MessageType::StartLoading;
		SendMessages(startLoading);
		return true;
	}
	return false;

	LevelParseResult result = ParseLevelFile(aFilePath);

	// Skybox  -----------------------------------------------------

	Skybox* skybox = myModelLoader->InstanciateSkybox(result.mySkyboxPath);
	if (skybox)
	{
		delete mySkybox;
		myScene->SetSkybox(skybox);
		mySkybox = skybox;
	}

	for (auto& objBuffer : result.myStaticObjects)
	{
		Message spawnStaticMessage;
		spawnStaticMessage.myMessageType = MessageType::SpawnStaticObject;
		spawnStaticMessage.myData = &objBuffer;
		Publisher::SendMessages(spawnStaticMessage);
	}

	for (auto& objBuffer : result.myEnemyObjects)
	{
		Message enemySpawnMessage;
		enemySpawnMessage.myMessageType = MessageType::SpawnEnemy;
		enemySpawnMessage.myData = &objBuffer;
		Publisher::SendMessages(enemySpawnMessage);
	}

	for (auto& objBuffer : result.myTriggerBuffer)
	{
		Message triggerSpawnMessage;
		triggerSpawnMessage.myMessageType = MessageType::SpawnTriggerBox;
		triggerSpawnMessage.myData = &objBuffer;
		Publisher::SendMessages(triggerSpawnMessage);
	}

	for (auto& objBuffer : result.myMayaPositions)
	{
		myNodePollingStation->AddMayaPos(objBuffer.first, objBuffer.second);
	}
	
	EnvironmentLight* envoLight = myLightLoader->LoadLight(result.mySkyboxPath.substr(0, result.mySkyboxPath.length() - 4) + "_light.dds");

	if (envoLight)
	{
		envoLight->myDirection = V3F(-result.myEnvironmentlight.direction[0], -result.myEnvironmentlight.direction[1], -result.myEnvironmentlight.direction[2]);
		envoLight->myColor = V3F(result.myEnvironmentlight.color[0], result.myEnvironmentlight.color[1], result.myEnvironmentlight.color[2]);
		envoLight->myIntensity = result.myEnvironmentlight.intensity;
		myScene->SetEnvironmentLight(envoLight);
	}


	for (auto& objBuffer : result.myPointLights)
	{
		Message triggerLightSpawnMessage;
		triggerLightSpawnMessage.myMessageType = MessageType::SpawnPointLight;
		triggerLightSpawnMessage.myData = &objBuffer;
		Publisher::SendMessages(triggerLightSpawnMessage);
	}

	PathFinder::GetInstance().GenerateFromMesh(result.myNavMeshPath);

	int nrOfTriangles = CAST(int, PathFinder::GetInstance().GetMyPathFinderData()->myNodes.size());
	std::vector<V3F> verts = PathFinder::GetInstance().GetMyPathFinderData()->myVertexCollection;
	std::vector<int> indices;
	for (auto& it : PathFinder::GetInstance().GetMyPathFinderData()->myNodes)
	{
		indices.push_back(CAST(int, it.myCorners[0]));
		indices.push_back(CAST(int, it.myCorners[1]));
		indices.push_back(CAST(int, it.myCorners[2]));
	}
	myGBPhysX->GBCreateNavMesh(CAST(int, verts.size()), verts, nrOfTriangles, indices);

	for (auto& objBuffer : result.myCollsionBoxes)
	{
		Message spawnGBPhysXStaticMessage;
		spawnGBPhysXStaticMessage.myMessageType = MessageType::SpawnGBPhysXBox;
		spawnGBPhysXStaticMessage.myData = &objBuffer;
		Publisher::SendMessages(spawnGBPhysXStaticMessage);
	}

	for (auto& objBuffer : result.myDestrucables)
	{
		Message destructibleSpawnMessage;
		destructibleSpawnMessage.myMessageType = MessageType::SpawnDestructibleObject;
		destructibleSpawnMessage.myData = &objBuffer;
		destructibleSpawnMessage.myBool = false;
		Publisher::SendMessages(destructibleSpawnMessage);
	}

	for (auto& objBuffer : result.mySpotlights)
	{
		SpotLight* l = mySpotlightFactory.LoadSpotlight(objBuffer.aFilePath, objBuffer.aFov, objBuffer.aRange, objBuffer.aIntensity, objBuffer.aRotation, objBuffer.aTranslation);
		myScene->AddToScene(l);
	}

	myCurrentLevel = aFilePath;

	Message newLevelLoadedMessage;
	newLevelLoadedMessage.myMessageType = MessageType::NewLevelLoaded;
	newLevelLoadedMessage.myIntValue = result.myWorldAxisSize;
	newLevelLoadedMessage.myIntValue2 = myCurrentLevelindex;
	newLevelLoadedMessage.myData = &result.myPlayerStart;
	Publisher::SendMessages(newLevelLoadedMessage);
	myGraphManager.ReTriggerTree();

	myLatestCheckpointPos = result.myPlayerStart;
	return true;
}

bool GameState::MergeQueuedLevelPartially(float aTimeBudget)
{
	PERFORMANCETAG("Level streaming");
	float start = Tools::GetTotalTime();
	while (myQueuedPartialLevel.myIsComplete)
	{
		float now = Tools::GetTotalTime();
		float used = now - start;
		if (used > aTimeBudget)
		{
			return false;
		}
		switch (myQueuedPartialLevel.myStep)
		{
		case 0:
		{
			PERFORMANCETAG("Skybox");
			Skybox * skybox = myModelLoader->InstanciateSkybox(myQueuedPartialLevel.mySkyboxPath);
			if (skybox)
			{
				delete mySkybox;
				myScene->SetSkybox(skybox);
				mySkybox = skybox;
			}
			++myQueuedPartialLevel.myStep;
		}
			break;
		case 1:
		{
			{
				PERFORMANCETAG("Static objects");
				if (myQueuedPartialLevel.myCounter >= myQueuedPartialLevel.myStaticObjects.size())
				{
					myQueuedPartialLevel.myCounter = 0;
					++myQueuedPartialLevel.myStep;
				}
				else
				{
					Message spawnStaticMessage;
					spawnStaticMessage.myMessageType = MessageType::SpawnStaticObject;
					spawnStaticMessage.myData = &myQueuedPartialLevel.myStaticObjects[myQueuedPartialLevel.myCounter];
					Publisher::SendMessages(spawnStaticMessage);
					++myQueuedPartialLevel.myCounter;
				}
			}
		}
			break;
		case 2:
		{
			{
				PERFORMANCETAG("Enemies");
				if (myQueuedPartialLevel.myCounter >= myQueuedPartialLevel.myEnemyObjects.size())
				{
					myQueuedPartialLevel.myCounter = 0;
					++myQueuedPartialLevel.myStep;
				}
				else
				{
					Message enemySpawnMessage;
					enemySpawnMessage.myMessageType = MessageType::SpawnEnemy;
					enemySpawnMessage.myData = &myQueuedPartialLevel.myEnemyObjects[myQueuedPartialLevel.myCounter];
					Publisher::SendMessages(enemySpawnMessage);
					++myQueuedPartialLevel.myCounter;
				}
			}
		}
			break;
		case 3:
		{
			PERFORMANCETAG("Triggers");
			if (myQueuedPartialLevel.myCounter >= myQueuedPartialLevel.myTriggerBuffer.size())
			{
				myQueuedPartialLevel.myCounter = 0;
				++myQueuedPartialLevel.myStep;
			}
			else
			{
				Message triggerSpawnMessage;
				triggerSpawnMessage.myMessageType = MessageType::SpawnTriggerBox;
				triggerSpawnMessage.myData = &myQueuedPartialLevel.myTriggerBuffer[myQueuedPartialLevel.myCounter];
				Publisher::SendMessages(triggerSpawnMessage);
				++myQueuedPartialLevel.myCounter;
			}
		}
			break;
		case 4:
		{
			PERFORMANCETAG("Waypoints");
			if (myQueuedPartialLevel.myCounter >= myQueuedPartialLevel.myMayaPositions.size())
			{
				myQueuedPartialLevel.myCounter = 0;
				++myQueuedPartialLevel.myStep;
			}
			else
			{
				myNodePollingStation->AddMayaPos(myQueuedPartialLevel.myMayaPositions[myQueuedPartialLevel.myCounter].first, myQueuedPartialLevel.myMayaPositions[myQueuedPartialLevel.myCounter].second);
				++myQueuedPartialLevel.myCounter;
			}
		}
			break;
		case 5:
		{
			PERFORMANCETAG("Environmentlight");
			EnvironmentLight * envoLight = myLightLoader->LoadLight(myQueuedPartialLevel.mySkyboxPath.substr(0, myQueuedPartialLevel.mySkyboxPath.length() - 4) + "_light.dds");

			if (envoLight)
			{
				envoLight->myDirection = V3F(-myQueuedPartialLevel.myEnvironmentlight.direction[0], -myQueuedPartialLevel.myEnvironmentlight.direction[1], -myQueuedPartialLevel.myEnvironmentlight.direction[2]);
				envoLight->myColor = V3F(myQueuedPartialLevel.myEnvironmentlight.color[0], myQueuedPartialLevel.myEnvironmentlight.color[1], myQueuedPartialLevel.myEnvironmentlight.color[2]);
				envoLight->myIntensity = myQueuedPartialLevel.myEnvironmentlight.intensity;
				myScene->SetEnvironmentLight(envoLight);
			}
			++myQueuedPartialLevel.myStep;
		}
			break;
		case 6:
		{
			PERFORMANCETAG("Pointlights");
			if (myQueuedPartialLevel.myCounter >= myQueuedPartialLevel.myPointLights.size())
			{
				myQueuedPartialLevel.myCounter = 0;
				++myQueuedPartialLevel.myStep;
			}
			else
			{
				Message triggerLightSpawnMessage;
				triggerLightSpawnMessage.myMessageType = MessageType::SpawnPointLight;
				triggerLightSpawnMessage.myData = &myQueuedPartialLevel.myPointLights[myQueuedPartialLevel.myCounter];
				Publisher::SendMessages(triggerLightSpawnMessage);
				++myQueuedPartialLevel.myCounter;
			}
		}
			break;
		case 7:
		{
			PERFORMANCETAG("NavMesh");
			PathFinder::GetInstance().GenerateFromMesh(myQueuedPartialLevel.myNavMeshPath);

			int nrOfTriangles = CAST(int, PathFinder::GetInstance().GetMyPathFinderData()->myNodes.size());
			std::vector<V3F> verts = PathFinder::GetInstance().GetMyPathFinderData()->myVertexCollection;
			std::vector<int> indices;
			for (auto& it : PathFinder::GetInstance().GetMyPathFinderData()->myNodes)
			{
				indices.push_back(CAST(int, it.myCorners[0]));
				indices.push_back(CAST(int, it.myCorners[1]));
				indices.push_back(CAST(int, it.myCorners[2]));
			}
			myGBPhysX->GBCreateNavMesh(CAST(int, verts.size()), verts, nrOfTriangles, indices);
			++myQueuedPartialLevel.myStep;
		}
			break;
		case 8:
		{
			PERFORMANCETAG("CollisionBoxes");
			if (myQueuedPartialLevel.myCounter >= myQueuedPartialLevel.myCollsionBoxes.size())
			{
				myQueuedPartialLevel.myCounter = 0;
				++myQueuedPartialLevel.myStep;
			}
			else
			{
				Message spawnGBPhysXStaticMessage;
				spawnGBPhysXStaticMessage.myMessageType = MessageType::SpawnGBPhysXBox;
				spawnGBPhysXStaticMessage.myData = &myQueuedPartialLevel.myCollsionBoxes[myQueuedPartialLevel.myCounter];
				Publisher::SendMessages(spawnGBPhysXStaticMessage);
				++myQueuedPartialLevel.myCounter;
			}
		}
			break;
		case 9:
		{
			PERFORMANCETAG("Destructables");
			if (myQueuedPartialLevel.myCounter >= myQueuedPartialLevel.myDestrucables.size())
			{
				myQueuedPartialLevel.myCounter = 0;
				++myQueuedPartialLevel.myStep;
			}
			else
			{
				Message destructibleSpawnMessage;
				destructibleSpawnMessage.myMessageType = MessageType::SpawnDestructibleObject;
				destructibleSpawnMessage.myData = &myQueuedPartialLevel.myDestrucables[myQueuedPartialLevel.myCounter];
				destructibleSpawnMessage.myBool = false;
				Publisher::SendMessages(destructibleSpawnMessage);
				++myQueuedPartialLevel.myCounter;
			}
		}
			break;
		case 10:
		{
			PERFORMANCETAG("Spotlights");
			if (myQueuedPartialLevel.myCounter >= myQueuedPartialLevel.mySpotlights.size())
			{
				myQueuedPartialLevel.myCounter = 0;
				++myQueuedPartialLevel.myStep;
			}
			else
			{
				auto& objBuffer = myQueuedPartialLevel.mySpotlights[myQueuedPartialLevel.myCounter];
				SpotLight* l = mySpotlightFactory.LoadSpotlight(objBuffer.aFilePath, objBuffer.aFov, objBuffer.aRange, objBuffer.aIntensity, objBuffer.aRotation, objBuffer.aTranslation);
				myScene->AddToScene(l);
				++myQueuedPartialLevel.myCounter;
			}
		}
			break;
		default:
		{
			PERFORMANCETAG("Finishing up");
			myCurrentLevel = myQueuedPartialLevel.myLevelFile;

			Message newLevelLoadedMessage;
			newLevelLoadedMessage.myMessageType = MessageType::NewLevelLoaded;
			newLevelLoadedMessage.myIntValue = myQueuedPartialLevel.myWorldAxisSize;
			newLevelLoadedMessage.myIntValue2 = myCurrentLevelindex;
			newLevelLoadedMessage.myData = &myQueuedPartialLevel.myPlayerStart;
			Publisher::SendMessages(newLevelLoadedMessage);
			myGraphManager.ReTriggerTree();

			myLatestCheckpointPos = myQueuedPartialLevel.myPlayerStart;

			myQueuedPartialLevel = LevelParseResult();
			myQueuedPartialLevel.myIsComplete = false;
		}
			break;
		}
	}

	return true;
}


bool ParseSkybox(std::istream& aStream, LevelParseResult& aResult)
{
	std::getline(aStream, aResult.mySkyboxPath);
	if (!aStream)
	{
		SYSERROR("Loading level failed at skybox setup", aResult.myLevelFile);
		return false;
	}
	return true;
}

bool ParseWorldAxis(std::istream& aStream, LevelParseResult& aResult)
{
	aStream >> aResult.myWorldAxisSize;
	if (!aStream)
	{
		SYSERROR("Loading level failed at world axis", aResult.myLevelFile);
		return false;
	}
	return true;
}

bool ParseStaticObjects(std::istream& aStream, LevelParseResult& aResult)
{

	int modelCount = 0;
	aStream >> modelCount;

	StaticObjectInstanceBuffer buffer;
	for (size_t i = 0; i < modelCount; i++)
	{
		if (aStream >> buffer.aFilePath
			>> buffer.position[0] >> buffer.position[1] >> buffer.position[2]
			>> buffer.rotation[0] >> buffer.rotation[1] >> buffer.rotation[2]
			>> buffer.scale[0] >> buffer.scale[1] >> buffer.scale[2])
		{
			aResult.myStaticObjects.push_back(buffer);
		}
	}

	if (!aStream)
	{
		SYSERROR("Static objects broke level loader", aResult.myLevelFile);
		return false;
	}
	return true;
}

bool ParseEnemies(std::istream& aStream, LevelParseResult& aResult)
{

	int enemyCount = 0;
	aStream >> enemyCount;
#define NEW false

	EnemyInstanceBuffer enemyBuffer;
	int spawnPosID; //Eventho we fucking get the position anyway
	int triggervolumeID; //I don't fucking know why tobbe added this
	int targetPosID;

	for (size_t i = 0; i < enemyCount; i++)
	{
		if (aStream >> enemyBuffer.aFilePath
			>> enemyBuffer.enemyID
#if NEW
			>> spawnPosID >> triggervolumeID >> targetPosID
#endif
			>> enemyBuffer.position[0] >> enemyBuffer.position[1] >> enemyBuffer.position[2]
			>> enemyBuffer.rotation[0] >> enemyBuffer.rotation[1] >> enemyBuffer.rotation[2]
			>> enemyBuffer.scale[0] >> enemyBuffer.scale[1] >> enemyBuffer.scale[2])
		{
			aResult.myEnemyObjects.push_back(enemyBuffer);
		}
	}

	if (!aStream)
	{
		SYSERROR("Enemies broke level loader", aResult.myLevelFile);
		return false;
	}
	return true;
}

bool ParseTriggers(std::istream& aStream, LevelParseResult& aResult)
{
	int triggerCount = 0;
	aStream >> triggerCount;

	TriggerBoxInstanceBuffer triggerBuffer;

	for (size_t i = 0; i < triggerCount; i++)
	{
		if (aStream >> triggerBuffer.aFilePath
			>> triggerBuffer.ID
			>> triggerBuffer.targetID
			>> triggerBuffer.position[0] >> triggerBuffer.position[1] >> triggerBuffer.position[2]
			>> triggerBuffer.rotation[0] >> triggerBuffer.rotation[1] >> triggerBuffer.rotation[2]
			>> triggerBuffer.scale[0] >> triggerBuffer.scale[1] >> triggerBuffer.scale[2] >> triggerBuffer.isAbilityTriggered)
		{
			aResult.myTriggerBuffer.push_back(triggerBuffer);
		}
	}

	if (!aStream)
	{
		SYSERROR("Triggers broke level loader", aResult.myLevelFile);
		return false;
	}
	return true;
}

bool ParsePoints(std::istream& aStream, LevelParseResult& aResult)
{
#if NEW
	//Positions
	int posCount = 0;
	aStream >> posCount;
	V3F pos;
	int ID;

	for (size_t i = 0; i < posCount; i++)
	{
		if (aStream >> ID >> pos.x >> pos.y >> pos.z)
		{
			aResult.myMayaPositions.emplace_back(ID, pos);
		}
	}

	if (!aStream)
	{
		SYSERROR("Maya positions broke level loader", aResult.myLevelFile);
		return false;
	}
#endif
	return true;
}

bool ParseEnvironmentLight(std::istream& aStream, LevelParseResult& aResult)
{
	DirLightInstanceBuffer dirLightBuffer;
	dirLightBuffer.color[0] = 1.f;
	dirLightBuffer.color[1] = 1.f;
	dirLightBuffer.color[2] = 1.f;

	dirLightBuffer.direction[0] = 0.3f;
	dirLightBuffer.direction[1] = 0.5f;
	dirLightBuffer.direction[2] = 0.1f;
	dirLightBuffer.intensity = 1.f;

	std::string dirLight = "";
	int bob;
	aStream >> bob;
	aStream >> dirLight;

	if (dirLight == "DirectionalLight" && aStream
		>> dirLightBuffer.direction[0] >> dirLightBuffer.direction[1] >> dirLightBuffer.direction[2]
		>> dirLightBuffer.color[0] >> dirLightBuffer.color[1] >> dirLightBuffer.color[2] >> dirLightBuffer.intensity)
	{
		aResult.myEnvironmentlight = dirLightBuffer;
	}
	if (!aStream)
	{
		SYSERROR("Environmentlight  broke level loader", aResult.myLevelFile);
		return false;
	}
	return true;
}

bool ParsePointLights(std::istream& aStream, LevelParseResult& aResult)
{
	int lightCount = 0;
	aStream >> lightCount;
	std::string bob;

	PointLightInstanceBuffer lightBuffer;

	for (size_t i = 0; i < lightCount; i++)
	{
		if (aStream >> bob
			>> lightBuffer.position[0] >> lightBuffer.position[1] >> lightBuffer.position[2]
			>> lightBuffer.intensity >> lightBuffer.period >> lightBuffer.minVal >> lightBuffer.color[0]
			>> lightBuffer.color[1] >> lightBuffer.color[2] >> lightBuffer.range)
		{
			aResult.myPointLights.push_back(lightBuffer);
		}
	}

	if (!aStream)
	{
		SYSERROR("point lights broke level loader", aResult.myLevelFile);
		return false;
	}
	return true;
}

bool ParsePlayerPos(std::istream& aStream, LevelParseResult& aResult)
{
	int dummyInt;
	int dummyInt2;
	if (!(aStream >> dummyInt >> dummyInt2 >> aResult.myPlayerStart.x >> aResult.myPlayerStart.y >> aResult.myPlayerStart.z))
	{
		SYSERROR("Player spawn position broke level loading", aResult.myLevelFile);
		return false;
	}
	return true;
}

bool ParseNavMesh(std::istream& aStream, LevelParseResult& aResult)
{
	int dummy;
	if (!(aStream >> dummy >> aResult.myNavMeshPath))
	{
		SYSERROR("navMesh broke level loading", aResult.myLevelFile);
		return false;
	}
	return true;
}

bool ParseCollisionBoxes(std::istream& aStream, LevelParseResult& aResult)
{
	int boxCount = 0;
	aStream >> boxCount;

	PhysXBoxInstanceBuffer physXBuffer;

	for (size_t i = 0; i < boxCount; i++)
	{
		if (aStream >> physXBuffer.aFilePath
			>> physXBuffer.position[0] >> physXBuffer.position[1] >> physXBuffer.position[2]
			>> physXBuffer.rotation[0] >> physXBuffer.rotation[1] >> physXBuffer.rotation[2]
			>> physXBuffer.scale[0] >> physXBuffer.scale[1] >> physXBuffer.scale[2])
		{
			aResult.myCollsionBoxes.push_back(physXBuffer);
		}
	}
	if (!aStream)
	{
		SYSERROR("physX boxes broke level loader", aResult.myLevelFile);
		return false;
	}
}

bool ParseDestructables(std::istream& aStream, LevelParseResult& aResult)
{
	int destructiblecount = 0;
	aStream >> destructiblecount;

	DestructibleObjectInstanceBuffer destructibleBuffer;

	for (size_t i = 0; i < destructiblecount; i++)
	{
		if (aStream >> destructibleBuffer.aFilePath
			>> destructibleBuffer.position[0] >> destructibleBuffer.position[1] >> destructibleBuffer.position[2]
			>> destructibleBuffer.rotation[0] >> destructibleBuffer.rotation[1] >> destructibleBuffer.rotation[2]
			>> destructibleBuffer.scale[0] >> destructibleBuffer.scale[1] >> destructibleBuffer.scale[2] >> destructibleBuffer.type)
		{
			aResult.myDestrucables.push_back(destructibleBuffer);
		}
	}

	if (!aStream)
	{
		SYSERROR("destructibles boxes broke level loader", aResult.myLevelFile);
		return false;
	}
}

bool ParseSpotlights(std::istream& aStream, LevelParseResult& aResult)
{

	int spotLightCount = 0;
	aStream >> spotLightCount;
	SpotLightBuffer spotBuffer;

	for (size_t i = 0; i < spotLightCount; i++)
	{
		if (aStream >> spotBuffer.aFilePath
			>> spotBuffer.aTranslation.x >> spotBuffer.aTranslation.y >> spotBuffer.aTranslation.z
			>> spotBuffer.aFov
			>> spotBuffer.aRange
			>> spotBuffer.aIntensity
			>> spotBuffer.aRotation.x >> spotBuffer.aRotation.y >> spotBuffer.aRotation.z)
		{
			aResult.mySpotlights.push_back(spotBuffer);
		}
	}
	if (!aStream)
	{
		SYSERROR("Spotlights broke level loader", aResult.myLevelFile);
		return false;
	}
}

LevelParseResult GameState::ParseLevelFile(const std::string& aFilePath)
{
	LevelParseResult result;
	result.myCounter = 0;
	result.myStep = 0;

	std::ifstream levelFile;
	levelFile.open(aFilePath);
	if (!levelFile.good())
	{
		SYSERROR("Could not open/find levelfile", aFilePath);
		return result;
	}

	ParseSkybox(levelFile, result);
	ParseWorldAxis(levelFile, result);
	ParseStaticObjects(levelFile, result);
	ParseEnemies(levelFile, result);
	ParseTriggers(levelFile, result);
	ParsePoints(levelFile, result);
	ParseEnvironmentLight(levelFile, result);
	ParsePointLights(levelFile, result);
	ParsePlayerPos(levelFile, result);
	ParseNavMesh(levelFile, result);
	ParseCollisionBoxes(levelFile, result);
	ParseDestructables(levelFile, result);
	ParseSpotlights(levelFile, result);
	return result;
}

void GameState::UnloadCurrentLevel()
{
	myGameWorld->ClearWorld();
	myScene->RemoveAll();
	//TEMPORARY PHYSX WORLD CLEAN probably to be exchanged for entities cleaning their own physx actor
	myGBPhysX->GBResetScene();
}

void GameState::UnloadLevel(std::string aFilepath)
{
	Message unloadMessage;
	unloadMessage.myText = aFilepath;
	unloadMessage.myMessageType = MessageType::UnloadLevel;
	SendMessages(unloadMessage);
}

void GameState::CreateWorld(WindowHandler* aWindowHandler, DirectX11Framework* aFramework, AudioManager* aAudioManager, SpriteRenderer* aSpriteRenderer)
{
	myScene = new Scene();
	Camera* camera = CCameraFactory::CreateCamera(90.f, true);
	camera->SetPosition(V3F(0, 0, -1000.f));
	camera->SetFov(81.f, false);
	camera->SetFov(65.f, true);

	myScene->AddInstance(camera);
	myScene->SetMainCamera(camera);
#ifdef _DEBUG
	DebugTools::myCamera = camera;
	DebugTools::Setup(myScene);
#endif // _DEBUG


	SAFE_DELETE(myNodePollingStation);
	myNodePollingStation = new NodePollingStation();
	CNodeInstance::ourPollingStation = myNodePollingStation;
	myGameWorld->SystemLoad(myModelLoader, mySpriteFactory, myScene, aFramework, aAudioManager, myGBPhysX, aSpriteRenderer, myLightLoader);

	myGameWorld->Init(myModelLoader, mySpriteFactory, myScene, aFramework, camera, myNodePollingStation);	//LoadDefaultLevel();
	myNodePollingStation->SetTimerController(&myTimerController);
	myNodePollingStation->SetParticleFactory(myGameWorld->GetParticleFactory());
	myNodePollingStation->SetModelLoader(myModelLoader);
	myNodePollingStation->SetScene(myScene);
	myNodePollingStation->SetSpriteFactory(mySpriteFactory);

	std::thread thread(std::bind(&GameState::SearchForFiles, this));
	thread.detach();


	std::ifstream levelSequencefile;
	levelSequencefile.open("Data/Levels/Sequence.levels");
	std::string row;
	while (std::getline(levelSequencefile, row))
	{
		Tools::trim(row);
		myLevelSequence.push_back(row);
	}



	//NextLevel();
}

void GameState::NextLevel()
{
#if DEMOSCENE
	LoadLevel("");
	return;
#endif
	if (myLevelSequence.empty())
	{
		EnvironmentLight* light = myLightLoader->LoadLight("Data/Textures/Skyboxes/skyBox_Level2_light.dds");
		if (light)
		{
			light->myDirection = V3F(1, 1, 1);
			light->myIntensity = 1;
			light->myColor = V3F(1, 1, 1);

			EnvironmentLight* oldLight = myScene->GetEnvironmentLight();
			myScene->SetEnvironmentLight(light);
			SAFE_DELETE(oldLight);
		}

		return;
	}

	++myCurrentLevelindex;
	if (myCurrentLevelindex >= myLevelSequence.size())
	{
		return;
	}

	std::string nextLevel = myLevelSequence[myCurrentLevelindex];
	//myLevelSequence.pop();
	if (!LoadLevel(nextLevel))
	{
		SYSERROR("Could not load level trying to load next level", nextLevel);
		NextLevel();
	}
	Message levelNumberMessage;
	levelNumberMessage.myMessageType = MessageType::CurrentLevel;
	levelNumberMessage.myIntValue = myCurrentLevelindex - 1;
	SendMessages(levelNumberMessage);
}

void GameState::WinGame()
{
	SYSINFO("Won the game!!");
	myIsGameWon = true;
}

void GameState::Activate()
{
	PostMaster::GetInstance()->SendMessages(MessageType::GameActive);
}

void GameState::Deactivate()
{
	PostMaster::GetInstance()->SendMessages(MessageType::GameNotActive);
}

void GameState::Unload()
{
	myGameWorld->ClearWorld(true);
	//mySplines.clear();
	myScene->RemoveAll();
}

void GameState::Update(const float aDeltaTime)
{
	PERFORMANCETAG("GameState update");
#ifdef _DEBUG
	DebugTools::UpdateGizmo();
#endif // _DEBUG

	if (myQueuedPartialLevel.myIsComplete)
	{
		MergeQueuedLevelPartially(0.1f * 0.001f);
	}
	else
	{
		if (myAsyncPartialLevel.valid() && myAsyncPartialLevel.wait_for(std::chrono::milliseconds(0)) == std::future_status::ready)
		{
			myQueuedPartialLevel = myAsyncPartialLevel.get();
			myAsyncPartialLevel = std::future<LevelParseResult>();
		}
	}


#if ENABLENUMPADLEVELSELECT
	for (int i = 0; i < 10; i++)
	{
		if (GetAsyncKeyState(VK_NUMPAD0 + i) && GetActiveWindow() == GetFocus())
		{
#if USEIMGUI
			if (!ImGui::GetIO().WantCaptureKeyboard)
			{
#endif // USEIMGUI
				myCurrentLevelindex = i - 1;
				NextLevel();
#if USEIMGUI
			}
#endif // USEIMGUI
		}
	}
	{
		static bool pressed = false;
		if (GetAsyncKeyState('N') && GetActiveWindow() == GetForegroundWindow())
		{
			if (!pressed)
			{
				pressed = true;
				MergeNextLevel();
			}
		}
		else
		{
			pressed = false;
		}
	}
	{
		static bool pressed = false;
		if (GetAsyncKeyState(VK_DELETE) && GetActiveWindow() == GetForegroundWindow())
		{
			if (!pressed)
			{
				pressed = true;
				static int counter = 0;
				UnloadLevel(myLevelSequence[counter++]);
			}
		}
		else
		{
			pressed = false;
		}
	}
#endif // 0
	myTimerController.CheckTimers();

	//DebugDrawer::GetInstance().DrawFrustum(myTestSpotlight->myCamera->GenerateFrustum());
	myTestSpotlight->myCamera->SetTransform(myScene->GetMainCamera()->GetTransform());

#if true // Decal/spotlight testing
	static bool pressedF = false;
	if (GetAsyncKeyState('F'))
	{
		if (!pressedF)
		{
			Decal* l = myDecalFactory.LoadDecal("Data/Textures/Particles/hastfan.dds", 30, 550, myScene->GetMainCamera());
			myScene->AddToScene(l);
			pressedF = true;
		}
	}
	else
	{
		pressedF = false;
	}

#endif // true // Decal/spotlight testing


#if USEFILEWATHCER
	{
		PERFORMANCETAG("FileWatchers");
		myWatcher.FlushChanges();
		FlushShaderChanges();
		Texture::Flush();
	}
#endif // USEFILEWATCHER
#if DEMOSCENE
	for (auto& i : myLights)
	{
		i->position.RotateY(aDeltaTime);
		if (i->myModel)
		{
			i->myModel->SetPosition(V4F(i->position, 1));
		}
	}
#endif // DEMOSCENE


#if USEIMGUI
	RunImGui(aDeltaTime); // calls mygameworld.Update inside so gameworld gets a tab inside the window;
#if !BAKEVALUES
	{
		PERFORMANCETAG("Metric editor")
			MetricHandler::ImGuiValueEditor();
	}
#endif
#else
	myGameWorld->Update(myScene->GetMainCamera(), *myInputManager->GetInputHandler(), aDeltaTime);
#endif
	// PHYSICS UPDATE ---------------------------------------
	{
		PERFORMANCETAG("PhysX");
		if (myGBPhysX->GetGBPhysXActive())
		{
			//if (aDeltaTime > 0.016f)
			//{
			//	myGBPhysX->GBStepPhysics(0.016f);
			//}
			//else
			//{
			myGBPhysX->GBStepPhysics(aDeltaTime);
			//}
		}
	}
#if USEAUDIO
	Camera* camera = myScene->GetMainCamera();
	Audio3DAttributes attributes;
	attributes.position = camera->GetPosition();
	attributes.forward = camera->GetForward();
	attributes.up = camera->GetUp();
	//TODO: add cameras velocity if gameplay needs it.
	attributes.velocity = { 0,0,0 };

	myAudioManager->Set3DListenerAttributes(attributes);
#endif
	{
		PERFORMANCETAG("Scripts");
		myGraphManager.ReTriggerUpdateringTrees();
	}
	{
		PERFORMANCETAG("Graphical updates");
		myScene->Update(aDeltaTime);
	}

	if (myIsGameWon)
	{
		myGameWorld->ClearWorld(true);

		Message message;
		message.myMessageType = MessageType::PopState;
		message.myBool = true;
		Publisher::SendMessages(message);
	}
}

#if USEIMGUI
void GameState::SearchForFiles()
{
	using namespace std::experimental::filesystem;
	myFoundFiles.clear();
	recursive_directory_iterator it = recursive_directory_iterator(canonical(""));
	size_t toIgnore = canonical("").string().length() + 1;
	recursive_directory_iterator end = recursive_directory_iterator();
	while (it != end)
	{
		if ((*it).path().has_extension())
		{
			std::string found = (*it).path().string();
			myFoundFiles[(*it).path().extension().string()].push_back(found.substr(toIgnore));
		}
		++it;
	}
}
#endif // !_RETAIL
void GameState::RecieveMessage(const Message& aMessage)
{

	switch (aMessage.myMessageType)
	{
	case MessageType::FinnishGame:
	{
		myFinnishGameAfterFadeOut = true;

		/*Message msg;
		msg.myMessageType = MessageType::PopState;
		msg.aBool = true;
		PostMaster::GetInstance()->SendMessages(msg);*/
		break;
	}
	case MessageType::FadeOutComplete:
		if (myFinnishGameAfterFadeOut)
		{
			myIsGameWon = true;
		}
		else
		{
#if DEMOSCENE
#else
			NextLevel();
#endif // DEMOSCENE
		}
		break;

	case MessageType::PlayerDied:
		myGameWorld->RespawnPlayer(myLatestCheckpointPos);

		break;

	case MessageType::ChangeLevel:
		LoadLevel(aMessage.myIntValue);
		break;

	case MessageType::NextLevel:
		NextLevel();
		break;
	case MessageType::BulletHit:
	{

		BulletHitReport* report = (BulletHitReport*)aMessage.myData;
		Decal* decal = myDecalFactory.LoadDecal("Data/Textures/Decals/BulletHole.dds", 30, 200, myScene->GetMainCamera());
		decal->myCamera->SetPosition(report->position + report->normal * 50.f);
		decal->myCamera->LookAt(report->position);
		myScene->AddToScene(decal);
	}
	break;
	case MessageType::LoadLevel:
		MergeLevel(aMessage.myText.data());
		break;
	default:
		LOGWARNING("Unknown messagetype in GameState");
		break;
	}
}