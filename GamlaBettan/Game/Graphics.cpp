#pragma once
#include "pch.h"
#include "Graphics.h"

Graphics::Graphics()
{
}

Graphics::~Graphics()
{
}

void Graphics::Init(Entity* aEntity)
{
}

void Graphics::SetUpModel(const std::string& aPath)
{
}

void Graphics::Update(const float aDeltaTime)
{
}

void Graphics::Reset()
{
}

void Graphics::OnAttach()
{
}

void Graphics::OnDetach()
{
}

void Graphics::OnKillMe()
{
}
