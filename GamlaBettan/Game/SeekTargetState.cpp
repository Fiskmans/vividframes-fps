#include "pch.h"
#include "SeekTargetState.h"
#include "AIPollingStation.h"
#include "Entity.h"
#include "AIController.h"
#include "GBPhysXKinematicComponent.h"
#include "AnimationComponent.h"

#include <iostream>


SeekTargetState::SeekTargetState(Entity* aEntity, AIPollingStation* aPollingStation) :
	myEntity(aEntity),
	myPollingStation(aPollingStation)
{
	myUpdatePathfindingTimer = 0;
}

SeekTargetState::~SeekTargetState()
{
}

bool SeekTargetState::CheckCondition(AIStates& aState)
{
	V3F enemyDir = myEntity->GetPosition() - myPollingStation->GetPlayer()->GetPosition();
	float distanceSqr = enemyDir.LengthSqr();

	if (distanceSqr < 30000.f)
	{
		aState = AIStates::Attack;
		return true;
	}
	if (V3F(myPollingStation->GetPlayer()->GetPosition() - myEntity->GetPosition()).LengthSqr() > (2000 * 2000))
	{
		if (myEntity->GetComponent<AIController>()->GetTargetPosition() != V3F() &&
			myEntity->GetPosition().DistanceSqr(myEntity->GetComponent<AIController>()->GetTargetPosition()) < 200 * 200)
		{
			aState = AIStates::Idle;
			myEntity->GetComponent<AIController>()->SetTargetPosition(V3F());
			return true;
		}
		else if (myEntity->GetComponent<AIController>()->GetTargetPosition() == V3F())
		{
			aState = AIStates::Idle;
			return true;
		}
	}


	aState = AIStates::None;
	return false;
}

bool SeekTargetState::UpdateSeparation(V3F aDirection)
{
	//Check if path is colliding with other enemies and if so change path
	V3F direction = aDirection;

	float threshold = 20000.f;
	float decayCoefficient = 100000000;
	float maxAcceleration = 200000.f;

	float strength = 0;

	bool couldntMoveForward = false;



	for (int i = 0; i < myPollingStation->GetSeekingEnemies().size(); ++i)
	{
		Entity* enemy = myPollingStation->GetSeekingEnemies()[i];

		if (enemy == myEntity)
		{
			continue;
		}

		V3F enemyDir = myEntity->GetPosition() - enemy->GetPosition();
		float distanceSqr = enemyDir.LengthSqr();

		strength = 0;
		if (distanceSqr < threshold)
		{
			strength = min(decayCoefficient / (distanceSqr * distanceSqr), maxAcceleration);
			couldntMoveForward = true;
		}

		if (enemyDir.Length() > 0)
		{
			enemyDir.Normalize();
		}
		direction += enemyDir * strength;
	}


	V3F enemyDir = myEntity->GetPosition() - myTargetPos;
	float distanceSqr = enemyDir.LengthSqr();

	strength = 0;
	if (distanceSqr < threshold)
	{
		strength = min(decayCoefficient / (distanceSqr * distanceSqr), maxAcceleration);
		couldntMoveForward = true;
	}

	if (enemyDir.Length() > 0)
	{
		enemyDir.Normalize();
	}
	direction += enemyDir * strength;

	if (couldntMoveForward)
	{
		myTargetPos = myEntity->GetPosition() + direction.GetNormalized() * 100.f;
		return false;
	}
	return true;
}

void SeekTargetState::UpdatePathfinder()
{
	//TODO: makes the same check again if should seek to player or target position. maybe optimize to just one check.
	if (V3F(myPollingStation->GetPlayer()->GetPosition() - myEntity->GetPosition()).LengthSqr() < (1000 * 1000))
	{
		myTargetPos = myPollingStation->GetPlayer()->GetPosition();
	}
	else if (myEntity->GetComponent<AIController>()->GetTargetPosition() != V3F())
	{
		myTargetPos = myEntity->GetComponent<AIController>()->GetTargetPosition();
	}
	//----------------------------------------------------------------------------

	myWayPoints = PathFinder::GetInstance().FindPath(myEntity->GetPosition(), myTargetPos);

	if (myWayPoints.size() > 0)
	{
		if (!UpdateSeparation(V3F(myWayPoints[0] - myEntity->GetPosition()).GetNormalized()))
		{
			myWayPoints = PathFinder::GetInstance().FindPath(myEntity->GetPosition(), myTargetPos);
		}
	}
}

void SeekTargetState::UpdateMovement()
{
	if (myWayPoints.size() > 0)
	{
		while (V3F(myEntity->GetPosition()).DistanceSqr(myWayPoints[0]) < 100)
		{
			myWayPoints.erase(myWayPoints.begin());
			if (myWayPoints.size() == 0)
			{
				break;
			}
		}

		if (myWayPoints.size() > 0)
		{
			V3F direction = myWayPoints[0] - myEntity->GetPosition();
			myEntity->GetComponent<GBPhysXKinematicComponent>()->SetTargetDirection(direction.GetNormalized());
			if (direction != V3F())
			{
				direction.y = 0;
				myEntity->SetRotation(CommonUtilities::Matrix4x4<float>::CreateRotationFromDirection(direction));
			}
		}

	}
	else
	{
		myEntity->GetComponent<GBPhysXKinematicComponent>()->SetTargetDirection(V3F());
	}
}

void SeekTargetState::Update(float aDeltaTime)
{
	if (!myIsStunlocked)
	{
		myUpdatePathfindingTimer -= aDeltaTime;

		if (myUpdatePathfindingTimer <= 0)
		{
			myUpdatePathfindingTimer = 0.5f;

			UpdatePathfinder();
		}

		UpdateMovement();

		if (myEntity->GetComponent<GBPhysXKinematicComponent>()->GetDeltaMovement().LengthSqr() > 10)
		{
			myEntity->GetComponent<AnimationComponent>()->SetState(AnimationComponent::States::Walking);
		}
		else
		{
			myEntity->GetComponent<AnimationComponent>()->SetState(AnimationComponent::States::Idle);
		}
	}
}

void SeekTargetState::OnEnter()
{
	myPollingStation->AddSeekingEnemy(myEntity);
}

void SeekTargetState::OnExit()
{
	myWayPoints.clear();
	myEntity->GetComponent<GBPhysXKinematicComponent>()->SetTargetDirection(V3F());
}

void SeekTargetState::SetStunlocked(bool aIsStunned)
{
	myIsStunlocked = aIsStunned;
}
