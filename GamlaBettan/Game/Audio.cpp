#include "pch.h"
#include "Audio.h"
#include "AudioInstance.h"
#include "AudioManager.h"
#include "Entity.h"
#include "Camera.h"
#include "Movement3D.h"
#include "fmod_studio_common.h"
#include "fmod_studio_guids.hpp"
#include "AbilityInstance.h"

Audio::Audio():
	myEntity(nullptr),
	myAudioManager(nullptr)
{
	for (auto& eventID : myEventIDs)
	{
		eventID = nullptr;
	}
}

Audio::~Audio()
{
	Reset();
}

void Audio::PreInit(AudioManager* aAudioManager)
{
	myAudioManager = aAudioManager;
}

void Audio::Init(Entity* aEntity)
{
	myEntity = aEntity;
}

void Audio::Update(const float aDeltaTime)
{
	Audio3DAttributes attributes;
	attributes.position = myEntity->GetPosition();
	attributes.forward = myEntity->GetForward();
	attributes.up = myEntity->GetUp();
	attributes.velocity = myEntity->GetForward() * 100.f;

	for (int i = 0 ; i < myInstances.size(); ++i)
	{
		if (myInstances[i])
		{
			myInstances[i]->Set3DAttributes(attributes);
		}
		if (myInstances[i]->GetisDead())
		{
			myInstances[i] = nullptr;
			myInstances.erase(myInstances.begin() + i);
		}
	}
}

void Audio::Reset()
{
	for (auto* instance : myInstances)
	{
		if (instance)
		{
			if (!instance->GetShouldFinnishPlaying())
			{
				instance->KillAudio();
			}
			instance = nullptr;
		}
	}
	myInstances.clear();
}

void Audio::CreateAudio(const FMOD_GUID& aSoundEventID, bool shouldFinnishPlaying)
{
	if (myEntity && myAudioManager)
	{
		Audio3DAttributes attributes;
		attributes.position = myEntity->GetPosition();
		attributes.forward = myEntity->GetForward();
		attributes.up = myEntity->GetUp();
		attributes.velocity = { 0,0,0 };


		AudioInstance* instance = myAudioManager->CreateAudioInstance(aSoundEventID, attributes, shouldFinnishPlaying);
		if (instance)
		{
			myInstances.push_back(instance);
		}
	}
}

void Audio::PlayOneShot(const FMOD_GUID& aSoundEventID)
{
	if (myEntity && myAudioManager)
	{
		Audio3DAttributes attributes;
		attributes.position = myEntity->GetPosition();
		attributes.forward = myEntity->GetForward();
		attributes.up = myEntity->GetUp();
		attributes.velocity = { 0,0,0 };

		myAudioManager->PlayOneShot3D(aSoundEventID, attributes);
	}
}

void Audio::PlayOneShot2D(const FMOD_GUID& aSoundEventID)
{
	if (myEntity && myAudioManager)
	{
		myAudioManager->PlayOneShot2D(aSoundEventID);
	}
}

void Audio::PlayAudioEvent(AudioEvent aAudioEvent)
{
	if (myEventIDs[static_cast<int>(aAudioEvent)] != nullptr)
	{
		if (aAudioEvent == AudioEvent::Death || aAudioEvent == AudioEvent::Hit || aAudioEvent == AudioEvent::Spawn || aAudioEvent == AudioEvent::Activated)
		{
			CreateAudio(*myEventIDs[static_cast<int>(aAudioEvent)], true);
		}
		else
		{
			CreateAudio(*myEventIDs[static_cast<int>(aAudioEvent)]);
		}
	}
	else if (aAudioEvent == AudioEvent::SILENCE)
	{
		Reset();
	}
	else
	{
		//SYSERROR("Entity has no sutch audio event");
	}
}

void Audio::OnAttach()
{
}

void Audio::OnDetach()
{
}

void Audio::OnKillMe()
{
}

void Audio::InitEventIDs()
{
	if (myEntity->GetEntityType() == EntityType::Player)
	{
		//Bara larv
		//myEventIDs[static_cast<int>(AudioEvent::Idle)] = new FMOD_GUID(FMODEvent::InGame_Abilities_LeapJump);
		//------
		myEventIDs[static_cast<int>(AudioEvent::Footstep)] = new FMOD_GUID(FMODEvent::InGame_Player_FootStep);
		myEventIDs[static_cast<int>(AudioEvent::Hit)] = new FMOD_GUID(FMODEvent::InGame_Player_TakeDamage);
	}
	else if (myEntity->GetEntityType() == EntityType::Enemy)
	{
		Stats* stats = myEntity->GetComponent<CharacterInstance>()->GetCharacterStats();

		if (stats->characterID == 3 || stats->characterID == 33) //Bloated
		{
			myEventIDs[static_cast<int>(AudioEvent::Death)] = new FMOD_GUID(FMODEvent::InGame_Enemies_BloatedDie);
		}
		else if (stats->characterID == 4) //Worm
		{
			myEventIDs[static_cast<int>(AudioEvent::Death)] = new FMOD_GUID(FMODEvent::InGame_Enemies_WormDie);
		}
		else
		{
			myEventIDs[static_cast<int>(AudioEvent::Death)] = new FMOD_GUID(FMODEvent::InGame_Enemies_SmallDie);
		}

		myEventIDs[static_cast<int>(AudioEvent::Hit)] = new FMOD_GUID(FMODEvent::InGame_Enemies_TakeDamage);
	}
	else if (myEntity->GetEntityType() == EntityType::Ability)
	{
		auto id = myEntity->GetComponent<AbilityInstance>()->GetAbilityID();
		switch (id)
		{
			case 0:
			{
				//Fail
				break;
			}
			case 1: //Leap
			{
				myEventIDs[static_cast<int>(AudioEvent::Spawn)] = new FMOD_GUID(FMODEvent::InGame_Abilities_LeapLand);
				break;
			}
			case 2: //Stomp
			{
				myEventIDs[static_cast<int>(AudioEvent::Activated)] = new FMOD_GUID(FMODEvent::InGame_Abilities_GroundStompSpawn);
				myEventIDs[static_cast<int>(AudioEvent::Hit)] = new FMOD_GUID(FMODEvent::InGame_Abilities_GroundStompHit);
				break;
			}
			case 3: //Range
			{
				myEventIDs[static_cast<int>(AudioEvent::Spawn)] = new FMOD_GUID(FMODEvent::InGame_Abilities_PlayerRangeSpawn);
				myEventIDs[static_cast<int>(AudioEvent::Hit)] = new FMOD_GUID(FMODEvent::InGame_Abilities_PlayerRangeHit);
				break;
			}
			case 4: // Shout
			{
				myEventIDs[static_cast<int>(AudioEvent::Spawn)] = new FMOD_GUID(FMODEvent::InGame_Abilities_ShoutSpawn);
				break;
			}
			case 5: //Bash
			{
				myEventIDs[static_cast<int>(AudioEvent::Spawn)] = new FMOD_GUID(FMODEvent::InGame_Abilities_BashSpawn);
				myEventIDs[static_cast<int>(AudioEvent::Hit)] = new FMOD_GUID(FMODEvent::InGame_Abilities_BashHit);
				break;
			}
			case 6: //Cleave
			{
				myEventIDs[static_cast<int>(AudioEvent::Spawn)] = new FMOD_GUID(FMODEvent::InGame_Abilities_CleaveSpawn);
				myEventIDs[static_cast<int>(AudioEvent::Hit)] = new FMOD_GUID(FMODEvent::InGame_Abilities_CleaveHit);
				break;
			}
			case 10: //EnemyAttack
			{
				myEventIDs[static_cast<int>(AudioEvent::Spawn)] = new FMOD_GUID(FMODEvent::InGame_Abilities_BasicEnemyAttackSpawn);
				myEventIDs[static_cast<int>(AudioEvent::Hit)] = new FMOD_GUID(FMODEvent::InGame_Abilities_BasicEnemyAttackHit);
				break;
			}
			case 11: //Enemy ranged
			{
				myEventIDs[static_cast<int>(AudioEvent::Spawn)] = new FMOD_GUID(FMODEvent::InGame_Abilities_EnemyRangeSpawn);
				myEventIDs[static_cast<int>(AudioEvent::Hit)] = new FMOD_GUID(FMODEvent::InGame_Abilities_EnemyRangeHit);
				break;
			}
			case 12: //Bloated attack
			{
				myEventIDs[static_cast<int>(AudioEvent::Spawn)] = new FMOD_GUID(FMODEvent::InGame_Abilities_BloatedAttackSpawn);
				myEventIDs[static_cast<int>(AudioEvent::Activated)] = new FMOD_GUID(FMODEvent::InGame_Abilities_BloatedAttackHit);
				break;
			}
			case 14: //WormAttack
			{
				myEventIDs[static_cast<int>(AudioEvent::Spawn)] = new FMOD_GUID(FMODEvent::InGame_Abilities_WormAttackSpawn);
				myEventIDs[static_cast<int>(AudioEvent::Hit)] = new FMOD_GUID(FMODEvent::InGame_Abilities_WormAttackHit);
				break;
			}
			case 81: //Boss fireball
			{
				myEventIDs[static_cast<int>(AudioEvent::Spawn)] = new FMOD_GUID(FMODEvent::InGame_Abilities_BossFireBallSpawn);
				myEventIDs[static_cast<int>(AudioEvent::Hit)] = new FMOD_GUID(FMODEvent::InGame_Abilities_BossFireBallHit);
				break;
			}
			case 82: //Boss phase1 melee
			{
				myEventIDs[static_cast<int>(AudioEvent::Spawn)] = new FMOD_GUID(FMODEvent::InGame_Abilities_BossPhase1MeleeSpawn);
				myEventIDs[static_cast<int>(AudioEvent::Hit)] = new FMOD_GUID(FMODEvent::InGame_Abilities_BossPhase1MeleeHit);
				break;
			}
			case 83: //Boss Spawn
			{
				myEventIDs[static_cast<int>(AudioEvent::Spawn)] = new FMOD_GUID(FMODEvent::InGame_Abilities_BossSpawnSpawn);
				myEventIDs[static_cast<int>(AudioEvent::Hit)] = new FMOD_GUID(FMODEvent::InGame_Abilities_BossSpawnHit);
				break;
			}
			case 84: //Boss Demon Melee
			{
				myEventIDs[static_cast<int>(AudioEvent::Spawn)] = new FMOD_GUID(FMODEvent::InGame_Abilities_DemonMeleeSpawn);
				myEventIDs[static_cast<int>(AudioEvent::Activated)] = new FMOD_GUID(FMODEvent::InGame_Abilities_DemonMeleeHit);
				break;
			}
			case 85: //Boss Demon Melee Double
			{
				myEventIDs[static_cast<int>(AudioEvent::Spawn)] = new FMOD_GUID(FMODEvent::InGame_Abilities_DemonMeleeDoubleSpawn);
				myEventIDs[static_cast<int>(AudioEvent::Activated)] = new FMOD_GUID(FMODEvent::InGame_Abilities_DemonMeleeDoubleHit);
				break;
			}
			case 86: //Boss beam
			{
				myEventIDs[static_cast<int>(AudioEvent::Spawn)] = new FMOD_GUID(FMODEvent::InGame_Abilities_DemonBeamSpawn);
				myEventIDs[static_cast<int>(AudioEvent::Hit)] = new FMOD_GUID(FMODEvent::InGame_Abilities_DemonBeamHit);
				break;
			}
		}
	}
	else if (myEntity->GetEntityType() == EntityType::Pickups)
	{
		{
			myEventIDs[static_cast<int>(AudioEvent::Death)] = new FMOD_GUID(FMODEvent::InGame_Environment_GemstonePickup);
		}
	}
	else if (myEntity->GetEntityType() == EntityType::EnvironmentDestructible)
	{
		myEventIDs[static_cast<int>(AudioEvent::Hit)] = new FMOD_GUID(FMODEvent::InGame_Environment_BarrelExplode);
	}
}