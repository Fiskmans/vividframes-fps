#include "pch.h"
#include "AIController.h"
#include "Entity.h"
#include "ComponentLake.h"
#include "DebugDrawer.h"
#include "Random.h"

#include <PathFinder.h>

#include "AIBaseState.h"
#include "IdleState.h"
#include "AlertedState.h"
#include "SeekTargetState.h"
#include "AttackState.h"
#include "AIPollingStation.h"

#include "GBPhysXKinematicComponent.h"
#include "AnimationComponent.h"

#include "Sphere.hpp"

#include "DebugDrawer.h"

AIController::AIController() :
	myCurrentState(AIStates::Idle),
	myTargetPosition(V3F())
{
	myUpdatePathfindingTimer = 0;
	myIsAnimLocked = false;
}

AIController::~AIController()
{

}

void AIController::Init(Entity* aEntity)
{
	myEntity = aEntity;

}

void AIController::AIInit(AIPollingStation* aPollingStation)
{
	myPollingStation = aPollingStation;

	IdleState* idleState = new IdleState(myEntity, aPollingStation);
	myStates[AIStates::Idle] = idleState;
	myCurrentState = AIStates::Idle;
	AlertedState* alertState = new AlertedState(myEntity, aPollingStation);
	myStates[AIStates::Alerted] = alertState;
	SeekTargetState* seekState = new SeekTargetState(myEntity, aPollingStation);
	myStates[AIStates::SeekTarget] = seekState;
	AttackState* attackState = new AttackState(myEntity, aPollingStation);
	myStates[AIStates::Attack] = attackState;

	//myTargetPosition = V3F(1610, 648.57, 6300 );
}

AIPollingStation* AIController::GetPollingStation()
{
	return myPollingStation;
}

void AIController::SetTargetPosition(const V3F& aTargetPosition)
{
	myTargetPosition = aTargetPosition;
}

V3F AIController::GetTargetPosition()
{
	return myTargetPosition;
}


void AIController::Update(const float aDeltaTime)
{
	AIStates state;
	if (myStates[myCurrentState]->CheckCondition(state))
	{
		SwitchState(state);
	}

	myStates[myCurrentState]->Update(aDeltaTime);
}

void AIController::Reset()
{
}

void AIController::Collided()
{
}

void AIController::RecieveEntityMessage(EntityMessage aMessage, void* someData)
{
	if (aMessage == EntityMessage::AttackAnimationOver)
	{
		myEntity->SendEntityMessage(EntityMessage::StopAttacking);
	}
	else if (aMessage == EntityMessage::AlertedAnimationFinnished)
	{
		reinterpret_cast<AlertedState*>(myStates[AIStates::Alerted])->SetAlertAnimFinnished();
	}
	else if (aMessage == EntityMessage::TookDamage)
	{
		myEntity->GetComponent<AnimationComponent>()->SetState(AnimationComponent::States::TakeDamage);
		reinterpret_cast<SeekTargetState*>(myStates[AIStates::SeekTarget])->SetStunlocked(true);
	}
	else if (aMessage == EntityMessage::TakeDamageAnimationFinnished)
	{
		myEntity->GetComponent<AnimationComponent>()->SetState(AnimationComponent::States::Idle);
		reinterpret_cast<SeekTargetState*>(myStates[AIStates::SeekTarget])->SetStunlocked(false);
	}
	else if (aMessage == EntityMessage::AttackAnimationFinnished)
	{
		myEntity->GetComponent<AnimationComponent>()->SetState(AnimationComponent::States::Idle);
	}
}

void AIController::SwitchState(AIStates aState)
{
	if (aState != myCurrentState && !myIsDead)
	{
		myStates[myCurrentState]->OnExit();

		myCurrentState = aState;

		myStates[myCurrentState]->OnEnter();
	}
	else if (myIsDead)
	{
		myPollingStation->RemoveSeekingEnemy(myEntity);
	}
}

void AIController::OnDisable()
{
	myPollingStation->RemoveSeekingEnemy(myEntity);
	myIsDead = true;
}

void AIController::OnAttach()
{

}

void AIController::OnDetach()
{

}

void AIController::OnKillMe()
{

}