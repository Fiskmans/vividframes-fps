#include "pch.h"
#include "PauseState.h"
#include "InputHandler.h"
#include "PostMaster.hpp"
#include "GraphicEngine.h"
#include "SpriteInstance.h"
#include <Xinput.h>
#include "AudioManager.h"

PauseState::PauseState(bool aShouldDeleteOnPop) :
	BaseState(aShouldDeleteOnPop)
{
	myDrawThrough = true;
	myMousePointer = nullptr;
	myFadeSprite = nullptr;
	myUpdateThrough = false;
	myIsMain = false;
}

PauseState::~PauseState()
{
	SAFE_DELETE(myFadeSprite);
	SAFE_DELETE(myMousePointer);
}

void PauseState::Update(const float aDeltaTime)
{
	if (myResumeButton.IsPressed())
	{
		PostMaster::GetInstance()->SendMessages(MessageType::InputUnPauseHit);

		Message msg;
		msg.myMessageType = MessageType::PopState;
		msg.myBool = false;
		PostMaster::GetInstance()->SendMessages(msg);

		Message message;
		message.myMessageType = MessageType::InputFPSMode;
		message.myBool = true;
		PostMaster::GetInstance()->SendMessages(message);
	}

	if (myMainMenuButton.IsPressed())
	{
		PostMaster::GetInstance()->SendMessages(MessageType::InputUnPauseHit);

		Message msg;
		msg.myMessageType = MessageType::PopState;
		msg.myBool = true;
		PostMaster::GetInstance()->SendMessages(msg);
	}
}

bool PauseState::Init()
{
	V2F pos = { 0.4327f, 0.4f };
	myResumeButton.Init("Data/UI/UIButtons", "Resume", pos, V2F(0.545f, 1.f));
	pos.y += 0.1f;
	myMainMenuButton.Init("Data/UI/UIButtons", "Main Menu", pos, V2F(0.545f, 1.f));

	SpriteFactory* spriteFactory = myResumeButton.GetSpriteFactory();
	myFadeSprite = spriteFactory->CreateSprite("Data/UI/UIButtons/Pause Menu Background.dds");

	myFadeSprite->SetPosition(0.0f, 0.0f);

	myMousePointer = spriteFactory->CreateSprite("Data/UI/mouse.dds");
	return true;
}

void PauseState::Render(CGraphicsEngine* aGraphicsEngine)
{
	std::vector<SpriteInstance*> sprites;

	sprites.push_back(myFadeSprite);
	sprites.push_back(myMainMenuButton.GetCurrentSprite());
	sprites.push_back(myResumeButton.GetCurrentSprite());
	sprites.push_back(myMousePointer);

	aGraphicsEngine->RenderMovie(sprites);
}

void PauseState::RecieveMessage(const Message& aMessage)
{/*
	if (aMessage.myMessageType == MessageType::InputControllerButtonHit)
	{
		bool acceptPressed = false;

		switch (aMessage.aIntValue)
		{
		case XINPUT_GAMEPAD_DPAD_UP:
			--selectionIndex;
			break;
		case XINPUT_GAMEPAD_DPAD_DOWN:
			++selectionIndex;
			break;
		case XINPUT_GAMEPAD_A:
			acceptPressed = true;
			break;
		}

		if (selectionIndex > 1)
			selectionIndex = 0;

		if (selectionIndex < 0)
			selectionIndex = 1;

		if (myResumeButton.IsPressed() == false && myMainMenuButton.IsPressed() == false)
		{
			if (selectionIndex == 0)
			{
				myMainMenuButton.Deactivate();

				myResumeButton.SetActive();
			}
			else if (selectionIndex == 1)
			{
				myResumeButton.Deactivate();

				myMainMenuButton.SetActive();
			}
		}

		if (acceptPressed)
		{
			if (myResumeButton.IsActive())
			{
				myResumeButton.SetPressed();
			}
			else if (myMainMenuButton.IsActive())
			{
				myMainMenuButton.SetPressed();
			}
		}
	}*/

	if (aMessage.myMessageType == MessageType::InputMouseMoved)
	{
		myMousePointer->SetPosition(CommonUtilities::Vector2<float>(aMessage.myFloatValue, aMessage.myFloatValue2));
	}
}

void PauseState::Activate()
{
	myResumeButton.Subscribe();
	myMainMenuButton.Subscribe();

	PostMaster::GetInstance()->Subscribe(MessageType::InputMouseMoved, this);

	Message message;
	message.myMessageType = MessageType::InputFPSMode;
	message.myBool = false;
	PostMaster::GetInstance()->SendMessages(message);
}

void PauseState::Deactivate()
{
	myResumeButton.Unsubscribe();
	myMainMenuButton.Unsubscribe();

	PostMaster::GetInstance()->UnSubscribe(MessageType::InputMouseMoved, this);
}

void PauseState::Unload()
{
}