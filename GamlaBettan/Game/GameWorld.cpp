#include "pch.h"
#include "GameWorld.h"
#include "Camera.h"
#include "InputHandler.h"
#include "Mesh.h"
#include "Movement3D.h"
#include "AnimationComponent.h"
#include "GBPhysXComponent.h"
#include "GBPhysXKinematicComponent.h"
#include "FPSCamera.h"
#include "FireWeapon.h"

#include "PlayerController.h"
#include "light.h"
#include "Life.h"
#include "FollowCamera.h"

#include "..//CommonUtilities/Vector3.hpp"
#include <ModelLoader.h>
#include <Scene.h>
#include "Octree.h"
#include "IntersectionRecord.h"

#include "ComponentLake.h"
#include "Sphere.hpp"
#include "DebugDrawer.h"
#include "Random.h"
#include <DebugTools.h>
#include <experimental/filesystem>
#include "Model.h"
#include "Audio.h"
#include "ShaderFlags.h"
#include "Animator.h"
#include "AIController.h"
#include "CharacterInstance.h"

#include "ParticleActivatable.h"

#include "EnemyFactory.h"
#include "StaticObjectFactory.h"
#include "DestructibleFactory.h"
#include "TriggerBoxFactory.h"
#include "PointLightFactory.h"
#include "ParticleFactory.h"
#include <SpriteFactory.h>
#include <SpriteInstance.h>
#include "AbilityFactory.h"

#include "TextFactory.h"

#include "GBPhysX.h"
#include "GBPhysXColliderFactory.h"

#include "TextInstance.h"

#include "UIManager.h"
#include "CharacterData.h"
#include "AbilityData.h"
#include <WindSystem.h>
#include "Skybox.h"
#include "perlin_noise.h"

#if USEIMGUI
#include <imgui.h>
#include <Environmentlight.h>
#include <WindowControl.h>
#include <ModelLoader.h>
#include <LightLoader.h>
#endif 

#include <Intersection.hpp>
#include "ParticleInstance.h"

GameWorld::GameWorld() :
	myEntityID(0),
	myUsingFreeCamera(true),
	myPlayer(nullptr),
	myObjectTree(nullptr),
	myScenePtr(nullptr),
	myParticleFactory(nullptr),
	myEnemyFactory(nullptr),
	myStaticObjectFactory(nullptr),
	myDestructibleFactory(nullptr),
	myTriggerBoxFactory(nullptr),
	myPointLightFactory(nullptr),
	myGBPhysXPtr(nullptr),
	myUIManager(nullptr),
	myTextFactory(nullptr),
	myCharacterData(nullptr),
	myAbilityData(nullptr),
	myNodePollingstationPtr(nullptr),
	myGBPhysXColliderFactory(nullptr),
#if USEIMGUI
	myLastPackage(nullptr),
#endif 
	myMainCameraPtr(nullptr),
	myAIPollingStation(nullptr)
{
	myEntityPool.Init(ENTITY_POOL_SIZE);
}

GameWorld::~GameWorld()
{
	ClearWorld(true);

	UnSubscribeToMessage(MessageType::NewLevelLoaded);
	UnSubscribeToMessage(MessageType::WindowResize);
	UnSubscribeToMessage(MessageType::EnemyDied);
	UnSubscribeToMessage(MessageType::TriggerEvent);
	UnSubscribeToMessage(MessageType::FadeInComplete);
	UnSubscribeToMessage(MessageType::SpawnBulletHitParticle);

	myEntityPool.Release();

	myScenePtr = nullptr;
	myMainCameraPtr = nullptr;
	myGBPhysXPtr = nullptr;
	myNodePollingstationPtr = nullptr;

	SAFE_DELETE(myObjectTree);
	SAFE_DELETE(myEnemyFactory);
	SAFE_DELETE(myStaticObjectFactory);
	SAFE_DELETE(myDestructibleFactory);
	SAFE_DELETE(myTriggerBoxFactory);
	SAFE_DELETE(myPointLightFactory);
	SAFE_DELETE(myParticleFactory);
	SAFE_DELETE(myUIManager);
	SAFE_DELETE(myCharacterData);
	SAFE_DELETE(myTextFactory);
	SAFE_DELETE(myAbilityData);
	SAFE_DELETE(myGBPhysXColliderFactory);
	SAFE_DELETE(myAIPollingStation);
}

void GameWorld::SystemLoad(ModelLoader* aModelLoader, SpriteFactory* aSpriteFactory, Scene* aScene, DirectX11Framework* aFramework, AudioManager* aAudioManager, GBPhysX* aGBPhysX, SpriteRenderer* aSpriteRenderer, LightLoader* aLightLoader)
{
	myWindowSize.x = 1920;
	myWindowSize.y = 1080;

	myEntitys.reserve(ENTITY_POOL_SIZE);
	myTriggers.reserve(ENTITY_POOL_SIZE);
	myEnemies.reserve(ENTITY_POOL_SIZE);

	myParticleFactory = new ParticleFactory;
	myEnemyFactory = new EnemyFactory;
	myStaticObjectFactory = new StaticObjectFactory;
	myDestructibleFactory = new DestructibleFactory;
	myTriggerBoxFactory = new TriggerBoxFactory;
	myPointLightFactory = new PointLightFactory;
	myTextFactory = new TextFactory;

	myGBPhysXPtr = aGBPhysX;
	myGBPhysXColliderFactory = new GBPhysXColliderFactory;

	myUIManager = new UIManager;

	myCharacterData = new CharacterData();
	myCharacterData->Load();

	myAbilityData = new AbilityData;
	myAbilityData->Load();

	myAIPollingStation = new AIPollingStation;

	myParticleFactory->Init(aFramework);
	myTextFactory->Init(aFramework, aSpriteRenderer, aSpriteFactory);
	ComponentLake::GetInstance().RegisterComponents();
	ComponentLake::GetInstance().PrepareObjectsInPools(aModelLoader, aScene, myParticleFactory, aAudioManager, aLightLoader);

	myEnemyFactory->Init(myAIPollingStation, myObjectTree, &myEnemies, &myEntityPool, &ComponentLake::GetInstance(), &myEntityID, myCharacterData, myGBPhysXPtr);
	myStaticObjectFactory->Init(myGBPhysXPtr, myObjectTree, &myEntitys, &myEntityPool, &ComponentLake::GetInstance(), &myEntityID);
	myDestructibleFactory->Init(myObjectTree, &myEntitys, &myEntityPool, &ComponentLake::GetInstance(), &myEntityID, myCharacterData, myAbilityData);
	myTriggerBoxFactory->Init(myObjectTree, &myTriggers, &myEntityPool, &ComponentLake::GetInstance(), &myEntityID);
	myPointLightFactory->Init(&myEntitys, &myEntityPool, &ComponentLake::GetInstance(), &myEntityID);
	myGBPhysXColliderFactory->Init(myGBPhysXPtr);

	aModelLoader->SetGbPhysX(myGBPhysXPtr);
}

void GameWorld::Init(ModelLoader* aModelLoader, SpriteFactory* aSpriteFactory, Scene* aScene, DirectX11Framework* aFramework, Camera* aCamera, NodePollingStation* aNodePollingStation)
{
	myMainCameraPtr = aCamera;

	SubscribeToMessage(MessageType::NewLevelLoaded);
	SubscribeToMessage(MessageType::WindowResize);
	SubscribeToMessage(MessageType::EnemyDied);
	SubscribeToMessage(MessageType::TriggerEvent);
	SubscribeToMessage(MessageType::FadeInComplete);
	SubscribeToMessage(MessageType::SpawnBulletHitParticle);

	myUsingFreeCamera = false;

	myScenePtr = aScene;
	myNodePollingstationPtr = aNodePollingStation;
	myNodePollingstationPtr->SetEnemyVector(&myEnemies);
	myNodePollingstationPtr->SetEnemyFactory(myEnemyFactory);

	SetupPlayerAndCamera(V3F(0, 0, 0));
	myUIManager->Init(myPlayer, *myScenePtr, *aSpriteFactory, *myTextFactory, aCamera);

	myCinEditor.Init(aModelLoader, aScene, myParticleFactory, aSpriteFactory);

}

void GameWorld::SetupPlayerAndCamera(CommonUtilities::Vector3<float> aSpawnPos)
{
	myPlayer = myEntityPool.Retrieve();
	myPlayer->Init(&ComponentLake::GetInstance(), EntityType::Player, myEntityID++);
	myEntitys.push_back(myPlayer);
	myNodePollingstationPtr->SetPlayer(myPlayer);
	myAIPollingStation->SetPlayer(myPlayer);

	SpawnPlayer();
}

void GameWorld::ClearWorld(bool isShouldDeleteplayer)
{
	for (int index = CAST(int, myEntitys.size()) - 1; index >= 0; index--)
	{
		if (myEntitys[index]->GetEntityType() != EntityType::Player && myEntitys[index]->GetEntityType() != EntityType::Camera)
		{
			myEntitys[index]->Dispose();
			myEntityPool.Dispose(myEntitys[index]);
			myEntitys.erase(myEntitys.begin() + index);
		}
	}

	for (int index = CAST(int, myTriggers.size()) - 1; index >= 0; index--)
	{
		myTriggers[index]->Dispose();
		myEntityPool.Dispose(myTriggers[index]);
		myTriggers.erase(myTriggers.begin() + index);
	}

	for (int index = CAST(int, myEnemies.size()) - 1; index >= 0; index--)
	{
		myEnemies[index]->Dispose();
		myEntityPool.Dispose(myEnemies[index]);
		myEnemies.erase(myEnemies.begin() + index);
	}

	for (int index = CAST(int, myPickups.size()) - 1; index >= 0; index--)
	{
		myPickups[index]->Dispose();
		myEntityPool.Dispose(myPickups[index]);
		myPickups.erase(myPickups.begin() + index);
	}

	if (isShouldDeleteplayer)
	{
		for (int index = CAST(int, myEntitys.size()) - 1; index >= 0; index--)
		{
			if (myEntitys[index]->GetEntityType() == EntityType::Player)
			{
				myEntitys[index]->Dispose();
				myEntityPool.Dispose(myEntitys[index]);
				myEntitys.erase(myEntitys.begin() + index);
			}
		}
	}

	myEnemyFactory->ClearQueue();
}

void GameWorld::SpawnPlayer()
{
	myPlayer->RemoveAllComponents();

	myPlayer->AddComponent<FPSCamera>()->Init(myPlayer);
	myPlayer->GetComponent<FPSCamera>()->SetCamera(myMainCameraPtr);

	//Fixa tillbaks eller l�s det

	myPlayer->AddComponent<Mesh>()->Init(myPlayer);
	myPlayer->GetComponent<Mesh>()->SetUpModel("Data/Models/Player_Arms_Mp5/Player_Arms_Mp5.fbx");
	//myPlayerCameraEntity->GetComponent<Mesh>()->SetShouldBeDrawnThroughWalls(true);
	//myPlayerCameraEntity->GetComponent<Mesh>()->SetUsePlayerThroughWallShader(true);
	myPlayer->GetComponent<Mesh>()->SetScale(V3F(1.f, 1.f, 1.f));
	myPlayer->GetComponent<Mesh>()->GetModelInstance()->SetUsingSecondaryFov(true);

	myPlayer->AddComponent<ParticleActivatable>()->Init(myPlayer);
	myPlayer->GetComponent<ParticleActivatable>()->PreInit(myScenePtr, myParticleFactory);
	myPlayer->GetComponent<ParticleActivatable>()->SetParticle("WeaponFire.part", 0, V3F(0, 0, 0));


	myPlayer->AddComponent<AnimationComponent>()->Init(myPlayer);
	myPlayer->SetIsMoving(true);

	myPlayer->AddComponent<PlayerController>()->InternalInit(myGBPhysXPtr);
	myPlayer->AddComponent<FireWeapon>()->Init(myPlayer);
	myPlayer->AddComponent<FireWeapon>()->SetWeaponProperties();

	myPlayer->AddComponent<Collision>()->Init(myPlayer);
	myPlayer->GetComponent<Collision>()->SetCollisionRadius(50.0f);
	myPlayer->GetComponent<Collision>()->SetHeightOffset(35.0f);
	myPlayer->GetComponent<Collision>()->SetIsFriendly(true);

	myPlayer->AddComponent<Life>()->Init(myPlayer);
	myPlayer->GetComponent<Life>()->Init(myPlayer, 100);

	myPlayer->AddComponent<Audio>()->Init(myPlayer);
	myPlayer->GetComponent<Audio>()->InitEventIDs();

	myPlayer->Spawn({ 0, 0, 0 });

	myPlayer->AddComponent<GBPhysXKinematicComponent>()->Init(myPlayer);
	myPlayer->GetComponent<GBPhysXKinematicComponent>()->AddGBPhysXCharacter(myGBPhysXPtr, myPlayer->GetPosition(), V3F(0.0f, 0.0f, 0.0f), 150.0f, 25.0f, true);
}

void GameWorld::RespawnPlayer(V3F& aPosition)
{
	//TODO CHECKPOINT POSITION?
	myPlayer->GetComponent<Mesh>()->SetTint({ 0, 0, 0, 1 });

	if (!myScenePtr->Contains(myPlayer->GetComponent<Mesh>()->GetModelInstance()))
	{
		myPlayer->GetComponent<Mesh>()->AddModelToScene();
	}

	myPlayer->GetComponent<PlayerController>()->SetInputBlock(false);

	myPlayer->SetPosition(V3F(aPosition.x, aPosition.y + 10.0f, aPosition.z));
	myPlayer->GetComponent<Life>()->HealInPercentage(100);

	GBPhysXKinematicComponent* component = myPlayer->GetComponent<GBPhysXKinematicComponent>();
	if (component->GetPhysXCharacter() == nullptr)
	{
		component->AddGBPhysXCharacter(myGBPhysXPtr, myPlayer->GetPosition(), V3F(0.0f, 0.0f, 0.0f), 100.0f, 50.0f, true);
	}
	else
	{
		component->GetPhysXCharacter()->Teleport(myPlayer->GetPosition());
		//TODO: add physx actor to physx scene
	}
	myPlayer->GetComponent<AnimationComponent>()->SetState(AnimationComponent::States::Idle, true);

	myPlayer->GetComponent<Mesh>()->SetFading(false);
}

void GameWorld::RecieveMessage(const Message& aMessage)
{
	switch (aMessage.myMessageType)
	{
	case MessageType::NewLevelLoaded:
	{
		static bool first = true;
		if (!first)
		{
			RespawnPlayer(*(V3F*)aMessage.myData);
		}
		else
		{
			V3F pos = *(V3F*)aMessage.myData;
			myPlayer->SetPosition(V3F(pos.x, pos.y + 10.0f, pos.z));
			myPlayer->GetComponent<GBPhysXKinematicComponent>()->GetPhysXCharacter()->Teleport(myPlayer->GetPosition());
		}
		first = false;

		myUIManager->AddUIToScene(*myScenePtr);

		if (aMessage.myIntValue2 == 9)
		{
			myPlayer->SetRotation(V3F(0.0f, TORAD(90), 0.0f));
		}

		std::vector<Collision*> colObjects;
		for (auto& it : myEntitys)
		{
			auto entityType = it->GetEntityType();
			if (entityType != EntityType::None && entityType != EntityType::Player && entityType != EntityType::EnvironmentStatic && entityType != EntityType::Camera)
			{
				colObjects.push_back(it->GetComponent<Collision>());
			}
		}

		//colObjects = myComponentLake.GetCollisionComponentVector();
		SAFE_DELETE(myObjectTree);
		myObjectTree = new Octree(CommonUtilities::AABB3D<float>(CommonUtilities::Vector3<float>(CAST(float, -aMessage.myIntValue), CAST(float, -aMessage.myIntValue), CAST(float, -aMessage.myIntValue)), CommonUtilities::Vector3<float>((float)(aMessage.myIntValue), (float)(aMessage.myIntValue), (float)(aMessage.myIntValue))), colObjects, -1);
		myObjectTree->BuildTree();

		Message newOctreeMessage;
		newOctreeMessage.myMessageType = MessageType::NewOctreeCreated;
		newOctreeMessage.myData = myObjectTree;
		SendMessages(newOctreeMessage);

		SendMessages(MessageType::FadeIn);
	}
	break;

	case MessageType::EnemyDied:
	{
		Entity* enemy = static_cast<Entity*>(aMessage.myData);
		if (enemy->GetComponent<CharacterInstance>()->GetCharacterStats()->characterID == 69)
		{
			// COMMENTED OUT FOR NOW 

			//Message message;
			//message.myMessageType = MessageType::FadeOut;
			//SendMessages(message);
			//
			//Message message2;
			//message2.myMessageType = MessageType::FinnishGame;
			//SendMessages(message2);
		}
	}
	break;

	case MessageType::WindowResize:
	{
		myWindowSize.x = CAST(float, aMessage.myIntValue);
		myWindowSize.y = CAST(float, aMessage.myIntValue2);
	}
	break;

	case MessageType::TriggerEvent:
		break;

	case MessageType::FadeInComplete:
	{
		myFirstFadeInComplete = true;
	}
	break;

	case MessageType::SpawnBulletHitParticle:
	{
		ParticleInstance* particle;
		BulletHitReport* rep = reinterpret_cast<BulletHitReport*>(aMessage.myData);

		if (aMessage.myBool) //Hit entity
		{
			particle = myParticleFactory->InstantiateParticle("Data/Particles/BulletHitEnemy.part");
		}
		else
		{
			particle = myParticleFactory->InstantiateParticle("Data/Particles/BulletHitObject.part");
		}

		const float size = 10.f;

		particle->SetBounds(V4F(rep->position, 1) - V4F(1, 1, 1, 1) * size, V4F(rep->position, 1) + V4F(1, 1, 1, 1) * size);
		particle->SetDirection(V4F(rep->normal, 0));
		particle->RefreshTimeout(0.5f);
		myScenePtr->AddInstance(particle);
	}
	break;

	default:
		break;
	}
}

#if USEIMGUI
void GameWorld::ImGuiNode(ImGuiNodePackage& aPackage)
{
	if (ImGui::CollapsingHeader("Objects"))
	{
		for (auto& i : myEntitys)
		{
			i->ImGuiNode(aPackage);
		}
	}
}

#endif // !_RETAIL

void GameWorld::Update(CommonUtilities::InputHandler& aInputHandler, float aDeltatime)
{
	PerlinNoise noise;
	float now = Tools::GetTotalTime();
	WindSystem::GetInstance().SetBaseWind(V3F((noise.noise(now * 2, now * PI, 5) - 0.5) * 10000, 0, (noise.noise(now * 2, now * PI, 10) - 0.5) * 10000));

	myEnemyFactory->SpawnQueued(aDeltatime);

#if USEIMGUI

	static bool gameIsPaused = false;
	static bool showBoundingBoxes = false;
	static bool editParticles = false;
	static bool pauseAnimations = false;
	static bool snapCameraOnLoad = true;
	static float expectedLifetime = 10.f;
	static ModelInstance* modelInstance = nullptr;
	static Animator* animator = nullptr;
	static std::vector<std::string> foundModels;
	static size_t offset;
	static Skybox* skybox;
	static std::string skyboxPath;
	auto SearchForModels = [&]()
	{
		offset = std::experimental::filesystem::canonical("Data/Models/").string().size() + 1;
		foundModels.clear();
		std::experimental::filesystem::recursive_directory_iterator it(std::experimental::filesystem::canonical("Data/Models/"));
		while (it != std::experimental::filesystem::recursive_directory_iterator())
		{
			if (it->path().has_extension())
			{
				if (it->path().extension() == ".fbx")
				{
					if (std::distance(it->path().begin(), it->path().end()) > 2)
					{
						std::string folderName = (----(it->path().end()))->string();
						std::string fileName = it->path().filename().string().substr(0, it->path().filename().string().size() - 4);

						std::transform(folderName.begin(), folderName.end(), folderName.begin(),
							[](unsigned char c) { return std::tolower(c); });
						std::transform(fileName.begin(), fileName.end(), fileName.begin(),
							[](unsigned char c) { return std::tolower(c); });

						if (folderName == fileName)
						{
							foundModels.push_back(it->path().string());
						}
					}
				}
			}
			++it;
		}
	};
	bool modelViewerOpen = WindowControl::Window("Model Viewer", [&]()
	{
#ifdef _DEBUG
		if (ImGui::BeginTabBar("Viewer"))
		{
			if (ImGui::BeginTabItem("Models"))
			{
				static bool movedCamera = false;
				if (ImGui::Button("Search"))
				{
					SearchForModels();
				}
				ImGui::SameLine();
				ImGui::Checkbox("Show Bounding Boxes", &showBoundingBoxes);
				ImGui::Text("LifeTime: %f", modelInstance ? (Tools::GetTotalTime() - modelInstance->GetSpawnTime()) : 0.0f);
				if (ImGui::InputFloat("Expected LifeTime", &expectedLifetime))
				{
					if (modelInstance)
					{
						modelInstance->SetExpectedLifeTime(expectedLifetime);
					}
				}
				ImGui::Checkbox("Snap camera", &snapCameraOnLoad);

				ImGui::Separator();
				if (ImGui::BeginChild("selection box"))
				{
					for (auto& i : foundModels)
					{
						if (ImGui::Selectable(i.c_str() + offset))
						{
							if (modelInstance)
							{
								myScenePtr->RemoveModel(modelInstance);
								delete modelInstance;
								modelInstance = nullptr;
							}
							SAFE_DELETE(animator);
							modelInstance = DebugTools::myModelLoader->InstantiateModel(i);
							if (modelInstance)
							{
								myScenePtr->AddToScene(modelInstance);
								movedCamera = false;
								modelInstance->SetExpectedLifeTime(expectedLifetime);
							}
						}
					}
				}
				ImGui::EndChild();
				if (!movedCamera && modelInstance && snapCameraOnLoad)
				{
					CommonUtilities::Sphere<float> sphere = modelInstance->GetGraphicBoundingSphere();
					if (abs(sphere.Radius() - 1.f) > 0.1f)
					{
						V3F pos = myScenePtr->GetMainCamera()->GetPosition();
						pos.Normalize();
						pos *= sphere.Radius() * 2;
						myScenePtr->GetMainCamera()->SetPosition(pos);
						movedCamera = true;
					}
				}
				if (showBoundingBoxes && modelInstance)
				{
					for (auto& i : modelInstance->GetModel()->myCollisions)
					{
						DebugDrawer::GetInstance().DrawBoundingBox(i);
					}
				}
				if (animator)
				{
					static float animationSpeed = 1.f;
					static float blend = 1.f;
					static size_t old = 1, cur = 1;
					if (!pauseAnimations)
					{
						animator->Step(aDeltatime * animationSpeed);
					}
					animator->SetBlend(1 - blend);
					if (ImGui::Begin("Animations"))
					{
						static const char* names[] =
						{
							"Spawning",
							"Idle",
							"IdleWalk",
							"Alerted",
							"Walking",
							"Running"
							"Dying",
							"AttackMelee",
							"FireWeapon",
							"FireLast",
							"ReloadTac",
							"ReloadFull",
							"ADSIdle",
							"ADSIn",
							"ADSOut",
							"cinematic",
							"TakeDamage",
							"Godis",
							"Falling"
						};
						static_assert(sizeof(names) / sizeof(*names) == static_cast<int>(AnimationComponent::States::Count) - 1, "Update here too");

						ImGui::Checkbox("Pause animation", &pauseAnimations);
						if (!pauseAnimations)
						{
							ImGui::DragFloat("Playbackspeed", &animationSpeed, 0.001f, 0.001f, 10.f, "%.2fX");
						}
						ImGui::DragFloat("Blend", &blend, 0.001f, 0.f, 1.f, "%.2fX");

						float at = animator->GetCurrentProgress();
						size_t size = animator->GetTickCount();
						float subat = fmodf(at, 1.f);
						int curTick = int(floor(at)) % size;

						if (pauseAnimations)
						{
							bool a = ImGui::SliderInt("Current tick", &curTick, 0, size);
							bool b = ImGui::SliderFloat("Tick progress", &subat, 0.0f, 0.999f);

							if (a || b)
							{
								animator->SetTime(curTick + subat);
							}
						}


						ImGui::Columns(2);
						ImGui::PushID("old");
						for (auto& mapping : myAnimMapping)
						{
							ImGui::Text(names[static_cast<int>(mapping.first)]);
							ImGui::Indent(40);
							ImGui::PushID(static_cast<int>(mapping.first));
							for (size_t i = mapping.second.first; i <= mapping.second.second; i++)
							{
								bool is = i == old;
								if (ImGui::Selectable(std::to_string(i - mapping.second.first).c_str(), &is))
								{
									old = i;
									animator->SetState(old);
									animator->SetState(cur);
								}
							}
							ImGui::PopID();
							ImGui::Unindent(40);
						}
						ImGui::NextColumn();
						ImGui::PopID();
						ImGui::PushID("new");
						for (auto& mapping : myAnimMapping)
						{
							ImGui::Text(names[static_cast<int>(mapping.first)]);
							ImGui::Indent(40);
							ImGui::PushID(static_cast<int>(mapping.first));
							for (size_t i = mapping.second.first; i <= mapping.second.second; i++)
							{
								bool is = i == cur;
								if (ImGui::Selectable(std::to_string(i - mapping.second.first).c_str(), &is))
								{
									cur = i;
									animator->SetState(old);
									animator->SetState(cur);
								}
							}
							ImGui::PopID();
							ImGui::Unindent(40);
						}
						ImGui::NextColumn();
						ImGui::PopID();

					}
					ImGui::End();
				}
				else
				{
					if (modelInstance && modelInstance->GetModel() && modelInstance->GetModel()->ShouldRender() && modelInstance->GetModel()->GetModelData()->myFilePath != "" && modelInstance->GetModel()->GetModelData()->myshaderTypeFlags & ShaderFlags::HasBones)
					{
						animator = new Animator();
						std::vector<std::string> allAnimations;
						myAnimMapping.clear();
						AnimationComponent::ParseAnimations(modelInstance->GetModel()->GetModelData()->myAnimations, myAnimMapping, allAnimations);

						animator->Init(modelInstance->GetModel()->GetModelData()->myFilePath, allAnimations);



						modelInstance->AttachAnimator(animator);
					}
				}
				ImGui::EndTabItem();
			}
			if (ImGui::BeginTabItem("Skyboxes"))
			{
				ImGui::BeginChild("selection");
				if (DebugTools::FileList)
				{
					for (auto& file : DebugTools::FileList->operator[](".dds"))
					{
						size_t pos = file.find("skybox");
						if (pos == std::string::npos)
						{
							continue;
						}


						bool selected = (file == skyboxPath);
						if (ImGui::Selectable(file.c_str(), &selected))
						{
							Skybox* newBox = DebugTools::myModelLoader->InstanciateSkybox(file);
							if (newBox)
							{
								myScenePtr->SetSkybox(newBox);
								SAFE_DELETE(skybox);
								skybox = newBox;
								skyboxPath = file;
								if (DebugTools::myLightLoader)
								{
									EnvironmentLight* l = DebugTools::myLightLoader->LoadLight(file.substr(0, file.size() - 4) + "_light" + file.substr(file.size() - 4));
									if (l)
									{
										EnvironmentLight* old = myScenePtr->GetEnvironmentLight();
										if (old)
										{
											l->myColor = old->myColor;
											l->myDirection = old->myDirection;
											l->myIntensity = old->myIntensity;
											delete old;
										}

										myScenePtr->SetEnvironmentLight(l);
									}
								}
							}
							else
							{
								LOGWARNING("Could not load a skybox of: " + file);
							}
						}
					}
				}
				else
				{
					ImGui::Text("Not currently enabled, run in debug to enable.");
				}
				ImGui::EndChild();
				ImGui::EndTabItem();
			}
			ImGui::EndTabBar();
		}
#endif // _DEBUG
	});
	bool particleEditorOpen = WindowControl::Window("Particle Editor", [&]()
	{
		myScenePtr->RefreshAll(1.f);
		myParticleFactory->EditParticles(myScenePtr);
	});
	WindowControl::Window("GameWorld", [&]()
	{
		if (!particleEditorOpen)
		{
			ImGui::Checkbox("Edit particles", &particleEditorOpen);
		}
		if (!editParticles)
		{
			ImGui::Checkbox("Model Viewer", &modelViewerOpen);
		}

		ImGui::Checkbox("Show Bounding Boxes", &showBoundingBoxes);
#ifdef _DEBUG
		ImGui::Checkbox("Show Paths", &Movement3D::ourShowPaths);
#endif
		ImGui::Checkbox("Pause Game", &gameIsPaused);
	});

	if (particleEditorOpen != editParticles)
	{
		editParticles = particleEditorOpen;
		static V3F cameraPos;
		if (editParticles)
		{
			myScenePtr->Stash(Scene::StashOp::Push);
			cameraPos = myScenePtr->GetMainCamera()->GetPosition();
			myScenePtr->GetMainCamera()->SetPosition(myScenePtr->GetMainCamera()->GetForward() * -400.f);
		}
		else
		{
			myScenePtr->Stash(Scene::StashOp::Pop);
			myScenePtr->GetMainCamera()->SetPosition(cameraPos);
		}
	}
	if (modelViewerOpen != myIsInModelViewerMode)
	{
		myIsInModelViewerMode = modelViewerOpen;
		static V3F cameraPos;
		if (myIsInModelViewerMode)
		{
			myScenePtr->Stash(Scene::StashOp::Push);
			cameraPos = myScenePtr->GetMainCamera()->GetPosition();
			myScenePtr->GetMainCamera()->SetPosition(myScenePtr->GetMainCamera()->GetForward() * -400.f);
			if (foundModels.empty())
			{
				SearchForModels();
			}
		}
		else
		{
			if (modelInstance)
			{
				myScenePtr->RemoveModel(modelInstance);
				delete modelInstance;
				modelInstance = nullptr;
			}
			SAFE_DELETE(animator);
			myScenePtr->Stash(Scene::StashOp::Pop);
			myScenePtr->GetMainCamera()->SetPosition(cameraPos);
		}
	}

	bool isRunningStandard = !gameIsPaused;
	if (myCinEditor.Run())
	{
		isRunningStandard = false;
	}


	if (editParticles || myIsInModelViewerMode) //Cameracontrols
	{
#pragma region Cameracontrols
		if (aInputHandler.IsKeyDown(CommonUtilities::InputHandler::Key_Alt))
		{
			static Point lastmp = aInputHandler.GetMousePosition();
			Point mp = aInputHandler.GetMousePosition();
			if (aInputHandler.IsMouseDown(CommonUtilities::InputHandler::Mouse::Mouse_Right))
			{
				const static float speed = 0.001f;

				float totalDiff = 1 / (1 + (mp.x - lastmp.x) * speed) * 1 / (1 + (mp.y - lastmp.y) * speed);
				myScenePtr->GetMainCamera()->SetPosition(myScenePtr->GetMainCamera()->GetPosition() * totalDiff);
			}
			else if (aInputHandler.IsMouseDown(CommonUtilities::InputHandler::Mouse::Mouse_Left))
			{

				V3F pos = myScenePtr->GetMainCamera()->GetPosition();
				float length = pos.Length();
				pos += myScenePtr->GetMainCamera()->GetRight() * length * -static_cast<float>(mp.x - lastmp.x) * 0.001f;
				pos += myScenePtr->GetMainCamera()->GetUp() * length * static_cast<float>(mp.y - lastmp.y) * 0.001f;
				pos = pos.GetNormalized() * length;

				myScenePtr->GetMainCamera()->SetPosition(pos);
				myScenePtr->GetMainCamera()->LookAt(V3F(0, 0, 0));
			}
			else
			{
				lastmp = aInputHandler.GetMousePosition();
			}
			lastmp = mp;
		}
#pragma endregion
	}
	if (myIsInModelViewerMode)
	{
#ifdef  _DEBUG
		if (modelInstance && DebugTools::FileList)
		{
			static bool drawSkeleton = false;
			if (ImGui::Begin("Model settings"))
			{
				ImGui::Checkbox("Render Skeleton", &drawSkeleton);
				modelInstance->ImGuiNode(*DebugTools::FileList, myScenePtr->GetMainCamera());
				Model* model = modelInstance->GetModel();
				if (model)
				{
					ImGui::Separator();
					model->ImGuiNode(*DebugTools::FileList);
				}
			}
			ImGui::End();
			if (drawSkeleton)
			{
				DebugDrawer::GetInstance().DrawSkeleton(modelInstance);
			}
		}
#endif
		static V3F lightOffset(-67, -34, 79);
		ImGui::DragFloat3("Light arrow position", &lightOffset.x);
		if (myScenePtr->GetEnvironmentLight())
		{
			DebugDrawer::GetInstance().DrawDirection(
				myScenePtr->GetMainCamera()->GetPosition() +
				myScenePtr->GetMainCamera()->GetForward() * lightOffset.z +
				myScenePtr->GetMainCamera()->GetUp() * lightOffset.y +
				myScenePtr->GetMainCamera()->GetRight() * lightOffset.x,
				-myScenePtr->GetEnvironmentLight()->myDirection);
		}
	}
	else if (editParticles) {/*NO-OP*/ }
	else if (isRunningStandard)
#endif // !USEIMGUI
	{
#if USEIMGUI
		if (showBoundingBoxes)
		{
			CommonUtilities::AABB3D<float>* bb = myPlayer->GetComponent<Collision>()->GetBoxCollider();
			auto pos = bb->Min() + ((bb->Max() - bb->Min()) / 2.0f);
			DebugDrawer::GetInstance().DrawBoundingBox(*bb);
		}
#endif // USEIMGUI

		// COMPONENTS UPDATE ---------------------------------------------------------------------------------------------

		ComponentLake::GetInstance().UpdateComponents(aDeltatime);

		// OCTREE UPDATE -------------------------------------------------------------------------------------------------

		if (myObjectTree != nullptr)
		{
			myObjectTree->Update(aDeltatime);
		}


		DebugDrawer::GetInstance().SetColor(V4F(0.0f, 1.0f, 0.0f, 1.0f));
		for (auto& entity : myEntitys)
		{
			if (entity)
			{
				if (!entity->GetShouldUpdate())
				{
					continue;
				}

				entity->Update(aDeltatime);
			}
			else
			{
				SYSWARNING("entity was nullptr", "");
			}
		}

		DebugDrawer::GetInstance().SetColor(V4F(1.0f, 0.0f, 0.0f, 1.0f));

		CommonUtilities::AABB3D<float>* playerBBox = myPlayer->GetComponent<Collision>()->GetBoxCollider();

		for (auto& trigger : myTriggers)
		{
			Collision* col = trigger->GetComponent<Collision>();
			DebugDrawer::GetInstance().DrawBoundingBox(*col->GetBoxCollider());

			auto currentObjectBBox = col->GetBoxCollider();
			V3F objectPos = currentObjectBBox->Min() + ((currentObjectBBox->Max() - currentObjectBBox->Min()) / 2.0f);

			objectPos.x = CLAMP(playerBBox->Min().x, playerBBox->Max().x, objectPos.x);
			objectPos.y = CLAMP(playerBBox->Min().y, playerBBox->Max().y, objectPos.y);
			objectPos.z = CLAMP(playerBBox->Min().z, playerBBox->Max().z, objectPos.z);

			if (myFirstFadeInComplete)
			{
				if (currentObjectBBox->IsInside(objectPos))
				{
					myPlayer->GetComponent<Collision>()->OnCollide(col);
					col->OnCollide(myPlayer->GetComponent<Collision>());
					break;
				}
			}

		}

		for (auto& pickup : myPickups)
		{
			if (!pickup->GetComponent<Movement3D>()->GetIsLaunching())
			{
				Collision* col = pickup->GetComponent<Collision>();
				DebugDrawer::GetInstance().DrawBoundingBox(*col->GetBoxCollider());

				if (myPlayer->GetComponent<Collision>()->GetBoxCollider()->IsInside(pickup->GetPosition()))
				{
					myPlayer->GetComponent<Collision>()->OnCollide(col);
					col->OnCollide(myPlayer->GetComponent<Collision>());
				}
			}
		}
	}


	CommonUtilities::Vector3<float> movement = { 0.f, 0.f, 0.f };
	CommonUtilities::Vector3<float> rotation = { 0.f, 0.f, 0.f };

#ifndef _RETAIL
#if !DEMOSCENE
#if USEIMGUI
	if (myUsingFreeCamera || gameIsPaused)
#else
	if (myUsingFreeCamera)
#endif // !USEIMGUI
#endif
	{
		if (aInputHandler.IsKeyDown(aInputHandler.Key_W))
		{
			movement.z += myFreecamSpeed * aDeltatime;
		}
		if (aInputHandler.IsKeyDown(aInputHandler.Key_S))
		{
			movement.z -= myFreecamSpeed * aDeltatime;
		}

		if (aInputHandler.IsKeyDown(aInputHandler.Key_D))
		{
			movement.x += myFreecamSpeed * aDeltatime;
		}
		if (aInputHandler.IsKeyDown(aInputHandler.Key_A))
		{
			movement.x -= myFreecamSpeed * aDeltatime;
		}

		if (aInputHandler.IsKeyDown(aInputHandler.Key_Space))
		{
			movement.y += myFreecamSpeed * aDeltatime;
		}
		if (aInputHandler.IsKeyDown(aInputHandler.Key_Shift))
		{
			movement.y -= myFreecamSpeed * aDeltatime;
		}

		if (aInputHandler.IsKeyDown(aInputHandler.Key_Q))
		{
			rotation.y -= myFreecamRotationSpeed * aDeltatime;
		}
		if (aInputHandler.IsKeyDown(aInputHandler.Key_E))
		{
			rotation.y += myFreecamRotationSpeed * aDeltatime;
		}
		if (aInputHandler.IsKeyDown(aInputHandler.Key_Z))
		{
			rotation.x -= myFreecamRotationSpeed * aDeltatime;
		}
		if (aInputHandler.IsKeyDown(aInputHandler.Key_X))
		{
			rotation.x += myFreecamRotationSpeed * aDeltatime;
		}
		myMainCameraPtr->Move(movement);
		myMainCameraPtr->Rotate(rotation);
		}
#if USEIMGUI
	else if (!isRunningStandard)
	{
		if (aInputHandler.IsKeyDown(aInputHandler.Key_W))
		{
			movement.z += myFreecamSpeed * aDeltatime;
		}
		if (aInputHandler.IsKeyDown(aInputHandler.Key_S))
		{
			movement.z -= myFreecamSpeed * aDeltatime;
		}

		if (aInputHandler.IsKeyDown(aInputHandler.Key_D))
		{
			movement.x += myFreecamSpeed * aDeltatime;
		}
		if (aInputHandler.IsKeyDown(aInputHandler.Key_A))
		{
			movement.x -= myFreecamSpeed * aDeltatime;
		}

		if (aInputHandler.IsKeyDown(aInputHandler.Key_Space))
		{
			movement.y += myFreecamSpeed * aDeltatime;
		}
		if (aInputHandler.IsKeyDown(aInputHandler.Key_Shift))
		{
			movement.y -= myFreecamSpeed * aDeltatime;
		}

		V3F forward = myMainCameraPtr->GetForward();
		forward.y = 0;
		forward.Normalize();
		V3F right = forward.Cross(V3F(0, -1, 0));

		myMainCameraPtr->SetPosition(movement.z * forward + movement.x * right + movement.y * V3F(0, 1, 0) + myMainCameraPtr->GetPosition());
	}
#endif // USEIMGUI
#endif // !_RETAIL


	for (int index = CAST(int, myEntitys.size()) - 1; index >= 0; index--)
	{
		if (myEntitys[index]->GetShouldBeRemoved())
		{
			myEntitys[index]->Dispose();
			myEntityPool.Dispose(myEntitys[index]);
			myEntitys.erase(myEntitys.begin() + index);
		}
	}


	for (int index = CAST(int, myTriggers.size()) - 1; index >= 0; index--)
	{
		if (!myTriggers[index]->GetIsAlive())
		{
			myTriggers[index]->Dispose();
			myEntityPool.Dispose(myTriggers[index]);
			myTriggers.erase(myTriggers.begin() + index);
		}
	}

	for (int index = CAST(int, myEnemies.size()) - 1; index >= 0; index--)
	{
		if (myEnemies[index]->GetShouldBeRemoved())
		{
			myEnemies[index]->Dispose();
			myEntityPool.Dispose(myEnemies[index]);
			myEnemies.erase(myEnemies.begin() + index);
		}
	}

	for (int index = CAST(int, myPickups.size()) - 1; index >= 0; index--)
	{
		if (!myPickups[index]->GetIsAlive())
		{
			myPickups[index]->Dispose();
			myEntityPool.Dispose(myPickups[index]);
			myPickups.erase(myPickups.begin() + index);
		}
	}

	myUIManager->Update(aDeltatime);
	}