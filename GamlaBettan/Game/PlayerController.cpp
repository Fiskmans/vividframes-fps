#include "pch.h"
#include "PlayerController.h"
#include "Entity.h"
#include "Camera.h"
#include "Movement3D.h"
#include "SlabRay.h"
#include "AABB3D.hpp"
#include "Octree.h"
#include "Life.h"
#include "DebugDrawer.h"
#include "Audio.h"
#include "AbilityData.h"
#include "CharacterInstance.h"
#include "GBPhysXKinematicComponent.h"
#include "FPSCamera.h"
#include "FireWeapon.h"
#include "GBPhysX.h"
#include "AnimationComponent.h"

#include "ParticleActivatable.h"

#include <PathFinder.h>
#if _DEBUG
#include <DebugTools.h>
#include <DebugDrawer.h>
#endif // _DEBUG


#pragma warning(push, 1)
#pragma warning(disable : 26451 26812)
#include <rapidjson/document.h>
#include <rapidjson/filereadstream.h>
#pragma warning(pop)

#define MARKERUPTIME .5f


PlayerController::PlayerController() :
	myOctree(nullptr),
	myMouseRay(nullptr),
	myCurrentMousedOverEntity(nullptr),
	myCurrentMousePos(V2F(0.5f, 0.5f)),
	myFPSMode(false),
	myIsAimDownSight(false)
{
	myMouseMovedThisFrame = false;
}

PlayerController::~PlayerController()
{
	UnSubscribeToMessage(MessageType::InputRightMouseHit);
	UnSubscribeToMessage(MessageType::InputLeftMouseHit);
	UnSubscribeToMessage(MessageType::InputLeftMouseDown);
	UnSubscribeToMessage(MessageType::InputMouseMoved);
	UnSubscribeToMessage(MessageType::InputMiddleMouseDown);
	UnSubscribeToMessage(MessageType::InputLeftMouseReleased);
	UnSubscribeToMessage(MessageType::InputMiddleMouseHit);
	UnSubscribeToMessage(MessageType::WindowResize);
	UnSubscribeToMessage(MessageType::NewOctreeCreated);

	UnSubscribeToMessage(MessageType::InputUpHit);
	UnSubscribeToMessage(MessageType::InputUpIsDown);
	UnSubscribeToMessage(MessageType::InputUpReleased);
	UnSubscribeToMessage(MessageType::InputDownHit);
	UnSubscribeToMessage(MessageType::InputDownIsDown);
	UnSubscribeToMessage(MessageType::InputDownReleased);
	UnSubscribeToMessage(MessageType::InputRightHit);
	UnSubscribeToMessage(MessageType::InputRightIsDown);
	UnSubscribeToMessage(MessageType::InputRightReleased);
	UnSubscribeToMessage(MessageType::InputLeftHit);
	UnSubscribeToMessage(MessageType::InputLeftIsDown);
	UnSubscribeToMessage(MessageType::InputLeftReleased);

	UnSubscribeToMessage(MessageType::InputFPSMode);

	UnSubscribeToMessage(MessageType::InputReload);

	UnSubscribeToMessage(MessageType::InputJumpHit);
	UnSubscribeToMessage(MessageType::InputCrouchHit);
	UnSubscribeToMessage(MessageType::InputCrouchReleased);
	UnSubscribeToMessage(MessageType::InputRunHit);
	UnSubscribeToMessage(MessageType::InputRunReleased);

	SAFE_DELETE(myMouseRay);

	WIPE(*this);
}

void PlayerController::Init(Entity* aEntity)
{
	myEntity = aEntity;
	SYSERROR("Playercontroller Inited with Init with no inventoryPtr, inventory wont scale", "");
}

void PlayerController::InternalInit(GBPhysX* aGBPhysX)
{
	myGBPhysX = aGBPhysX;

	CommonUtilities::Vector3<float> normal = { 0.0f, 1.0f, 0.0f };
	myMapPlane.InitWithPointAndNormal(CommonUtilities::Vector3<float>(0.0f, -0.5f, 0.0f), normal);

	SubscribeToMessage(MessageType::WindowResize);
	SubscribeToMessage(MessageType::NewOctreeCreated);

	ResetControlerInput();

	myInputMovementState = InputMovementState::Idle;
	myInputWeaponState = InputWeaponState::Idle;

	myMouseRay = new SlabRay(V3F(0.0f, 0.0f, 0.0f), V3F(0.0f, 0.0f, 0.0f));

	/*Message message;
	message.myMessageType = MessageType::InputFPSMode;
	message.aBool = true;
	SendMessages(message);*/

	myEntity->GetComponent<FPSCamera>()->SetTargetFOV(700.f, 80.f);
	myEntity->GetComponent<FPSCamera>()->SetOffset(V3F(0, myCameraOffset, 0));
	myEntity->GetComponent<Mesh>()->SetOffsetPosition(V3F(0, myCameraOffset, 0));

	SubscribeToMessage(MessageType::InputLeftMouseHit);
	SubscribeToMessage(MessageType::InputLeftMouseDown);
	SubscribeToMessage(MessageType::InputLeftMouseReleased);
	SubscribeToMessage(MessageType::InputRightMouseHit);
	SubscribeToMessage(MessageType::InputRightMouseReleased);
	SubscribeToMessage(MessageType::InputMouseMoved);
	SubscribeToMessage(MessageType::InputMouseStopedMoving);
	SubscribeToMessage(MessageType::InputMiddleMouseDown);
	SubscribeToMessage(MessageType::InputMiddleMouseHit);
	SubscribeToMessage(MessageType::FadeInComplete);

	SubscribeToMessage(MessageType::InputReload);

	SubscribeToMessage(MessageType::InputUpHit);
	SubscribeToMessage(MessageType::InputUpIsDown);
	SubscribeToMessage(MessageType::InputUpReleased);
	SubscribeToMessage(MessageType::InputDownHit);
	SubscribeToMessage(MessageType::InputDownIsDown);
	SubscribeToMessage(MessageType::InputDownReleased);
	SubscribeToMessage(MessageType::InputRightHit);
	SubscribeToMessage(MessageType::InputRightIsDown);
	SubscribeToMessage(MessageType::InputRightReleased);
	SubscribeToMessage(MessageType::InputLeftHit);
	SubscribeToMessage(MessageType::InputLeftIsDown);
	SubscribeToMessage(MessageType::InputLeftReleased);
	SubscribeToMessage(MessageType::InputCrouchHit);
	SubscribeToMessage(MessageType::InputCrouchReleased);
	SubscribeToMessage(MessageType::InputRunHit);
	SubscribeToMessage(MessageType::InputRunReleased);
	SubscribeToMessage(MessageType::InputJumpHit);
	SubscribeToMessage(MessageType::InputFPSMode);
}

void PlayerController::RecieveEntityMessage(EntityMessage aMessage, void* someData)
{
	if (aMessage == EntityMessage::ReloadAnimationFinnished)
	{
		myEntity->GetComponent<FireWeapon>()->Reload();
		myInputWeaponState = InputWeaponState::Idle;
	}
	else if (aMessage == EntityMessage::FireAnimationFinnished)
	{
		//myInputWeaponState = InputWeaponState::Idle;
	}
	else if (aMessage == EntityMessage::ADSInAnimationFinnished)
	{
		myInputWeaponState = InputWeaponState::AimIdle;
	}
	else if (aMessage == EntityMessage::ADSOutAnimationFinnished)
	{
		myInputWeaponState = InputWeaponState::Idle;
	}
}

void PlayerController::Update(const float aDeltaTime)
{
	if (myInputIsBlocked)
	{
		return;
	}

	//Ray shooting trace
	*myMouseRay = GetRayFromMousePos(V2F(myCurrentMousePos.x, myCurrentMousePos.y));
#if _DEBUG
	DebugTools::LastKnownMouseRay = myMouseRay;
#endif // _DEBUG

#if KILLWHENOUTOFBOUNDS
	Life* life = myEntity->GetComponent<Life>();
	if (life)
	{
		if (myEntity->GetPosition().y < -10000)
		{
			life->TakeDamage(10);
		}
	}
#endif // KILLWHENOUTOFBOUNDS


	CheckMouseRayVSOctree(*myMouseRay);

	//Movement
	if (myInputMovement.x != 0 || myInputMovement.y != 0 || myInputMovement.z != 0)
	{
		myInputMovement.Normalize();
		myInputMovement.y = 0;
		myEntity->GetComponent<GBPhysXKinematicComponent>()->SetTargetDirection(myInputMovement);
	}
	else
	{
		myEntity->GetComponent<GBPhysXKinematicComponent>()->StopMoving();
	}

	//Aiming
	//V3F offsetPos = {0,0,0};
	V3F targetOffsetPos = { 0,0,0 };
	V3F targetOffsetRot = { 0,0,0 };

	if (myMouseMovedThisFrame && myFPSMode)
	{
		float aimSensitivityModifyer = 1.f;
		if (myIsAimDownSight)
		{
			aimSensitivityModifyer = 0.3f;
		}


		myMouseMovement *= aDeltaTime * (mySensitivity * aimSensitivityModifyer);

		myEntity->GetComponent<FPSCamera>()->RotateCameraFromMouse(myMouseMovement);
		myEntity->SetRotation(myEntity->GetComponent<FPSCamera>()->GetCamera()->GetTransform());
		myMouseMovedThisFrame = false;

		//Smooth weapon movement (Kanske flytta n�gon annan stans)

		targetOffsetPos = myEntity->GetComponent<FPSCamera>()->GetCamera()->GetRight() * (50.f * -myMouseMovement.x);
		targetOffsetPos += myEntity->GetComponent<FPSCamera>()->GetCamera()->GetUp() * (50.f * myMouseMovement.y);

		targetOffsetRot.y = LERP((myMouseMovement.x * aDeltaTime * 10.f), targetOffsetRot.y, 0.5f);
		targetOffsetRot.x = LERP((myMouseMovement.y * aDeltaTime * 10.f), targetOffsetRot.x, 0.5f);


		//--------------
	}

	myCurrentMeshPosOffset = LERP(myCurrentMeshPosOffset, targetOffsetPos, 0.1f);
	myCurrentMeshRotOffset = LERP(myCurrentMeshRotOffset, targetOffsetRot, 0.1f);

	myEntity->GetComponent<Mesh>()->SetSecondOffsetPosition(myCurrentMeshPosOffset);
	myEntity->GetComponent<Mesh>()->SetOffsetRotation(targetOffsetRot);


	myEntity->SetPosition(myEntity->GetPosition() + V3F(0, myCameraOffset, 0));

	FireInfo fireInfo;
	if (myEntity->GetComponent<FireWeapon>()->Fire(fireInfo))
	{
		//TODO: Stuff from fire to use for bulletRaycast;
		//fireInfo.BulletDirectionOffset;
		//fireInfo.bulletsShot;
		//fireInfo.recoilVelocity;

		myWeaponFiredThisFrame = true;

		BulletHitReport hitReport = myGBPhysX->RayPickActor(myMouseRay->orig, myMouseRay->dir);
		bool shouldSpawnBulletHole = true;

		Message mess;
		mess.myData = &hitReport;
		mess.myMessageType = MessageType::SpawnBulletHitParticle;

		if (hitReport.actor)
		{
			SYSINFO(hitReport.actor->GetName());
			Entity* hitEntity = hitReport.actor->GetEntity();
			if (hitEntity)
			{
				if (hitEntity->GetEntityType() == EntityType::Enemy)
				{
					Life* targetLife = hitEntity->GetComponent<Life>();
					V3F dir = hitEntity->GetPosition() - myEntity->GetPosition();
					dir.Normalize();
					targetLife->TakeDamage(5, dir);
					shouldSpawnBulletHole = false;
				}
				SYSINFO(std::to_string(hitEntity->GetEntityID()));
				mess.myBool = true;
			}
		}
		SendMessages(mess);

		DebugDrawer::GetInstance().DrawCross(hitReport.position, 50.0f);
		DebugDrawer::GetInstance().DrawDirection(hitReport.position, hitReport.normal, 10.0f);
		SYSINFO("hit location: x: " + std::to_string(hitReport.position.x) + " y: " + std::to_string(hitReport.position.y) + " z: " + std::to_string(hitReport.position.z));
		SYSINFO("hit normal: x: " + std::to_string(hitReport.normal.x) + " y: " + std::to_string(hitReport.normal.y) + " z: " + std::to_string(hitReport.normal.z));

		mess = Message();

		mess.myData = &hitReport;
		mess.myMessageType = MessageType::BulletHit;
		PostMaster::GetInstance()->SendMessages(mess);


		if (shouldSpawnBulletHole)
		{
			Message bulletHitMess;
			bulletHitMess.myData = &hitReport;
			bulletHitMess.myMessageType = MessageType::BulletHit;
			PostMaster::GetInstance()->SendMessages(bulletHitMess);
		}
	}
	else
	{
		myWeaponFiredThisFrame = false;
	}

	HandleInputStates();
}

void PlayerController::Reset()
{
	UnSubscribeToMessage(MessageType::InputRightMouseHit);
	UnSubscribeToMessage(MessageType::InputLeftMouseHit);
	UnSubscribeToMessage(MessageType::InputLeftMouseDown);
	UnSubscribeToMessage(MessageType::InputMouseMoved);
	UnSubscribeToMessage(MessageType::InputMouseStopedMoving);
	UnSubscribeToMessage(MessageType::InputMiddleMouseDown);
	UnSubscribeToMessage(MessageType::InputMiddleMouseHit);
	UnSubscribeToMessage(MessageType::WindowResize);
	UnSubscribeToMessage(MessageType::NewOctreeCreated);
	UnSubscribeToMessage(MessageType::FadeInComplete);
}

void PlayerController::ResetControlerInput()
{
	myAimButtonIsDown = false;
	myFireButtonIsDown = false;
	myRunButtonIsDown = false;
}

void PlayerController::RecieveMessage(const Message& aMessage)
{
	if (myInputIsBlocked)
		return;

	switch (aMessage.myMessageType)
	{
	case MessageType::FadeInComplete:
	{
		myCanMove = true;
		break;
	}
	case MessageType::InputMouseMoved:
	{
		myMouseMovedThisFrame = true;

		myCurrentMousePos = { 0.5f, 0.5f };
		myMouseMovement.x = aMessage.myFloatValue - myCurrentMousePos.x;
		myMouseMovement.y = aMessage.myFloatValue2 - myCurrentMousePos.y;

#if _DEBUG
		* myMouseRay = GetRayFromMousePos(V2F(myCurrentMousePos.x, myCurrentMousePos.y));
		DebugTools::LastKnownMouseRay = myMouseRay;
#endif

		break;
	}
	case MessageType::InputMouseStopedMoving:
	{
		myMouseMovement = { 0.f, 0.f };
		myMouseMovedThisFrame = false;
		break;
	}
	case MessageType::InputLeftMouseDown:
	{
		break;
	}
	case MessageType::InputLeftMouseReleased:
	{
		if (myFPSMode)
		{
			myEntity->GetComponent<FireWeapon>()->ReleaseTrigger();
			myFireButtonIsDown = false;
		}
		break;
	}
	case MessageType::InputLeftMouseHit:
	{
		if (myFPSMode)
		{
			myEntity->GetComponent<FireWeapon>()->PressTrigger();
			myFireButtonIsDown = true;
		}
		break;
	}
	case MessageType::InputReload:
	{
		if (myEntity->GetComponent<FireWeapon>()->CanReload())
		{
			myInputWeaponState = InputWeaponState::Reloading;
		}
		else
		{
			//out of ammo or full mag
			std::cout << "out of ammo or full mag" << std::endl;
		}
		break;
	}
	case MessageType::InputRightMouseHit:
	{
		myEntity->GetComponent<FPSCamera>()->ZoomIn();
		myIsAimDownSight = true;
		break;
	}
	case MessageType::InputRightMouseReleased:
	{
		myEntity->GetComponent<FPSCamera>()->ZoomOut();
		myIsAimDownSight = false;
		myInputWeaponState = InputWeaponState::AimOut;
		break;
	}
	case MessageType::InputJumpHit:
	{
		myEntity->GetComponent<GBPhysXKinematicComponent>()->Jump();
		break;
	}
	case MessageType::InputUpIsDown:
	{
		myInputMovement += myEntity->GetComponent<FPSCamera>()->GetCamera()->GetFlatForward();
		break;
	}
	case MessageType::InputDownIsDown:
	{
		myInputMovement -= myEntity->GetComponent<FPSCamera>()->GetCamera()->GetFlatForward();
		break;
	}
	case MessageType::InputRightIsDown:
	{
		V3F right = myEntity->GetComponent<FPSCamera>()->GetCamera()->GetRight();
		right.y = 0;
		myInputMovement += right;
		break;
	}
	case MessageType::InputLeftIsDown:
	{
		V3F right = myEntity->GetComponent<FPSCamera>()->GetCamera()->GetRight();
		right.y = 0;
		myInputMovement -= right;
		break;
	}
	case MessageType::InputFPSMode:
	{
		myFPSMode = aMessage.myBool;
		break;
	}
	case MessageType::WindowResize:
	{
		break;
	}
	case MessageType::NewOctreeCreated:
	{
		myOctree = CAST(Octree*, aMessage.myData);
		break;
	}
	default:
		break;
	}

	if (aMessage.myMessageType == MessageType::InputJumpHit)
	{
		myEntity->GetComponent<GBPhysXKinematicComponent>()->Jump();
	}
	if (aMessage.myMessageType == MessageType::InputRunHit)
	{
		myEntity->GetComponent<GBPhysXKinematicComponent>()->Run();
		myRunButtonIsDown = true;
	}
	if (aMessage.myMessageType == MessageType::InputRunReleased)
	{
		myEntity->GetComponent<GBPhysXKinematicComponent>()->Walk();
		myRunButtonIsDown = false;
	}
	if (aMessage.myMessageType == MessageType::InputCrouchHit)
	{
		myEntity->GetComponent<GBPhysXKinematicComponent>()->Crouch();
	}
	if (aMessage.myMessageType == MessageType::InputCrouchReleased)
	{
		myEntity->GetComponent<GBPhysXKinematicComponent>()->Walk();
	}
}



void PlayerController::OnAttach()
{
}

void PlayerController::OnDetach()
{
}

void PlayerController::OnKillMe()
{
}

void PlayerController::HandleInputStates()
{
	if (myWeaponFiredThisFrame)
	{
		if (myIsAimDownSight)
		{
			myInputWeaponState = InputWeaponState::AimFire;
		}
		else
		{
			myInputWeaponState = InputWeaponState::HipFire;
		}
	}
	else if (myInputWeaponState != InputWeaponState::Reloading)
	{
		if (!myFireButtonIsDown || (myFireButtonIsDown && myEntity->GetComponent<FireWeapon>()->GetCurrentClipSize() == 0))
		{
			if (myIsAimDownSight)
			{
				if (myInputWeaponState != InputWeaponState::AimIdle)
				{
					myInputWeaponState = InputWeaponState::AimIn;
				}
			}
			else if (myInputWeaponState != InputWeaponState::AimOut)
			{
				myInputWeaponState = InputWeaponState::Idle;
			}
		}
	}

	if (myInputMovement.LengthSqr() > 0)
	{
		if (myRunButtonIsDown)
		{
			myInputMovementState = InputMovementState::Running;
		}
		else
		{
			myInputMovementState = InputMovementState::Walking;
		}
	}
	else
	{
		myInputMovementState = InputMovementState::Idle;
	}
	myInputMovement = V3F(0, 0, 0);



	if (myRunButtonIsDown && myFireButtonIsDown)
	{
		myEntity->GetComponent<GBPhysXKinematicComponent>()->Walk();
		myInputMovementState = InputMovementState::Walking;
		myRunButtonIsDown = false;
	}

	if (myInputWeaponState == InputWeaponState::HipFire)
	{
		myEntity->GetComponent<AnimationComponent>()->SetState(AnimationComponent::States::FireWeapon);
		//std::cout << "Start Fire anim" << std::endl;
	}
	else if (myInputWeaponState == InputWeaponState::AimFire)
	{
		//myEntity->GetComponent<AnimationComponent>()->SetState(AnimationComponent::States::ADSIdle);
	}
	else if (myInputWeaponState == InputWeaponState::Reloading)
	{
		myEntity->GetComponent<AnimationComponent>()->SetState(AnimationComponent::States::ReloadFull);
		//std::cout << "Start Reload anim" << std::endl;
	}
	else if (myInputMovementState == InputMovementState::Running && !myFireButtonIsDown)
	{
		myEntity->GetComponent<AnimationComponent>()->SetState(AnimationComponent::States::Running);
	}
	else if (myInputWeaponState == InputWeaponState::AimIn)
	{
		myEntity->GetComponent<AnimationComponent>()->SetState(AnimationComponent::States::ADSIn);
	}
	else if (myInputWeaponState == InputWeaponState::AimOut)
	{
		myEntity->GetComponent<AnimationComponent>()->SetState(AnimationComponent::States::ADSOut);
	}
	else if (myInputWeaponState == InputWeaponState::AimIdle)
	{
		myEntity->GetComponent<AnimationComponent>()->SetState(AnimationComponent::States::ADSIdle, false, true);
	}
	else if (myInputMovementState == InputMovementState::Walking)
	{
		myEntity->GetComponent<AnimationComponent>()->SetState(AnimationComponent::States::Walking);
	}
	else if (myInputMovementState == InputMovementState::Idle && myInputWeaponState == InputWeaponState::Idle)
	{
		myEntity->GetComponent<AnimationComponent>()->SetState(AnimationComponent::States::Idle);
	}
}

V3F PlayerController::Unproject(const V3F& aViewportPoint)
{
	auto view = myEntity->GetComponent<FPSCamera>()->GetCamera()->GetTransform();
	auto invViev = CommonUtilities::Matrix4x4<float>::GetFastInverse(view);

	auto invProjectionMatrix = CommonUtilities::Matrix4x4<float>::GetRealInverse(CommonUtilities::Matrix4x4<float>::GetFastInverse(myEntity->GetComponent<FPSCamera>()->GetCamera()->GetTransform()) * myEntity->GetComponent<FPSCamera>()->GetCamera()->GetProjection(false));
	V4F result =
	{
		(aViewportPoint.x - .5f) * 2.f,
		(aViewportPoint.y - .5f) * -2.f,
		aViewportPoint.z,
		1
	};

	result = result * invProjectionMatrix;

	result /= result.w;

	return V3F(result.x, result.y, result.z);
}

SlabRay PlayerController::GetRayFromMousePos(V2F aMousePos)
{
	//V3F rayA = Unproject({ aMousePos.x, aMousePos.y, 0.f });
	V3F rayA = Unproject({ 0.5f, 0.5f, 0.f });
	//V3F rayB = Unproject({ aMousePos.x, aMousePos.y, 1.f });
	V3F rayB = Unproject({ 0.5f, 0.5f, 1.f });

	SlabRay ray = SlabRay(V4F(rayA, 1.0f), V4F(rayB, 1.0f));

	return ray;

}

void PlayerController::CheckMouseRayVSOctree(SlabRay& r)
{
	if (myOctree != nullptr)
	{
		Entity* pickedEntity = myOctree->RayPickEntity(r);
		if (myCurrentMousedOverEntity && myCurrentMousedOverEntity->GetComponent<Mesh>())
		{
			auto* modl = myCurrentMousedOverEntity->GetComponent<Mesh>()->GetModelInstance();
			if (modl)
			{
				modl->SetIsHighlighted(false);
			}
		}

		if (pickedEntity != nullptr)
		{
			if (pickedEntity->GetComponent<Mesh>())
			{
				auto* modl = pickedEntity->GetComponent<Mesh>()->GetModelInstance();
				if (modl)
				{
					modl->SetIsHighlighted(true);
				}
			}

			myCurrentMousedOverEntity = pickedEntity;
			if (pickedEntity->GetComponent<Life>())
			{
				Message message;
				message.myMessageType = MessageType::EnemyMouseHover;
				message.myFloatValue = pickedEntity->GetComponent<Life>()->GetLifePercentage();
				message.myData = pickedEntity;
				SendMessages(message);
			}
			else
			{
				//IS CONTAINER OR SOMETHING ELSE OTHER THAN ENEMY
			}
			return;
		}
		else
		{
			Message message;
			message.myMessageType = MessageType::EnemyMouseHoverReleased;
			SendMessages(message);
		}
	}

	myCurrentMousedOverEntity = nullptr;
}

V3F PlayerController::GetRayVsMapPoint(SlabRay& aRay)
{
	if (myEntity->GetComponent<FPSCamera>()->GetCamera())
	{
		CommonUtilities::Plane<float> plane(myEntity->GetPosition(), V3F(0, 1, 0));
		float tvoid;
		return myMouseRay->FindIntersection(plane, tvoid);
	}
	return V3F();
}