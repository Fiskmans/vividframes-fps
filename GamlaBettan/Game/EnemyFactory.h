#pragma once
#include "Observer.hpp"
#include "Vector3.hpp"
#include <vector>
#include "ObjectPoolSizes.h"
#include "Spline.h"
#include <imgui.h>
#include "MetricValue.h"
#include "EmilsEnums.h"

namespace CommonUtilities
{
	template <typename Type>
	class ObjectPool;
}
class Octree;
class Entity;
class ComponentLake;
class CharacterData;
class AbilityData;
class AIPollingStation;
class GBPhysX;

struct EnemyMetrics
{
	EnemyType myEnemyType = EnemyType::None;
	unsigned int myHP = 1;
	float mySpeed = 0.0f;
	float myWeaponCooldown = 0.0f;
	float myCollisionRadius = 1.0f;
	float myProjectileDamage = 0.0f;
	float myProjectileSpeed = 0.0f;
};

class EnemyFactory : public Observer
{
public:

	EnemyFactory();
	~EnemyFactory();

	void Init(AIPollingStation* aAIPollingStation, Octree* aOctTree, std::vector<Entity*>* aEntityVector, CommonUtilities::ObjectPool<Entity>* aEntityPool, ComponentLake* aComponentLake, unsigned int* aEntityIDInt, CharacterData* aCharData, GBPhysX* aGBPhysXPtr);
	Entity* CreateEnemy(std::string aFilePath, int aEnemyID, CommonUtilities::Vector3<float> aPos, CommonUtilities::Vector3<float> aRot, CommonUtilities::Vector3<float> aScale, bool aIsSpawnedInRuntime = false);

	void SpawnQueued(const float aDeltaTime);

	void QueueSpawn(int aCharID, int aNumberOfEnemiesToSpawn, float timeIntervalMin, float timeIntervalMax, const V3F& aPosition, const V3F & aTargetPos, bool aTargetPlayer = false, bool aSpawnFalling = false);

	void ClearQueue();

private:
	CharacterData* myCharacterData;

	float GetFloatMetricValueFromLine(const std::string& aLine);
	int GetIntMetricValueFromLine(const std::string& aLine);

	virtual void RecieveMessage(const Message& aMessage) override;
	
	Octree* myOctree;
	Entity* myEnemyTarget;
	std::vector<Entity*>* myEntityVector;
	CommonUtilities::ObjectPool<Entity>* myEntityPoolPtr;
	unsigned int* myEntityIDInt;
	ComponentLake* myComponentLake;
	GBPhysX* myGBPhysX = nullptr;

	std::map<std::string, std::vector<Entity*>*> myLevelObjectMapping;
	std::vector<Entity*>* myCurrentLevel;

	AIPollingStation* myAIPollingStation;

	struct QueuedSpawn
	{
		int typeID;
		int numberOfEnemiesToSpawn;
		int numberOfEnemiesSpawned;
		float timeIntervalMin;
		float timeIntervalMax;
		float currentTimeInterval;
		V3F position;
		V3F targetPos;
		bool targetPlayer;
		bool spawnFalling;
	};

	std::vector<QueuedSpawn> myQueue;
};

