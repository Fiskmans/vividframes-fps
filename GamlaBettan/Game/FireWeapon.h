#pragma once
#include "Component.h"

enum class TriggerStatus
{
	Pressed,
	Down,
	Released,
};

struct FireInfo
{
	V3F recoilVelocity;
	int bulletsShot;
	std::vector<V2F> BulletDirectionOffset;
};

class FireWeapon :
	public Component
{
public:
	FireWeapon();
	virtual ~FireWeapon();

	virtual void Init(Entity* aEntity) override;
	virtual void Update(const float aDeltaTime) override;
	virtual void Reset() override;

	void SetWeaponProperties();

	void PressTrigger();
	void ReleaseTrigger();

	bool Fire(FireInfo& aFireInfo);
	bool Reload();
	bool CanReload();

	int GetCurrentClipSize();
	int GetTotalAmmo();

protected:
	virtual void OnAttach() override;
	virtual void OnDetach() override;
	virtual void OnKillMe() override;

private:

	int InternalFire();

	TriggerStatus myTrigger;

	float myFireRateTimer;
	float myFirerate;

	bool myIsAutomatic;

	int myClipSize;
	int myTotalAmmoLimit;

	unsigned int myCurrentClipSize;
	unsigned int myTotalAmmoAmount;
	
	//Beh�vs bara om man har shotgun
	int myBulletsPerShot;

	GAMEMETRIC(bool, myTotalAmmoIsUnlimited, UNLIMITEDAMMO, true);
	GAMEMETRIC(bool, myClipIsUnlimited, UNLIMITEDCLIP, false);
	float myFireTimer;
};
