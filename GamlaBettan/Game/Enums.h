#pragma once

enum class EntityType
{
	Player,
	Enemy,
	Projectile,
	EnvironmentStatic,
	EnvironmentDynamic,
	EnvironmentDestructible,
	TriggerBox,
	Pickups,
	BreakableContainer,
	UnbreakableContainer,
	LevelGate,
	Ability,
	Camera,
	Count,
	None
};