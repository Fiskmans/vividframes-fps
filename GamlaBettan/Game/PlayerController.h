#pragma once
#include "Component.h"
#include "Vector.hpp"
#include "Observer.hpp"
#include "Publisher.hpp"
#include "Plane.hpp"

class Entity;
class Camera;
class SlabRay;
class Octree;
class GBPhysX;

enum class InputMovementState
{
	Idle,
	Walking,
	Running,
};

enum class InputWeaponState
{
	Idle,
	HipFire,
	AimIn,
	AimIdle,
	AimOut,
	AimFire,
	Reloading
};

class PlayerController : public Component, public Observer, public Publisher
{
public:

	PlayerController();
	~PlayerController();

	virtual void Init(Entity* aEntity) override;
	void InternalInit(GBPhysX* aGBPhysX);
	virtual void Update(const float aDeltaTime) override;
	virtual void Reset() override;

	void ResetControlerInput();

	void RecieveMessage(const Message& aMessage) override;
	//void SetCamera(Camera* aCamera, Entity* aCameraEntity);

	virtual void RecieveEntityMessage(EntityMessage aMessage, void* someData) override;

	void SetInputBlock(bool aInputBlocked)
	{
		myInputIsBlocked = aInputBlocked;
	}

protected:
	virtual void OnAttach() override;
	virtual void OnDetach() override;
	virtual void OnKillMe() override;

private:

	void HandleInputStates();

	bool myCanMove = false;

	V3F Unproject(const V3F& aViewportPoint);
	SlabRay GetRayFromMousePos(V2F aMousePos);
	void CheckMouseRayVSOctree(SlabRay& r);
	V3F GetRayVsMapPoint(SlabRay& aRay);
	CommonUtilities::Plane<float> myMapPlane;

	//Camera* myCameraPtr;
	Octree* myOctree;
	V2F myCurrentMousePos;
	SlabRay* myMouseRay;
	Entity* myCurrentMousedOverEntity;
	GBPhysX* myGBPhysX;

	bool myAimButtonIsDown;
	bool myFireButtonIsDown;
	bool myRunButtonIsDown = false;
	bool myInputIsBlocked = false;
	bool myWeaponFiredThisFrame = false;

	V3F myInputMovement;
	V3F myMouseMovement;
	V3F myLastMouseMovement;
	V3F myCurrentMeshPosOffset;
	V3F myCurrentMeshRotOffset;

	bool myMouseMovedThisFrame;

	bool myFPSMode;

	bool myIsAimDownSight;

	InputMovementState myInputMovementState;
	InputWeaponState myInputWeaponState;

	GAMEMETRIC(float, mySensitivity, SENSITIVITY, 90.0f);
	GAMEMETRIC(float, myCameraOffset, CAMERAHEIGHT, 50.f);
};