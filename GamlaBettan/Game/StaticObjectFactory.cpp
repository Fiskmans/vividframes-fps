#include "pch.h"
#include "StaticObjectFactory.h"
#include "Entity.h"
#include "DataStructs.h"
#include "ObjectPool.hpp"
#include "ComponentLake.h"
#include "Mesh.h"
#include "Collision.h"
#include "GBPhysX.h"
#include "GBPhysXStaticComponent.h"

StaticObjectFactory::StaticObjectFactory() :
	myOctree(nullptr),
	myEntityPool(nullptr),
	myEntityIDInt(nullptr),
	myEntityVector(nullptr)
{
}

StaticObjectFactory::~StaticObjectFactory()
{
	UnSubscribeToMessage(MessageType::SpawnStaticObject);
	UnSubscribeToMessage(MessageType::StartLoading);
	UnSubscribeToMessage(MessageType::UnloadLevel);
}

void StaticObjectFactory::Init(GBPhysX* aGBPhysX, Octree* aOctTree, std::vector<Entity*>* aEntityVector, CommonUtilities::ObjectPool<Entity>* aEntityPool, ComponentLake* aComponentLake, unsigned int* aEntityIDInt)
{
	SubscribeToMessage(MessageType::SpawnStaticObject);
	SubscribeToMessage(MessageType::StartLoading);
	SubscribeToMessage(MessageType::UnloadLevel);

	myGBPhysX = aGBPhysX;
	myOctree = aOctTree;
	myEntityVector = aEntityVector;
	myEntityPool = aEntityPool;
	myEntityIDInt = aEntityIDInt;
}

void StaticObjectFactory::CreateStaticObject(std::string aFilePath, CommonUtilities::Vector3<float> aPos, CommonUtilities::Vector3<float> aRot, CommonUtilities::Vector3<float> aScale)
{
	const float degToRad = 57.2957f;
	Entity* entity = myEntityPool->Retrieve();

	if (entity)
	{
		entity->Init(&ComponentLake::GetInstance(), EntityType::EnvironmentStatic, *myEntityIDInt);
		*myEntityIDInt = *myEntityIDInt + 1;
		entity->AddComponent<Mesh>()->Init(entity);
		entity->GetComponent<Mesh>()->SetUpModel(aFilePath);
		entity->GetComponent<Mesh>()->GetModelInstance()->Rotate(CommonUtilities::Vector3<float>(aRot.x / degToRad, aRot.y / degToRad, aRot.z / degToRad));
		entity->GetComponent<Mesh>()->SetScale(aScale);
		entity->SetScale(aScale);
		entity->Spawn(aPos);
		entity->SetRotation(CommonUtilities::Vector3<float>(aRot.x / degToRad, aRot.y / degToRad, aRot.z / degToRad));
		entity->SetSavedRotationValues(CommonUtilities::Vector3<float>(aRot.x / degToRad, aRot.y / degToRad, aRot.z / degToRad));
		entity->SetShouldUpdate(false);
		entity->GetComponent<Mesh>()->Disable();

		entity->AddComponent<GBPhysXStaticComponent>()->Init(entity);
		entity->GetComponent<GBPhysXStaticComponent>()->SetGBPhysXPtr(myGBPhysX);
		entity->GetComponent<GBPhysXStaticComponent>()->SetFilePath(aFilePath);

		myEntityVector->push_back(entity);
		if (myCurrentLevel)
		{
			myCurrentLevel->push_back(entity);
		}
		else
		{
			SYSWARNING("Static object was loaded outside of levelstreaming, it wont be cleaned up properly", aFilePath);
		}
	}
	else
	{
		SYSWARNING("entity retrieved from entitypool was nullptr", aFilePath);
	}

}

void StaticObjectFactory::RecieveMessage(const Message& aMessage)
{

	switch (aMessage.myMessageType)
	{
	case MessageType::StartLoading:
	{
		myCurrentLevel = new std::vector<Entity*>();
		myLevelObjectMapping[aMessage.myText.data()] = myCurrentLevel;
	}
		break;
	case MessageType::UnloadLevel:
	{
		if (myLevelObjectMapping.count(aMessage.myText.data()) != 0)
		{
			auto& toRemove = *myLevelObjectMapping[aMessage.myText.data()];
			for (auto& ent : toRemove)
			{
				for (int i = int(myEntityVector->size()) - 1; i >= 0; --i)
				{
					if (myEntityVector->operator[](i) == ent)
					{
						ent->Dispose();
						myEntityPool->Dispose(ent);
						myEntityVector->erase(myEntityVector->begin() + i);
					}
				}
			}
			delete myLevelObjectMapping[aMessage.myText.data()];
			myLevelObjectMapping.erase(aMessage.myText.data());
		}
		else
		{
			SYSWARNING("Unloading level thats not loaded", aMessage.myText.data());
		}
	}
		break;
	case MessageType::SpawnStaticObject:
	{

		StaticObjectInstanceBuffer* buffer = static_cast<StaticObjectInstanceBuffer*>(aMessage.myData);
		CommonUtilities::Vector3<float> pos = { buffer->position[0],buffer->position[1], buffer->position[2] };
		CommonUtilities::Vector3<float> rot = { buffer->rotation[0],buffer->rotation[1], buffer->rotation[2] };
		CommonUtilities::Vector3<float> scale = { buffer->scale[0],buffer->scale[1], buffer->scale[2] };


		CreateStaticObject(buffer->aFilePath, pos, rot, scale);
	}
		break;
	}
}