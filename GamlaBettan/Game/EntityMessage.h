#pragma once

enum class EntityMessage
{
	StartWalking,
	StopWalking,
	StartDying,
	DeathAnimationOver,
	StartAttacking,
	AttackAnimationOver,
	StopAttacking,
	WasHit,
	StartFalling,
	TookDamage,

	ReloadAnimationFinnished,
	FireAnimationFinnished,
	ADSInAnimationFinnished,
	ADSOutAnimationFinnished,
	AlertedAnimationFinnished,
	TakeDamageAnimationFinnished,
	AttackAnimationFinnished,

	Count
};