#include "pch.h"
#include "UIManager.h"
#include "Scene.h"
#include "SpriteInstance.h"
#include "SpriteFactory.h"
#include "EmilsEnums.h"
#include "TextFactory.h"
#include "TextInstance.h"
#include "Entity.h"
#include "Random.h"
#include "Camera.h"
#include "FireWeapon.h"


UIManager::UIManager() :
	myUIFadeTime(3.0f),
	myUIFadeTimer(-1.0f),
	myUIFadeDirection(1.0f),
	myCurrentLevel(0),
	myCamera(nullptr),
	myFadeSprite(nullptr),
	myFirstFadeInDone(false),
	myIsFadingIn(false),
	myIsFadingOut(false),
	myCrosshair(nullptr),
	myScene(nullptr),
	myShouldKillPlayerAfterFadeOut(false),
	myShouldSwapLevelAfterFade(false),
	mySpriteFactory(nullptr),
	myTextFactory(nullptr),
	myVignette(nullptr),
	myAmmoCounter(nullptr)
{
	SubscribeToMessage(MessageType::FadeIn);
	SubscribeToMessage(MessageType::FadeOut);
	SubscribeToMessage(MessageType::CurrentLevel);
	SubscribeToMessage(MessageType::TriggerEvent);
}

UIManager::~UIManager()
{
	UnSubscribeToMessage(MessageType::FadeIn);
	UnSubscribeToMessage(MessageType::FadeOut);
	UnSubscribeToMessage(MessageType::TriggerEvent);
	UnSubscribeToMessage(MessageType::CurrentLevel);

	for (size_t i = 0; i < mySprites.size(); i++)
	{
		myScene->RemoveSprite(mySprites[i]);
		delete mySprites[i];
	}

	mySprites.clear();

	WIPE(*this);
}

void UIManager::Init(Entity* aPlayer, Scene& aScene, SpriteFactory& aSpriteFactory, TextFactory& aTextFactory, Camera* aCamera)
{
	myCamera = aCamera;
	myScene = &aScene;
	myTextFactory = &aTextFactory;
	mySpriteFactory = &aSpriteFactory;
	myPlayerPtr = aPlayer;

	ProgressBar::SetSpriteFactory(mySpriteFactory);

	myFadeSprite = mySpriteFactory->CreateSprite("Data/UI/fadeScreen.dds");

#if !DEMOSCENE
#if !DIRECTTOGAME
	aScene.AddSprite(myFadeSprite);
#endif
#endif

	myFadeSprite->SetPosition(0.0f, 0.0f);
	myFadeSprite->SetScale(CommonUtilities::Vector2<float>(1000.0f, 1000.0f));
	mySprites.push_back(myFadeSprite);

	myVignette = CreateAndAddSpriteToScene("Data/UI/vignette.dds", V2F(0.0f, 0.0f));
	myCrosshair = CreateAndAddSpriteToScene("Data/UI/Crosshair.dds", V2F(0.5f, 0.5f), V2F(0.5f, 0.5f));

	myIsFadingIn = true;
	myFirstFadeInDone = false;
	myCurrentLevel = 0;
	myUIFadeTimer = 5.0f;

	Life* component = aPlayer->GetComponent<Life>();
	healthBar.Init(V2F(0.03, 0.9), &component->GetLife(), &component->GetMaxLife(), "Data/UI/HpBar_Hp.dds", true, "Data/UI/HpBar_frame.dds", "Data/UI/HpBar_Glans.dds");
	myScene->AddProgressBar(&healthBar);

	myAmmoCounter = myTextFactory->CreateToolTip("Data/UI/AmmoCounterBackground.dds", {20, 20}, "", "Data/Fonts/ammoCounter.spritefont");
	myAmmoCounter->SetPosition(V2F(0.2, 0.9));
	myScene->AddText(myAmmoCounter);
}

void UIManager::Update(const float aDeltaTime)
{
	Fade(aDeltaTime);

	healthBar.Update();

	FireWeapon* weapon = myPlayerPtr->GetComponent<FireWeapon>();
	myAmmoCounter->SetText(std::to_string(weapon->GetCurrentClipSize()) + " / " + std::to_string(weapon->GetTotalAmmo()));
}

void UIManager::AddUIToScene(Scene& aScene)
{
	myScene = &aScene;

	for (auto& sprite : mySprites)
	{
		if (!sprite->HasBeenAddedToScene())
		{
			myScene->AddSprite(sprite);
		}
	}

	myScene->AddProgressBar(&healthBar);
	myScene->AddText(myAmmoCounter); 
}

void UIManager::FadeOut()
{
	myFadeSprite->SetScale(CU::Vector2<float>(1000.0f, 1000.0f));
	myFadeSprite->SetPosition(0.0f, 0.0f);
	myUIFadeTimer = 0.01f;
}

void UIManager::FadeIn()
{
	//myUIFadeTimer = myUIFadeTime;
}

void UIManager::RecieveMessage(const Message& aMessage)
{
	switch (aMessage.myMessageType)
	{

	case MessageType::FadeIn:
	{
		FadeIn();
	}
	break;

	case MessageType::FadeOut:
	{
		if (!myIsFadingOut)
		{
			myShouldSwapLevelAfterFade = aMessage.myBool;
			myShouldKillPlayerAfterFadeOut = static_cast<bool>(aMessage.myIntValue);
			FadeOut();
			myIsFadingOut = true;
		}
	}
	break;

	case MessageType::TriggerEvent:
	{
		if (aMessage.myText == "1000")
		{
			if (!myIsFadingOut)
			{
				myShouldSwapLevelAfterFade = true;
				myShouldKillPlayerAfterFadeOut = false;
				FadeOut();
				myIsFadingOut = true;
			}
		}
	}
	break;

	case MessageType::CurrentLevel:
	{
		myCurrentLevel = aMessage.myIntValue;
	}
	break;
	}
}

void UIManager::ReAddMouseToScene()
{
	if (myCrosshair)
	{
		myScene->RemoveSprite(myCrosshair);
		myScene->AddSprite(myCrosshair);
	}
}

void UIManager::Fade(const float aDeltaTime)
{
	if (myIsFadingOut)
	{
		myUIFadeTimer += aDeltaTime;
		myFadeSprite->SetColor(CommonUtilities::Vector4<float>(1.0f, 1.0f, 1.0f, myUIFadeTimer / myUIFadeTime));
		if (myUIFadeTimer > myUIFadeTime)
		{
			myUIFadeTimer = myUIFadeTime;
			myIsFadingOut = false;

			if (myShouldKillPlayerAfterFadeOut)
			{
				PostMaster::GetInstance()->SendMessages(MessageType::PlayerDied);
			}
			else
			{
				Message fadeOutCompleteMessage;
				fadeOutCompleteMessage.myMessageType = MessageType::FadeOutComplete;
				SendMessages(fadeOutCompleteMessage);
			}
			myIsFadingIn = true;
		}
	}

	if (myIsFadingIn)
	{
		if (myUIFadeTimer > 0.0f)
		{
			myUIFadeTimer -= aDeltaTime;
			myFadeSprite->SetColor(CommonUtilities::Vector4<float>(1.0f, 1.0f, 1.0f, myUIFadeTimer / myUIFadeTime));
		}
		else
		{
			myUIFadeTimer = -1;
			myIsFadingIn = false;

			if (!myFirstFadeInDone)
			{
				myFirstFadeInDone = true;
				Message fadeInCompleteMessage;
				fadeInCompleteMessage.myMessageType = MessageType::FadeInComplete;
				SendMessages(fadeInCompleteMessage);
			}
		}
	}
}

SpriteInstance* UIManager::CreateAndAddSpriteToScene(const std::string& aPath, const V2F& aPosition, const V2F aPivot)
{
	SpriteInstance* sprite = mySpriteFactory->CreateSprite(aPath);

	if (sprite)
	{
		sprite->SetPivot(aPivot);
		sprite->SetPosition(aPosition);
		myScene->AddSprite(sprite);
		mySprites.push_back(sprite);
	}

	return sprite;
}

void UIManager::RemoveSpriteFromSceneAndDelete(SpriteInstance*& aSprite)
{
	if (aSprite)
	{
		for (size_t i = 0; i < mySprites.size(); i++)
		{
			if (mySprites[i] == aSprite)
			{
				mySprites.erase(mySprites.begin() + i);
				break;
			}
		}

		myScene->RemoveSprite(aSprite);

		delete aSprite;
		aSprite = nullptr;
	}
}
