#include "pch.h"
#include "AnimationComponent.h"
#include "Mesh.h"
#include "Model.h"
#include "Animator.h"
#include "Entity.h"
#include "Random.h"


void AnimationComponent::Init(Entity* aEntity)
{
	LOGVERBOSE("AnimationComponent Inited")
		myEntity = aEntity;
	myBlend = 0;
	Mesh* mesh = aEntity->GetComponent<Mesh>();
	if (mesh)
	{
		AttachToMesh(mesh);
	}
	else
	{
		LOGWARNING("Animation component need to be added after a mesh component");
	}
	myEntity->AddActivity();
	myShouldAnimate = true;
	SetState(States::Idle);
}

void AnimationComponent::Update(const float aDeltaTime)
{

	if (myModelImWaitingFor)
	{
		if (myModelImWaitingFor->GetModel()->ShouldRender())
		{
			AttachToModel(myModelImWaitingFor);
			myModelImWaitingFor = nullptr;
			myAnimator->SetState(IndexFromState(myNextState));
		}
		return;
	}
	if (!myAnimator)
	{
		LOGWARNING("You need to call init on Animation components");
		return;
	}
	myAnimator->Step(myShouldAnimate ? aDeltaTime : 0);
	//if (myShouldAnimate && myCurrentState != myNextState)
	if (myShouldAnimate && myBlend > 0.0f)
	{
		myBlend -= aDeltaTime * myBlendSpeed;
		myAnimator->SetBlend(myBlend);
		if (myAnimator->DoneBlending())
		{
			myCurrentState = myNextState;
		}
	}
	if (myCurrentState == States::Dying && myAnimator->Looped())
	{
		myEntity->SendEntityMessage(EntityMessage::DeathAnimationOver);
		myEntity->FinishActivity();
		myShouldAnimate = false;
	}
	else if (myAnimator->Looped())
	{
		if (myCurrentState == States::AttackMelee)
		{
			myEntity->SendEntityMessage(EntityMessage::AttackAnimationOver);
		}
		else if (myCurrentState == States::FireWeapon)
		{
			myEntity->SendEntityMessage(EntityMessage::FireAnimationFinnished);
		}
		else if (myCurrentState == States::ReloadFull || myCurrentState == States::ReloadTac)
		{
			myEntity->SendEntityMessage(EntityMessage::ReloadAnimationFinnished);
		}
		else if (myCurrentState == States::ADSIn)
		{
			myEntity->SendEntityMessage(EntityMessage::ADSInAnimationFinnished);
		}
		else if (myCurrentState == States::ADSOut)
		{
			myEntity->SendEntityMessage(EntityMessage::ADSOutAnimationFinnished);
		}
		else if (myCurrentState == States::Alerted)
		{
			myEntity->SendEntityMessage(EntityMessage::AlertedAnimationFinnished);
		}
		else if (myCurrentState == States::TakeDamage)
		{
			myEntity->SendEntityMessage(EntityMessage::TakeDamageAnimationFinnished);
		}
		else if (myCurrentState == States::AttackMelee)
		{
			myEntity->SendEntityMessage(EntityMessage::AttackAnimationFinnished);
		}
	}
}

void AnimationComponent::Reset()
{
	LOGVERBOSE("AnimationComponent Reset")
		//TODO RAGDOLL
		/*
		myEntity->GetComponent<Mesh>()->GetModelInstance()->DetachAnimator();
		SAFE_DELETE(myAnimator);
		*/
}

void AnimationComponent::OnAttach()
{
	LOGVERBOSE("AnimationComponent Attached")
}

void AnimationComponent::OnDetach()
{
	LOGVERBOSE("AnimationComponent Detached")
}

void AnimationComponent::OnKillMe()
{
	LOGVERBOSE("AnimationComponent Killed")
}

void AnimationComponent::RecieveEntityMessage(EntityMessage aMessage, void* someData)
{
	if (myShouldUseEntityMessage)
	{
		switch (aMessage)
		{
		case EntityMessage::StartDying:
			SetState(States::Dying);
			break;
		case EntityMessage::StartFalling:
			SetState(States::Falling);
				break;
		default:
			//NO-OP
			break;
		}
	}
}

void AnimationComponent::AttachToMesh(Mesh* aMeshcomponent)
{
	LOGVERBOSE("AnimationComponent Attached To mesh")
		ModelInstance* model = aMeshcomponent->GetModelInstance();
	if (model->GetModel()->ShouldRender())
	{
		myModelImWaitingFor = model;
		AttachToModel(model);
	}
	else
	{
		myModelImWaitingFor = model;
	}
}

void AnimationComponent::AttachToModel(ModelInstance* aModel)
{
	LOGVERBOSE("AnimationComponent attached To model")
		Model::CModelData* modelData = aModel->GetModel()->GetModelData();
	myAnimator = new Animator();
	std::vector<std::string> allAnimations;
	ParseAnimations(modelData->myAnimations, myStateMapping, allAnimations);
	myAnimator->Init(modelData->myFilePath, allAnimations);
	myModelImWaitingFor->AttachAnimator(myAnimator);
}

void AnimationComponent::SetState(States aNewState, bool aForceState, bool aPreventLoopingOld)
{
	if (aNewState == myNextState)
	{
		return;
	}

	std::cout << "New anim state: " << static_cast<int>(aNewState) << std::endl;

	if (!aForceState && myNextState == States::Dying)
	{
		return;
	}

	if (aForceState)
	{
		myShouldAnimate = true;
		myBlend = 0.001;
	}
	else
	{
		myBlend = 1;
	}

	myNextState = aNewState;
	if (!myAnimator || myModelImWaitingFor)
	{
		return;
	}
	myAnimator->SetState(IndexFromState(myNextState), !aPreventLoopingOld);
	myAnimator->SetTime(0.f);
	//TODO: let old animation keep time , make smooth and silky :D
}

void AnimationComponent::ParseAnimations(const std::vector<std::string>& aAnimations, std::unordered_map<States, std::pair<size_t, size_t>>& aStatemapping, std::vector<std::string>& aFilteredAnimations)
{
	std::unordered_map<std::string, States> stateMapping;
	{
		stateMapping["Spawn"] = States::Spawning;
		stateMapping["spawn"] = States::Spawning;
		stateMapping["Idle"] = States::Idle;
		stateMapping["idle"] = States::Idle;
		stateMapping["idleWalk"] = States::IdleWalk;
		stateMapping["IdleWalk"] = States::IdleWalk;
		stateMapping["Alerted"] = States::Alerted;
		stateMapping["alerted"] = States::Alerted;
		stateMapping["Walk"] = States::Walking;
		stateMapping["walk"] = States::Walking;
		stateMapping["Run"] = States::Running;
		stateMapping["run"] = States::Running;
		stateMapping["Death"] = States::Dying;
		stateMapping["death"] = States::Dying;
		stateMapping["AttackMelee"] = States::AttackMelee;
		stateMapping["Fire"] = States::FireWeapon;
		stateMapping["FireLast"] = States::FireLast;
		stateMapping["ReloadTac"] = States::ReloadTac;
		stateMapping["ReloadFull"] = States::ReloadFull;
		stateMapping["ADSIdle"] = States::ADSIdle;
		stateMapping["ADSIn"] = States::ADSIn;
		stateMapping["ADSOut"] = States::ADSOut;
		stateMapping["Cinematic"] = States::Cinematic;
		stateMapping["cinematic"] = States::Cinematic;
		stateMapping["takeDamage"] = States::TakeDamage;
		stateMapping["TakeDamage"] = States::TakeDamage;
		stateMapping["godis"] = States::Godis;
		stateMapping["Godis"] = States::Godis;
		stateMapping["falling"] = States::Falling;
		stateMapping["Falling"] = States::Falling;
	}

	States state = States::Count;
	size_t start;
	size_t end;
	for (auto& i : aAnimations)
	{
		if (i.empty())
		{
			continue;
		}
		if ((i.length() > 0 && i[0] == '#') || (i.length() > 1 && i.substr(0, 2) == "//"))
		{
			continue;
		}
		auto it = stateMapping.find(i);
		if (it != stateMapping.end())
		{
			if (state != States::Count)
			{
				if (start <= aFilteredAnimations.size() - 1)
				{
					aStatemapping[state] = std::make_pair(start, aFilteredAnimations.size() - 1);
				}
			}
			state = it->second;
			start = aFilteredAnimations.size();
		}
		else
		{
			aFilteredAnimations.push_back("Data/Animations/" + i);
		}
	}
	if (state != States::Count)
	{
		if (start <= aFilteredAnimations.size() - 1)
		{
			aStatemapping[state] = std::make_pair(start, aFilteredAnimations.size() - 1);
		}
	}
}

void AnimationComponent::SetShouldUseEntityMessage(bool aVal)
{
	myShouldUseEntityMessage = aVal;
}

Animator* AnimationComponent::GetAnimator()
{
	return myAnimator;
}

size_t AnimationComponent::IndexFromState(States aState)
{
	if (myStateMapping.count(aState) == 0)
	{
		switch (aState)
		{
		case AnimationComponent::States::AttackMelee:
			ONETIMEWARNING("Missing mapping for attack2 defaulting to 1","");
			return IndexFromState(States::AttackMelee);
		/*case AnimationComponent::States::Attack3:
			ONETIMEWARNING("Missing mapping for attack3 defaulting to 2","");
			return IndexFromState(States::Attack2);
		case AnimationComponent::States::Attack4:
			ONETIMEWARNING("Missing mapping for attack4 defaulting to 3","");
			return IndexFromState(States::Attack3);
		case AnimationComponent::States::Attack5:
			ONETIMEWARNING("Missing mapping for attack5 defaulting to 4","");
			return IndexFromState(States::Attack4);
		case AnimationComponent::States::Attack6:
			ONETIMEWARNING("Missing mapping for attack6 defaulting to 5","");
			return IndexFromState(States::Attack5);*/
		}

		LOGERROR("Model missing animation for state " + std::to_string(static_cast<int>(aState)));
		myStateMapping[aState] = std::make_pair(0ULL, 0ULL);
		return 0;
	}
	std::pair<size_t, size_t> range = myStateMapping[aState];
	if (range.second < range.first)
	{
		LOGWARNING("Animation range for " + std::to_string(static_cast<int>(aState)) + " is missing");
		return 0;
	}
	size_t result = Tools::RandomRange(range.first, range.second);
	SYSVERBOSE("Swapping to animationstate[" + std::to_string(range.first) + "," + std::to_string(range.second) + "]: " + std::to_string(result));
	return result;
}
