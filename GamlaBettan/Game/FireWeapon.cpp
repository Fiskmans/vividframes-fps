#include "pch.h"
#include "FireWeapon.h"

#include <iostream>

FireWeapon::FireWeapon()
{
	myTrigger = TriggerStatus::Released;
	
	myFirerate = 0;
	myFireRateTimer = 0;

	myIsAutomatic = true;
	myClipSize = 0;
	myTotalAmmoAmount = 0;
	myCurrentClipSize = 0;
	myTotalAmmoLimit = 0;

	myBulletsPerShot = 0;

	myTotalAmmoIsUnlimited;
	myClipIsUnlimited;
}

FireWeapon::~FireWeapon()
{
}

void FireWeapon::Init(Entity* aEntity)
{
	myEntity = aEntity;
}

void FireWeapon::Update(const float aDeltaTime)
{
	if (myFireRateTimer > 0)
	{
		myFireRateTimer -= aDeltaTime;
	}
}

void FireWeapon::Reset()
{
}

void FireWeapon::SetWeaponProperties()
{
	myIsAutomatic = true;
	myFirerate = 0.1f;
	myClipSize = 30;
	myTotalAmmoLimit = 300;
	myBulletsPerShot = 1;

	myCurrentClipSize = myClipSize;
	myTotalAmmoAmount = myTotalAmmoLimit;
}

void FireWeapon::PressTrigger()
{
	if (myTrigger == TriggerStatus::Released)
	{
		myTrigger = TriggerStatus::Pressed;
	}
}

void FireWeapon::ReleaseTrigger()
{
	myTrigger = TriggerStatus::Released;
}

bool FireWeapon::Fire(FireInfo& aFireInfo)
{
	if (myFireRateTimer <= 0 && myCurrentClipSize > 0)
	{
		if (myTrigger == TriggerStatus::Pressed)
		{
			myTrigger = TriggerStatus::Down;
			aFireInfo.bulletsShot = InternalFire();
		}
		else if (myTrigger == TriggerStatus::Down && myIsAutomatic)
		{
			aFireInfo.bulletsShot = InternalFire();
		}
		else
		{
			return false;
		}
	}
	else
	{
		return false;
	}

	//TODO: L�gg till en offset riktning f�r skott om man t ex h�ftskjuter eller springer och skjuter
	//aFireInfo.BulletDirectionOffset.push_back(V2F(random, random));

	//TODO: L�gg till recoilVelocity
	//aFireInfo.recoilVelocity = ?

	//Temp UI
	std::cout << "Temp UI: Ammo: " << myCurrentClipSize << " / " << myTotalAmmoAmount << std::endl;

	return true;
}

bool FireWeapon::Reload()
{
	if (myTotalAmmoIsUnlimited)
	{
		myCurrentClipSize = myClipSize;
		return true;
	}

	int ammoToLoad = myClipSize - myCurrentClipSize;
	
	if (myTotalAmmoAmount <= 0 || ammoToLoad <= 0)
	{
		return false;
	}
	else if (myTotalAmmoAmount < ammoToLoad)
	{
		myCurrentClipSize = myTotalAmmoAmount;
		myTotalAmmoAmount = 0;
		return true;
	}
	else
	{
		myCurrentClipSize = myClipSize;
		myTotalAmmoAmount -= ammoToLoad;
		return true;
	}
}

bool FireWeapon::CanReload()
{
	int ammoToLoad = myClipSize - myCurrentClipSize;

	if (myTotalAmmoAmount <= 0 || ammoToLoad <= 0)
	{
		return false;
	}

	return true;
}

int FireWeapon::GetCurrentClipSize()
{
	return myCurrentClipSize;
}

int FireWeapon::GetTotalAmmo()
{
	return myTotalAmmoAmount;
}

void FireWeapon::OnAttach()
{
}

void FireWeapon::OnDetach()
{
}

void FireWeapon::OnKillMe()
{
}

int FireWeapon::InternalFire()
{
	std::cout << "Fired" << std::endl;

	myFireRateTimer = myFirerate;
	myTrigger == TriggerStatus::Down;

	int bulletsFired;
	if (myBulletsPerShot > myCurrentClipSize)
	{
		bulletsFired = myCurrentClipSize;
	}
	else
	{
		bulletsFired = myBulletsPerShot;
	}

	if (!myClipIsUnlimited)
	{
		myCurrentClipSize -= myBulletsPerShot;
	}

	if (myCurrentClipSize < 0)
	{
		myCurrentClipSize = 0;
	}

	return bulletsFired;
}
