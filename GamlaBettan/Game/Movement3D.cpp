#include "pch.h"
#include "Movement3D.h"
#include "Entity.h"
#include "Audio.h"
#include "DebugDrawer.h"
#include <PathFinder.h>

#define MARKERDURATION 0.5f

#ifdef _DEBUG
bool Movement3D::ourShowPaths;
#endif

Movement3D::Movement3D() :
	myAccelerationSpeed(1.0f),
	myCanTurn(true),
	myCurrentDirectionVector(CommonUtilities::Vector3<float>(0.0f, 0.0f, 0.1f)),
	mySpeed(0.0f),
	myTurnRate(1.0f),
	myAnimationPauseTimer(-1.0f),
	myLaunchDownTimer(0.f),
	myLaunchTopPosInTime(0.f),
	myLaunchUpTimer(0.f),
	myTargetMarker(nullptr),
	myRenderMarkerTimer(0.f)
{
	myIsMoving = false;
	myIsLaunching = false;
	myLaunchTime = 0.0f;

	myFootStepInterval = 0.0f;
}

Movement3D::~Movement3D()
{
}

void Movement3D::Init(Entity* aParentEntity)
{
	LOGVERBOSE("Movement3D Inited")
		myIsMoving = false;
	myEntity = aParentEntity;
	// SET my variables to some loaddata
	mySpeed = 550.0f;

	ClearMovementTargetPoints();
}

void Movement3D::Update(const float aDeltaTime)
{
	if (myAnimationPauseTimer > 0.0f)
	{
		myAnimationPauseTimer -= aDeltaTime;

		if (myAnimationPauseTimer <= 0)
		{
			ClearMovementTargetPoints();
		}
	}

	if (myTargetMarker)
	{
		if (myRenderMarkerTimer > 0)
		{
			myRenderMarkerTimer -= aDeltaTime;
		}
		else
		{
			myTargetMarker->SetShouldRender(false);
		}
	}

	if (myAnimationPauseTimer <= 0.0f)
	{
		
		if (myMovementTargetPoints.size() > 0)
		{
#ifdef _DEBUG
			if (ourShowPaths)
			{
				V3F at = myEntity->GetPosition();
				DebugDrawer::GetInstance().SetColor(V4F(0.3f, 1.f, 0.3f, myEntity->GetEntityType() == EntityType::Player ? 1.f : 0.2f));
				for (auto& i : myMovementTargetPoints)
				{
					V3F next = i;
					DebugDrawer::GetInstance().DrawArrow(at, next);
					at = next;
				}
			}
#endif // _DEBUG
			if (!myIsMoving)
			{
				myEntity->SendEntityMessage(EntityMessage::StartWalking);
				myIsMoving = true;
			}
			CommonUtilities::Vector3<float> curPos = myEntity->GetPosition();
			float movementVal = mySpeed * aDeltaTime;

			while ((curPos - myMovementTargetPoints.front()).LengthSqr() < (movementVal * movementVal * 1.2f))
			{
				myMovementTargetPoints.erase(myMovementTargetPoints.begin());

				if (myMovementTargetPoints.empty())
				{
					return;
				}
				else
				{
					myCurrentTargetPoint = myMovementTargetPoints.front();
				}
			}

			myTargetDirection = myCurrentTargetPoint - curPos;
			myTargetDirection.Normalize();

			// Soft rotation
			float interpolationPercent = 0.99f;

			V3F interpolatedDirection = (myCurrentDirectionVector * interpolationPercent) + ((1 - interpolationPercent) * myTargetDirection);

			interpolatedDirection.Normalize();
			myCurrentDirectionVector = interpolatedDirection * aDeltaTime;

			//------------------------------


			myEntity->SetPosition(curPos + myTargetDirection * movementVal);

			myCurrentDirectionVector.y = 0;
			myEntity->SetRotation(CU::Matrix4x4<float>::CreateRotationFromDirection(myCurrentDirectionVector));


			//Only for audio. should be removed if anim callback is in.
			if (myFootStepInterval <= 0)
			{
				myEntity->GetComponent<Audio>()->PlayAudioEvent(AudioEvent::Footstep);
				myFootStepInterval = 0.36f;
			}
			else
			{
				myFootStepInterval -= aDeltaTime;
			}
			//----------
		}
		else
		{
			if (myIsMoving)
			{
				myEntity->SendEntityMessage(EntityMessage::StopWalking);
				myIsMoving = false;
			}
		}

		// Soft rotation
		myTargetDirection = myCurrentTargetPoint - myEntity->GetPosition();
		myTargetDirection.Normalize();

		float interpolationPercent = 0.98f;

		V3F interpolatedDirection = (myCurrentDirectionVector * interpolationPercent) + ((1 - interpolationPercent) * myTargetDirection);

		interpolatedDirection *= aDeltaTime;
		interpolatedDirection.Normalize();
		myCurrentDirectionVector = interpolatedDirection;

		//------------------------------
	}
}

void Movement3D::Reset()
{
	LOGVERBOSE("Movement3D Reset")
		myIsMoving = false;
}

V3F Movement3D::GetDirection()
{
	return myCurrentDirectionVector;
}

float Movement3D::GetSpeed()
{
	return mySpeed;
}

void Movement3D::SetSpeed(const float& aSpeed)
{
	mySpeed = aSpeed;
}

float Movement3D::GetDistanceLeft()
{
	V3F at = myEntity->GetPosition();
	float length = 0.f;
	for (auto& i : myMovementTargetPoints)
	{
		length += i.Distance(at);
		at = i;
	}
	return length;
}

void Movement3D::SetValues(float aSpeed, CommonUtilities::Vector3<float> aDirectionVector, float aAccelerationSpeed, float aTurnRate)
{
	mySpeed = aSpeed;
	myAccelerationSpeed = aAccelerationSpeed;
	myCurrentDirectionVector = aDirectionVector;
	myTurnRate = aTurnRate;
}

void Movement3D::SetTargetMarker(ModelInstance* aMarker)
{
	myTargetMarker = aMarker;
}

void Movement3D::SetTargetPosition(V3F aPosition)
{
	std::vector<V3F> points = PathFinder::GetInstance().FindPath(myEntity->GetPosition(), aPosition);
	if (!points.empty())
	{
		AddMovementTargetPoints(points, true);

		if (myTargetMarker)
		{
			myRenderMarkerTimer = MARKERDURATION;
			myTargetMarker->SetPosition(V4F(aPosition, 1.f));
			myTargetMarker->SetShouldRender(true);
		}
	}
	//AddMovementTargetPoint(aPosition, true);
}

bool Movement3D::SetTargetPositionOnNavmeshAtPosition(V3F aPosition)
{
	V3F setPos;

	V3F rayStart = aPosition;
	rayStart.y += 100;

	V3F rayDir = aPosition - rayStart;
	rayDir.Normalize();

	SlabRay ray(rayStart, rayDir);
	setPos = PathFinder::GetInstance().FindPoint(ray);

	if (setPos.LengthSqr() > 0)
	{
		SetTargetPosition(setPos);
		return true;
	}
	else
	{
		return false;
		//SetTargetPosition(aPosition);
	}
}

void Movement3D::AddMovementTargetPoint(CommonUtilities::Vector3<float> aTargetPoint, bool aShouldClearList)
{
	if (aShouldClearList)
	{
		myMovementTargetPoints.clear();
	}

	myMovementTargetPoints.push_back(aTargetPoint);

	myCurrentTargetPoint = myMovementTargetPoints[0];

	if (myTargetMarker)
	{
		myRenderMarkerTimer = MARKERDURATION;
		myTargetMarker->SetPosition(V4F(aTargetPoint, 1));
		myTargetMarker->SetShouldRender(true);
	}

	//FaceTowards(myCurrentTargetPoint);
}

void Movement3D::AddMovementTargetPoints(std::vector<CommonUtilities::Vector3<float>> someTargetPoints, bool aShouldClearList)
{
	if (aShouldClearList)
	{
		myMovementTargetPoints.clear();
	}
	if (someTargetPoints.empty())
	{
		return;
	}

	for (auto& it : someTargetPoints)
	{
		if ((it - myEntity->GetPosition()).LengthSqr() > 0)
		{
			myMovementTargetPoints.push_back(it);
		}
	}

	if (myMovementTargetPoints.empty())
	{
		return;
	}

	myCurrentTargetPoint = myMovementTargetPoints[0];

	if (myTargetMarker)
	{
		myRenderMarkerTimer = MARKERDURATION;
		myTargetMarker->SetPosition(V4F(myMovementTargetPoints.back(), 1));
		myTargetMarker->SetShouldRender(true);
	}
	//FaceTowards(myCurrentTargetPoint);
}

void Movement3D::ClearMovementTargetPoints()
{
	myMovementTargetPoints.clear();
}

void Movement3D::SetAnimationPauseTimer(float aAnimationPauseDuration)
{
	myAnimationPauseTimer = aAnimationPauseDuration;
}

void Movement3D::FaceTowards(V3F aPoint, bool aBruteForce)
{
	if (myAnimationPauseTimer < 0.0f || aBruteForce == true)
	{
		myTargetDirection = (aPoint - myEntity->GetPosition()).GetNormalized();
		myTargetDirection.y = 0;
		myEntity->SetRotation(CommonUtilities::Matrix4x4<float>::CreateRotationFromDirection(myTargetDirection));
		myCurrentDirectionVector = myTargetDirection;
	}
}

bool Movement3D::empty()
{
	return myMovementTargetPoints.empty();
}

void Movement3D::Launch(V3F aLaunchTargetPoint, float aLaunchHeight, float aLaunchTime, float aTopPosInTime)
{
	myIsLaunching = true;
	myLaunchTargetPoint = aLaunchTargetPoint;
	myLaunchTime = aLaunchTime;
	myLaunchStartPoint = myEntity->GetPosition();
	myLaunchUpTimer = 0;
	myLaunchDownTimer = 0;

	myLaunchTopPosInTime = aTopPosInTime;
	myLaunchSplineTopPoint = (myLaunchTargetPoint - myLaunchStartPoint);

	myLaunchSplineTopPoint.x *= myLaunchTopPosInTime;
	myLaunchSplineTopPoint.z *= myLaunchTopPosInTime;

	float heightDifference = aLaunchTargetPoint.y - myLaunchStartPoint.y;
	if (heightDifference > 0)
	{
		myLaunchSplineTopPoint.y = aLaunchHeight + heightDifference;
	}
	else if (heightDifference < 0)
	{
		myLaunchSplineTopPoint.y = aLaunchHeight / 2;
	}
	else
	{
		myLaunchSplineTopPoint.y = aLaunchHeight;
	}

	myLaunchSplineTopPoint = myLaunchStartPoint + myLaunchSplineTopPoint;

	myLaunchSplineStartpoint = myLaunchStartPoint + V3F(0, -100, 0);
	myLaunchSplineEndPoint = myLaunchTargetPoint + V3F(0, -100, 0);
}

bool Movement3D::GetIsLaunching()
{
	return myIsLaunching;
}

void Movement3D::OnAttach()
{
}

void Movement3D::OnDetach()
{
}

void Movement3D::OnKillMe()
{
}

V3F Movement3D::GetSplinePosInTime(float t, V3F p0, V3F p1, V3F p2, V3F p3)
{
	//The coefficients of the cubic polynomial (except the 0.5f * which I added later for performance)
	const auto a = 2.f * p1;
	const auto b = p2 - p0;
	const auto c = 2.f * p0 - 5.f * p1 + 4.f * p2 - p3;
	const auto d = -p0 + 3.f * p1 - 3.f * p2 + p3;

	//The cubic polynomial: a + b * t + c * t^2 + d * t^3
	const auto pos = 0.5f * (a + (b * t) + (c * t * t) + (d * t * t * t));

	return pos;
}