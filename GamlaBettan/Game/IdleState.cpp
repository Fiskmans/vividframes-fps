#include "pch.h"
#include "IdleState.h"
#include "Entity.h"
#include <iostream>
#include "AnimationComponent.h"
#include "AIPollingStation.h"
#include "AIController.h"

IdleState::IdleState(Entity* aParentEntity, AIPollingStation* aPollingStation):
	myEntity(aParentEntity),
	myPollingStation(aPollingStation)
{

}

IdleState::~IdleState()
{
}

bool IdleState::CheckCondition(AIStates& aState)
{
	aState = AIStates::None;

	float aggroDistance = 1000;

	if (V3F(myPollingStation->GetPlayer()->GetPosition() - myEntity->GetPosition()).LengthSqr() < (aggroDistance * aggroDistance))
	{
		aState = AIStates::Alerted;
		return true;
	}
	else if (myEntity->GetComponent<AIController>()->GetTargetPosition() != V3F() &&
		myEntity->GetPosition().DistanceSqr(myEntity->GetComponent<AIController>()->GetTargetPosition()) > 100 * 100)
	{
		aState = AIStates::SeekTarget;
		return true;
	}


	return false;
}

void IdleState::Update(float aDeltaTime)
{
	//start ideling whatever that is
}

void IdleState::OnEnter()
{
	myEntity->GetComponent<AnimationComponent>()->SetState(AnimationComponent::States::Idle);
	std::cout << "Idle" << std::endl;
}

void IdleState::OnExit()
{
}
