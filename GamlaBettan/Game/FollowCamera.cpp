#include "pch.h"
#include "FollowCamera.h"
#include "Entity.h"
#include "Matrix.hpp"
#include "Scene.h"
#include "Camera.h"

#include "DebugDrawer.h"
#include "perlin_noise.h"

#if USEIMGUI
#include <imgui.h>
#endif // !USEIMGUI


FollowCamera::FollowCamera() :
	myCamera(nullptr),
	myEntity(nullptr),
	myTargetEntity(nullptr)
{

}

FollowCamera::~FollowCamera()
{
	UnSubscribeToMessage(MessageType::StartCameraShake);
}

void FollowCamera::Init(Entity* aEntity)
{
	myEntity = aEntity;
	SubscribeToMessage(MessageType::StartCameraShake);
}

void FollowCamera::SetTargetEntityAndDistance(Entity* aEntity, Entity* aOffsetEntity)
{
	myTargetEntity = aEntity;
	myOffsetEntity = aOffsetEntity;

	CommonUtilities::Matrix4x4<float> mat;
	//mat *= CommonUtilities::Matrix4x4<float>::CreateRotationAroundX(1.0f);
	mat *= CommonUtilities::Matrix4x4<float>::CreateRotationAroundX(3.1415926f / 4.0f);
	mat *= CommonUtilities::Matrix4x4<float>::CreateRotationAroundY(3.1415926f / 4.0f);
	myEntity->SetRotation(mat);
}

void FollowCamera::SetTargetCamera(Camera* aCamera)
{
	myCamera = aCamera;
}

void FollowCamera::Update(const float aDeltaTime)
{
	CommonUtilities::Vector4<float> pos = CommonUtilities::Vector4<float>(myTargetEntity->GetPosition(), 1.0f);
	CommonUtilities::Matrix4x4<float> mat = myEntity->GetRotation();

	if (myCamera)
	{
		myCamera->SetRotation(mat);
	}

	// FOV CHANGE
	if (myCamera)
	{
		if (myIsChangingFov)
		{
			float currentFov = myCamera->GetFoV();
			if (currentFov < myTargetFov)
			{
				myCamera->SetFov(currentFov + aDeltaTime * 3);
				if (myCamera->GetFoV() > myTargetFov)
				{
					myCamera->SetFov(myTargetFov);
					myIsChangingFov = false;
				}
			}
			else
			{
				myCamera->SetFov(currentFov - aDeltaTime);
				if (myCamera->GetFoV() < myTargetFov)
				{
					myCamera->SetFov(myTargetFov);
					myIsChangingFov = false;
				}
			}
		}
	}

	////CAMERASHAKE----------------------------------------------------------------------------------------

	//static float movementCS = 0.0f;
	//movementCS += myCameraShakeCurrentMovementValue;
	//if (myCameraShakeCurrentMovementValue > myCameraShakeEndMovementValue)
	//{
	//	myCameraShakeCurrentMovementValue -= 0.006f;
	//}
	//else
	//{
	//	myCameraShakeCurrentMovementValue = myCameraShakeEndMovementValue;
	//}
	//
	//float powerMultiplier = 0.1f;
	//
	//static PerlinNoise p1;
	//float offsetCS = p1.noise(movementCS, 0, 0) * powerMultiplier;
	//
	//static PerlinNoise p2;
	//float offset2CS = p2.noise(-movementCS, 0, 0) * powerMultiplier;
	//
	//offsetCS -= 0.5f * powerMultiplier;
	//offset2CS -= 0.5f * powerMultiplier;
	//
	//CommonUtilities::Matrix4x4<float> myMatrixStructure;
	// Set to camera matrix, or world, you might want to add to translation and rotation instead of just setting the values like me
	//myMatrixStructure(4 , 1) = offsetCS;
	//myMatrixStructure(4 , 2) = offset2CS;

	//static float rotation = 0.0f;
	//myMatrixStructure(1, 1) = cos(offsetCS);
	//myMatrixStructure(1, 3) = sin(offsetCS);
	//myMatrixStructure(3, 1) = -sin(offsetCS);
	//myMatrixStructure(3, 3) = cos(offsetCS);

	// END CAMERASHAKE---------------------------------------------------------------------------------------

	//Movement2D* movement = reinterpret_cast<Movement2D*>(myOffsetEntity->GetComponent(ComponentType::Movement2D));
	V2F offset;
	//if (movement)
	//{
	//	offset = movement->GetPositionalOffset();
	//}

	//CommonUtilities::Vector4<float> forward = CommonUtilities::Vector4<float>(0.0f, 0.0f, 1.0f, 0.0f) * mat;
	//CommonUtilities::Vector4<float> up = CommonUtilities::Vector4<float>(0.0f, 1.0f, 0.0f, 0.0f) * mat;
	//CommonUtilities::Vector4<float> right = CommonUtilities::Vector4<float>(1.0f, 0.0f, 0.0f, 0.0f) * mat;
	//
	//CommonUtilities::Vector4<float> resultPos;
	//resultPos = pos + (right * (CAST(float, -myOffsetFromTargetX) + offset.x * myFollowPowerX)) + (up * (CAST(float, -myOffsetFromTargetY) + offset.y * myFollowPowerY)) + (forward * -myOffsetFromTargetZ);
	//resultPos = pos + (right * (CAST(float, -myOffsetFromTargetX) + offset.x * myFollowPowerX)) + (up * (CAST(float, -myOffsetFromTargetY) + offset.y * myFollowPowerY)) + (forward * -myOffsetFromTargetZ);
	V3F targetpos = myTargetEntity->GetPosition();
	CommonUtilities::Vector3<float> outputPos = CommonUtilities::Vector3<float>(targetpos.x + myOffsetFromTargetX, targetpos.y + myOffsetFromTargetY, targetpos.z + myOffsetFromTargetZ);
	myEntity->SetPosition(outputPos);
	//myEntity->SetRotation(mat);
	if (myCamera != nullptr)
	{
		//without shake
		myCamera->SetPosition(outputPos);
		//myCamera->SetRotation(mat);

		////with shake
		//myCamera->SetPosition(CommonUtilities::Vector3<float>(outputPos.x + offsetCS, outputPos.y + offset2CS, outputPos.z));
		//myCamera->SetRotation(mat * myMatrixStructure);
	}

}

void FollowCamera::Reset()
{
	myCamera = nullptr;
	myEntity = nullptr;
	myTargetEntity = nullptr;
	UnSubscribeToMessage(MessageType::StartCameraShake);
}

void FollowCamera::RecieveMessage(const Message& aMessage)
{
	if (aMessage.myMessageType == MessageType::StartCameraShake)
	{
		myCameraShakeCurrentMovementValue = myCameraShakeStartMovementValue * aMessage.myFloatValue;
	}
}

void FollowCamera::SetCameraTargetFov(int aFov)
{
	myTargetFov = aFov;
	myIsChangingFov = true;
}

void FollowCamera::OnAttach()
{
}

void FollowCamera::OnDetach()
{
}

void FollowCamera::OnKillMe()
{
}

