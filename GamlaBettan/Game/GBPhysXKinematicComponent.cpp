#include "pch.h"
#include "GBPhysXKinematicComponent.h"
#include "AnimationComponent.h"
#include "GBPhysX.h"


#define AccWalkModifyer 25.0f
#define AccRunModifyer 10.0f
#define AccCrouchModifyer 25.0f
#define DeAccModifyer 25.0f

GBPhysXKinematicComponent::GBPhysXKinematicComponent() :
	myGBPhysXCharacter(nullptr),
	myMeshPtr(nullptr),
	myCurrentFallVelocity(0.f)
{
	myCurrentMovementState = MovementState::Walk;
	myCurrentVelocity = V3F();
	myTargetVelocity = V3F();
	myAccModifyer = 0;
	myCurrentAccModifyerMode = 0;
	myCurrentAccModifyerMode = 0;
}

GBPhysXKinematicComponent::~GBPhysXKinematicComponent()
{
	if (myGBPhysXCharacter != nullptr)
	{
		myGBPhysXCharacter->ReleaseHitBoxes();
		myGBPhysXCharacter->RemoveFromScene();
		myGBPhysXCharacter->Release();
		SAFE_DELETE(myGBPhysXCharacter);
	}

	WIPE(*this);
}

void GBPhysXKinematicComponent::Init(Entity* aEntity)
{
	myEntity = aEntity;
	myCurrentFallVelocity = -9.82f;
	myDirection = V3F();
	myDeltaMovement = V3F();
	myLastPosition = V3F();
	mySpeedModifyer = 1.0f;
	myLastPosition = myEntity->GetPosition();

	myCurrentSpeed = 0;

	Walk();
}


void GBPhysXKinematicComponent::Update(const float aDeltaTime)
{
	if (myMeshPtr != nullptr)
	{
		V3F lerpDistance = LERP(myCurrentVelocity, myDirection * static_cast<float>(myBaseSpeed)* mySpeedModifyer, 0.5f);
		myCurrentVelocity += (lerpDistance - myCurrentVelocity) * aDeltaTime * myAccModifyer;
		myGBPhysXCharacter->Move(myCurrentVelocity, aDeltaTime);

		myDeltaMovement = myLastPosition - myEntity->GetPosition();
		myLastPosition = myEntity->GetPosition();

		if (myEntity->GetEntityType() == EntityType::Enemy)
		{
			V3F offset = V3F(0.0f, 100.0f, 0.0f);
			myEntity->SetPosition(myGBPhysXCharacter->GetPosition() - offset);
		}
		else
		{
			myEntity->SetPosition(myGBPhysXCharacter->GetPosition());
		}

		myGBPhysXCharacter->UpdateHitBoxes();
	}
}

void GBPhysXKinematicComponent::Reset()
{
	myMeshPtr = nullptr;
	myGBPhysXCharacter->ReleaseHitBoxes();
	myGBPhysXCharacter->RemoveFromScene();
	myGBPhysXCharacter->Release();
	SAFE_DELETE(myGBPhysXCharacter);

	myEntity = nullptr;
	myCurrentFallVelocity = 0.0f;
}

void GBPhysXKinematicComponent::AddGBPhysXCharacter(GBPhysX* aGBPhysX, const V3F& aPosition, const V3F& aRotation, float aHeight, float aRadius, bool aIsPlayer)
{
	if (myGBPhysXCharacter != nullptr)
	{
		myGBPhysXCharacter->RemoveFromScene();
		myGBPhysXCharacter->Release();
		SAFE_DELETE(myGBPhysXCharacter);
		SYSERROR("ADD GB PHYSXCHARACTER CALLED MORE THAN ONCE ON GB PHYSXKINEMATIC CONTROLLER", "");
	}

	myGBPhysXCharacter = aGBPhysX->GBCreateCapsuleController(aPosition, aRotation, aHeight, aRadius, aIsPlayer);
	myMeshPtr = myEntity->GetComponent<Mesh>();
	myGBPhysXCharacter->SetIsKinematic(true);
	myGBPhysXCharacter->SetEntity(myEntity);
}

void GBPhysXKinematicComponent::SetTargetDirection(const V3F& aDirection)
{
	myDirection = aDirection;
	mySpeedModifyer = myCurrentSpeedModifyerMode;
	myAccModifyer = myCurrentAccModifyerMode;
}

void GBPhysXKinematicComponent::StopMoving()
{
	myAccModifyer = DeAccModifyer;
	mySpeedModifyer = 0;
}

void GBPhysXKinematicComponent::Jump()
{
	if (myGBPhysXCharacter->GetIsGrounded())
	{
		myGBPhysXCharacter->SetIsGrounded(false);
		myGBPhysXCharacter->Jump();
	}
}

void GBPhysXKinematicComponent::Run()
{
	if (myCurrentMovementState == MovementState::Crouch)
	{
		myGBPhysXCharacter->Stand();
	}

	myCurrentSpeedModifyerMode = myRunSpeedModifyer;
	myCurrentAccModifyerMode = AccRunModifyer;
	myCurrentMovementState = MovementState::Run;
}

void GBPhysXKinematicComponent::Walk()
{
	if (myCurrentMovementState == MovementState::Crouch)
	{
		myGBPhysXCharacter->Stand();
	}

	myCurrentSpeedModifyerMode = myWalkSpeedModifyer;
	myCurrentAccModifyerMode = AccWalkModifyer;
	myCurrentMovementState = MovementState::Walk;
}

void GBPhysXKinematicComponent::Crouch()
{
	myCurrentSpeedModifyerMode = myCrouchSpeedModifyer;
	myCurrentAccModifyerMode = AccCrouchModifyer;
	myGBPhysXCharacter->Crouch();

	myCurrentMovementState = MovementState::Crouch;
}

void GBPhysXKinematicComponent::Teleport(const V3F& aPosition)
{
	myGBPhysXCharacter->Teleport(aPosition);
}

V3F GBPhysXKinematicComponent::GetDeltaMovement()
{
	return myDeltaMovement;
}

GBPhysXCharacter* GBPhysXKinematicComponent::GetPhysXCharacter()
{
	return myGBPhysXCharacter;
}

void GBPhysXKinematicComponent::OnAttach()
{
}

void GBPhysXKinematicComponent::OnDetach()
{
}

void GBPhysXKinematicComponent::OnKillMe()
{
}
