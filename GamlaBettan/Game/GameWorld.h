#pragma once
#include "ComponentLake.h"
#include "Entity.h"
#include "MetricValue.h"
#include "Macros.h"
#include "Observer.hpp"
#include "Publisher.hpp"
#include "ObjectPool.hpp"
#include "ObjectPoolSizes.h"
#include <NodePollingStation.h>
#include "AIPollingStation.h"
#include "CinematicEditor.h"

#include "AnimationComponent.h"


namespace CommonUtilities
{
	class InputHandler;
}

class Octree;
class CCamera;
class ModelLoader;
class SpriteFactory;
class SpriteInstance;
class SpriteRenderer;
class EnemyFactory;
class StaticObjectFactory;
class DestructibleFactory;
class TriggerBoxFactory;
class PointLightFactory;
class ParticleFactory;
class AbilityFactory;
class UIManager;
class Scene;
class DirectX11Framework;
class Inventory;
class AudioManager;
class CharacterData;
class AbilityData;
class TextFactory;
class GBPhysX;
class GBPhysXColliderFactory;

class GameWorld : public Observer , public Publisher
{
public:
	GameWorld();
	~GameWorld();

	void Init(ModelLoader* aModelLoader, SpriteFactory* aSpriteFactory, Scene* aScene, DirectX11Framework* aFramework, Camera* aCamera,NodePollingStation* aNodePollingStation);
	void SystemLoad(ModelLoader* aModelLoader, SpriteFactory* aSpriteFactory, Scene* aScene, DirectX11Framework* aFramework, AudioManager* aAudioManager, GBPhysX* aGBPhysX, SpriteRenderer* aSpriteRenderer, LightLoader* aLightLoader);
	void Update(CommonUtilities::InputHandler& aInputHandler, float aDeltatime);
	void SetupPlayerAndCamera(CommonUtilities::Vector3<float> aSpawnPos);

	void ClearWorld(bool isShouldDeletePlayer = false);

	void RecieveMessage(const Message& aMessage) override;

	void RespawnPlayer(V3F& aPosition);
#if USEIMGUI
	bool myIsInModelViewerMode = false;
	void ImGuiNode(ImGuiNodePackage& aPackage);
#endif // !_RETAIL

	ParticleFactory* GetParticleFactory();


private:
#if USEIMGUI
	ImGuiNodePackage* myLastPackage;
#endif // USEIMGUI

	CinematicEditor myCinEditor;
	NodePollingStation* myNodePollingstationPtr;
	AIPollingStation* myAIPollingStation;
	void SpawnPlayer();
	void CreateHangingLanternAtPos(V3F aPosition);
	CommonUtilities::Vector2<float> myWindowSize;
	GAMEMETRIC(float, myFreecamSpeed, FREECAMSPEED, 0.3f);
	GAMEMETRIC(float, myFreecamRotationSpeed, FREECAMROTATIONSPEED, 0.3f);
	bool myUsingFreeCamera;
	Camera* myMainCameraPtr;

	Scene* myScenePtr;
	Octree* myObjectTree;

	CommonUtilities::ObjectPool<Entity> myEntityPool;
	std::vector<Entity*> myEntitys;
	unsigned int myEntityID;

	std::vector<Entity*> myEnemies;
	std::vector<Entity*> myTriggers;
	std::vector<Entity*> myPickups;


	Entity* myPlayer;

	EnemyFactory* myEnemyFactory;
	StaticObjectFactory* myStaticObjectFactory;
	DestructibleFactory* myDestructibleFactory;
	TriggerBoxFactory* myTriggerBoxFactory;
	PointLightFactory* myPointLightFactory;
	ParticleFactory* myParticleFactory;
	TextFactory* myTextFactory;

	GBPhysX* myGBPhysXPtr;
	GBPhysXColliderFactory* myGBPhysXColliderFactory;

	UIManager* myUIManager;
	CharacterData* myCharacterData;
	AbilityData* myAbilityData;

	std::unordered_map<AnimationComponent::States, std::pair<size_t, size_t>> myAnimMapping;
	bool myHasSetFollowCamera = false;

	float myTempBallTimer = 0.0f;
	bool myFirstFadeInComplete = false;
};

inline ParticleFactory* GameWorld::GetParticleFactory()
{
	return myParticleFactory;
}


