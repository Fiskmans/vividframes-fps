#pragma once

#include "Component.h"
#include <vector>
#include <array>


class AudioInstance;
class AudioManager;
class Entity;
struct FMOD_GUID;

enum class AudioEvent
{
	SILENCE,

	Spawn,
	Idle,
	Death,
	Hit,
	Activated,

	Footstep,

	Count
};

class Audio : public Component
{
public:
	Audio();
	~Audio();

	void PreInit(AudioManager* aAudioManager);
	virtual void Init(Entity* aEntity) override;
	virtual void Update(const float aDeltaTime) override;
	virtual void Reset() override;

	void CreateAudio(const FMOD_GUID& aSoundEventID, bool shouldFinnishPlaying = false);
	void PlayOneShot(const FMOD_GUID& aSoundEventID);
	void PlayOneShot2D(const FMOD_GUID& aSoundEventID);

	void PlayAudioEvent(AudioEvent aAudioEvent);
	void InitEventIDs();

protected:
	virtual void OnAttach() override;
	virtual void OnDetach() override;
	virtual void OnKillMe() override;

private:

	AudioManager* myAudioManager;
	Entity* myEntity;

	std::vector<AudioInstance*> myInstances;
	std::array<FMOD_GUID*, static_cast<int>(AudioEvent::Count)> myEventIDs;
};