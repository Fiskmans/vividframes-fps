#pragma once
#include "Component.h"

#include "Macros.h"
#include "Publisher.hpp"

class AIBaseState;
class AIPollingStation;
enum class AIStates;

namespace CommonUtilities
{
	template<class T>
	class Vector3;
}

class ComponentLake;

class AIController : public Component, public Publisher
{
public:

	AIController();
	~AIController();

	void Init(Entity* aEntity) override;

	void AIInit(AIPollingStation* aPollingStation);
	AIPollingStation* GetPollingStation();
	
	void Update(const float aDeltaTime) override;
	void Reset() override;

	virtual void Collided();
	virtual void RecieveEntityMessage(EntityMessage aMessage, void* someData) override;

	void SetTargetPosition(const V3F& aTargetPosition);
	V3F GetTargetPosition();

	void SwitchState(AIStates aState);

	void OnDisable() override;

	bool withinRangeOfPlayer = false;

protected:
	virtual void OnAttach() override;
	virtual void OnDetach() override;
	virtual void OnKillMe() override;

private:
	bool myIsDead = false;
	std::unordered_map<AIStates, AIBaseState*> myStates;
	AIStates myCurrentState;
	AIPollingStation* myPollingStation;
	AbilityData* myAbilityData;

	bool myIsAnimLocked;

	V3F myTargetPosition;

	float myUpdatePathfindingTimer;
};