#include "pch.h"
#include "GBPhysXQueryFilterCallback.h"

PxQueryHitType::Enum GBPhysXQueryFilterCallback::preFilter(const PxFilterData& filterData, const PxShape* shape, const PxRigidActor* actor, PxHitFlags& queryFlags)
{
	PX_UNUSED(actor);
	PX_UNUSED(queryFlags);
	if ((shape->getQueryFilterData().word0 & filterData.word1) && (filterData.word0 & shape->getQueryFilterData().word1))
	{
		return PxQueryHitType::eBLOCK;
	}
	return PxQueryHitType::eNONE;
}

PxQueryHitType::Enum GBPhysXQueryFilterCallback::postFilter(const PxFilterData& filterData, const PxQueryHit& hit)
{
	PxShape* shape = hit.shape;

	if ((filterData.word0 & shape->getQueryFilterData().word1) && (filterData.word1 & shape->getQueryFilterData().word0))
	{
		return PxQueryHitType::eBLOCK;
	}
	return PxQueryHitType::eNONE;
}
