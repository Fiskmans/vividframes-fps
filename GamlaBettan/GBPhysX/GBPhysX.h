#pragma once
#include <Vector3.hpp>
#include <Matrix4x4.hpp>
#include <vector>
#include "AnimationData.h"

namespace physx
{
	class PxRigidActor;
	class PxController;
	struct PxFilterData;
}
class GBPhysX;
class StaticMeshCooker;
class Entity;

namespace GBPhysXFilters
{
	enum CollisionFilter : UINT32
	{
		None = 0,
		Player = 1,
		Enemy = 2,
		Bullet = 4,
		EnvironmentStatic = 8,
		EnvironmentMoveBlock = 16,
		EnvironmentDynamic = 32,
		EnemyHitBox = 64,
		All = ~0U
	};
}

enum class GBPhysXGeometryType
{
	Cube,
	Sphere,
	Mesh,
	Count,
	None
};

struct BulletHitReport
{
	GBPhysXActor* actor;
	V3F position;
	V3F normal;
};

class GBPhysXActor
{
	friend GBPhysX;
public:
	GBPhysXActor();
	~GBPhysXActor();

	M44F GetTransformMatrix();
	V3F GetPosition();
	void RemoveFromScene();
	void Release();
	void SetIsKinematic(bool aIsKinematic);
	void SetKinematicTargetPos(V3F aPos);
	void ApplyForce(V3F aDirection, float aMagnitude);

	void SleepActor();

	void SetGBPhysXPtr(GBPhysX* aGBPhysXPtr);
	void SetEntity(Entity* aEntity);
	Entity* GetEntity();

	void SetRigidActor(physx::PxRigidActor* aRigidActor);
	physx::PxRigidActor* GetRigidActor();

	void SetIsHitBox(bool aIsHitBox);
	bool GetIsHitBox();

	std::string GetName();
protected:
	GBPhysX* myGBPhysX = nullptr;
	physx::PxRigidActor* myRigidActor = nullptr;

private:
	Entity* myEntity = nullptr;
	bool myIsKinematic = false;
	bool myIsHitBox = false;
};

class GBPhysXHitBox : public GBPhysXActor
{
public:
	void SetHitBox(HitBox aHitBox);
	HitBox GetHitBox();
private:
	HitBox myHitBox;
};

class GBPhysXCharacter : public GBPhysXActor
{
	friend GBPhysX;
public:
	GBPhysXCharacter();
	~GBPhysXCharacter();

	void Init(bool aIsPlayer);
	void Move(V3F aDirection, float aDeltaTime);
	void SetIsGrounded(bool aIsGrounded);
	bool GetIsGrounded() const;
	void Jump();
	void Crouch();
	void Stand();
	bool GetIsCrouching() const;
	void Teleport(V3F aPosition);
	void CreateHitBoxes();
	void UpdateHitBoxes();
	void Kill();
	void ReleaseHitBoxes();
	//void SetIsRagDoll(bool aIsRagDoll);
	//bool GetIsRagDoll();

	//void SetHasCreatedRagDoll(bool aHasCreatedRagDoll);

	//std::array<physx::PxRigidActor*, 64> GetMyRagDollActors();
	//void UpdateRagDollMatrices(std::array<CommonUtilities::Matrix4x4<float>, 64> & aMatrixes);
private:
	physx::PxController* myController = nullptr;
	physx::PxFilterData* myFilterData = nullptr;
	bool myIsCrouching;
	bool myIsGrounded;
	float myCurrentFallVelocity = 0.0f;
	//std::array<physx::PxRigidActor*, 64> myRagdollActors{ nullptr };
	//bool myIsRagDoll = false;
	//bool myHasCreatedRagDoll = false;
	std::vector<GBPhysXHitBox> myHitBoxes;
};



class GBPhysX
{
public:
	~GBPhysX();

	GBPhysXActor* GBCreateDynamicSphere(M44F aMatrixTransform, int aRadius, float aDensity = 10.0f);
	GBPhysXActor* GBCreateDynamicBox(V3F aPosition, V3F aSize, V3F aForce, float aDensity);
	GBPhysXActor* GBCreateStaticCube(V3F aPosition, float aHalfSize);
	GBPhysXActor* GBCreatePlayerBlockBox(V3F aPosition, V3F aSize, V3F aRotation);
	GBPhysXActor* GBCreateKinematicBox(V3F aPosition, V3F aSize, V3F aForce, float aDensity);
	std::vector<GBPhysXActor*> GBCreateChain(V3F aPosition, V3F aSize, int aLength, float aSeparation);

	std::vector<GBPhysXActor*> GBCreateHangingLantern(V3F aPosition);
	std::array<GBPhysXActor*, 8> GBCreateBarrelDestructable(V3F aPosition, V3F aRotation);
	std::array<GBPhysXActor*, 8> GBCreateBoxDestructable(V3F aPosition, V3F aRotation);

	GBPhysXCharacter* GBCreateCapsuleController(V3F aPosition, V3F aRotation, float aHeight, float aRadius, bool aIsPlayer);

	BulletHitReport RayPickActor(V3F aOrigin, V3F aDirection);
	//std::vector<GBPhysXActor> GBCreateBodyParts(V3F aPosition, int aEnemyType);
	//std::array<physx::PxRigidActor*, 64> GBCreateRagDoll(std::array<M44F, 64> boneMatrices);
	void GBCreateNavMesh(int aNumberOfVerts, std::vector<V3F> someVerts, int aNumberOfTriangles, std::vector<int> someIndices);
	void GBInitPhysics(bool aInteractive);
	void GBStepPhysics(float aDeltaTime);
	void GBCleanUpPhysics();
	bool GetGBPhysXActive();

	void GBSetKinematicActorTargetPos(physx::PxRigidActor* aActor, V3F aPosition, M44F aRotation);
	void GBApplyForceToActor(physx::PxRigidActor* aActor, V3F aForce);
	void GBResetScene();
	void RemoveActor(physx::PxRigidActor* aRigidActor);
	void ReleaseActor(physx::PxRigidActor* aRigidActor);

	//std::unordered_map<std::string, int> myCookedTriangleMeshIndexes;
	GBPhysXActor* CreateStaticTriangleMeshObject(std::string& aFilePath, M44F aTransform);
	bool CookStaticTriangleMesh(std::string& aFilePath, const aiScene* aAiScene, aiNode* aAiNode);

	void SetZombieHitBoxData(std::vector<HitBox>& aHitBoxes);
	std::vector<HitBox>& GetZombieHitBoxes();
private:
	void SetGBPhysXActive(bool aActive);
	bool myIsActive = false;
	StaticMeshCooker* myStaticMeshCooker;
	std::vector<HitBox> myZombieHitBoxes;
};