#pragma once
#include "PxQueryFiltering.h"
#include "PxShape.h"
#include "PxRigidActor.h"
using namespace physx;

class GBPhysXQueryFilterCallback : public PxQueryFilterCallback
{
	// Inherited via PxQueryFilterCallback
	virtual PxQueryHitType::Enum preFilter(const PxFilterData& filterData, const PxShape* shape, const PxRigidActor* actor, PxHitFlags& queryFlags) override;
	virtual PxQueryHitType::Enum postFilter(const PxFilterData& filterData, const PxQueryHit& hit) override;
};

