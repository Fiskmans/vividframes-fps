#pragma once



#include <Macros.h>


//Windows
#include <ws2tcpip.h>
#include <windows.h>

//DirectX
#include <d3dcommon.h>
#include <d3d11.h>

// Std
#pragma warning(push)
#pragma warning(disable : 4244)
#include <string>
#pragma warning(pop)
#include <vector>
#include <variant>
#include <future>
#include <array>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <iostream>
#include <typeindex>
#include <algorithm>
#include <set>
#include <iostream>
#include <sstream>
#include <fstream>
#include <stdio.h>
#include <mutex>
#include <stack>
#include <queue>
#define _SILENCE_EXPERIMENTAL_FILESYSTEM_DEPRECATION_WARNING
#include <experimental/filesystem>

//C
#include <cassert>

//CommonUtilities
#include <Matrix.hpp>
#include <Vector.hpp>
#include <PostMaster.hpp>
#include <Observer.hpp>
#include <Publisher.hpp>

//TOOLS
#include <Logger.h>
#include <FiskJSON.h>
#include <MetricValue.h>
#include <TimeHelper.h>

//Gamla Bettan
#include <ModelInstance.h>
#include <DebugDrawer.h>
#include <PathFinder.h>
#include <Camera.h>
#include <Scene.h>
#include <TextureLoader.h>

//Game
#include <Entity.h>
#include <Mesh.h>
#include <Light.h>
#include <ParticleOnDeath.h>
#include <ParticlesWhileAlive.h>
#include <Audio.h>
#include <FollowCamera.h>
#include <Collision.h>
#include <Life.h>
#include <LifeTime.h>
#include <Movement3D.h>
#include <PlayerController.h>
#include <CharacterInstance.h>
#include <AbilityInstance.h>
#include <ParticleOnHit.h>
#include "AssetImportHelpers.h"

//Conditionals
#if USEIMGUI
#include <imgui.h>
#include <ImGuiHelpers.h>
#endif

#ifdef _DEBUG
#include <DebugTools.h>
#endif

#if USEFILEWATHCER
#include <FileWatcher.h>
#endif