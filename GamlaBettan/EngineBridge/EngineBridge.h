#pragma once
#include "framework.h"
#include <maya/MDagPath.h>
#include <FBXLoaderCustom.h>

class EngineBridge
{
public:
	EngineBridge(ID3D11Device* aDevice, ID3D11DeviceContext* aContext);
	~EngineBridge();

	bool SetupShadersAndInputLayout(ID3D11VertexShader** vsShader, ID3D11PixelShader** psShader, ID3D11InputLayout** aLayout);
	bool LoadTextures(const MDagPath& aPath, ID3D11ShaderResourceView** aAlbedo, ID3D11ShaderResourceView** aNormals, ID3D11ShaderResourceView** aMaterial);

	bool LoadModel(const MDagPath& aPath, CLoaderModel** aModel);

	void SetupRender();

private:

	class CGraphicsEngine* Engine;
	class CScene* myScene;
	ID3D11Device* myDevice;
	ID3D11DeviceContext* myContext;
	std::wstring myRootPath;
	ID3D11ShaderResourceView* mySkybox = nullptr;
	
	
};