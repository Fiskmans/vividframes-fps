// EngineBridge.cpp : Defines the functions for the static library.
//

#include "pch.h"
#include "framework.h"
#include <maya/MDagPath.h>
#include <d3d11.h>
#include "EngineBridge.h"
#include <d3dcompiler.h>
#pragma comment(lib, "d3dcompiler.lib")
#include <string>
#include <comdef.h>
#include "../GamlaBettan/DirectXTK/Inc/DDSTextureLoader.h"
#include <maya/MFnMesh.h>
#include <maya/MFnDagNode.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnDependencyNode.h>
#include <maya/MPlug.h>
#include <maya/MItDag.h>
#include <shlobj.h>

//ours
#include <GraphicEngine.h>
#include <Scene.h>
#include <Skybox.h>
#include <WindowHandler.h>

#define STRING(arg) #arg
#define STRINGVALUE(arg) STRING(arg)

#define NUMBEROFPOINTLIGHTS 1
const D3D_SHADER_MACRO defines[] =
{
	STRING(NUMBEROFPOINTLIGHTS), STRINGVALUE(NUMBEROFPOINTLIGHTS),
	NULL, NULL
};
using namespace DirectX;
bool LoadVertexShader(ID3D11Device* aDevice, std::wstring aFilePath, ID3D11VertexShader*& aShaderOutput, void* aCompiledOutput)
{
	UINT flags = D3DCOMPILE_ENABLE_STRICTNESS;
#ifdef _DEBUG 
	flags |= D3DCOMPILE_DEBUG;
#endif
	ID3DBlob** shaderBlob = static_cast<ID3DBlob**>(aCompiledOutput);
	ID3DBlob* errorBlob = nullptr;
	HRESULT hr = D3DCompileFromFile(aFilePath.c_str(), defines, D3D_COMPILE_STANDARD_FILE_INCLUDE, "vertexShader", "vs_5_0", flags, 0, shaderBlob, &errorBlob);

	if (FAILED(hr))
	{
		if (errorBlob)
		{
			errorBlob->Release();
		}

		if ((*shaderBlob))
		{
			(*shaderBlob)->Release();
		}

		return false;
	}
	hr = aDevice->CreateVertexShader((*shaderBlob)->GetBufferPointer(), (*shaderBlob)->GetBufferSize(), nullptr, &aShaderOutput);


	if (FAILED(hr))
	{
		return false;
	}

	return true;
}

bool LoadPixelShader(ID3D11Device* aDevice, std::wstring aFilePath, ID3D11PixelShader*& aShaderOutput)
{
	UINT flags = D3DCOMPILE_ENABLE_STRICTNESS;
#if defined( DEBUG ) || defined( _DEBUG )
	flags |= D3DCOMPILE_DEBUG;
#endif
	ID3DBlob* shaderBlob = nullptr;
	ID3DBlob* errorBlob = nullptr;


	HRESULT hr = D3DCompileFromFile(aFilePath.c_str(), defines, D3D_COMPILE_STANDARD_FILE_INCLUDE, "pixelShader", "ps_5_0", flags, 0, &shaderBlob, &errorBlob);

	if (FAILED(hr))
	{
		MessageBoxW(nullptr, _com_error(hr).ErrorMessage(), L"Error", MB_OK);
		MessageBoxA(nullptr, static_cast<char*>(errorBlob->GetBufferPointer()), "Error", MB_OK);


		if (errorBlob)
		{
			errorBlob->Release();
		}

		if (shaderBlob)
		{
			shaderBlob->Release();
		}

		return false;
	}

	hr = aDevice->CreatePixelShader(shaderBlob->GetBufferPointer(), shaderBlob->GetBufferSize(), nullptr, &aShaderOutput);

	shaderBlob->Release();

	if (FAILED(hr))
	{
		return false;
	}

	return true;
}

static int CALLBACK BrowseCallbackProc(HWND hwnd, UINT uMsg, LPARAM lParam, LPARAM lpData)
{
	if (uMsg == BFFM_INITIALIZED)
	{
		std::string tmp = (const char*)lpData;
		std::cout << "path: " << tmp << std::endl;
		SendMessage(hwnd, BFFM_SETSELECTION, TRUE, lpData);
	}

	return 0;
}
std::wstring BrowseFolder(std::wstring saved_path)
{
	TCHAR path[MAX_PATH];

	const wchar_t* path_param = saved_path.c_str();

	BROWSEINFO bi = { 0 };
	bi.lpszTitle = (L"Select Game Folder");
	bi.ulFlags = BIF_RETURNONLYFSDIRS | BIF_NEWDIALOGSTYLE;
	bi.lpfn = BrowseCallbackProc;
	bi.lParam = (LPARAM)path_param;

	LPITEMIDLIST pidl = SHBrowseForFolder(&bi);

	if (pidl != 0)
	{
		//get the name of the folder and put it in path
		SHGetPathFromIDList(pidl, path);

		//free memory used
		IMalloc* imalloc = 0;
		if (SUCCEEDED(SHGetMalloc(&imalloc)))
		{
			imalloc->Free(pidl);
			imalloc->Release();
		}

		return path;
	}

	return L"";
}

const LPWSTR desktop_directory()
{
	static TCHAR buffer[MAX_PATH];
	if (SHGetSpecialFolderPath(HWND_DESKTOP, buffer, CSIDL_DESKTOP, FALSE)) {
		return buffer;
	}
	else
	{
		return LPWSTR(L"C:");
	}
}

#define _SILENCE_EXPERIMENTAL_FILESYSTEM_DEPRECATION_WARNING
#include <experimental/filesystem>

BOOL DirectoryExists(LPCTSTR szPath)
{
	DWORD dwAttrib = GetFileAttributes(szPath);

	return (dwAttrib != INVALID_FILE_ATTRIBUTES &&
		(dwAttrib & FILE_ATTRIBUTE_DIRECTORY));
}

std::wstring FindRoot(std::wstring aStartPosition)
{
	using namespace std::experimental::filesystem;
	recursive_directory_iterator it(aStartPosition);
	while (it != recursive_directory_iterator())
	{
		std::wstring at = it.operator->()->path().c_str();
		if (DirectoryExists((at + L"/Game").c_str()) == TRUE && DirectoryExists((at + L"/.svn").c_str()) == TRUE)
		{
			return at + L"/Game";
		}
		it++;
	}
	MessageBoxA(nullptr, "could not find root", "Error", MB_OK);
	return aStartPosition;
}

EngineBridge::EngineBridge(ID3D11Device* aDevice, ID3D11DeviceContext* aContext)
{
	Engine = new CGraphicsEngine();
	WindowHandler::SWindowData data;
	data.myWidth = 1000;
	data.myHeight = 1000;
	data.myX = 0;
	data.myY = 0;
	Engine->Init(data, aDevice,aContext);
	myScene = new CScene();

	//skyboxTextureName
	myRootPath = desktop_directory();
	myRootPath = FindRoot(myRootPath);
	myDevice = aDevice;
	myContext = aContext;

	std::wstring skyboxName = L"../angySkybox.dds";
	{
		MItDag iter(MItDag::kBreadthFirst, MFn::kTransform);

		for (; !iter.isDone(); iter.next())
		{
			MStatus status;
			MFnDependencyNode dep(iter.currentItem());


			MPlug plug = dep.findPlug("skyboxTextureName", false, &status);
			if (status == MStatus::kSuccess)
			{
				skyboxName = plug.asString().asWChar();
			}
		}
	}

	std::wstring filepath = myRootPath + L"/Data/Textures/SkyBoxes/" + skyboxName;
	   

	Skybox* skyBox = Engine->GetModelLoader().InstanciateSkybox(filepath);


	HRESULT result = CreateDDSTextureFromFileEx(
		myDevice,
		filepath.c_str(),
		0,D3D11_USAGE::D3D11_USAGE_DEFAULT,D3D11_BIND_FLAG::D3D11_BIND_SHADER_RESOURCE,0,0,false,
		/* D3DX11_IMAGE_LOAD_INFO * */ NULL,
		&mySkybox
	);
	if (FAILED(result))
	{
		MessageBoxW(nullptr, (L"Could not load skyboxTexture: " + filepath).c_str(), L"Error", MB_OK);
		mySkybox = nullptr;
	}

}

EngineBridge::~EngineBridge()
{
	delete myScene;
	delete Engine;
}

bool EngineBridge::SetupShadersAndInputLayout(ID3D11VertexShader** vsShader, ID3D11PixelShader** psShader, ID3D11InputLayout** aLayout)
{
	HRESULT result;
	ID3DBlob* vsBlob;
	if (!LoadVertexShader(myDevice, myRootPath + L"/Data/Shaders/VertexShader.hlsl", *vsShader, &vsBlob))
	{
		return false;
	}
	if (!LoadPixelShader(myDevice, myRootPath + L"/Data/Shaders/PBR.hlsl", *psShader))
	{
		return false;
	}
	static_assert(sizeof(float) * CHAR_BIT == 32 && CHAR_BIT == 8, "is a fuck");
	D3D11_INPUT_ELEMENT_DESC layout[] =
	{
		{"POSITION" ,0,DXGI_FORMAT_R32G32B32A32_FLOAT,0,D3D11_APPEND_ALIGNED_ELEMENT,D3D11_INPUT_PER_VERTEX_DATA,0},
		{"NORMAL"   ,0,DXGI_FORMAT_R32G32B32A32_FLOAT,0,D3D11_APPEND_ALIGNED_ELEMENT,D3D11_INPUT_PER_VERTEX_DATA,0},
		{"TANGENT"  ,0,DXGI_FORMAT_R32G32B32A32_FLOAT,0,D3D11_APPEND_ALIGNED_ELEMENT,D3D11_INPUT_PER_VERTEX_DATA,0},
		{"BITANGENT",0,DXGI_FORMAT_R32G32B32A32_FLOAT,0,D3D11_APPEND_ALIGNED_ELEMENT,D3D11_INPUT_PER_VERTEX_DATA,0},
		{"UV"		,0,DXGI_FORMAT_R32G32_FLOAT		 ,0,D3D11_APPEND_ALIGNED_ELEMENT,D3D11_INPUT_PER_VERTEX_DATA,0}
	};


	result = myDevice->CreateInputLayout(layout, sizeof(layout) / sizeof(layout[0]), vsBlob->GetBufferPointer(), vsBlob->GetBufferSize(), aLayout);

	if (FAILED(result))
	{
		MessageBoxW(nullptr, _com_error(result).ErrorMessage(), L"Input error", MB_OK);
		return false;
	}
	return true;
}

bool EngineBridge::LoadTextures(const MDagPath& aPath, ID3D11ShaderResourceView** aAlbedo, ID3D11ShaderResourceView** aNormals, ID3D11ShaderResourceView** aMaterial)
{

	ID3D11ShaderResourceView* albedo;
	ID3D11ShaderResourceView* material;
	ID3D11ShaderResourceView* normal;
	HRESULT result;
	MFnDagNode node(aPath.node());
	MFnDagNode parent(node.parent(0));
	if (!parent.isParentOf(node.object()))
	{
		return false;
	}

	MStatus status;
	MObject attr = parent.attribute("originalModelName", &status);

	std::wstring origName;

	if (status == MStatus::kSuccess)
	{
		if (((MFnTypedAttribute)attr).attrType() == MFnData::kString)
		{
			MFnDependencyNode dep(parent.object());

			
			MPlug plug = dep.findPlug("originalModelName",false,&status);
			if (status == MStatus::kSuccess)
			{
				origName = plug.asString().asWChar();
			}
		}
	}
	else
	{
		MessageBoxA(nullptr, attr.apiTypeStr(), "Failure", MB_OK);
	}

	std::wstring path = myRootPath + L"/Data/Models/" + origName + L"/" + origName;

	result = CreateDDSTextureFromFile(
		myDevice,
		(path + L"_mat_D.dds").c_str(),
		/* D3DX11_IMAGE_LOAD_INFO * */ NULL,
		aAlbedo
	);
	if (FAILED(result))
	{
		return false;
	}

	result = CreateDDSTextureFromFile(
		myDevice,
		(path + L"_mat_N.dds").c_str(),
		/* D3DX11_IMAGE_LOAD_INFO * */ NULL,
		aNormals
	);
	if (FAILED(result))
	{
		return false;
	}

	result = CreateDDSTextureFromFile(
		myDevice,
		(path + L"_mat_M.dds").c_str(),
		/* D3DX11_IMAGE_LOAD_INFO * */ NULL,
		aMaterial
	);
	if (FAILED(result))
	{
		return false;
	}
	return true;
}

bool EngineBridge::LoadModel(const MDagPath& aPath, CLoaderModel** aModel)
{

	HRESULT result;
	std::string origName;
	MFnDagNode node(aPath.node());
	if (node.parentCount() > 0U)
	{
		MFnDagNode parent(node.parent(0));
		if (!parent.isParentOf(node.object()))
		{
			return false;
		}
		MStatus status;
		MObject attr = parent.attribute("originalModelName", &status);

		if (status == MStatus::kSuccess)
		{
			if (((MFnTypedAttribute)attr).attrType() == MFnData::kString)
			{
				MFnDependencyNode dep(parent.object());


				MPlug plug = dep.findPlug("originalModelName", false, &status);
				if (status == MStatus::kSuccess)
				{
					origName = plug.asString().asChar();
				}
			}
		}
		else
		{
			MessageBoxA(nullptr, attr.apiTypeStr(), "Failure", MB_OK);
		}
	}
	

	std::string path = std::string(myRootPath.begin(),myRootPath.end()) + "/Data/Models/" + origName + "/" + origName + ".fbx";


	CFBXLoaderCustom loader;
	*aModel = loader.LoadModel(path.c_str());

	if (!*aModel)
	{
		return false;
	}
	return true;
}

void EngineBridge::SetupRender()
{
	if (mySkybox)
	{
		myContext->PSSetShaderResources(3, 1, &mySkybox);
	}
}

