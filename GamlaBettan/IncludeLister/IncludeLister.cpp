// IncludeLister.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
#include <pch.h>

#include <iostream>
#include <fstream>
#include <filesystem>
#include <string>
#include <vector>
#include <sstream>
#include <stdio.h>

std::string ReadWholeFile(const std::string& aFilePath)
{
	std::ifstream t(aFilePath);
	t.seekg(0, std::ios::end);
	size_t size = t.tellg();
	std::string buffer(size, ' ');
	t.seekg(0);
	t.read(&buffer[0], size);
	return buffer;
}

std::unordered_map<std::string, std::vector<std::string>> SearchForIncludes(const std::string& aFolder)
{
	static std::unordered_set<std::string> validExtensions = {".h",".hpp",".c",".cpp"};
	static const char* Filter = "#include";
	static const int FilterLength = strlen(Filter);

	std::unordered_map<std::string, std::vector<std::string>> depends;
	std::filesystem::recursive_directory_iterator it(aFolder);
	while (it != std::filesystem::recursive_directory_iterator())
	{
		if (it->is_regular_file())
		{
			std::filesystem::path path = it->path();
			if (path.has_extension() && validExtensions.count(path.extension().string()) != 0)
			{
				std::string filePath = path.string();
				std::string fileName = path.filename().string();
				std::string fileContent = ReadWholeFile(filePath);
				std::vector<std::string> includes;
				size_t at = 0;
				while (true)
				{
					size_t next = fileContent.find(Filter, at);
					if (next == std::string::npos) { break; }
					size_t start = fileContent.find_first_of("\"<", next+FilterLength);
					if (start == std::string::npos) { break; }
					size_t end = fileContent.find_first_of("\">", start+1);
					if (end == std::string::npos) { break; }

					includes.push_back(fileContent.substr(start+1,end-start-1));
					at = end;
				}

				depends[fileName] = includes;
			}
		}
		++it;
	}
	return depends;
}

std::vector<std::string> GeneratePrintStringCollection(const std::unordered_map<std::string, std::vector<std::string>>& aDepends, const std::string& aFile, std::unordered_set<std::string> visited = {})
{
	std::vector<std::string> coll;
	coll.push_back(aFile);
	size_t rowToHighlight = 0;
	std::unordered_set<size_t> marks;
	visited.emplace(aFile);
	if (aDepends.count(aFile) != 0)
	{
		const std::vector<std::string>& collection = aDepends.at(aFile);
		for (auto& includedFile : collection)
		{
			if (visited.count(includedFile) == 0)
			{
				std::vector<std::string> dependcoll = GeneratePrintStringCollection(aDepends, includedFile,visited);
				rowToHighlight = coll.size();
				marks.emplace(coll.size());
				for (auto& i : dependcoll)
				{
					coll.push_back(i);
				}
			}
			else
			{
				rowToHighlight = coll.size();
				marks.emplace(coll.size());
				coll.push_back("Circular include");
			}
		}
	}
	for (size_t i = 1; i < coll.size(); i++)
	{
		if (i < rowToHighlight)
		{
			if (marks.count(i) != 0)
			{
				coll[i] = "\xCC\xCD" + coll[i];
			}
			else
			{
				coll[i] = "\xBA " + coll[i];
			}
		}
		else if (i == rowToHighlight)
		{
			coll[i] = "\xC8\xCD" + coll[i];
		}
		else
		{
			coll[i] = "  " + coll[i];
		}
	}

	return coll;
}

std::string GeneratePrintString(const std::unordered_map<std::string, std::vector<std::string>>& aDepends, const std::string& aFile)
{
	std::stringstream stream;
	for (auto& i : GeneratePrintStringCollection(aDepends,aFile))
	{
		stream << i << "\n";
	}
	return stream.str();
}

size_t DepthToBottom(const std::unordered_map<std::string, std::vector<std::string>>& aDagGraph, const std::string& aQuery,std::unordered_map<std::string,size_t>& aCache, std::unordered_set<std::string> passed = {})
{
	size_t out = 0;
	if (passed.count(aQuery) != 0)
	{
		return out;
	}
	passed.emplace(aQuery);

	if (aCache.find(aQuery) != aCache.end())
	{
		return aCache[aQuery];
	}
	if (aDagGraph.find(aQuery) != aDagGraph.end())
	{
		for (const auto& i : aDagGraph.at(aQuery))
		{
			out = MAX(out,DepthToBottom(aDagGraph,i, aCache,passed)+1);
		}
	}
	return out;
}

std::string ConvertToSVG(const std::unordered_map<std::string, std::vector<std::string>>& aDagGraph)
{
	std::unordered_map<std::string, size_t> heights;
	size_t maxvalue = 0;
	for (auto& i : aDagGraph)
	{
		heights[i.first] = DepthToBottom(aDagGraph, i.first,heights);
		maxvalue = MAX(maxvalue, heights[i.first]);
	}
	std::vector<std::vector<std::string>> buckets;
	buckets.resize(maxvalue +1);
	for (auto& i : aDagGraph)
	{
		buckets[heights.at(i.first)].push_back(i.first);
	}
	std::unordered_map<std::string, size_t> depth;
	for (size_t y = 0; y < maxvalue; y++)
	{
		size_t x = 0;
		for (auto& i : buckets[y])
		{
			depth[i] = x++;
		}
	}
	std::unordered_map<std::string, size_t> ids;
	size_t idCounter = 0;
	for (auto& i : aDagGraph)
	{
		ids[i.first] = idCounter++;
	}

	std::stringstream stream;

	stream << R"(<?xml version="1.0" encoding="UTF-8" standalone="no"?>)" << "\n";
	stream << R"(<svg xmlns:dc="http://purl.org/dc/elements/1.1/"
xmlns:cc="http://creativecommons.org/ns#"
xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
xmlns:svg="http://www.w3.org/2000/svg"
xmlns="http://www.w3.org/2000/svg"
xmlns:xlink="http://www.w3.org/1999/xlink"
xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd"
xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape"
width="500mm"
height="500mm"
viewBox="0 0 500 500"
version="1.1"
id="svg8"
sodipodi:docname = "testtest.svg" > )";
	stream << R"(<g id="layer1">)" << "\n";

	const auto id = [&idCounter]() -> std::string
	{
		return std::to_string(idCounter++);
	};
	for (auto& i : aDagGraph)
	{
		size_t x1 = depth[i.first] * 50;
		size_t y1 = heights[i.first] * 50;
		std::string cid = id();
		std::string circle = "<circle id=\"path" + cid + "\"\n";
		circle += "cx=\"" + std::to_string(x1) + ".0\"\n";
		circle += "cy=\"" + std::to_string(y1) + ".0\"\n";
		circle += "style = \"stroke-width:0.25\"\n";
		circle += "r=\"8.5\" />\n";
		stream << circle;

		std::string text =
			"<text \n"
			"\txml:space=\"preserve\"\n"
			"\tstyle=\"font-style:normal;font-weight:normal;font-size:4.23333333px;line-height:1.25;font-family:sans-serif;letter-spacing:0px;word-spacing:0px;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.26458332;-inkscape-font-specification:'sans-serif, Normal';font-stretch:normal;font-variant:normal;text-anchor:start;text-align:start;writing-mode:lr;font-variant-ligatures:normal;font-variant-caps:normal;font-variant-numeric:normal;font-feature-settings:normal;\"\n"
			"\tid=\"text" + id() + "\" > <textPath\n"
			"\txlink:href=\"#path" + cid + "\"\n"
			"\tid=\"textPath" + id() + "\"><tspan\n"
			"\tid=\"tspan" + id() + "\"\n"
			"\tstyle = \"stroke-width:0.26458332;-inkscape-font-specification:'sans-serif, Normal';font-family:sans-serif;font-weight:normal;font-style:normal;font-stretch:normal;font-variant:normal;font-size:4.23333333px;text-anchor:start;text-align:start;writing-mode:lr;font-variant-ligatures:normal;font-variant-caps:normal;font-variant-numeric:normal;font-feature-settings:normal;\">" + i.first + "</tspan></textPath></text>\n";
		stream << text;

		for (auto& it : aDagGraph.at(i.first))
		{
			if (aDagGraph.find(it) != aDagGraph.end())
			{
				size_t x2 = depth[it] * 50;
				size_t y2 = heights[it] * 50;
				std::string line =
					"<path \n"
					"\tstyle=\"fill:none;stroke:#000000;stroke-width:0.26458332px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1\" \n"
					"\td=\"M " + std::to_string(x1) + ".0," + std::to_string(y1) + ".0 " + std::to_string(x2) + ".0," + std::to_string(y2) + ".0\" \n"
					"\tid=\"path" + id() + "\" \n"
					"\tinkscape:connector - curvature = \"0\" />\n";
				stream << line;

			}
		}


	}
	stream << "</g>\n";
	stream << "</svg>\n";

	return stream.str();
}

int main(int argc, char* argv[])
{
	std::cout << "Args: " << std::endl;
	for (size_t i = 0; i < argc; i++)
	{
		std::cout << argv[i] << std::endl;
	}
	for (size_t i = 2; i < argc; i++)
	{
		std::unordered_map<std::string, std::vector<std::string>> depends = SearchForIncludes(argv[i]);
		{
			std::ofstream dagout;
			std::string path = std::string(argv[i]);
			size_t end = path.find_last_of("\\");
			size_t begin = path.find_last_of("\\",end-1);
			std::string name = path.substr(begin+1, end - begin-1);
			std::cout << "stripped: " << name << std::endl;
			dagout.open("includeGraph_" + name + ".svg");
			dagout << ConvertToSVG(depends);
		}
	}
	if (argc > 1)
	{
		std::unordered_map<std::string, std::vector<std::string>> depends = SearchForIncludes(argv[1]);
		{
			std::ofstream dagout;
			dagout.open("includeGraph.svg");
			dagout << ConvertToSVG(depends);
		}


		std::unordered_set<std::string> rootFiles;
		for (auto& file : depends)
		{
			rootFiles.emplace(file.first);
		}
		for (auto& origfile : depends)
		{
			for (auto& includedFilename : origfile.second)
			{
				rootFiles.erase(includedFilename);
			}
		}
		size_t count = 0;
		for (auto& i : depends)
		{
			count += i.second.size();
		}
		std::cout << "Average includes per file: " << (count / depends.size()) << std::endl;
		std::cout << "Total includes: " << (count ) << std::endl;
		std::cout << "Total files: " << (depends.size()) << std::endl;


		while (true)
		{
			std::cout << "Enter File To Query: ";
			std::string query;
			std::cin >> query;
			if (depends.count(query) != 0)
			{
				std::cout << GeneratePrintString(depends, query) << std::endl;
			}
			else
			{
				std::cout << "404 File not found" << std::endl;
			}
			std::cout << "included by: " << std::endl;
			for (auto& i : depends)
			{
				for (auto& f : i.second)
				{
					if (f == query)
					{
						std::cout << "\t" << i.first << std::endl;
					}
				}
			}
		}

	}


	system("pause");
}