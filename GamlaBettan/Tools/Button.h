#pragma once
#include <functional>
#include "Vector4.hpp"
#include "Vector2.hpp"
#include "Observer.hpp"
#include <array>

class SpriteFactory;
class SpriteInstance;
class Button : public Observer
{
public:
	Button();
	~Button();
	void SetOnPressedFunction(const std::function<void(void)> aOnPressed);
	void TriggerOnPressed();
	void SetActive();
	void SetPressed();
	void Deactivate();
	SpriteInstance* GetCurrentSprite();
	SpriteInstance* GetTopBorderSprite();
	SpriteInstance* GetLowerBorderSprite();

	bool CheckHover(float aMouseX, float aMouseY);
	bool Init(const std::string& aFolderPath, const std::string& aButtonName, const CommonUtilities::Vector2<float>& aPosition, 
		const V2F& aHitBoxSize = V2F(1.f, 1.f), SpriteFactory* aSpriteFactory = ourSpriteFactoryPtr);
	bool IsActive();
	bool IsPressed();
	void RecieveMessage(const Message& aMessage) override;

	void Subscribe();
	void Unsubscribe();

	static void SetSpriteFactory(SpriteFactory* aSpriteFactory);
	//sorry...

	static SpriteFactory* GetSpriteFactory();
private:
	void SetupBounds();
	enum ButtonState
	{
		Inactive,
		Active,
		Pressed
	};
	std::function<void(void)> myOnPressed;
	ButtonState myState;
	std::array<SpriteInstance*, 3> mySprites;

	CommonUtilities::Vector4<float> myBounds;
	CommonUtilities::Vector2<float> myPosition;
	CommonUtilities::Vector2<float> myScreenSize;
	CommonUtilities::Vector2<float> mySize;

	static SpriteFactory* ourSpriteFactoryPtr;
	bool myIsListening;
};

