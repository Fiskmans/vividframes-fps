#pragma once
#include <string>

class Entity;

enum class MessageType
{
	None,

	//Input
	InputAccept,
	InputBack,

	InputUpHit,
	InputUpIsDown,
	InputUpReleased,

	InputDownHit,
	InputDownIsDown,
	InputDownReleased,

	InputLeftHit,
	InputLeftIsDown,
	InputLeftReleased,

	InputReload,

	InputRightHit,
	InputRightIsDown,
	InputRightReleased,
	InputEscHit,

	InputPauseHit,
	InputUnPauseHit,

	InputMouseMoved,
	InputMouseStopedMoving,
	InputLeftMouseHit,
	InputLeftMouseDown,
	InputShootButtonDown,
	InputLeftMouseReleased,
	InputRightMouseHit,
	InputRightMouseDown,
	InputRightMouseReleased,
	InputMiddleMouseHit,
	InputMiddleMouseDown,

	InputJumpHit,
	InputCrouchHit,
	InputCrouchReleased,
	InputRunHit,
	InputRunReleased,
	InputInteractHit,

	InputFPSMode,

	//STATES
	PushState,
	PopState,

	NextRenderPass,
	MainMenuStateActivated,

	//LEVEL
	StartLoading,
	UnloadLevel,
	NewLevelLoaded,
	NewOctreeCreated,
	CurrentLevel,
	ChangeLevel,
	NextLevel,
	LoadLevel,

	//PLAYER
	PlayerDying,
	PlayerDied,
	SetPlayerMaxHP,
	SetPlayerHP,
	PlayerWalking,
	PlayerStoppedWalking,

	//Enemies
	EnemyTakeDamage,
	EnemyDied,
	EnemyMouseHover,
	EnemyMouseHoverReleased,

	//Spawn
	SpawnEnemy,
	SpawnStaticObject,
	SpawnDestructibleObject,
	SpawnTriggerBox,
	SpawnPointLight,
	CreateDebris,
	SpawnAbility,
	SpawnAbilityAt,
	CreateDamageNumber,
	SpawnGBPhysXBox,
	SpawnBulletHitParticle,

	//Event
	FadeIn,
	FadeInComplete,
	FadeOut,
	FadeOutComplete,
	FinnishGame,
	StartCameraShake,
	TriggerSplineSpawn,
	TriggerEvent,
	WindowResize,
	ResizeWindow,
	GameActive,
	GameNotActive,
	BulletHit,

	//Audio
	ActivateReverb,
	DeactivateReverb,

	//Secrets
	EnableDiscoMode,
	EnableHastfan,
	SuperTinyWindow,
	WindowSmallerThanAMouse,
	PleaseResetTheWindowThanks,
	GiveMeAdam,
	EnableScreeps,

	//Menu
	MenuButtonActive,
	MenuButtonHit,

	count,
};

class BaseState;

class Message
{
public:
	Message()
	{
		MessageType myMessageType = MessageType::None;
		std::string_view myText = "";
		int myIntValue = 0;
		int myIntValue2 = 0;
		float myFloatValue = 0.0f;
		float myFloatValue2 = 0.0f;
		bool myBool = false;
		BaseState* myState = nullptr;
		void* myData = nullptr;
	}

	Message(const MessageType& aMessageType)
	{
		myMessageType = aMessageType;
	}

	MessageType myMessageType;
	std::string_view myText;
	int myIntValue;
	int myIntValue2;
	float myFloatValue;
	float myFloatValue2;
	bool myBool;
	void* myData;
};