#include <pch.h>
#include "Button.h"
#include "..\\GamlaBettan\SpriteInstance.h"
#include "..\\GamlaBettan\SpriteFactory.h"
#include "PostMaster.hpp"
SpriteFactory* Button::ourSpriteFactoryPtr = nullptr;

Button::Button()
{
	myState = ButtonState::Inactive;
	myOnPressed = nullptr;
	mySprites[ButtonState::Inactive] = nullptr;
	mySprites[ButtonState::Active] = nullptr;
	mySprites[ButtonState::Pressed] = nullptr;
	myBounds = { 0,0,0,0 };
	myPosition = { 0,0 };
	myScreenSize = { 1600.f, 900.f };
	myIsListening = false;
}

Button::~Button()
{
	Unsubscribe();

	//for (int i = 0; i < mySprites.size(); ++i)
	//{
	//	if (mySprites[i])
	//	{
	//		mySprites[i] = nullptr;
	//	}
	//}
	//SAFE_DELETE(mySprites[ButtonState::Active]);
	//SAFE_DELETE(mySprites[ButtonState::Inactive]);
	//SAFE_DELETE(mySprites[ButtonState::Pressed]);
}

void Button::SetOnPressedFunction(const std::function<void(void)> aOnPressed)
{
	myOnPressed = aOnPressed;
}

void Button::TriggerOnPressed()
{
	if (myOnPressed != nullptr)
	{
		myOnPressed();
	}
}

SpriteInstance* Button::GetCurrentSprite()
{
	if (mySprites[myState] != nullptr)
	{
		return mySprites[myState];
	}
	return nullptr;
}

void Button::SetActive()
{
	myState = ButtonState::Active;

	Message message;
	message.myMessageType = MessageType::MenuButtonActive;
	PostMaster::GetInstance()->SendMessages(message);
}

void Button::SetPressed()
{
	myState = ButtonState::Pressed;

	Message message;
	message.myMessageType = MessageType::MenuButtonHit;
	PostMaster::GetInstance()->SendMessages(message);
}

void Button::Deactivate()
{
	myState = ButtonState::Inactive;
}

bool Button::CheckHover(float aMouseX, float aMouseY)
{
	if (aMouseX >= myBounds.x && aMouseX <= myBounds.z)
	{
		if (aMouseY >= myBounds.y && aMouseY <= myBounds.w)
		{
			return true;
		}
	}
	return false;
}

bool Button::Init(const std::string& aFolderPath, const std::string& aButtonName, const CommonUtilities::Vector2<float>& aPosition,
	const V2F& aHitBoxSize, SpriteFactory* aSpriteFactory)
{
	myPosition = aPosition;

	std::string path = aFolderPath + "\\" + aButtonName;
	mySprites[ButtonState::Inactive] = aSpriteFactory->CreateSprite(path + " button off.dds");
	mySprites[ButtonState::Inactive]->SetPosition(myPosition);

	mySprites[ButtonState::Active] = aSpriteFactory->CreateSprite(path + " button hover.dds");
	mySprites[ButtonState::Active]->SetPosition(myPosition);

	mySprites[ButtonState::Pressed] = aSpriteFactory->CreateSprite(path + " button on.dds");
	mySprites[ButtonState::Pressed]->SetPosition(myPosition);
	
	mySize = aHitBoxSize;
	
	SetupBounds();

	Subscribe();

	return true;
}

bool Button::IsActive()
{
	if (myState == ButtonState::Active)
	{
		return true;
	}
	return false;
}

bool Button::IsPressed()
{
	if (myState == ButtonState::Pressed)
	{
		return true;
	}
	return false;
}

void Button::RecieveMessage(const Message& aMessage)
{
	if (aMessage.myMessageType == MessageType::InputMouseMoved)
	{
		if (IsPressed() == false)
		{
			if (CheckHover(aMessage.myFloatValue, aMessage.myFloatValue2) == true && !IsActive())
			{
				SetActive();
			}
			else if (!CheckHover(aMessage.myFloatValue, aMessage.myFloatValue2))
			{
				Deactivate();
			}
		}

	}
	else if (aMessage.myMessageType == MessageType::InputLeftMouseReleased)
	{
		if (IsPressed() == true)
		{
			TriggerOnPressed();
			SetActive();
		}
	}
	else if (aMessage.myMessageType == MessageType::InputLeftMouseHit)
	{
		if (IsActive() == true)
		{
			SetPressed();
		}
	}
	else if (aMessage.myMessageType == MessageType::WindowResize)
	{
		myScreenSize = V2F(CAST(float, aMessage.myIntValue), CAST(float, aMessage.myIntValue2));
		SetupBounds();

	}
}

void Button::Subscribe()
{
	if (!myIsListening)
	{
		SubscribeToMessage(MessageType::InputMouseMoved);
		SubscribeToMessage(MessageType::InputLeftMouseReleased);
		SubscribeToMessage(MessageType::InputLeftMouseHit);
		SubscribeToMessage(MessageType::WindowResize);
		myIsListening = true;
	}
}

void Button::Unsubscribe()
{
	if (myIsListening)
	{
		UnSubscribeToMessage(MessageType::InputMouseMoved);
		UnSubscribeToMessage(MessageType::InputLeftMouseReleased);
		UnSubscribeToMessage(MessageType::InputLeftMouseHit);
		UnSubscribeToMessage(MessageType::WindowResize);
		myIsListening = false;
	}
}

void Button::SetSpriteFactory(SpriteFactory* aSpriteFactory)
{
	ourSpriteFactoryPtr = aSpriteFactory;
}

SpriteFactory* Button::GetSpriteFactory()
{
	return ourSpriteFactoryPtr;
}

void Button::SetupBounds()
{
	CommonUtilities::Vector2<float> imageSize;

	imageSize = mySprites[ButtonState::Inactive]->GetSizeOnScreen();

	myBounds.x = myPosition.x;
	myBounds.y = myPosition.y;
	myBounds.z = myPosition.x + (imageSize.x * mySize.x);
	myBounds.w = myPosition.y + (imageSize.y * mySize.y);
}
