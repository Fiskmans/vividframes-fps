#pragma once
#include "imgui.h"


namespace Tools
{
	struct TimeTree;
	void ZoomableImGuiImage(void* aTexture,ImVec2 aSize);
	bool EditPosition(const char* aName, V3F& aVector, V3F* aAdditionalInput = nullptr, const char* aAdditionalInputName = nullptr);

	void DrawTimeTree(Tools::TimeTree* aTree);
}

