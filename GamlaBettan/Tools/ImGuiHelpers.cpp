#include <pch.h>
#include "ImGuiHelpers.h"
#include <math.h>
#include "TimeHelper.h"

namespace Tools
{

	void ZoomableImGuiImage(void* aTexture, ImVec2 aSize)
	{
		ImGui::Text("pointer = %p", aTexture);
		ImGui::Text(("size: " + std::to_string(aSize.x) + "x" + std::to_string(aSize.y)).c_str());
		if (!aTexture)
		{
			ImGui::Dummy(aSize);
		}
		return;
		ImGui::Image(aTexture, aSize);
		ImVec2 drawPos = ImGui::GetCursorScreenPos();
		if (ImGui::IsItemHovered(ImGuiHoveredFlags_AllowWhenBlockedByActiveItem))
		{
			ImVec2 iopos = ImVec2(ImGui::GetIO().MousePos.x, ImGui::GetIO().MousePos.y);

			ImGui::BeginTooltip();
			static float region_sz = 32.0f;
			static float zoom = 4.0f;
			float delta = ImGui::GetIO().MouseWheel;
			if (ImGui::GetIO().KeyShift)
			{
				zoom *= pow(1.1, delta);
			}
			else
			{
				region_sz *= pow(1.1, -delta);
				zoom *= pow(1.1, delta);
			}
			float region_x = iopos.x - drawPos.x - region_sz * 0.5f; if (region_x < 0.0f) region_x = 0.0f; else if (region_x > aSize.x - region_sz) region_x = aSize.x - region_sz;
			float region_y = iopos.y - drawPos.y - region_sz * 0.5f; if (region_y < 0.0f) region_y = 0.0f; else if (region_y > aSize.y - region_sz) region_y = aSize.y - region_sz;
			ImVec2 uv0 = ImVec2((region_x) / aSize.x, (region_y) / aSize.y);
			ImVec2 uv1 = ImVec2((region_x + region_sz) / aSize.x, (region_y + region_sz) / aSize.y);
			ImGui::Image(aTexture, ImVec2(region_sz * zoom, region_sz * zoom), uv0, uv1, ImVec4(1.0f, 1.0f, 1.0f, 1.0f), ImVec4(1.0f, 1.0f, 1.0f, 0.5f));
			ImGui::EndTooltip();
		}

	}
	bool EditPosition(const char* aName, V3F& aVector, V3F* aAdditionalInput, const char* aAdditionalInputName)
	{
#if USEIMGUI
		ImGui::PushID(aName);
		bool hovered = false;
		ImGui::InputFloat3(aName, aVector.begin());
		hovered |= ImGui::IsItemHovered();
#ifdef _DEBUG
		static ImGuiID PickingID;
		ImGuiID thisId = ImGui::GetID("Picker");


		ImGui::SameLine();
		if (ImGui::Button("Camera"))
		{
			aVector = DebugTools::myCamera->GetPosition();
		}
		hovered |= ImGui::IsItemHovered();
		ImGui::SameLine();
		bool shouldPopColor = false;
		if (PickingID == thisId)
		{
			hovered |= true;
			shouldPopColor = true;
			ImGui::PushStyleColor(ImGuiCol_Border, ImGui::GetStyle().Colors[ImGuiCol_BorderShadow]);
			if (GetAsyncKeyState(VK_LBUTTON) && !ImGui::GetIO().WantCaptureMouse)
			{
				aVector = PathFinder::GetInstance().FindPoint(*DebugTools::LastKnownMouseRay);
				PickingID = ImGuiID();
			}
		}
		if (ImGui::Button("Pick"))
		{
			PickingID = thisId;
		}
		if (shouldPopColor)
		{
			ImGui::PopStyleColor(1);
		}
		hovered |= ImGui::IsItemHovered();
		ImGui::SameLine();
		if (ImGui::Button("Goto"))
		{
			DebugTools::myCamera->SetPosition(aVector);
		}
		hovered |= ImGui::IsItemHovered();
		ImGui::SameLine();
		if (ImGui::Button("Gizmo"))
		{
			DebugTools::AttachToGizmo(aVector);
		}
		hovered |= ImGui::IsItemHovered();
		hovered |= DebugTools::IsGizmo(aVector);
#endif // _DEBUG
		if (aAdditionalInput)
		{
			ImGui::SameLine();
			if (ImGui::Button(aAdditionalInputName))
			{
				aVector = *aAdditionalInput;
			}
			hovered |= ImGui::IsItemHovered();
		}



		if (hovered)
		{
			DebugDrawer::GetInstance().DrawCross(aVector, 50);
		}
		ImGui::PopID();
		return hovered;
#else
		return false;
#endif
	}

	ImVec4 GetColor(int aDepth)
	{
		static std::vector<ImVec4> colors = {
			ImVec4(0.0f,1.0f,0.0f,1.0f),
			ImVec4(1.0f,0.0f,0.0f,1.0f),
			ImVec4(0.5f,0.7f,0.5f,1.0f),
			ImVec4(0.5f,0.5f,0.8f,1.0f),
			ImVec4(0.9f,0.9f,0.5f,1.0f),
			ImVec4(0.8f,0.2f,0.5f,1.0f),
			ImVec4(0.6f,0.9f,0.1f,1.0f)
		};
		return colors[aDepth % colors.size()];
	}
	ImVec4 DefaultColor(int aDepth)
	{
		return ImGui::GetStyleColorVec4(ImGuiCol_Text);
	}

	const char* GetCovarage(float aProcent)
	{
		if (aProcent > 0.95)
		{
			return "All";
		}
		if (aProcent > 0.85)
		{
			return "Good";
		}
		if (aProcent > 0.70)
		{
			return "Lacking";
		}
		if (aProcent > 0.40)
		{
			return "Half";
		}
		if (aProcent > 0.3)
		{
			return "Insignificant";
		}
		if (aProcent > 0.1)
		{
			return "Miniscule";
		}
		return "None";
	}
	std::string PadOrTrimTo(const std::string& aString, int aLength)
	{
		if (aString.length() > aLength)
		{
			return aString.substr(0, aLength);
		}
		return aString + std::string(aLength - aString.length(), ' ');
	}

	void DrawTimeTree_internal(Tools::TimeTree* aTree, int aDepth, std::function<ImVec4(int)> aColorGetter, const char* aFormat, std::function<float(TimeTree*)> aArgumentGetter)
	{
		ImGui::PushStyleColor(ImGuiCol_Text, aColorGetter(aDepth));
		if (ImGui::TreeNode(aTree->myName, aFormat, PadOrTrimTo(aTree->myName, 20).c_str(),(int)aTree->myCallCount,  aArgumentGetter(aTree), GetCovarage(aTree->myCovarage)))
		{
			for (auto& i : aTree->myChildren)
			{
				DrawTimeTree_internal(i, aDepth + 1, aColorGetter, aFormat, aArgumentGetter);
			}
			if (aTree->myChildren.empty())
			{
				ImGui::BulletText("No futher breakdown available");
			}
			ImGui::TreePop();
		}
		ImGui::PopStyleColor(1);
	}

	void DrawTimeTree(Tools::TimeTree* aTree)
	{

		ImGui::PushID(aTree->myName);
		static bool colorize = true;
		ImGui::Checkbox("Colorize", &colorize);
		static int mode = 0;

		const char* names[3] = { "time","Percent of parent","percent of performance" };
		ImGui::SameLine();
		if (ImGui::BeginCombo("Value Mode", names[mode]))
		{
			for (size_t i = 0; i < 3; i++)
			{
				bool selected = mode == i;
				if (ImGui::Selectable(names[i], &selected))
				{
					mode = i;
				}
			}
			ImGui::EndCombo();
		}


		const char* format = nullptr;
		std::function<float(TimeTree*)> valueGetter;
		switch (mode)
		{
		case 0:
			format = "%s: [%3d] %5.2fms covarage: %s";
			valueGetter = [](TimeTree* aNode) -> float { return aNode->myTime * 1000.f; };
			break;
		case 1:
			format = "%s: [%3d] %5.1f%% covarage: %s";
			valueGetter = [](TimeTree* aNode) -> float { return (aNode->myParent ? aNode->myTime / aNode->myParent->myTime : 1.f) * 100.f; };
			break;
		case 2:
			format = "%s: [%3d] %5.1f%% covarage: %s";
			valueGetter = [](TimeTree* aNode) -> float
			{
				std::function< float(TimeTree*)> GetRootTime = [&GetRootTime](TimeTree* aNode) -> float
				{
					if (aNode->myParent)
					{
						return GetRootTime(aNode->myParent);
					}
					return aNode->myTime;
				};
				return aNode->myTime / GetRootTime(aNode) * 100.f;
			};
			break;
		default:
			break;
		}

		ImGui::BeginChild(aTree->myName);
		if (colorize)
		{
			DrawTimeTree_internal(aTree, 0, &GetColor, format, valueGetter);
		}
		else
		{
			DrawTimeTree_internal(aTree, 0, &DefaultColor, format, valueGetter);
		}
		ImGui::EndChild();
		ImGui::PopID();
	}

}