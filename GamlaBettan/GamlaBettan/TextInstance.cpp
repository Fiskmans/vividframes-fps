#include "pch.h"
#include "TextInstance.h"
#include "SpriteRenderer.h"
#include "SpriteInstance.h"
#include <SpriteFont.h>
#include "TimeHelper.h"

SpriteRenderer* TextInstance::ourBackgroundRendererPtr;

DirectX::FXMVECTOR ToShitVector(const V2F& aVec);
DirectX::FXMVECTOR ToShitVector(const V4F& aVec);
V2F FromShitVector(const DirectX::FXMVECTOR& aVec);

TextInstance::TextInstance() :
	mySpriteFontPtr(nullptr),
	myTitle(L""),
	myColor({ 1.f, 1.f, 1.f, 1.f }),
	myTitleColor({ 1.f, 1.f, 1.f, 1.f }),
	myPosition({ 0.f, 0.f }),
	myScale({ 0.f, 0.f }),
	myTitleScale({ 0.f, 0.f }),
	myPivot({ 0.f, 0.f }),
	myBackgroundBuffer({ 0.f, 0.f }),
	myRotation(0.f),
	myShouldDraw(true),
	myEffect(TextEffect::None),
	myScreenSizePtr(nullptr),
	myBackground(nullptr)
{
}

TextInstance::TextInstance(DirectX::SpriteFont* aFont, const V2F* aScreenSize) :
	mySpriteFontPtr(aFont),
	myText(L""),
	myTitle(L""),
	myColor({ 1.f, 1.f, 1.f, 1.f }),
	myTitleColor({ 1.f, 1.f, 1.f, 1.f }),
	myPosition({ 0.f, 0.f }),
	myScale({ 1.f, 1.f }),
	myTitleScale({ 1.2f, 1.2f }),
	myPivot({ 0.f, 0.f }),
	myBackgroundBuffer({ 0.f, 0.f }),
	myRotation(0.f),
	myShouldDraw(true),
	myEffect(TextEffect::None),
	myScreenSizePtr(aScreenSize),
	myBackground(nullptr)
{
}

TextInstance::~TextInstance()
{
	SAFE_DELETE(myBackground);
	myText.clear();
	myTitle.clear();
	WIPE(*this);
}

void TextInstance::Render(DirectX::SpriteBatch* aSpriteBatch)
{
	if (!mySpriteFontPtr)
	{
		SYSERROR("Oh no text no have font :c","");
		return;
	}

	if (myShouldDraw && (!myText.empty() || !myTitle.empty()))
	{
		if (myBackground)
		{
			static std::vector<SpriteInstance*> sprite; //It's intentional trust me (SpriteRenderer.Render() only accepts vectors as of writing)
			sprite.push_back(myBackground);

			CalculateBackgroundSize();

			myBackground->SetPosition((myPosition + (myBackgroundBuffer * myPivot)) / V2F(1920.f, 1080.f));
			myBackground->SetRotation(myRotation);

			ourBackgroundRendererPtr->Render(sprite);

			sprite.clear();
		}

		V2F pos = (myPosition / V2F(1920.f, 1080.f)) * (*myScreenSizePtr);

		mySpriteFontPtr->DrawString(aSpriteBatch, myText.c_str(), ToShitVector(pos - (myBackgroundBuffer * myPivot) + (myBackgroundBuffer * 0.5f) + V2F(0.f, GetTitleSize().y)), ToShitVector(myColor), myRotation, ToShitVector(GetSize() * myPivot), ToShitVector(myScale), CAST(DirectX::SpriteEffects, myEffect));

		if (!myTitle.empty())
		{
			mySpriteFontPtr->DrawString(aSpriteBatch, myTitle.c_str(), ToShitVector(pos - (myBackgroundBuffer * myPivot) + (myBackgroundBuffer * 0.5f) + GetTitleSize() * (myPivot * V2F(myTitleScale.x - myScale.x, 0.5f))), ToShitVector(myTitleColor), myRotation, ToShitVector(GetSize() * myPivot), ToShitVector(myTitleScale), CAST(DirectX::SpriteEffects, myEffect));

		}
	}
}

void TextInstance::CalculateBackgroundSize()
{
	myBackground->SetScale((GetSize() + myBackgroundBuffer * 2.f) / myBackground->GetImageSize());
}

V2F TextInstance::GetTitleSize() const
{
	return (FromShitVector(mySpriteFontPtr->MeasureString(myTitle.c_str())) * myTitleScale / (*myScreenSizePtr)) * V2F(1920.f, 1080.f);
}

float TextInstance::GetWidth() const
{
	return GetSize().x;
}

float TextInstance::GetHeight() const
{
	return GetSize().y;
}

V2F TextInstance::GetSize() const
{
	V2F size = FromShitVector(mySpriteFontPtr->MeasureString(myText.c_str())) * myScale;
	V2F titleSize = (myTitle.empty() ? V2F() : FromShitVector(mySpriteFontPtr->MeasureString(myTitle.c_str())) * myTitleScale);

	size.y += titleSize.y;
	size.x = MAX(titleSize.x, size.x);

	return (size / (*myScreenSizePtr)) * V2F(1920.f, 1080.f);
}

void TextInstance::SetText(const std::wstring& aText)
{
	myText = aText;
}

void TextInstance::SetText(const std::string& aText)
{
	myText = std::wstring(aText.begin(), aText.end());
}

const std::wstring& TextInstance::GetWideText() const
{
	return myText;
}

std::string TextInstance::GetSlimText() const
{
	return std::string(myText.begin(), myText.end());
}

void TextInstance::SetTitle(const std::wstring& aTitle)
{
	myTitle = aTitle;
}

void TextInstance::SetTitle(const std::string& aTitle)
{
	myTitle = std::wstring(aTitle.begin(), aTitle.end());
}

const std::wstring& TextInstance::GetWideTitle() const
{
	return myTitle;
}

std::string TextInstance::GetSlimTitle() const
{
	return std::string(myTitle.begin(), myTitle.end());
}

const V2F& TextInstance::GetPixelPosition() const
{
	return myPosition;
}

void TextInstance::SetPixelPosition(const V2F& aPosition)
{
	myPosition = aPosition;
}

V2F TextInstance::GetPosition() const
{
	return V2F(myPosition.x / 1920.f, myPosition.y / 1080.f);
}

void TextInstance::SetPosition(const V2F& aPosition)
{
	SetPixelPosition({ aPosition.x * 1920.f, aPosition.y * 1080.f });
}

const V4F& TextInstance::GetColor() const
{
	return myColor;
}

void TextInstance::SetColor(const V4F& aColor)
{
	myColor = aColor;
}

const V4F& TextInstance::GetTitleColor() const
{
	return myTitleColor;
}

void TextInstance::SetTitleColor(const V4F& aColor)
{
	myTitleColor = aColor;
}

const V2F& TextInstance::GetScale() const
{
	return myScale;
}

const V2F& TextInstance::GetTitleScale() const
{
	return myTitleScale;
}

void TextInstance::SetTitleScale(const V2F& aScale)
{
	myTitleScale = aScale;
}

void TextInstance::SetScale(const V2F& aScale)
{
	myScale = aScale;
}

float TextInstance::GetRotation() const
{
	return myRotation;
}

void TextInstance::SetRotation(const float aRotation)
{
	myRotation = aRotation;
}

const V2F& TextInstance::GetPivot() const
{
	return myPivot;
}

void TextInstance::SetPivot(const V2F& aPivot)
{
	myPivot = aPivot;

	if (myBackground)
	{
		myBackground->SetPivot(myPivot);
	}
}

const V2F& TextInstance::GetBuffer() const
{
	return myBackgroundBuffer;
}

void TextInstance::SetBuffer(const V2F& aBuffer)
{
	myBackgroundBuffer = aBuffer;
}

const TextEffect TextInstance::GetEffect() const
{
	return myEffect;
}

void TextInstance::SetEffect(const TextEffect anEffect)
{
	myEffect = anEffect;
}

const SpriteInstance* TextInstance::GetBackground() const
{
	return myBackground;
}

void TextInstance::SetBackground(SpriteInstance* aBackground)
{
	if (myBackground)
	{
		delete myBackground;
	}

	myBackground = aBackground;
	myBackground->SetPivot(myPivot);
}

bool TextInstance::GetShouldDraw() const
{
	return myShouldDraw;
}

void TextInstance::SetShouldDraw(const bool aFlag)
{
	myShouldDraw = aFlag;
}

DirectX::FXMVECTOR ToShitVector(const V2F& aVec) //Not member functions so that Spritefont.h doesn't have to be included in header (it is full of general bs)
{
	return DirectX::XMVectorSet(aVec.x, aVec.y, 0.f, 0.f);
}

DirectX::FXMVECTOR ToShitVector(const V4F& aVec)
{
	return DirectX::XMVectorSet(aVec.x, aVec.y, aVec.z, aVec.w);
}

V2F FromShitVector(const DirectX::FXMVECTOR& aVec)
{
	return V2F(DirectX::XMVectorGetX(aVec), DirectX::XMVectorGetY(aVec));
}