#pragma once
#include "CNodeType.h"
class NodeUnloadLevel :
	public CNodeType
{
public:
	NodeUnloadLevel();
	int OnEnter(class CNodeInstance* aTriggeringNodeInstance) override;
	virtual std::string GetNodeName() override { return "UnloadLevel"; }
	virtual std::string GetNodeTypeCategory() override { return "Game control"; }
};

