#pragma once
#include "FullscreenTextureFactory.h"
#include "FullscreenTexture.h"

#include "ForwardRenderer.h"
#include "FullscreenRenderer.h"
#include "SpriteRenderer.h"
#include "ParticleRenderer.h"
#include "DeferredRenderer.h"
#include "HighlightRenderer.h"
#include "DepthRenderer.h"

#include "Observer.hpp"
#include "RenderStateManager.h"
#include "GBuffer.h"
#include "TextRenderer.h"

class RenderManager : public Observer
{
public:
	RenderManager();
	~RenderManager() = default;

	bool Init(class DirectX11Framework* aFramework, class WindowHandler* aWindowHandler);
	bool Release();

	void BeginFrame(float aClearColor[4]);
	void EndFrame();

	void Render(class Scene* aScene);
	void RenderMovie(const std::vector<SpriteInstance*>& aSpriteList);
	void RenderSprites(const std::vector<SpriteInstance*>& aSpriteList, const bool aShouldRenderExtraSprites = false);
	void RenderText(const std::vector<TextInstance*>& aTextList);

	void AddExtraSpriteToRender(SpriteInstance* aSprite);

	void SubscribeToMessages();
	void UnsubscribeToMessages();
	void RecieveMessage(const Message& aMessage);

	void SetupBoneTexture(const std::vector<ModelInstance*>& aModelList, float aDeltaTime);

	SpriteRenderer* GetSpriteRenderer();

#if USEIMGUI
	void Imgui();
#endif // !_RETAIL


private:
	bool myDoSSAO = true;

#if ENABLERENDERPASSITERATIONS
	enum class RenderStep
	{
		All,

		GAlbedo,
		GNormal,
		GVertexNormal,
		GMetalness,
		GRoughness,
		GAmbientOcclusion,
		GEmissive,

		SSAO,
		SSAOPost,

		DeferredDepth,
		Deferred,

		ForwardDepth,
		Forward,
		Particles,
		DebugLines,

		Luminance,

		ScaleDownHalf,
		ScaleDownQuater,
		ScaleDownHalfQuater,

		Blur,

		ScaleUpQuater,
		ScaleUpHalf,

		Bloom,

		Sprites,

		Count
	};
	const char* names[ENUM_CAST(RenderStep::Count)] =
	{
		"All",

		"GAlbedo",
		"GNormal",
		"GVertexNormal",
		"GMetalness",
		"GRoughness",
		"GAmbientOcclusion",
		"GEmissive",

		"SSAO",
		"SSAOPost",

		"DeferredDepth",
		"Deferred",

		"ForwardDepth",
		"Forward",
		"Particles",
		"DebugLines",

		"Luminance",

		"ScaleDownHalf",
		"ScaleDownQuater",
		"ScaleDownHalfQuater",

		"Blur",

		"ScaleUpQuater",
		"ScaleUpHalf",

		"Bloom",

		"Sprites"
	};
	RenderStep myCurrentRenderStep;
#endif // ENABLERENDERPASSITERATIONS

#if USEIMGUI
	bool myShouldRenderWireFrame;
#endif // !_RETAIL
	std::vector<SpriteInstance*> myExtraSpritesToRenderThisFrame;
	std::unordered_map<ModelInstance*, short> myBoneOffsetMap;

	long long myStartedAt;
	RenderStateManager myStateManerger;
	ForwardRenderer myForwardRenderer;
	FullscreenRenderer myFullscreenRenderer;
	SpriteRenderer mySpriteRenderer;
	ParticleRenderer myParticleRenderer;
	DeferredRenderer myDeferredRenderer;
	TextRenderer myTextRenderer;
	HighlightRenderer myHighlightRenderer;
	DepthRenderer myShadowRenderer;
	class DirectX11Framework* myFrameworkPtr;
	struct ID3D11ShaderResourceView* myBoneTextureView = nullptr;
	Texture* myPerlinView = nullptr;
	Texture* myRandomNormalview = nullptr;
	struct ID3D11Texture2D* myBoneBufferTexture = nullptr;

	FullscreenTextureFactory myFullscreenFactory;

	enum class Textures
	{
		BackBuffer,
		IntermediateTexture,
		HalfSize,
		QuaterSize,
		HalfQuaterSize,
		Guassian1,
		Guassian2,
		IntermediateDepth,
		Luminance,
		Selection,
		SelEdgesHalf,
		SelEdges,
		Selection2,
		SelectionScaleDown1,
		SelectionScaleDown2,
		SSAOBuffer,
		BackFaceBuffer,
		Count
	};
	std::array<FullscreenTexture, static_cast<int>(Textures::Count)> myTextures;
	GBuffer myGBuffer;
	GBuffer myBufferGBuffer;

	CU::Vector2<unsigned int> myScreenSize;

	void RenderSelection(const std::vector<ModelInstance*>& aModelsToHighlight, Camera* aCamera);
	bool CreateTextures(const unsigned int aWidth, const unsigned int aHeight);
	void Resize(const unsigned int aWidth, const unsigned int aHeight);


#if ENABLERENDERPASSITERATIONS
	bool EarlyReturn(RenderStep aStep, Textures aTexture);
#endif
};

