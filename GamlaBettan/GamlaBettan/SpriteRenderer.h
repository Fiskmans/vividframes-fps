#pragma once
#include "Matrix.hpp"
#include <vector>
#include "Vector.hpp"

class DirectX11Framework;
class SpriteInstance;
class Camera;
struct ID3D11DeviceContext;
struct ID3D11Buffer;

class SpriteRenderer
{
public:
	SpriteRenderer();
	~SpriteRenderer();

	bool Init(DirectX11Framework* aFramework);

	void Render(/*const Camera* aCamera,*/ const std::vector<SpriteInstance*>& aSpriteList);

private:
	struct SFrameBufferData
	{
		V2F myScreenSize;
		V2F junk;
	} myFrameBufferData;

	struct SObjectBufferData
	{
		CommonUtilities::Matrix4x4<float> myToWorld;
		CommonUtilities::Vector4<float> myColor;
		CommonUtilities::Vector4<float> myUVMinMax;
		CommonUtilities::Vector4<float> myPivot;
	} myObjectBufferData;

	ID3D11DeviceContext*	myContext;
	ID3D11Buffer*			myFrameBuffer;
	ID3D11Buffer*			myObjectBuffer;
};

