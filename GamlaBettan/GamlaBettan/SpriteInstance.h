#pragma once
#include "Vector.hpp"
#include "Observer.hpp"
#include "Matrix.hpp"

class Sprite;
class Scene;

class SpriteInstance : public Observer
{
public:
	SpriteInstance(Sprite* aSprite);
	~SpriteInstance();

	Sprite* GetSprite();

	void SetPosition(const CommonUtilities::Vector2<float>& aPosition);
	void SetPosition(float aPositionX, float aPositionY);
	void SetScale(const CommonUtilities::Vector2<float>& aScale);
	void Rotate(const float aRotation);
	void SetRotation(const float aRotation);
	void SetPivot(const CommonUtilities::Vector2<float>& aPivot);
	void SetColor(const CommonUtilities::Vector4<float>& aColor);
	void SetUVMinMax(const CommonUtilities::Vector2<float>& aMin, const CommonUtilities::Vector2<float>& aMax);
	void SetUVMinMax(const CommonUtilities::Vector4<float>& aMinMax);

	const CommonUtilities::Matrix4x4<float>& GetTransform() const;
	const CommonUtilities::Vector2<float> GetPosition() const;
	const CommonUtilities::Vector2<float> GetPivotPosition() const;
	const CommonUtilities::Vector4<float>& GetColor() const;
	const CommonUtilities::Vector2<float> GetScale() const;
	const CommonUtilities::Vector2<float>& GetPivot() const;
	const CommonUtilities::Vector4<float>& GetUVMinMax() const;
	const CommonUtilities::Vector2<float> GetImageSize() const;
	const CommonUtilities::Vector2<float> GetSizeWithScale() const;
	const CommonUtilities::Vector2<float> GetSizeOnScreen() const;

	void RecieveMessage(const Message& aMessage) override;

	void AddToScene();
	void RemoveFromScene();
	bool HasBeenAddedToScene();

private:
	Sprite* mySprite;
	CommonUtilities::Matrix4x4<float> myTransform;
	CommonUtilities::Vector4<float> myColor;
	CommonUtilities::Vector2<float> myPivot;
	CommonUtilities::Vector2<float> myPrecalculationPivot;
	CommonUtilities::Vector4<float> myUVMinMax;

	float& myXPos = myTransform(4, 1);
	float& myYPos = myTransform(4, 2);
	bool myIsListening;
	bool myIsAddedToScene;

#ifdef _DEBUG
	V2F myScale;
#endif
};