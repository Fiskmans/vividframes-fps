#include "pch.h"
#include "DebugDrawer.h"
#include <d3d11.h>
#include "DirectX11Framework.h"
#include <Matrix4x4.hpp>
#include "ModelLoader.h"
#include "Camera.h"
#include "ShaderCompiler.h"
#include "ModelInstance.h"
#include "Model.h"
#include "ShaderFlags.h"
#include "Intersection.hpp"

struct CameraBuffer
{
	CommonUtilities::Matrix4x4<float> myToCamera;
	CommonUtilities::Matrix4x4<float> myToProjection;
};

void DebugDrawer::Init(DirectX11Framework* aFramework)
{
#if USEDEBUGLINES
	myFramework = aFramework;
	ID3D11Device* device = aFramework->GetDevice();

	std::string pixelShaderData =
		"cbuffer ColorBuffer : register(b0)"
		"{"
		"	float4 color : COLOR;"
		"};"
		"float4 pixelShader(float4 position : SV_POSITION) : SV_TARGET\n"
		"{\n"
		"	return color;\n"
		"}\n"
		;

	if (!CompilePixelShader(device, pixelShaderData, myPixelShader))
	{
		SYSERROR("Could not compile pixelshader","debugdrawer");
		return;
	}

	std::string vertexShaderData =
		"cbuffer matrixes : register(b0)\n"
		"{\n"
		"	float4x4 toCamera;\n"
		"	float4x4 toProjection;\n"
		"};\n"
		"struct inputStruct\n"
		"{\n"
		"	float4 position : POSITION; \n"
		"};\n"
		"float4 vertexShader(inputStruct pos) : SV_POSITION\n"
		"{\n"
		"	float4 camerapos = mul(pos.position,toCamera);\n"
		"	return mul(camerapos,toProjection);\n"
		"}\n"
		;

	ID3DBlob* vertexBlob;
	if (!CompileVertexShader(device, vertexShaderData, myVertexShader, &vertexBlob))
	{
		if (vertexBlob)
		{
			vertexBlob->Release();
		}
		SYSERROR("Could not Compile vertexshader","debugdrawer");
		return;
	}

	D3D11_INPUT_ELEMENT_DESC layout[] =
	{
		{"POSITION" ,0,DXGI_FORMAT_R32G32B32A32_FLOAT,0,D3D11_APPEND_ALIGNED_ELEMENT,D3D11_INPUT_PER_VERTEX_DATA,0}
	};

	HRESULT result = device->CreateInputLayout(layout, sizeof(layout) / sizeof(layout[0]), vertexBlob->GetBufferPointer(), vertexBlob->GetBufferSize(), &myInputLayout);
	if (FAILED(result))
	{
		SYSERROR("could not create input layout","debugdrawer");
		return;
	}

	CD3D11_BUFFER_DESC desc;
	ZeroMemory(&desc, sizeof(desc));
	desc.ByteWidth = sizeof(V4F) * myMaxCount;
	desc.Usage = D3D11_USAGE_DYNAMIC;
	desc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	desc.MiscFlags = 0;
	desc.StructureByteStride = sizeof(V4F);

	result = device->CreateBuffer(&desc, nullptr, &myVertexBuffer);
	if (FAILED(result))
	{
		SYSERROR("could not create vertex buffer","debugdrawer");
		return;
	}

	desc.ByteWidth = sizeof(CameraBuffer);
	desc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	desc.StructureByteStride = 0;

	result = device->CreateBuffer(&desc, nullptr, &myConstantBuffer);
	if (FAILED(result))
	{
		SYSERROR("could not create constant buffer","debugdrawer");
		return;
	}

	desc.ByteWidth = sizeof(V4F);
	desc.StructureByteStride = 0;

	result = device->CreateBuffer(&desc, nullptr, &myColorBuffer);
	if (FAILED(result))
	{
		SYSERROR("could not create Color buffer","debugdrawer");
		return;
	}


	myIsWorking = true;
#endif // !_RETAIL
}

void DebugDrawer::SetColor(V4F aColor)
{
#if USEDEBUGLINES
	if (myColor != aColor)
	{
		myColorRanges.emplace_back(myColor, myPoints.size());
		myColor = aColor;
	}
#endif
}

void DebugDrawer::DrawLine(const V3F& aFrom, const V3F& aTo)
{
#if USEDEBUGLINES
	if (myIsWorking)
	{
		if (myPointCount + 2 < myMaxCount)
		{
			myPoints.push_back(V4F(aFrom, 1));
			myPoints.push_back(V4F(aTo, 1));
			myPointCount += 2;
		}
		else
		{
			if (!warned)
			{
				SYSWARNING(std::string() + "Drawing a lot of debug lines consider reducing amount: " + STRINGVALUE(MAXDEBUGLINES),"");
				warned = true;
			}
		}
	}
#endif // !_RETAIL
}

void DebugDrawer::DrawLines(const std::vector<V3F>& aPoints)
{
	for (size_t i = 1; i < aPoints.size(); i++)
	{
		DrawLine(aPoints[i - 1], aPoints[i]);
	}
}

void DebugDrawer::DrawCross(const V3F& at, const float aSize)
{
	DrawLine(at + V3F(-aSize, -aSize, -aSize), at + V3F(aSize, aSize, aSize));
	DrawLine(at + V3F(-aSize, -aSize, aSize), at + V3F(aSize, aSize, -aSize));
	DrawLine(at + V3F(-aSize, aSize, -aSize), at + V3F(aSize, -aSize, aSize));
	DrawLine(at + V3F(-aSize, aSize, aSize), at + V3F(aSize, -aSize, -aSize));
}

void DebugDrawer::DrawArrow(const V3F& aFrom, const V3F& aTo)
{
	DrawLine(aFrom, aTo);

	V3F worldUp = { 0,1,0 };
	V3F forward = (aTo - aFrom);
	V3F right = { 1,0,0 };
	if (forward.x != 0.f || forward.z != 0.f)
	{
		right = forward.Cross(worldUp);
	}
	V3F up = right.Cross(forward);
	right.Normalize();
	up.Normalize();

	DrawLine(aTo, aFrom + forward * 0.6f + right * forward.Length() * 0.16f);
	DrawLine(aTo, aFrom + forward * 0.6f - right * forward.Length() * 0.16f);

	DrawLine(aTo, aFrom + forward * 0.6f + up * forward.Length() * 0.16f);
	DrawLine(aTo, aFrom + forward * 0.6f - up * forward.Length() * 0.16f);

}

void DebugDrawer::DrawDirection(const V3F& aSource, const V3F& aDirection, const float aLength)
{
	DrawArrow(aSource, aSource + aDirection.GetNormalized() * aLength);
}

void DebugDrawer::DrawSphere(const CommonUtilities::Sphere<float>& aSphere, const size_t aLaps)
{
	const int linesperlap = 20;
	V3F last = aSphere.Position() - V3F(0, -1, 0) * aSphere.Radius();
	for (size_t i = 0; i < aLaps * linesperlap; i++)
	{
		float l = i / static_cast<float>(aLaps * linesperlap)* PI;
		float y = cos(l);
		float r = sin(l);

		V3F next = V3F(sin(i * PI / linesperlap) * r, y, cos(i * PI / linesperlap) * r) * aSphere.Radius() + aSphere.Position();
		DrawLine(last, next);
		last = next;
	}

}

void DebugDrawer::DrawX(const V3F& at, const float aSize)
{
	DrawLine(at + V3F(0, aSize, aSize), at - V3F(0, aSize, aSize));
	DrawLine(at + V3F(0, -aSize, aSize), at - V3F(0, -aSize, aSize));
}

void DebugDrawer::DrawY(const V3F& at, const float aSize)
{
	DrawLine(at, at + V3F(aSize, aSize, 0));
	DrawLine(at, at + V3F(-aSize, aSize, 0));
	DrawLine(at, at + V3F(0, -aSize, 0));
}

void DebugDrawer::DrawZ(const V3F& at, const float aSize)
{
	DrawLine(at + V3F(-aSize, aSize, 0), at + V3F(aSize, aSize, 0));
	DrawLine(at + V3F(-aSize, -aSize, 0), at + V3F(aSize, aSize, 0));
	DrawLine(at + V3F(-aSize, -aSize, 0), at + V3F(aSize, -aSize, 0));
}

void DebugDrawer::DrawGizmo(const V3F& at, const float aSize)
{
	DebugDrawer::GetInstance().DrawDirection(at, V3F(1, 0, 0), aSize);
	DebugDrawer::GetInstance().DrawDirection(at, V3F(0, 1, 0), aSize);
	DebugDrawer::GetInstance().DrawDirection(at, V3F(0, 0, 1), aSize);
	DebugDrawer::GetInstance().DrawX(at + V3F(1, 0, 0) * aSize * 1.3f, aSize * 0.2f);
	DebugDrawer::GetInstance().DrawY(at + V3F(0, 1, 0) * aSize * 1.3f, aSize * 0.2f);
	DebugDrawer::GetInstance().DrawZ(at + V3F(0, 0, 1) * aSize * 1.3f, aSize * 0.2f);
}

void DebugDrawer::DrawBoundingBox(CommonUtilities::AABB3D<float> aBoundingBox)
{
	V3F iii = aBoundingBox.Min();
	V3F aaa = aBoundingBox.Max();

	V3F iaa = V3F(iii.x, aaa.y, aaa.z);
	V3F aia = V3F(aaa.x, iii.y, aaa.z);
	V3F iia = V3F(iii.x, iii.y, aaa.z);
	V3F aai = V3F(aaa.x, aaa.y, iii.z);
	V3F iai = V3F(iii.x, aaa.y, iii.z);
	V3F aii = V3F(aaa.x, iii.y, iii.z);

	DrawLine(iii, aii);
	DrawLine(iai, aai);
	DrawLine(iia, aia);
	DrawLine(iaa, aaa);

	DrawLine(iii, iai);
	DrawLine(aii, aai);
	DrawLine(iia, iaa);
	DrawLine(aia, aaa);

	DrawLine(iii, iia);
	DrawLine(aii, aia);
	DrawLine(iai, iaa);
	DrawLine(aai, aaa);
}

void DebugDrawer::DrawRotatedBoundingBox(CommonUtilities::AABB3D<float> aBoundingBox, CommonUtilities::Matrix4x4<float> aTransformMatrix)
{
	V3F iii = aBoundingBox.Min();
	V3F aaa = aBoundingBox.Max();

	V4F iii4 = V4F(iii.x, iii.y, iii.z, 1.0f);
	V4F aaa4 = V4F(aaa.x, aaa.y, aaa.z, 1.0f);

	V4F iaa = V4F(iii.x, aaa.y, aaa.z, 1.0f);
	V4F aia = V4F(aaa.x, iii.y, aaa.z, 1.0f);
	V4F iia = V4F(iii.x, iii.y, aaa.z, 1.0f);
	V4F aai = V4F(aaa.x, aaa.y, iii.z, 1.0f);
	V4F iai = V4F(iii.x, aaa.y, iii.z, 1.0f);
	V4F aii = V4F(aaa.x, iii.y, iii.z, 1.0f);

	iii4 = iii4 * aTransformMatrix;
	aaa4 = aaa4 * aTransformMatrix;

	iaa = iaa * aTransformMatrix;
	aia = aia * aTransformMatrix;
	iia = iia * aTransformMatrix;
	aai = aai * aTransformMatrix;
	iai = iai * aTransformMatrix;
	aii = aii * aTransformMatrix;

	DrawLine(V3F(iii4), V3F(aii));
	DrawLine(iai, aai);
	DrawLine(iia, aia);
	DrawLine(iaa, aaa4);

	DrawLine(iii4, iai);
	DrawLine(aii, aai);
	DrawLine(iia, iaa);
	DrawLine(aia, aaa4);

	DrawLine(iii4, iia);
	DrawLine(aii, aia);
	DrawLine(iai, iaa);
	DrawLine(aai, aaa4);

}

void DebugDrawer::DrawProgress(V3F at, float aSize, float aProgress)
{
	V3F right = V3F(1, 0, 0);
	V3F up = V3F(0, 1, 0);
	DrawLine(at - right * aSize + up * aSize * 0.1f, at - right * aSize - up * aSize * 0.1f);
	DrawLine(at + right * aSize + up * aSize * 0.1f, at + right * aSize - up * aSize * 0.1f);

	DrawLine(at - right * aSize, at + right * aSize * (aProgress * 2 - 1));
}

void DebugDrawer::DrawSkeleton(ModelInstance* aInstance)
{
	SetColor(V4F(0, 1, 0, 1));
	static std::array<M44F,NUMBEROFANIMATIONBONES>transforms;
	if (aInstance->GetModel()->GetModelData()->myshaderTypeFlags & ShaderFlags::HasBones)
	{
		aInstance->SetupanimationMatrixes(transforms);
		static std::vector<V4F> positions;
		positions.clear();
		positions.resize(aInstance->GetModel()->myBoneData.size(), V4F(0.0f,0.0f,0.0f,1.0f));
		
		auto boneData = aInstance->GetModel()->myBoneData;

		for (size_t i = 0; i < boneData.size(); i++)
		{
			auto finalTrans = transforms[i];
			auto offset = boneData[i].BoneOffset;
			M44F total = finalTrans * M44F::GetRealInverse(offset);
			positions[i] = positions[i] * M44F::Transpose(total);
		}
		for (size_t i = 0; i < aInstance->GetModel()->myBoneData.size(); i++)
		{
			if (aInstance->GetModel()->myBoneData[i].parent != -1)
			{
				DrawLine(positions[aInstance->GetModel()->myBoneData[i].parent], positions[i]);
			}
			else
			{
				DrawLine(V3F(0, 0, 0), positions[i]);
			}
			//DrawSphere(CommonUtilities::Sphere<float>(positions[i], 5), 8);

		}
	}
}

void DebugDrawer::DrawPlane(CommonUtilities::Plane<float> aPlane, float aLineSpacing, size_t aSubdevisions)
{
	V3F right = aPlane.Normal().Cross(V3F(0, 1, 0));
	V3F up = right.Cross(aPlane.Normal());
	right.Normalize();
	up.Normalize();
	right *= aLineSpacing;
	up *= aLineSpacing;

	V3F center = aPlane.Point();
	float negativity = -float(aSubdevisions) / 2;

	for (size_t x = 0; x < aSubdevisions+1; x++)
	{
		for (size_t y = 0; y < aSubdevisions+1; y++)
		{
			DrawLine(right * negativity + up * (y + negativity) + center, right * -negativity + up * (y + negativity) + center);
			DrawLine(right * (x + negativity) + up * negativity + center, right * (x + negativity) + up * -negativity + center);
		}
	}
}

void DebugDrawer::DrawFrustum(const CommonUtilities::PlaneVolume<float>& aFrustum)
{
	const std::vector<CommonUtilities::Plane<float>>& planes = aFrustum.Planes();
	if (planes.size() == 6)
	{
		static int iterations = 120;
		V3F nearTopRight = CommonUtilities::SolvePlaneIntersection<float>(planes[0], planes[2], planes[5], iterations);
		V3F nearBottomRight = CommonUtilities::SolvePlaneIntersection<float>(planes[0], planes[3], planes[5], iterations);

		V3F nearTopLeft = CommonUtilities::SolvePlaneIntersection<float>(planes[1], planes[2], planes[5], iterations);
		V3F nearBottomLeft = CommonUtilities::SolvePlaneIntersection<float>(planes[1], planes[3], planes[5], iterations);

		V3F farTopRight = CommonUtilities::SolvePlaneIntersection<float>(planes[0], planes[2], planes[4], iterations);
		V3F farBottomRight = CommonUtilities::SolvePlaneIntersection<float>(planes[0], planes[3], planes[4], iterations);

		V3F farTopLeft = CommonUtilities::SolvePlaneIntersection<float>(planes[1], planes[2], planes[4], iterations);
		V3F farBottomLeft = CommonUtilities::SolvePlaneIntersection<float>(planes[1], planes[3], planes[4], iterations);

		for (size_t i = 0; i < 6; i++)
		{
			float part = float(i) / 5.f;

			//near
			DrawArrow(LERP(nearBottomRight,nearBottomLeft,part),LERP(nearTopRight,nearTopLeft,part));
			DrawLine(LERP(nearBottomLeft,nearTopLeft,part), LERP(nearBottomRight,nearTopRight,part));

			//left
			DrawLine(LERP(nearBottomLeft, nearTopLeft, part), LERP(farBottomLeft, farTopLeft, part));
			DrawLine(LERP(nearBottomLeft, farBottomLeft, part), LERP(nearTopLeft, farTopLeft, part));

			//right
			DrawLine(LERP(nearBottomRight, nearTopRight, part), LERP(farBottomRight, farTopRight, part));
			DrawLine(LERP(nearBottomRight, farBottomRight, part), LERP(nearTopRight, farTopRight, part));

			//Top
			DrawLine(LERP(nearTopLeft, nearTopRight, part), LERP(farTopLeft, farTopRight, part));
			DrawLine(LERP(nearTopRight, farTopRight, part), LERP(nearTopLeft, farTopLeft, part));

			//Bottom
			DrawLine(LERP(nearBottomLeft, nearBottomRight, part), LERP(farBottomLeft, farBottomRight, part));
			DrawLine(LERP(nearBottomRight, farBottomRight, part), LERP(nearBottomLeft, farBottomLeft, part));

			//Far
			DrawArrow(LERP(farBottomRight, farBottomLeft, part), LERP(farTopRight, farTopLeft, part));
			DrawLine(LERP(farBottomLeft, farTopLeft, part), LERP(farBottomRight, farTopRight, part));
		}

		for (auto& i : aFrustum.Planes())
		{
			DrawDirection(i.Point(), i.Normal(), 5);
		}

	}
}

void DebugDrawer::Render(Camera* aCamera)
{
#if USEDEBUGLINES
	if (myIsWorking)
	{
		ID3D11DeviceContext* context = myFramework->GetContext();

		D3D11_MAPPED_SUBRESOURCE bufferData;
		WIPE(bufferData);

		HRESULT result = context->Map(myVertexBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &bufferData);
		if (FAILED(result))
		{
			SYSERROR("Oh no, buffer ded","vertexbuffer");
			myIsWorking = false;
			return;
		}
		if (!myPoints.empty())
		{
			memcpy(bufferData.pData, &myPoints[0], sizeof(V4F) * myPointCount);
		}
		context->Unmap(myVertexBuffer, 0);

		result = context->Map(myConstantBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &bufferData);
		if (FAILED(result))
		{
			SYSERROR("Oh no, bluffer ded", "constantbuffer");
			return;
		}
		CameraBuffer* buff = reinterpret_cast<CameraBuffer*>(bufferData.pData);
		buff->myToCamera = CommonUtilities::Matrix4x4<float>::Transpose(CommonUtilities::Matrix4x4<float>::GetFastInverse(aCamera->GetTransform()));
		buff->myToProjection = CommonUtilities::Matrix4x4<float>::Transpose(aCamera->GetProjection(false));
		context->Unmap(myConstantBuffer, 0);


		context->IASetInputLayout(myInputLayout);
		context->PSSetShader(myPixelShader, nullptr, 0);
		context->VSSetShader(myVertexShader, nullptr, 0);
		context->GSSetShader(nullptr, nullptr, 0);
		context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY::D3D10_PRIMITIVE_TOPOLOGY_LINELIST);


		UINT stride = sizeof(V4F);
		UINT offset = 0;
		context->IASetVertexBuffers(0, 1, &myVertexBuffer, &stride, &offset);

		context->VSSetConstantBuffers(0, 1, &myConstantBuffer);

		context->PSSetConstantBuffers(0, 1, &myColorBuffer);
		size_t at = 0;
		auto DrawTo = [&](size_t aNumber)
		{
			context->Draw(CAST(UINT, aNumber - at), CAST(UINT, at));
			at = aNumber;
		};
		auto SetColor = [&](V4F aColor)
		{
			if (SUCCEEDED(context->Map(myColorBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &bufferData)))
			{
				*((V4F*)bufferData.pData) = aColor;
				context->Unmap(myColorBuffer, 0);
			}
		};

		for (auto& i : myColorRanges)
		{
			SetColor(i.first);
			DrawTo(i.second);
		}
		SetColor(myColor);
		DrawTo(myPointCount);

		myPointCount = 0;
		myPoints.clear();
		myColorRanges.clear();
	}
#endif // !_RETAIL
}
