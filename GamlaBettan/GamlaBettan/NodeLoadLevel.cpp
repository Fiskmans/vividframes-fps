#include "pch.h"
#include "NodeLoadLevel.h"
#include <iostream>
#include "CNodeInstance.h"
#include "NodeDataTypes.h"

NodeLoadLevel::NodeLoadLevel()
{
	myPins.push_back(CPin("File name", CPin::PinTypeInOut::PinTypeInOut_IN,  CPin::PinType::Data));
	myPins.push_back(CPin("IN", CPin::PinTypeInOut::PinTypeInOut_IN,  CPin::PinType::Flow));
	myPins.push_back(CPin("IN", CPin::PinTypeInOut::PinTypeInOut_OUT, CPin::PinType::Flow));
	SetPinType<std::string>(0);
}

int NodeLoadLevel::OnEnter(CNodeInstance* aTriggeringNodeInstance)
{
	std::string input1;
	if (!aTriggeringNodeInstance->ReadData(0, input1)) { return -1; }
	Message loadmessage;
	loadmessage.myMessageType = MessageType::LoadLevel;
	loadmessage.myText = input1;
	PostMaster::GetInstance()->SendMessages(loadmessage);

	return 2;
}
