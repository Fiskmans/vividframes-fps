#include "pch.h"
#include "SpriteRenderer.h"
#include "DirectX11Framework.h"
#include "SpriteInstance.h"
#include "Sprite.h"
#include <d3d11.h>
#include "Shaders.h"

SpriteRenderer::SpriteRenderer()
{
	myContext = nullptr;
	myFrameBuffer = nullptr;
	myObjectBuffer = nullptr;
}

SpriteRenderer::~SpriteRenderer()
{
	myContext = nullptr;

	SAFE_RELEASE(myFrameBuffer);

	SAFE_RELEASE(myObjectBuffer);
}

bool SpriteRenderer::Init(DirectX11Framework* aFramework)
{
	if (!aFramework)
	{
		SYSERROR("Framework was nullptr in Init SpriteRenderer","");
		return false;
	}

	myContext = aFramework->GetContext();
	if (!myContext)
	{
		SYSERROR("Framework's context was nullptr in Init SpriteRenderer","");
		return false;
	}

	ID3D11Device* device = aFramework->GetDevice();
	if (!device)
	{
		SYSERROR("Framework's device was nullptr in Init SpriteRenderer","");
		return false;
	}
	HRESULT result;
	D3D11_BUFFER_DESC bufferDescription = { 0 };
	bufferDescription.Usage = D3D11_USAGE_DYNAMIC;
	bufferDescription.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	bufferDescription.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

	bufferDescription.ByteWidth = sizeof(SFrameBufferData);
	result = device->CreateBuffer(&bufferDescription, nullptr, &myFrameBuffer);
	if (FAILED(result))
	{
		SYSERROR("Failed to create buffer in Init SpriteRenderer","");
		return false;
	}

	bufferDescription.ByteWidth = sizeof(SObjectBufferData);
	result = device->CreateBuffer(&bufferDescription, nullptr, &myObjectBuffer);

	if (FAILED(result))
	{
		SYSERROR("Failed to create buffer in Init SpriteRenderer","");
		return false;
	}

	return true;
}

void SpriteRenderer::Render(/*const Camera* aCamera,*/ const std::vector<SpriteInstance*>& aSpriteList)
{
	HRESULT result;
	D3D11_MAPPED_SUBRESOURCE bufferData;

	myFrameBufferData.myScreenSize = aSpriteList.back()->GetSprite()->GetWindowSize();
	ZeroMemory(&bufferData, sizeof(D3D11_MAPPED_SUBRESOURCE));
	result = myContext->Map(myFrameBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &bufferData);
	if (FAILED(result))
	{
		SYSERROR("myContext->Map failed in Init SpriteRenderer","");
		return;
	}

	memcpy(bufferData.pData, &myFrameBufferData, sizeof(SFrameBufferData));
	myContext->Unmap(myFrameBuffer, 0);
	myContext->VSSetConstantBuffers(0, 1, &myFrameBuffer);
	for (SpriteInstance* spriteInstance : aSpriteList)
	{
		const Sprite::SpriteData* spriteData = &spriteInstance->GetSprite()->GetSpriteData();

		myObjectBufferData.myToWorld = spriteInstance->GetTransform();
		myObjectBufferData.myColor = spriteInstance->GetColor();
		myObjectBufferData.myUVMinMax = spriteInstance->GetUVMinMax();
		myObjectBufferData.myPivot = V4F(spriteInstance->GetPivot(), 0, 1);

		ZeroMemory(&bufferData, sizeof(D3D11_MAPPED_SUBRESOURCE));
		result = myContext->Map(myObjectBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &bufferData);
		if (FAILED(result))
		{
			SYSERROR("myContext->Map failed in Render SpriteRenderer","");
			return;
		}

		memcpy(bufferData.pData, &myObjectBufferData, sizeof(SObjectBufferData));
		myContext->Unmap(myObjectBuffer, 0);

		myContext->IASetPrimitiveTopology((D3D11_PRIMITIVE_TOPOLOGY)spriteData->myPrimitiveTopology);
		myContext->IASetInputLayout(spriteData->myInputLayout);
		myContext->IASetVertexBuffers(0, 1, &spriteData->myVertexBuffer, &spriteData->myStride, &spriteData->myOffset);
		myContext->IASetIndexBuffer(spriteData->myIndexBuffer, DXGI_FORMAT_R32_UINT, 0);

		myContext->VSSetConstantBuffers(1, 1, &myObjectBuffer);
		myContext->VSSetShader(*spriteData->myVertexShader, nullptr, 0);

		myContext->PSSetConstantBuffers(1, 1, &myObjectBuffer);
		myContext->PSSetShaderResources(0, 1, &spriteData->myTexture);
		myContext->PSSetShader(*spriteData->myPixelShader, nullptr, 0);

		myContext->DrawIndexed(spriteData->myIndexCount, 0, 0);
	}
}
