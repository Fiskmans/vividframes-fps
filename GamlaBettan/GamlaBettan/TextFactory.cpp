#include "pch.h"
#include "TextFactory.h"
#include "TextInstance.h"
#include <SpriteFont.h>
#include "DirectX11Framework.h"
#include "SpriteFactory.h"

TextFactory::TextFactory() :
	myScreenSize({1920.f, 1080.f})
{
	myDevicePtr = nullptr;
	mySpriteFactoryPtr = nullptr;
}

TextFactory::~TextFactory()
{
	UnSubscribeToMessage(MessageType::WindowResize);

	for (auto& font : myFonts)
	{
		SAFE_DELETE(font.second);
	}

	myFonts.clear();

	myScreenSize = V2F(0, 0);
	myDevicePtr = nullptr;
	mySpriteFactoryPtr = nullptr;
}

bool TextFactory::Init(DirectX11Framework* aFramework, SpriteRenderer* aRenderer, SpriteFactory* aSpriteFactory)
{
	myDevicePtr = aFramework->GetDevice();
	
	if (!myDevicePtr)
	{
		return false;
	}

	if (!aRenderer)
	{
		SYSERROR("Text factory was not given a sprite renderer!","");
		return false;
	}

	TextInstance::ourBackgroundRendererPtr = aRenderer;

	mySpriteFactoryPtr = aSpriteFactory;

	SubscribeToMessage(MessageType::WindowResize);

	return true;
}

TextInstance* TextFactory::CreateToolTip(SpriteInstance* aBackground, const V2F& aBuffer, const std::string& someText, const std::string& aFontPath)
{
	TextInstance* toolTip = CreateText(aFontPath);

	if (toolTip)
	{
		toolTip->SetText(someText);
		toolTip->SetBackground(aBackground);
		toolTip->SetBuffer(aBuffer);
	}

	return toolTip;
}

TextInstance* TextFactory::CreateToolTip(const std::string& aBackgroundPath, const V2F& aBuffer, const std::string& someText, const std::string& aFontPath)
{
	return CreateToolTip(mySpriteFactoryPtr->CreateSprite(aBackgroundPath), aBuffer, someText, aFontPath);
}

TextInstance* TextFactory::CreateText(const std::string& aFontPath)
{
	return new TextInstance(GetFont(aFontPath), &myScreenSize);
}

void TextFactory::RecieveMessage(const Message& aMessage)
{
	if (aMessage.myMessageType == MessageType::WindowResize)
	{
		//myScreenSize.x = CAST(float, aMessage.myIntValue);
		//myScreenSize.y = CAST(float, aMessage.myIntValue2);
	}
}

DirectX::SpriteFont* TextFactory::GetFont(const std::string& aFontPath)
{
	if (myFonts.find(aFontPath) == myFonts.end())
	{
		DirectX::SpriteFont* newFont = LoadFont(aFontPath);
		
		if (!newFont)
		{
			SYSERROR("Failed to load in TextFactory.GetFont()", aFontPath);
			return nullptr;
		}
		return newFont;
	}

	return myFonts[aFontPath];
}

DirectX::SpriteFont* TextFactory::LoadFont(const std::string& aFontPath)
{
	DirectX::SpriteFont*  newFont = new DirectX::SpriteFont(myDevicePtr, (std::wstring(aFontPath.begin(),aFontPath.end())).c_str());

	myFonts[aFontPath] = newFont;
	return myFonts[aFontPath];
}
