#include "pch.h"
#include "DecalFactory.h"
#include "CameraFactory.h"
#include "TextureLoader.h"
#include <TimeHelper.h>

void DecalFactory::Init(ID3D11Device* aDevice)
{
	myDevice = aDevice;
}

Decal* DecalFactory::LoadDecal(const std::string& aTexturePath, float aFOV, float aRange, V3F aRotation, V3F aPosition)
{
	Decal* decal = new Decal();
	decal->myCamera = CCameraFactory::CreateCamera(aFOV, false, 20.f, aRange);
	decal->myCamera->Rotate(aRotation);
	decal->myCamera->SetPosition(aPosition);
	decal->myCamera->SetResolution(V2F(128, 128));
	decal->myRange = aRange;
	decal->myTexture = LoadTexture(myDevice, aTexturePath);
	decal->myTimestamp = Tools::GetTotalTime();
	return decal;
}

Decal* DecalFactory::LoadDecal(const std::string& aTexturePath, float aFOV, float aRange, Camera* aCameraToCopyFrom)
{
	Decal* decal = new Decal();
	decal->myCamera = CCameraFactory::CreateCamera(aFOV, false, 20.f, aRange);
	decal->myCamera->SetTransform(aCameraToCopyFrom->GetTransform());
	decal->myCamera->SetResolution(V2F(128, 128));
	decal->myRange = aRange;
	decal->myTexture = LoadTexture(myDevice, aTexturePath);
	decal->myTimestamp = Tools::GetTotalTime();
	return decal;
}