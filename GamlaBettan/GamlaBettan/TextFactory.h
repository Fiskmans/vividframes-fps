#pragma once
#include <string>
#include <unordered_map>
#include "Vector.hpp"
#include "Observer.hpp"

namespace DirectX
{
	class SpriteFont;
}

class TextInstance;
class DirectX11Framework;
class SpriteRenderer;
class SpriteFactory;
struct ID3D11Device;

class TextFactory : public Observer
{
public:
	TextFactory();
	~TextFactory();
	bool Init(DirectX11Framework* aFramework, SpriteRenderer* aRenderer, SpriteFactory* aSpriteFactory);

	TextInstance* CreateText(const std::string& aFontPath = "Data/Fonts/default.spritefont");
	TextInstance* CreateToolTip(class SpriteInstance* aBackground, const V2F& aBuffer, const std::string& someText = "", const std::string& aFontPath = "Data/Fonts/default.spritefont");
	TextInstance* CreateToolTip(const std::string& aBackgroundPath, const V2F& aBuffer, const std::string& someText = "", const std::string& aFontPath = "Data/Fonts/default.spritefont");

	void RecieveMessage(const Message& aMessage) override;

private:
	DirectX::SpriteFont* GetFont(const std::string& aFontPath);
	DirectX::SpriteFont* LoadFont(const std::string& aFontPath);

	std::unordered_map<std::string, DirectX::SpriteFont*> myFonts;
	V2F myScreenSize;
	ID3D11Device* myDevicePtr;
	SpriteFactory* mySpriteFactoryPtr;
};

