#include "pch.h"
#include "NodeEnemySpawner.h"
#include "CNodeInstance.h"
#include "NodeDataTypes.h"
#include "..//Game/DataStructs.h"
#include "..//Game/EnemyFactory.h"

NodeEnemySpawner::NodeEnemySpawner() :
	myEnemyFactory(nullptr)
{
	myPins.push_back(CPin("In", CPin::PinTypeInOut::PinTypeInOut_IN, CPin::PinType::Flow));
	myPins.push_back(CPin("Out", CPin::PinTypeInOut::PinTypeInOut_OUT, CPin::PinType::Flow));

	myPins.push_back(CPin("CharacterID", CPin::PinTypeInOut::PinTypeInOut_IN, CPin::PinType::Data));
	myPins.push_back(CPin("Amount", CPin::PinTypeInOut::PinTypeInOut_IN, CPin::PinType::Data));
	myPins.push_back(CPin("SpawnIntervalMin", CPin::PinTypeInOut::PinTypeInOut_IN, CPin::PinType::Data));
	myPins.push_back(CPin("SpawnIntervalMax", CPin::PinTypeInOut::PinTypeInOut_IN, CPin::PinType::Data));
	myPins.push_back(CPin("Position", CPin::PinTypeInOut::PinTypeInOut_IN, CPin::PinType::Data));
	myPins.push_back(CPin("TargetPosition", CPin::PinTypeInOut::PinTypeInOut_IN, CPin::PinType::Data));
	myPins.push_back(CPin("TargetPlayer", CPin::PinTypeInOut::PinTypeInOut_IN, CPin::PinType::Data));
	myPins.push_back(CPin("SpawnFalling", CPin::PinTypeInOut::PinTypeInOut_IN, CPin::PinType::Data));

	SetPinType<int>(2);
	SetPinType<int>(3);
	SetPinType<float>(4);
	SetPinType<float>(5);
	SetPinType<V3F>(6);
	SetPinType<V3F>(7);
	SetPinType<bool>(8);
	SetPinType<bool>(9);
}

int NodeEnemySpawner::OnEnter(CNodeInstance* aTriggeringNodeInstance)
{
	int charID;
	int amount;
	float intervalMin;
	float intervalMax;
	V3F position;
	V3F targetPos;
	bool targetPlayer = false;
	bool fall = false;

	if (!aTriggeringNodeInstance->ReadData(2, charID)) { return -1; }
	if (!aTriggeringNodeInstance->ReadData(3, amount)) { return -1; }
	if (!aTriggeringNodeInstance->ReadData(4, intervalMin)) { return -1; }
	if (!aTriggeringNodeInstance->ReadData(5, intervalMax)) { return -1; }
	if (!aTriggeringNodeInstance->ReadData(6, position)) { return -1; }
	if (!aTriggeringNodeInstance->ReadData(8, targetPlayer)) { return -1; }

	if (!aTriggeringNodeInstance->ReadData(7, targetPos)) { return -1; }

	myEnemyFactory = aTriggeringNodeInstance->ourPollingStation->GetEnemyFactory();
	myEnemyFactory->QueueSpawn(charID, amount, intervalMin, intervalMax, position, targetPos, targetPlayer, fall);

	return 1;
}
