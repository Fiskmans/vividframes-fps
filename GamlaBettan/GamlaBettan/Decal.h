#pragma once

#if __INTELLISENSE__
#include <pch.h>
#endif // __INTELLISENSE__


struct Decal
{
	Camera* myCamera;
	Texture* myTexture;
	ID3D11ShaderResourceView* myDepth = nullptr;
	float myRange;
	float myIntensity;
	float myTimestamp;
};