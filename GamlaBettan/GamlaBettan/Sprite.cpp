#include "pch.h"
#include "Sprite.h"
#include <d3d11.h>

Sprite::Sprite() : myWindowSize({ 1920.f, 1080.f })
{
}

Sprite::~Sprite()
{
	UnSubscribeToMessage(MessageType::WindowResize);

	mySpriteData.myIndexBuffer->Release();
	mySpriteData.myVertexBuffer->Release();
	mySpriteData.myInputLayout->Release();
	mySpriteData.myTexture->Release();

	WIPE(mySpriteData);
}

void Sprite::Init(const SpriteData& aSpriteData, const V2F& aWindowSize)
{
	SubscribeToMessage(MessageType::WindowResize);

	mySpriteData = aSpriteData;
	myWindowSize = aWindowSize;
}

Sprite::SpriteData& Sprite::GetSpriteData()
{
	return mySpriteData;
}

const Sprite::SpriteData& Sprite::GetSpriteData() const
{
	return mySpriteData;
}

const CommonUtilities::Vector2<float>& Sprite::GetWindowSize() const
{
	return myWindowSize;
}

void Sprite::RecieveMessage(const Message& aMessage)
{
	if (aMessage.myMessageType == MessageType::WindowResize)
	{
		myWindowSize.x = CAST(float, aMessage.myIntValue);
		myWindowSize.y = CAST(float, aMessage.myIntValue2);
	}
}
