#pragma once

class DirectX11Framework;
class PixelShader;
class SpotLight;
struct Decal;
class DepthRenderer
{
public:
	bool Init(DirectX11Framework* aFramework);

	const std::array<Camera*, 6>& GetCameras();
	void BindshadowsToSlots(int aSlot);
	void Render(PointLight* aLight,Scene* aScene, std::unordered_map<ModelInstance*, short>& aBoneMapping);
	void RenderSpotLightDepth(SpotLight* aSpotlight, Scene* aScene, std::unordered_map<ModelInstance*, short>& aBoneMapping);
	ID3D11ShaderResourceView* RenderDecalDepth(Decal* aSpotlight, Scene* aScene, std::unordered_map<ModelInstance*, short>& aBoneMapping);

private:

	void Render(Camera* aCamera, const std::vector<ModelInstance*>& aModelList, std::unordered_map<ModelInstance*, short>& aBoneMapping);
	struct ID3D11Buffer* myFrameBuffer = nullptr;
	struct ID3D11Buffer* myObjectBuffer = nullptr;
	ID3D11DeviceContext* myContext;
	PixelShader* myShader;
	ID3D11Device* myDevice;
	std::array<Camera*,6> myCameras = { nullptr };
	ID3D11DepthStencilView* myDepth = { nullptr };
	ID3D11ShaderResourceView* myDepthsResource = { nullptr };
	ID3D11RenderTargetView* myRenderTarget = { nullptr };
	ID3D11RenderTargetView* myDecalRenderTarget = { nullptr };
};

