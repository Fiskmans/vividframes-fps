#pragma once

enum ShaderFlags: unsigned long long
{
	None = 0,
	HasVertexColors = 1 << 0,
	HasUvSets = 1 << 1,
	HasBones = 1 << 2,


	NumBonesOffset = 32,
	BoneMask = 1ULL << 32 | 1ULL << 33 | 1ULL << 34 | 1ULL << 35,
	Count
};
