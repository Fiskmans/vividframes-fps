#include "pch.h"
#include "ModelInstance.h"
#include "Model.h"
#include "ShaderFlags.h"
#include "Animator.h"
#include "TimeHelper.h"
//#pragma push_macro(MIN)
//#pragma push_macro(MAX)

#include <GBPhysX.h>
//#pragma pop_macro(MIN)
//#pragma pop_macro(MAX)

#if USEIMGUI
#include "Camera.h"
#include <imgui.h>
#endif // !_RETAIL


ModelInstance::ModelInstance(Model* aModel) : myGaphicBoundsModifier(1.f)
{
	static unsigned int ids;
	myFriendlyName = "[" + std::to_string(ids++) + "] ";

	if (!aModel)
	{
		SYSCRASH("no model make me sad :c");
	}
	myModel = aModel;
	myScale = { 1.0f,1.0f,1.0f };
	myAnimator = nullptr;
	myTint = V4F(0.0f, 0.0f, 0.0f, 1.0f);
	myShouldBeDrawnThroughWalls = false;
	myUsePlayerThroughWallShader = false;
	myShouldRender = true;
	myExpectedLifeTime = 0.0f;
	ResetSpawnTime();
}

Model* ModelInstance::GetModel()
{
	return myModel;
}

void ModelInstance::ResetSpawnTime()
{
	mySpawnTime = Tools::GetTotalTime();
}

CommonUtilities::Matrix4x4<float> ModelInstance::GetModelToWorldTransform()
{
	//CommonUtilities::Matrix4x4<float> mat = myScaleAndRotate;
	CommonUtilities::Matrix4x4<float> toWorld = myRotation;

	CommonUtilities::Matrix4x4<float> scale;
	scale(1, 1) = myScale.x;
	scale(2, 2) = myScale.y;
	scale(3, 3) = myScale.z;

	toWorld *= scale;

	toWorld(4, 1) = myPosition.x;
	toWorld(4, 2) = myPosition.y;
	toWorld(4, 3) = myPosition.z;

	return toWorld;
}

void ModelInstance::SetPosition(CommonUtilities::Vector4<float> aPosition)
{
	myPosition = aPosition;
}

void ModelInstance::Rotate(CommonUtilities::Vector3<float> aRotation)
{
	myRotation *= CommonUtilities::Matrix4x4<float>::CreateRotationAroundX(aRotation.x);
	myRotation *= CommonUtilities::Matrix4x4<float>::CreateRotationAroundY(aRotation.y);
	myRotation *= CommonUtilities::Matrix4x4<float>::CreateRotationAroundZ(aRotation.z);
}

void ModelInstance::Rotate(const CommonUtilities::Matrix4x4<float>& aRotationMatrix)
{
	myRotation *= aRotationMatrix;
}

void ModelInstance::SetRotation(CommonUtilities::Vector3<float> aRotation)
{
	CommonUtilities::Matrix4x4<float> mat;
	mat.CreateRotationAroundX(aRotation.x);
	mat.CreateRotationAroundY(aRotation.y);
	mat.CreateRotationAroundZ(aRotation.z);

	myRotation = mat;
}

void ModelInstance::SetRotation(const CommonUtilities::Matrix4x4<float>& aTargetRotation)
{
	myRotation = aTargetRotation;
}

void ModelInstance::SetScale(CommonUtilities::Vector3<float> aScale)
{
	myScale = aScale;
	myGaphicBoundsModifier = MAX(MAX(aScale.x, aScale.y), aScale.z);

	/*CommonUtilities::Matrix4x4<float> mat;
	mat(1, 1) = aScale.x;
	mat(2, 2) = aScale.y;
	mat(3, 3) = aScale.z;
	myScaleAndRotate *= mat;*/
}

void ModelInstance::SetShouldBeDrawnThroughWalls(const bool aFlag)
{
	myShouldBeDrawnThroughWalls = aFlag;
}

void ModelInstance::SetUsePlayerThroughWallShader(const bool aFlag)
{
	myUsePlayerThroughWallShader = aFlag;
}

void ModelInstance::SetShouldRender(const bool aFlag)
{
	myShouldRender = aFlag;
}

void ModelInstance::AttachAnimator(Animator* aAnimator)
{
	myAnimator = aAnimator;
}

void ModelInstance::SetAnimation(int aAnimation)
{
	if (myAnimator)
	{
		if (myAnimator->GetAnimationCount() > aAnimation)
		{
			myAnimator->SetState(aAnimation);
			myAnimator->SetBlend(0.f);
			myAnimator->SetTime(0.f);
		}
		else
		{
			SYSWARNING("Trying to play animation that does not exist: [" + std::to_string(aAnimation) + "] max is [" + std::to_string(myAnimator->GetAnimationCount()+1) + "]","")
		}
	}
	else
	{
		SYSWARNING("Trying to animate something without an animator",myModel->GetModelData()->myFilePath);
	}
}

void ModelInstance::StepAnimation(float aDeltaTime)
{
	if (myAnimator)
	{
		myAnimator->Step(aDeltaTime);
	}
	else
	{
		SYSERROR("Trying to step animation on model that doesnt have an animator", myModel->GetModelData()->myFilePath);
	}
}

//TODO RAGDOLL
/*
void ModelInstance::DetachAnimator()
{
	std::array<CommonUtilities::Matrix4x4<float>, NUMBEROFANIMATIONBONES> currentStateMatrixes;
	myAnimator->BoneTransform(currentStateMatrixes);
	myGBPhysXActor->UpdateRagDollMatrices(currentStateMatrixes);
	myAnimator = nullptr;
}

void ModelInstance::SetGBPhysXActor(GBPhysXActor* aGBPhysXActor)
{
	myGBPhysXActor = aGBPhysXActor;
}
*/

bool ModelInstance::HasAnimations()
{
	return !!(myModel->GetModelData()->myshaderTypeFlags & ShaderFlags::HasBones);
}

void ModelInstance::SetTint(V4F aTint)
{
	myTint = aTint;
}

V4F ModelInstance::GetTint()
{
	return myTint;
}

bool ModelInstance::ShouldBeDrawnThroughWalls() const
{
	return myShouldBeDrawnThroughWalls;
}

bool ModelInstance::IsUsingPlayerThroughWallShader() const
{
	return myUsePlayerThroughWallShader;
}

bool ModelInstance::ShouldRender() const
{
	return myShouldRender;
}

CommonUtilities::Vector3<float> ModelInstance::GetPosition() const
{
	return { myPosition.x, myPosition.y, myPosition.z };
}

const std::string ModelInstance::GetFriendlyName()
{
	return myFriendlyName + myModel->GetFriendlyName();
}

void ModelInstance::SetupanimationMatrixes(std::array<CommonUtilities::Matrix4x4<float>, NUMBEROFANIMATIONBONES>& aMatrixes)
{
	if (myAnimator)
	{
		myAnimator->BoneTransform(aMatrixes);
	}
	//TODO RAGDOLL
	/*
	else if (myGBPhysXActor)
	{
		myGBPhysXActor->UpdateRagDollMatrices(aMatrixes);
	}
	*/
	else
	{
		static bool onetimeWarning = true;
		if (onetimeWarning)
		{
			SYSWARNING("Model instance with bones tried to animate without an attached controller",myModel->GetModelData()->myFilePath);
			onetimeWarning = false;
		}
	}
}

#if USEIMGUI
bool ModelInstance::ImGuiNode(std::map<std::string, std::vector<std::string>>& aFiles, Camera* aCamera)
{
	CommonUtilities::Vector3<float> pos = GetPosition();
	if (ImGui::DragFloat3("Position", &pos.x, 0.1f))
	{
		SetPosition({ pos.x,pos.y,pos.z,1 });
	}
	float rotation[3] = { 0,0,0 };
	if (ImGui::DragFloat3("Rotate", rotation, 0.01f, 0.0f, 0.0f, "%.1f"))
	{
		Rotate(CommonUtilities::Vector3<float>(rotation[0], rotation[1], rotation[2]));
	}
	if (ImGui::DragFloat3("Scale", &myScale.x, 0.003f, 0.0f, 0.0f, "%.5f"))
	{
	}
	if (ImGui::Button("Find"))
	{
		CommonUtilities::Vector3<float> pos = GetPosition();
		aCamera->SetPosition(pos + CommonUtilities::Vector3<float>(0, 0, -3));
		aCamera->SetRotation(CommonUtilities::Matrix3x3<float>());
	}
	ImGui::SameLine();

	if (ImGui::Button("Remove"))
	{
		return true;
	}
	return false;
}
#endif

CommonUtilities::Sphere<float> ModelInstance::GetGraphicBoundingSphere(float aRangeModifier)
{
	return CommonUtilities::Sphere<float>(myPosition, myModel->GetGraphicSize() * myGaphicBoundsModifier * aRangeModifier);
}

const float ModelInstance::GetSpawnTime()
{
	return mySpawnTime;
}

void ModelInstance::SetExpectedLifeTime(float aLifeTime)
{
	myExpectedLifeTime = aLifeTime;
}

const float ModelInstance::GetExpectedLifeTime()
{
	return myExpectedLifeTime;
}

void ModelInstance::SetCustomPixelData(float aData[MODELSAMOUNTOFCUSTOMDATA])
{
	memcpy(myCustomData + MODELSAMOUNTOFCUSTOMDATA, aData, MODELSAMOUNTOFCUSTOMDATA);
}

void ModelInstance::SetCustomVertexData(float aData[MODELSAMOUNTOFCUSTOMDATA])
{
	memcpy(myCustomData, aData, MODELSAMOUNTOFCUSTOMDATA);
}

void ModelInstance::SetIsHighlighted(bool aState)
{
	myIsHighlighted = aState;
}

bool ModelInstance::GetIsHighlighted()
{
	return myIsHighlighted;
}

void ModelInstance::SetUsingSecondaryFov(bool aState)
{
	myIsUsingSecondaryFov = aState;
}

bool ModelInstance::GetIsUsingSecondaryFov()
{
	return myIsUsingSecondaryFov;
}
