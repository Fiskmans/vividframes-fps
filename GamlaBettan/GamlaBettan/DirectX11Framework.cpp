#include "pch.h"
#include "DirectX11Framework.h"
#include "WindowHandler.h"
#include <Windows.h>
#include <d3d11_4.h>
#include "PostMaster.hpp"


DirectX11Framework::DirectX11Framework()
{
	mySwapChain = nullptr;
	myDevice = nullptr;
	myDeviceContext = nullptr;
	myDepthBuffer = nullptr;
}

DirectX11Framework::~DirectX11Framework()
{
	mySwapChain = nullptr;
	myDevice = nullptr;
	myDeviceContext = nullptr;
	myDepthBuffer = nullptr;
}

bool DirectX11Framework::Init(void* aWindowHandle)
{
	HRESULT result = E_FAIL;

	DXGI_SWAP_CHAIN_DESC desc = {  };
	//ZeroMemory(&desc, sizeof(desc));
	desc.BufferCount = 1;
	desc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	desc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	desc.OutputWindow = static_cast<HWND>(aWindowHandle);
	desc.SampleDesc.Count = 1;
	desc.Windowed = true;

#ifdef _DEBUG
	if (DebugTools::CommandLineFlags.count(L"-DebugDxDevice"))
	{
		result = D3D11CreateDeviceAndSwapChain(nullptr, D3D_DRIVER_TYPE_HARDWARE, nullptr, D3D11_CREATE_DEVICE_DEBUG, nullptr, 0, D3D11_SDK_VERSION, &desc, &mySwapChain, &myDevice, nullptr, &myDeviceContext);
		if (FAILED(result))
		{
			SYSERROR("Could not start requested debug dx device","");
		}
	}
#endif // _DEBUG



	if (FAILED(result))
	{
		result = D3D11CreateDeviceAndSwapChain(nullptr, D3D_DRIVER_TYPE_HARDWARE, nullptr, 0 , nullptr, 0, D3D11_SDK_VERSION,
			&desc, &mySwapChain, &myDevice, nullptr, &myDeviceContext);
		if (FAILED(result))
		{
			SYSCRASH("Could not create swapchain","oh noes")
			return false;
		}
	}


	result = mySwapChain->QueryInterface(GUID({ 0x3D585D5A,0xBD4A,0x489E,0xB1,0xF4,0x3D,0xBC,0xB6,0x45,0x2F,0xFB }), (void**)&mySwapChain4);
	if (FAILED(result))
	{
		mySwapChain4 = nullptr;
	}

#if USEFULLSCREEN
	//mySwapChain->SetFullscreenState(TRUE,NULL);
#endif

	return true;
}

bool DirectX11Framework::Init(ID3D11Device* aDevice, ID3D11DeviceContext* aContext)
{
	myDeviceContext = aContext;
	myDevice = aDevice;

	return !!aDevice && !!aContext;
}

void DirectX11Framework::SetRenderTargetAndDepthStencil(ID3D11RenderTargetView* aRenterTarget, ID3D11DepthStencilView* aDepthStencil)
{
	myBackBuffer = aRenterTarget;
	myDepthBuffer = aDepthStencil;
}


void DirectX11Framework::EndFrame()
{
	if (mySwapChain)
	{
		PERFORMANCETAG("Present");
		if (mySwapChain4 && false)
		{
			DXGI_PRESENT_PARAMETERS fancyPresentDesc;
			WIPE(fancyPresentDesc);
			fancyPresentDesc.DirtyRectsCount = 0; // Updates whole screen
			fancyPresentDesc.pDirtyRects = nullptr; // no rects since whole screen
			fancyPresentDesc.pScrollRect = nullptr; // down want to scroll screen content
			fancyPresentDesc.pScrollOffset = nullptr; //^^
			
			mySwapResult = mySwapChain4->Present1(0, DXGI_PRESENT_DO_NOT_WAIT,&fancyPresentDesc);
		}
		else
		{
			mySwapResult = mySwapChain->Present(0, 0);
		}
	}
}

void DirectX11Framework::WaitForFrameBuffer()
{
	if (mySwapResult == DXGI_ERROR_WAS_STILL_DRAWING)
	{
		mySwapResult = mySwapChain->Present(0, 0);
	}
}

void DirectX11Framework::Resize(void* aHWND)
{
	if (mySwapChain && myDeviceContext && myDevice)
	{
		RECT rect;
		GetWindowRect(static_cast<HWND>(aHWND), &rect);

		Message msg;
		msg.myMessageType = MessageType::WindowResize;
		msg.myIntValue = rect.right - rect.left;
		msg.myIntValue2 = rect.bottom - rect.top;
		PostMaster::GetInstance()->SendMessages(msg);
	}
}

ID3D11DeviceContext* DirectX11Framework::GetContext()
{
	return myDeviceContext;
}

ID3D11Device* DirectX11Framework::GetDevice()
{
	return myDevice;
}

ID3D11Texture2D* DirectX11Framework::GetBackbufferTexture()
{
	HRESULT result;
	if (mySwapChain)
	{
		ID3D11Texture2D* texture;
		result = mySwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (void**)&texture);
		if (FAILED(result))
		{
			SYSERROR("Could not get backbuffer from swapchain","");
			return nullptr;
		}
		return texture;
	}
	
	ID3D11Resource* backbufferTexture = nullptr;
	myBackBuffer->GetResource(&backbufferTexture);
	if (!backbufferTexture)
	{
		SYSERROR("Could not get backbuffer texture","");
		return nullptr;
	}
	return static_cast<ID3D11Texture2D*>(backbufferTexture);
}

IDXGISwapChain* DirectX11Framework::GetSwapChain()
{
	return mySwapChain;
}

