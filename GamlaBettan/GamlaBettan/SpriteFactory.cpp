#include "pch.h"
#include "SpriteFactory.h"
#include "Sprite.h"
#include "SpriteInstance.h"
#include <fstream>
#include <DDSTextureLoader.h>
#include "ShaderCompiler.h"
#include "DirectX11Framework.h"

SpriteFactory::SpriteFactory() : myWindowSize({ 1920.f, 1080.f })
{
	myDevice = nullptr;
}

SpriteFactory::~SpriteFactory()
{
	UnSubscribeToMessage(MessageType::WindowResize);

	for (auto& i : mySprites)
	{
		SAFE_DELETE(i.second);
	}
	mySprites["!"] = nullptr;
}

bool SpriteFactory::Init(DirectX11Framework* aFramework)
{
	SubscribeToMessage(MessageType::WindowResize);

	if (aFramework->GetDevice() == nullptr)
	{
		return false;
	}

	myDevice = aFramework->GetDevice();
	return true;
}

SpriteInstance* SpriteFactory::CreateSprite(const std::string& aDDSPath)
{
	SpriteInstance* newSpriteInstance = new SpriteInstance(GetSprite(aDDSPath, false));
	return newSpriteInstance;
}

SpriteInstance* SpriteFactory::CreateTriangleQuad()
{
	SpriteInstance* newTriangle = new SpriteInstance(GetTriangleQuad());
	return newTriangle;
}

SpriteInstance* SpriteFactory::CreateVideoSprite()
{
	SpriteInstance* newSpriteInstance = new SpriteInstance(GetSprite(myVideoSpritePath, true));
	return newSpriteInstance;
}

void SpriteFactory::RecieveMessage(const Message& aMessage)
{
	if (aMessage.myMessageType == MessageType::WindowResize)
	{
		myWindowSize.x = CAST(float, aMessage.myIntValue);
		myWindowSize.y = CAST(float, aMessage.myIntValue2);
	}
}

Sprite* SpriteFactory::GetSprite(const std::string& aDDSPath, const bool aIsMovie)
{
	if (mySprites.find(aDDSPath) == mySprites.end())
	{
		Sprite* newSprite = LoadSprite(aDDSPath, aIsMovie);
		if (!newSprite)
		{
			SYSERROR("Failed to load sprite in SpriteFactory.GetSprite()", aDDSPath);
			return nullptr;
		}
		return newSprite;
	}
	else
	{
		return mySprites[aDDSPath];
	}

	SYSERROR("Oh no wat?","");
	return nullptr;
}

Sprite* SpriteFactory::LoadSprite(const std::string& aDDSPath, const bool aIsMovie)
{
	HRESULT result;

	//Start texture
	std::wstring texturePath(aDDSPath.begin(), aDDSPath.end());
	ID3D11ShaderResourceView* textureShaderResourceView;
	result = DirectX::CreateDDSTextureFromFile(myDevice, texturePath.c_str(), nullptr, &textureShaderResourceView);
	if (FAILED(result))
	{
		SYSERROR("Failed to create texture from file in SpriteFactory LoadSprite",aDDSPath);
		return nullptr;
	}
	ID3D11Resource* resource;
	textureShaderResourceView->GetResource(&resource);
	D3D11_TEXTURE2D_DESC desc;
	((ID3D11Texture2D*)resource)->GetDesc(&desc);
	UINT ddsWidth = desc.Width;
	UINT ddsHeight = desc.Height;
	//End texture

	//Start vertex
	struct Vertex
	{
		float x, y, z, w;
		float u, v;
	} vertices[4];

	if (aIsMovie)
	{
		vertices[0] = { -1.f, 1.f, 0.f, 1.f, 0.f, 0.f };
		vertices[1] = { 1.f, 1.f, 0.f, 1.f, 1.f, 0.f };
		vertices[2] = { -1.f, -1.f , 0.f, 1.f, 0.f, 1.f };
		vertices[3] = { 1.f, -1.f, 0.f, 1.f, 1.f, 1.f };
	}
	else
	{
		float width = CAST(float, ddsWidth) / (1920.f * 0.5f);
		float height = CAST(float, ddsHeight) / (1080.f * 0.5f);

		vertices[0] = { 0.f, 0.f, 0.f, 1.f, 0.f, 0.f };
		vertices[1] = { width, 0.f, 0.f, 1.f, 1.f, 0.f };
		vertices[2] = { 0.f, -1.f * height, 0.f, 1.f, 0.f, 1.f };
		vertices[3] = { width, -1.f * height, 0.f, 1.f, 1.f, 1.f };
	}

	D3D11_BUFFER_DESC bufferDescription = { 0 };
	bufferDescription.ByteWidth = sizeof(vertices);
	bufferDescription.Usage = D3D11_USAGE_IMMUTABLE;
	bufferDescription.BindFlags = D3D11_BIND_VERTEX_BUFFER;

	D3D11_SUBRESOURCE_DATA subresourceData = { 0 };
	subresourceData.pSysMem = vertices;

	ID3D11Buffer* vertexBuffer;
	result = myDevice->CreateBuffer(&bufferDescription, &subresourceData, &vertexBuffer);
	if (FAILED(result))
	{
		SYSERROR("Failed to create buffer in SpriteFactory::LoadSprite",aDDSPath);
		return nullptr;
	}
	//End vertex

	//Start index
	UINT indices[6] =
	{
		0, 1, 2,
		2, 1, 3
	};
	D3D11_BUFFER_DESC indexBufferDescription = { 0 };
	indexBufferDescription.ByteWidth = sizeof(UINT) * 6;
	indexBufferDescription.Usage = D3D11_USAGE_IMMUTABLE;
	indexBufferDescription.BindFlags = D3D11_BIND_INDEX_BUFFER;
	D3D11_SUBRESOURCE_DATA indexSubResourceData = { 0 };
	indexSubResourceData.pSysMem = indices;

	ID3D11Buffer* indexBuffer;
	result = myDevice->CreateBuffer(&indexBufferDescription, &indexSubResourceData, &indexBuffer);
	if (FAILED(result))
	{
		return nullptr;
	}
	//End index
	std::vector<char> vsBlob;
	VertexShader* vertexShader = GetVertexShader(myDevice, "Data/Shaders/Sprites/SpriteVertexShader.hlsl", vsBlob);
	if (!vertexShader)
	{
		SYSERROR("Failed to create vertex shader in SpriteFactory::LoadSprite",aDDSPath);
		return nullptr;
	}

	PixelShader* pixelShader = GetPixelShader(myDevice, "Data/Shaders/Sprites/SpritePixelShader.hlsl");

	if (!pixelShader)
	{
		SYSERROR("Failed to create pixel shader in SpriteFactory::LoadSprite", aDDSPath);
		return nullptr;
	}

	//End shader

	//Start layout
	D3D11_INPUT_ELEMENT_DESC layout[] =
	{
		{ "POSITION",	0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "UV",			0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};

	ID3D11InputLayout* inputLayout;
	result = myDevice->CreateInputLayout(layout, 2, vsBlob.data(), vsBlob.size(), &inputLayout);
	if (FAILED(result))
	{
		SYSERROR("Failed to create input layout in SpriteFactory::LoadSprite", aDDSPath);
		return nullptr;
	}
	//End layout

	Sprite* newSprite = new Sprite();

	Sprite::SpriteData spriteData;
	spriteData.myIndexCount = 6;
	spriteData.myVertexCount = sizeof(vertices) / sizeof(Vertex);
	spriteData.myStride = sizeof(Vertex);
	spriteData.myOffset = 0;
	spriteData.myIndexBuffer = indexBuffer;
	spriteData.myVertexBuffer = vertexBuffer;
	spriteData.myVertexShader = vertexShader;
	spriteData.myPixelShader = pixelShader;
	spriteData.myPrimitiveTopology = D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
	spriteData.myInputLayout = inputLayout;
	spriteData.myTexture = textureShaderResourceView;
	spriteData.mySize.y = ddsHeight;
	spriteData.mySize.x = ddsWidth;

	newSprite->Init(spriteData, myWindowSize);
	mySprites[aDDSPath] = newSprite;
	return mySprites[aDDSPath];
}

Sprite* SpriteFactory::GetTriangleQuad()
{
	if (mySprites.find(myTriangleName) == mySprites.end())
	{
		Sprite* newSprite = LoadTriangleQuad();
		if (!newSprite)
		{
			SYSERROR("Failed to load sprite in SpriteFactory::GetTriangleQuad","");
			return nullptr;
		}
		return newSprite;
	}
	else
	{
		return mySprites[myTriangleName];
	}
}

Sprite* SpriteFactory::LoadTriangleQuad()
{
	HRESULT result;

	//Start vertex
	struct Vertex
	{
		float x, y, z, w;
		float r, g, b, a;
	} vertices[6] =
	{
		//{ -0.8f, 0.8f, 0, 1, 0, 0, 1, 1 },
		//{ -0.5f, 0.5f, 0, 1, 0, 1, 0, 1 },
		//{ -0.8f, 0.5f, 0, 1, 1, 0, 0, 1 }, 

		//{ -0.8f, 0.8f, 0, 1, 0, 0, 1, 1 },
		//{ -0.5f, 0.8f, 0, 1, 1, 0, 0, 1 },
		//{ -0.5f, 0.5f, 0, 1, 0, 1, 0, 1 }

		{ -0.8f, 0.8f, 0, 1, 0, 0, 1, 1 },
		{ -0.5f, 0.5f, 0, 1, 0, 1, 0, 1 },
		{ -0.8f, 0.5f, 0, 1, 1, 0, 0, 1 }, 

		{ -0.8f, 0.8f, 0, 1, 0, 0, 1, 1 },
		{ -0.5f, 0.8f, 0, 1, 1, 0, 0, 1 },
		{ -0.5f, 0.5f, 0, 1, 0, 1, 0, 1 }

	};

	D3D11_BUFFER_DESC bufferDescription = { 0 };
	bufferDescription.ByteWidth = sizeof(vertices);
	bufferDescription.Usage = D3D11_USAGE_IMMUTABLE;
	bufferDescription.BindFlags = D3D11_BIND_VERTEX_BUFFER;

	D3D11_SUBRESOURCE_DATA subresourceData = { 0 };
	subresourceData.pSysMem = vertices;

	ID3D11Buffer* vertexBuffer;
	result = myDevice->CreateBuffer(&bufferDescription, &subresourceData, &vertexBuffer);
	if (FAILED(result))
	{
		SYSERROR("Failed to create buffer in SpriteFactory::LoadTriangleQuad","");
		return nullptr;
	}
	//End vertex

	std::vector<char> vsBlob;
	VertexShader* vertexShader = GetVertexShader(myDevice, "Data/Shaders/Sprites/SpriteVertexShader.hlsl", vsBlob);
	if (!vertexShader)
	{
		SYSERROR("Failed to create vertex shader in SpriteFactory::LoadTriangle","");
		return nullptr;
	}


	PixelShader* pixelShader = GetPixelShader(myDevice, "Data/Shaders/Sprites/SpritePixelShader.hlsl");
	if (!pixelShader)
	{
		SYSERROR("Failed to create pixel shader in SpriteFactory::LoadTriangle","");
		return nullptr;
	}

	//End shader




	//Start layout
	D3D11_INPUT_ELEMENT_DESC layout[] =
	{
		{ "POSITION",	0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "COLOR",		0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};

	ID3D11InputLayout* inputLayout;
	result = myDevice->CreateInputLayout(layout, 2, vsBlob.data(), vsBlob.size(), &inputLayout);
	if (FAILED(result))
	{
		SYSERROR("Failed to create input layout in SpriteFactory::LoadTriangle","");
		return nullptr;
	}
	//End layout

	Sprite* newSprite = new Sprite();

	Sprite::SpriteData spriteData;
	spriteData.myVertexCount = sizeof(vertices) / sizeof(Vertex);
	spriteData.myStride = sizeof(Vertex);
	spriteData.myOffset = 0;
	spriteData.myVertexBuffer = vertexBuffer;
	spriteData.myVertexShader = vertexShader;
	spriteData.myPixelShader = pixelShader;
	spriteData.myPrimitiveTopology = D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
	spriteData.myInputLayout = inputLayout;
	newSprite->Init(spriteData, myWindowSize);
	mySprites[myTriangleName] = newSprite;

	return mySprites[myTriangleName];
}
