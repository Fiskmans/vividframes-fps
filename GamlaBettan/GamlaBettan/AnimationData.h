#pragma once
#include <Matrix4x4.hpp>
#include <Vector4.hpp>

struct VertexBoneData
{
	unsigned int IDs[MAX_NUM_BONES_PER_VEREX];
	float Weights[MAX_NUM_BONES_PER_VEREX];

	VertexBoneData();

	void Reset();

	void AddBoneData(unsigned int BoneID, float Weight);
};

enum class HitBoxType
{
	Head,
	Neck,
	Torso,
	Clavicle,
	UpperArm,
	LowerArm,
	Hand,
	Thigh,
	LowerLeg,
	Ankle,
	Foot,
	Count,
	None
};

struct BoneInfo
{
	CommonUtilities::Matrix4x4<float> BoneOffset;
	CommonUtilities::Matrix4x4<float> FinalTransformation;
	V4F BonePosition;
	size_t parent = -1;
	std::string myName;
};

struct HitBox
{
	std::string myName;
	HitBoxType myHitBoxType;
	int myOriginNodeIndex;
	int myTargetNodeIndex;
	BoneInfo boneData;
};


