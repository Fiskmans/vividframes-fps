#include "pch.h"
#include "SpriteInstance.h"
#include "Sprite.h"

SpriteInstance::SpriteInstance(Sprite* aSprite)
{
	mySprite = aSprite;

	myPivot = CU::Vector2<float>(0.0f, 0.0f);

	myColor = CU::Vector4<float>(1.0f, 1.0f, 1.0f, 1.0f);

	myUVMinMax = CU::Vector4<float>(0.f, 0.f, 1.f, 1.f);

	myTransform = CommonUtilities::Matrix4x4<float>::Identity();

	SetScale({ 1.f, 1.f });

	myIsListening = false;

	myIsAddedToScene = false;

#ifdef _DEBUG
	myScale = { 1.f, 1.f };
#endif
}

SpriteInstance::~SpriteInstance()
{
	if (myIsListening)
	{
		UnSubscribeToMessage(MessageType::WindowResize);
	}

	WIPE(*this);
}

Sprite* SpriteInstance::GetSprite()
{
	return mySprite;
}

void SpriteInstance::SetPosition(const CommonUtilities::Vector2<float>& aPosition)
{
	SetPosition(aPosition.x, aPosition.y);
}

void SpriteInstance::SetPosition(float aPositionX, float aPositionY)
{
	myXPos = (aPositionX * 2.f - 1.f);
	myYPos = -(aPositionY * 2.f - 1.f);
}

void SpriteInstance::SetScale(const CommonUtilities::Vector2<float>& aScale)
{
	//TODO: Set scale
	myTransform(1, 1) = aScale.x;
	myTransform(2, 2) = aScale.y;

#ifdef _DEBUG
	myScale = aScale;
#endif
}

void SpriteInstance::Rotate(const float aRotation)
{
	//RemovePivot();

	myTransform = M44F::CreateRotationAroundZ(aRotation) * myTransform;

	//AddPivot();
}

void SpriteInstance::SetRotation(const float aRotation)
{
	//V2F pos = { myXPos, myYPos };
	//V2F scale = GetScale(); //wrong, fix later (1,1 and 2,2 are affected by rot)
	//
	//myTransform = M44F::CreateRotationAroundZ(aRotation);
	//
	//myXPos = pos.x;
	//myYPos = pos.y;
	//
	//SetScale(scale);
}

void SpriteInstance::SetPivot(const CommonUtilities::Vector2<float>& aPivot)
{
	myPivot.x = (mySprite->GetSpriteData().mySize.x) / 1920.f * aPivot.x * 2.f; // 0 -> 1 space to -1 -> 1 space
	myPivot.y = (mySprite->GetSpriteData().mySize.y) / 1080.f * aPivot.y * 2.f;

	//myPivot.x = (mySprite->GetSpriteData().mySize.x) / 1920.f * aPivot.x * 2.f; // 0 -> 1 space to -1 -> 1 space
	//myPivot.y = (mySprite->GetSpriteData().mySize.y) / 1080.f * aPivot.y * 2.f;

	myPrecalculationPivot = aPivot;

	if (aPivot == V2F(0.f, 0.f))
	{
		if (!myIsListening)
		{
			return;
		}

		UnSubscribeToMessage(MessageType::WindowResize);
		myIsListening = false;
	}
	else if (!myIsListening)
	{
		SubscribeToMessage(MessageType::WindowResize);
		myIsListening = true;
	}

}

void SpriteInstance::SetColor(const CommonUtilities::Vector4<float>& aColor)
{
	myColor = aColor;
}

void SpriteInstance::SetUVMinMax(const CommonUtilities::Vector2<float>& aMin, const CommonUtilities::Vector2<float>& aMax)
{
	myUVMinMax.x = aMin.x;
	myUVMinMax.y = aMin.y;

	myUVMinMax.z = aMax.x;
	myUVMinMax.w = aMax.y;
}

void SpriteInstance::SetUVMinMax(const CommonUtilities::Vector4<float>& aMinMax)
{
	SetUVMinMax(V2F(aMinMax.x, aMinMax.y), V2F(aMinMax.z, aMinMax.w));
}

const CommonUtilities::Matrix4x4<float>& SpriteInstance::GetTransform() const
{
	return myTransform;
}

const CommonUtilities::Vector2<float> SpriteInstance::GetPosition() const
{
	return GetPivotPosition() - myPivot;
}

const CommonUtilities::Vector2<float> SpriteInstance::GetPivotPosition() const
{
	return CommonUtilities::Vector2<float>((myXPos + 1.f) * 0.5f, ((myYPos - 1.f) * -0.5f));
}

const CommonUtilities::Vector4<float>& SpriteInstance::GetColor() const
{
	return myColor;
}

const CommonUtilities::Vector2<float> SpriteInstance::GetScale() const
{
	return V2F(myTransform(1, 1), myTransform(2, 2));
}

const CommonUtilities::Vector2<float>& SpriteInstance::GetPivot() const
{
	return myPivot;
}

const CommonUtilities::Vector4<float>& SpriteInstance::GetUVMinMax() const
{
	return myUVMinMax;
}

const CommonUtilities::Vector2<float> SpriteInstance::GetImageSize() const
{
	return { CAST(float, mySprite->GetSpriteData().mySize.x), CAST(float, mySprite->GetSpriteData().mySize.y) };
}

const CommonUtilities::Vector2<float> SpriteInstance::GetSizeWithScale() const
{
	return { CAST(float, mySprite->GetSpriteData().mySize.x) * GetScale().x, CAST(float, mySprite->GetSpriteData().mySize.y) * GetScale().y };
}

const CommonUtilities::Vector2<float> SpriteInstance::GetSizeOnScreen() const
{
	return GetSizeWithScale() / mySprite->GetWindowSize();
}

void SpriteInstance::RecieveMessage(const Message& aMessage)
{
	if (aMessage.myMessageType == MessageType::WindowResize)
	{
		//SetPivot(myPrecalculationPivot);
	}
}

void SpriteInstance::AddToScene()
{
	myIsAddedToScene = true;
}

void SpriteInstance::RemoveFromScene()
{
	myIsAddedToScene = false;
}

bool SpriteInstance::HasBeenAddedToScene()
{
	return myIsAddedToScene;
}