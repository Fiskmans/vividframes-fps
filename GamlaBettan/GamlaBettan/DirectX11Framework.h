#pragma once

struct IDXGISwapChain;
struct IDXGISwapChain4;
struct ID3D11Device;
struct ID3D11DeviceContext;
struct ID3D11RenderTargetView;
struct ID3D11DepthStencilView;


class DirectX11Framework
{
public:
	DirectX11Framework();
	~DirectX11Framework();

	bool Init(void* aWindowHandle);
	bool Init(ID3D11Device* aDevice, ID3D11DeviceContext* aContext);

	void SetRenderTargetAndDepthStencil(ID3D11RenderTargetView* aRenterTarget, ID3D11DepthStencilView* aDepthStencil);


	void EndFrame();
	void WaitForFrameBuffer();
	void Resize(void* aHWND);

	ID3D11DeviceContext* GetContext();
	ID3D11Device* GetDevice();
	struct ID3D11Texture2D* GetBackbufferTexture();
	IDXGISwapChain* GetSwapChain();

private:
	HRESULT mySwapResult;
	ID3D11Device* myDevice;
	IDXGISwapChain* mySwapChain;
	IDXGISwapChain4* mySwapChain4;
	ID3D11DeviceContext* myDeviceContext;
	ID3D11RenderTargetView* myBackBuffer;
	ID3D11DepthStencilView* myDepthBuffer;
};

