#include "pch.h"
#include "RenderManager.h"
#include "DirectX11Framework.h"
#include "WindowHandler.h"
#include "Scene.h"
#include "Skybox.h"
#include "PostMaster.hpp"
#include <d3d11.h>
#include <imgui.h>
#include "DebugDrawer.h"
#include "Camera.h"

#include "ParticleFactory.h"
#include "ShaderFlags.h"
#include "ModelInstance.h"
#include "Model.h"
#include "TextureLoader.h"

#if USEIMGUI
#include <ImGuiHelpers.h>
#endif


ID3D11DeviceContext* GetAndOrSetContext(ID3D11DeviceContext* aContext = nullptr, bool aReallyShouldSet = false)
{
	static ID3D11DeviceContext* context = nullptr;
	if (aContext || aReallyShouldSet)
	{
		context = aContext;
	}
	return context;
}

ID3D11Resource* GetAndOrSetResource(ID3D11Resource* aResource = nullptr, bool aReallyShouldSet = false)
{
	static ID3D11Resource* resource = nullptr;
	if (aResource || aReallyShouldSet)
	{
		resource = aResource;
	}
	return resource;
}

void SetScreendumpTexture(ID3D11DeviceContext* aContext, ID3D11Resource* aResource)
{
	GetAndOrSetContext(aContext);
	GetAndOrSetResource(aResource);
}

void ClearScreendumpTexture()
{
	if (GetAndOrSetResource())
	{
		GetAndOrSetResource()->Release();
	}

	GetAndOrSetContext(nullptr, true);
	GetAndOrSetResource(nullptr, true);
}

RenderManager::RenderManager() :
	myFrameworkPtr(nullptr),
#if USEIMGUI
	myShouldRenderWireFrame(false),
#endif // USEIMGUI
	myStateManerger(RenderStateManager()),
#if ENABLERENDERPASSITERATIONS
	myCurrentRenderStep(RenderStep::All),
#endif
	myStartedAt(0)
{
}

bool RenderManager::Init(DirectX11Framework* aFramework, WindowHandler* aWindowHandler)
{
	if (!myStateManerger.Init(aFramework))
	{
		return false;
	}
	myPerlinView = GeneratePerlin(aFramework->GetDevice(), { 256,256 }, { 1,1 }, 2);
	if (!myForwardRenderer.Init(aFramework, "Data/Shaders/ThroughWall.hlsl", "Data/Shaders/ThroughWallEnemy.hlsl", &myPerlinView))
	{
		return false;
	}
	if (!myFullscreenRenderer.Init(aFramework))
	{
		return false;
	}
	if (!myFullscreenFactory.Init(aFramework))
	{
		return false;
	}
	if (!mySpriteRenderer.Init(aFramework))
	{
		return false;
	}
	if (!myParticleRenderer.Init(aFramework))
	{
		return false;
	}
	if (!myDeferredRenderer.Init(aFramework, &myPerlinView, &myShadowRenderer))
	{
		return false;
	}
	if (!myTextRenderer.Init(aFramework))
	{
		return false;
	}
	if (!myHighlightRenderer.Init(aFramework))
	{
		return false;
	}
	if (!myShadowRenderer.Init(aFramework))
	{
		return false;
	}

	DebugDrawer::GetInstance().Init(aFramework);
	DebugDrawer::GetInstance().SetColor(V4F(0.8f, 0.2f, 0.2f, 1.f));
	myFrameworkPtr = aFramework;

	if (!CreateTextures(aWindowHandler->GetWidth(), aWindowHandler->GetHeight()))
	{
		return false;
	}

	myStartedAt = std::chrono::time_point_cast<std::chrono::milliseconds>(std::chrono::system_clock::now()).time_since_epoch().count();

	return true;
}

bool RenderManager::Release()
{
	myGBuffer.Release();
	myBufferGBuffer.Release();
	for (auto& i : myTextures)
	{
		i.Release();
	}
	return true;
}

void RenderManager::BeginFrame(float aClearColor[4])
{
	myTextures[static_cast<int>(Textures::BackBuffer)].ClearTexture({ 0 });
	myTextures[static_cast<int>(Textures::IntermediateTexture)].ClearTexture({ aClearColor[0],aClearColor[1],aClearColor[2],aClearColor[3] });
	myTextures[static_cast<int>(Textures::IntermediateDepth)].ClearDepth();
	myTextures[static_cast<int>(Textures::Selection)].ClearTexture({ 0 });
	myTextures[static_cast<int>(Textures::Selection2)].ClearTexture({ 0 });
	myTextures[static_cast<int>(Textures::SelEdges)].ClearTexture({ 0 });
	myTextures[static_cast<int>(Textures::SelEdgesHalf)].ClearTexture({ 0 });
	myTextures[static_cast<int>(Textures::BackFaceBuffer)].ClearTexture({ 0 });
	myGBuffer.ClearTextures();
	myBufferGBuffer.ClearTextures();

	myExtraSpritesToRenderThisFrame.clear();
}

void RenderManager::EndFrame()
{
}

void RenderManager::Render(Scene* aScene)
{
	long long now = std::chrono::time_point_cast<std::chrono::milliseconds>(std::chrono::system_clock::now()).time_since_epoch().count();
	float totalTime = static_cast<float>(now - myStartedAt) * 0.001f;
	static float lastTime = totalTime;
	float deltatime = totalTime - lastTime;
	lastTime = totalTime;

	static std::vector<ModelInstance*> modelsLeftToRender;
	static std::vector<SpriteInstance*> sprites;
	static std::vector<std::array<PointLight*, NUMBEROFPOINTLIGHTS>> affectingLights;
	{
		PERFORMANCETAG("Culling");
		modelsLeftToRender = aScene->Cull(aScene->GetMainCamera());
	}
	std::vector<ModelInstance*> modelsToHighlight;
	std::vector<ModelInstance*> shadowCasters = modelsLeftToRender;
	for (auto& mod : modelsLeftToRender)
	{
		if (mod->GetIsHighlighted())
		{
			modelsToHighlight.push_back(mod);
		}
	}
	sprites = aScene->GetSprites();
	for (auto& sprite : myExtraSpritesToRenderThisFrame)
	{
		sprites.push_back(sprite);
	}

	affectingLights.clear();
	{
		PERFORMANCETAG("BoneTexture Generation");
		SetupBoneTexture(modelsLeftToRender, deltatime);
	}



	myStateManerger.SetAllStates();
#if USEIMGUI
	if (myShouldRenderWireFrame)
	{
		myStateManerger.SetRasterizerState(RenderStateManager::RasterizerState::Wireframe);;
	}
#endif // !_RETAIL
	ID3D11RenderTargetView* resetTargets[8] = { nullptr };

	ID3D11ShaderResourceView* resetViews[16] = { nullptr };
	{
		PERFORMANCETAG("Deferred Gbuffer");
		myFrameworkPtr->GetContext()->OMSetRenderTargets(8, resetTargets, nullptr);
		myFrameworkPtr->GetContext()->PSSetShaderResources(0, 16, resetViews);
		myStateManerger.SetDepthStencilState(RenderStateManager::DepthStencilState::Default);
		myFrameworkPtr->GetContext()->VSSetShaderResources(5, 1, &myBoneTextureView);
		modelsLeftToRender = myDeferredRenderer.GenerateGBuffer(aScene->GetMainCamera(), modelsLeftToRender, myBoneOffsetMap, &myTextures[static_cast<int>(Textures::BackFaceBuffer)], &myStateManerger,aScene->GetDecals(),&myGBuffer,&myBufferGBuffer,myFullscreenRenderer,aScene, &myTextures[ENUM_CAST(Textures::IntermediateDepth)]);
		myStateManerger.SetAllStates();
	}
#if ENABLERENDERPASSITERATIONS
	switch (myCurrentRenderStep)
	{
	case RenderStep::GAlbedo:
		myTextures[ENUM_CAST(Textures::BackBuffer)].SetAsActiveTarget();
		myGBuffer.SetAsResourceOnSlot(GBuffer::Textures::Albedo, 0);
		myFullscreenRenderer.Render(FullscreenRenderer::Shader::COPY);
		return;
	case RenderStep::GNormal:
		myTextures[ENUM_CAST(Textures::BackBuffer)].SetAsActiveTarget();
		myGBuffer.SetAsResourceOnSlot(GBuffer::Textures::Normal, 0);
		myFullscreenRenderer.Render(FullscreenRenderer::Shader::COPY);
		return;
	case RenderStep::GVertexNormal:
		myTextures[ENUM_CAST(Textures::BackBuffer)].SetAsActiveTarget();
		myGBuffer.SetAsResourceOnSlot(GBuffer::Textures::VertexNormal, 0);
		myFullscreenRenderer.Render(FullscreenRenderer::Shader::COPY);
		return;
	case RenderStep::GMetalness:
		myTextures[ENUM_CAST(Textures::BackBuffer)].SetAsActiveTarget();
		myGBuffer.SetAsResourceOnSlot(GBuffer::Textures::Metalness, 0);
		myFullscreenRenderer.Render(FullscreenRenderer::Shader::COPY);
		return;
	case RenderStep::GRoughness:
		myTextures[ENUM_CAST(Textures::BackBuffer)].SetAsActiveTarget();
		myGBuffer.SetAsResourceOnSlot(GBuffer::Textures::Roughness, 0);
		myFullscreenRenderer.Render(FullscreenRenderer::Shader::COPY);
		return;
	case RenderStep::GAmbientOcclusion:
		myTextures[ENUM_CAST(Textures::BackBuffer)].SetAsActiveTarget();
		myGBuffer.SetAsResourceOnSlot(GBuffer::Textures::AmbientOcclusion, 0);
		myFullscreenRenderer.Render(FullscreenRenderer::Shader::COPY);
		return;
	case RenderStep::GEmissive:
		myTextures[ENUM_CAST(Textures::BackBuffer)].SetAsActiveTarget();
		myGBuffer.SetAsResourceOnSlot(GBuffer::Textures::Emissive, 0);
		myFullscreenRenderer.Render(FullscreenRenderer::Shader::COPY);
		return;
	}
#endif

	if (myDoSSAO)
	{
		PERFORMANCETAG("SSAO");
		myFrameworkPtr->GetContext()->OMSetRenderTargets(8, resetTargets, nullptr);
		myFrameworkPtr->GetContext()->PSSetShaderResources(0, 16, resetViews);
		myGBuffer.SetAllAsResources();
		myFrameworkPtr->GetContext()->PSSetShaderResources(9, 1, *myRandomNormalview);
		myDeferredRenderer.MapEnvLightBuffer(aScene);
		myTextures[static_cast<int>(Textures::BackFaceBuffer)].SetAsResourceOnSlot(10);
		myTextures[ENUM_CAST(Textures::IntermediateDepth)].SetAsResourceOnSlot(8);
		myTextures[static_cast<int>(Textures::SSAOBuffer)].SetAsActiveTarget();

		myFullscreenRenderer.Render(FullscreenRenderer::Shader::SSAO);

		myFrameworkPtr->GetContext()->OMSetRenderTargets(8, resetTargets, nullptr);
		myFrameworkPtr->GetContext()->PSSetShaderResources(0, 16, resetViews);
		myTextures[static_cast<int>(Textures::SSAOBuffer)].SetAsResourceOnSlot(0);
		myGBuffer.SetAsActiveTarget(GBuffer::Textures::AmbientOcclusion);

		myFullscreenRenderer.Render(FullscreenRenderer::Shader::COPY);
	}


#if ENABLERENDERPASSITERATIONS
	if (myCurrentRenderStep == RenderStep::SSAO)
	{
		myTextures[ENUM_CAST(Textures::BackBuffer)].SetAsActiveTarget();
		myGBuffer.SetAsResourceOnSlot(GBuffer::Textures::AmbientOcclusion, 0);
		myFullscreenRenderer.Render(FullscreenRenderer::Shader::COPY);
		return;
	}
	if (myCurrentRenderStep == RenderStep::SSAOPost)
	{
		myFrameworkPtr->GetContext()->OMSetRenderTargets(8, resetTargets, nullptr);
		myFrameworkPtr->GetContext()->PSSetShaderResources(0, 16, resetViews);
		myTextures[ENUM_CAST(Textures::BackBuffer)].SetAsActiveTarget();
		myGBuffer.SetAsResourceOnSlot(GBuffer::Textures::AmbientOcclusion, 0);
		myFullscreenRenderer.Render(FullscreenRenderer::Shader::COPY);
		return;
	}
#endif // ENABLERENDERPASSITERATIONS

	{
		PERFORMANCETAG("Deferred render");
		myFrameworkPtr->GetContext()->OMSetRenderTargets(8, resetTargets, nullptr);
		myGBuffer.SetAllAsResources();
		myTextures[static_cast<int>(Textures::IntermediateTexture)].SetAsActiveTarget();
		myTextures[ENUM_CAST(Textures::IntermediateDepth)].SetAsResourceOnSlot(15);
		myStateManerger.SetSamplerState(RenderStateManager::SamplerState::Point);
		myStateManerger.SetRasterizerState(RenderStateManager::RasterizerState::Default);
		myDeferredRenderer.Render(myFullscreenRenderer, aScene->GetPointLights(),aScene->GetSpotLights(), aScene, &myStateManerger, myBoneOffsetMap);
		myStateManerger.SetSamplerState(RenderStateManager::SamplerState::Trilinear);
	}
#if ENABLERENDERPASSITERATIONS
	if (EarlyReturn(RenderStep::DeferredDepth, Textures::IntermediateDepth))
	{
		return;
	}
	if (EarlyReturn(RenderStep::Deferred, Textures::IntermediateTexture))
	{
		return;
	}
#endif

	for (auto& model : modelsLeftToRender)
	{
		affectingLights.push_back(aScene->CullPointLights(model));
	}

	Skybox* skybox = aScene->GetSkybox();
	if (skybox)
	{
		myForwardRenderer.SetSkybox(skybox);
	}


	myStateManerger.SetAllStates();
#if USEIMGUI
	if (myShouldRenderWireFrame)
	{
		myStateManerger.SetRasterizerState(RenderStateManager::RasterizerState::Wireframe);;
	}
#endif // !_RETAIL
	ID3D11ShaderResourceView* views[] = { nullptr };
	{
		PERFORMANCETAG("Forward render");
		myFrameworkPtr->GetContext()->PSSetShaderResources(15, 1, views);
		myStateManerger.SetBlendState(RenderStateManager::BlendState::AlphaBlend);
		//myStateManerger.SetDepthStencilState(RenderStateManager::DepthStencilState::ReadOnly);
		myTextures[static_cast<int>(Textures::IntermediateTexture)].SetAsActiveTarget(&myTextures[static_cast<int>(Textures::IntermediateDepth)]);
		myForwardRenderer.Render(modelsLeftToRender, aScene->GetMainCamera(), aScene, affectingLights, myBoneOffsetMap, myStateManerger);
		myStateManerger.SetBlendState(RenderStateManager::BlendState::Disable);
	}
#if ENABLERENDERPASSITERATIONS
	if (EarlyReturn(RenderStep::ForwardDepth, Textures::IntermediateDepth))
	{
		return;
	}
	if (EarlyReturn(RenderStep::Forward, Textures::IntermediateTexture))
	{
		return;
	}
#endif
	{
		PERFORMANCETAG("Particles");
		myStateManerger.SetBlendState(RenderStateManager::BlendState::AlphaBlend);
		myStateManerger.SetDepthStencilState(RenderStateManager::DepthStencilState::ReadOnly);
		ID3D11ShaderResourceView* arr[16] = { nullptr };
		myFrameworkPtr->GetContext()->PSSetShaderResources(0, 16, arr);
		myTextures[static_cast<int>(Textures::IntermediateTexture)].SetAsActiveTarget(&myTextures[static_cast<int>(Textures::IntermediateDepth)]);
		myParticleRenderer.Render(aScene->GetMainCamera(), aScene->GetParticles());
	}

#if ENABLERENDERPASSITERATIONS
	if (EarlyReturn(RenderStep::Particles, Textures::IntermediateTexture))
	{
		return;
	}
#endif

	{
		PERFORMANCETAG("Debuglines");
		myStateManerger.SetAllStates();
		myStateManerger.SetBlendState(RenderStateManager::BlendState::AlphaBlend);
		myTextures[static_cast<int>(Textures::IntermediateTexture)].SetAsActiveTarget(&myTextures[static_cast<int>(Textures::IntermediateDepth)]);
		DebugDrawer::GetInstance().Render(aScene->GetMainCamera());
		myStateManerger.SetBlendState(RenderStateManager::BlendState::Disable);
	}

#if ENABLERENDERPASSITERATIONS
	if (EarlyReturn(RenderStep::DebugLines, Textures::IntermediateTexture))
	{
		return;
	}
#endif
	myFrameworkPtr->WaitForFrameBuffer();
	{
		PERFORMANCETAG("Bloom");
		myTextures[static_cast<int>(Textures::Luminance)].SetAsActiveTarget();
		myTextures[static_cast<int>(Textures::IntermediateTexture)].SetAsResourceOnSlot(0);
		myFullscreenRenderer.Render(FullscreenRenderer::Shader::LUMINANCE);

#if ENABLERENDERPASSITERATIONS
		if (EarlyReturn(RenderStep::Luminance, Textures::Luminance))
		{
			return;
		}
#endif

		myTextures[static_cast<int>(Textures::HalfSize)].SetAsActiveTarget();
		myTextures[static_cast<int>(Textures::Luminance)].SetAsResourceOnSlot(0);
		myFullscreenRenderer.Render(FullscreenRenderer::Shader::COPY);

		myTextures[static_cast<int>(Textures::QuaterSize)].SetAsActiveTarget();
		myTextures[static_cast<int>(Textures::HalfSize)].SetAsResourceOnSlot(0);
		myFullscreenRenderer.Render(FullscreenRenderer::Shader::COPY);

		myTextures[static_cast<int>(Textures::HalfQuaterSize)].SetAsActiveTarget();
		myTextures[static_cast<int>(Textures::QuaterSize)].SetAsResourceOnSlot(0);
		myFullscreenRenderer.Render(FullscreenRenderer::Shader::COPY);

		myTextures[static_cast<int>(Textures::Guassian1)].SetAsActiveTarget();
		myTextures[static_cast<int>(Textures::HalfQuaterSize)].SetAsResourceOnSlot(0);
		myFullscreenRenderer.Render(FullscreenRenderer::Shader::GAUSSIANHORIZONTAL);

		myTextures[static_cast<int>(Textures::Guassian2)].SetAsActiveTarget();
		myTextures[static_cast<int>(Textures::Guassian1)].SetAsResourceOnSlot(0);
		myFullscreenRenderer.Render(FullscreenRenderer::Shader::GAUSSIANVERTICAL);

#if ENABLERENDERPASSITERATIONS
		if (myCurrentRenderStep == RenderStep::Blur)
		{
			myTextures[ENUM_CAST(Textures::BackBuffer)].SetAsActiveTarget();
			myTextures[static_cast<int>(Textures::Guassian2)].SetAsResourceOnSlot(0);
			myFullscreenRenderer.Render(FullscreenRenderer::Shader::COPY);
			return;
		}
#endif

		myTextures[static_cast<int>(Textures::HalfQuaterSize)].SetAsActiveTarget();
		myTextures[static_cast<int>(Textures::Guassian2)].SetAsResourceOnSlot(0);
		myFullscreenRenderer.Render(FullscreenRenderer::Shader::COPY);

		myTextures[static_cast<int>(Textures::QuaterSize)].SetAsActiveTarget();
		myTextures[static_cast<int>(Textures::HalfQuaterSize)].SetAsResourceOnSlot(0);
		myFullscreenRenderer.Render(FullscreenRenderer::Shader::COPY);

		myTextures[static_cast<int>(Textures::HalfSize)].SetAsActiveTarget();
		myTextures[static_cast<int>(Textures::QuaterSize)].SetAsResourceOnSlot(0);
		myFullscreenRenderer.Render(FullscreenRenderer::Shader::COPY);

		myTextures[static_cast<int>(Textures::BackBuffer)].SetAsActiveTarget();
		myTextures[static_cast<int>(Textures::HalfSize)].SetAsResourceOnSlot(1);
		myTextures[static_cast<int>(Textures::IntermediateTexture)].SetAsResourceOnSlot(0);
		myFullscreenRenderer.Render(FullscreenRenderer::Shader::MERGE);
	}

#if ENABLERENDERPASSITERATIONS
	if (myCurrentRenderStep == RenderStep::Bloom)
	{
		return;
	}
#endif
	RenderSelection(modelsToHighlight, aScene->GetMainCamera());

	myTextures[static_cast<int>(Textures::BackBuffer)].SetAsActiveTarget();


	RenderSprites(sprites);
	RenderText(aScene->GetText()); //Todo: probs some culling
}

void RenderManager::RenderMovie(const std::vector<SpriteInstance*>& aSpriteList)
{
	myFrameworkPtr->WaitForFrameBuffer();

	myTextures[static_cast<int>(Textures::BackBuffer)].SetAsActiveTarget();

	myStateManerger.SetBlendState(RenderStateManager::BlendState::AlphaBlend);
	myStateManerger.SetSamplerState(RenderStateManager::SamplerState::Trilinear);
	mySpriteRenderer.Render(aSpriteList);
	myStateManerger.SetBlendState(RenderStateManager::BlendState::Disable);

}

void RenderManager::RenderSprites(const std::vector<SpriteInstance*>& aSpriteList, const bool aShouldRenderExtraSprites)
{
	if (aSpriteList.size() == 0)
	{
		return;
	}

	myFrameworkPtr->WaitForFrameBuffer();
	myTextures[static_cast<int>(Textures::BackBuffer)].SetAsActiveTarget();
	myStateManerger.SetBlendState(RenderStateManager::BlendState::AlphaBlend);
	myStateManerger.SetRasterizerState(RenderStateManager::RasterizerState::NoBackfaceCulling);

	mySpriteRenderer.Render(aSpriteList);

	if (aShouldRenderExtraSprites && myExtraSpritesToRenderThisFrame.size() > 0)
	{
		mySpriteRenderer.Render(myExtraSpritesToRenderThisFrame);
	}

	myStateManerger.SetBlendState(RenderStateManager::BlendState::Disable);
	myStateManerger.SetRasterizerState(RenderStateManager::RasterizerState::Default);
}

void RenderManager::RenderText(const std::vector<TextInstance*>& aTextList)
{
	myFrameworkPtr->WaitForFrameBuffer();
	myTextures[static_cast<int>(Textures::BackBuffer)].SetAsActiveTarget();
	myStateManerger.SetBlendState(RenderStateManager::BlendState::AlphaBlend);
	myStateManerger.SetRasterizerState(RenderStateManager::RasterizerState::NoBackfaceCulling); //Not sure if needed :o

	myTextRenderer.Render(aTextList);

	myStateManerger.SetBlendState(RenderStateManager::BlendState::Disable);
	myStateManerger.SetRasterizerState(RenderStateManager::RasterizerState::Default);

}

void RenderManager::AddExtraSpriteToRender(SpriteInstance* aSprite)
{
	if (aSprite)
	{
		myExtraSpritesToRenderThisFrame.push_back(aSprite);
		return;
	}

	SYSWARNING("Tried to add null sprite to render list", "");
}

void RenderManager::SubscribeToMessages()
{
	PostMaster::GetInstance()->Subscribe(MessageType::WindowResize, this);
	PostMaster::GetInstance()->Subscribe(MessageType::NextRenderPass, this);
	myDeferredRenderer.SubscribeToMessages();
	myParticleRenderer.SubscribeToMessages();
	myForwardRenderer.SubscribeToMessages();
}

void RenderManager::UnsubscribeToMessages()
{
	PostMaster::GetInstance()->UnSubscribe(MessageType::WindowResize, this);
	PostMaster::GetInstance()->UnSubscribe(MessageType::NextRenderPass, this);
	myDeferredRenderer.UnsubscribeToMessages();
	myParticleRenderer.UnsubscribeToMessages();
	myForwardRenderer.UnsubscribeToMessages();
}

void RenderManager::RecieveMessage(const Message& aMessage)
{
	switch (aMessage.myMessageType)
	{
	case MessageType::WindowResize:
		Resize(aMessage.myIntValue, aMessage.myIntValue2);
		break;
#if ENABLERENDERPASSITERATIONS
	case MessageType::NextRenderPass:
		myCurrentRenderStep = static_cast<RenderStep>((static_cast<int>(myCurrentRenderStep) + 1) % static_cast<int>(RenderStep::Count));
		break;
#endif // ENABLERENDERPASSITERATIONS
	default:
		break;
	}
}

void RenderManager::SetupBoneTexture(const std::vector<ModelInstance*>& aModelList, float aDeltaTime)
{
	static_assert(sizeof(char) == 1 && CHAR_BIT == 8, "Look over this code");
	myBoneOffsetMap.clear();


	std::array<CommonUtilities::Matrix4x4<float>, NUMBEROFANIMATIONBONES> matrixes;
	unsigned char boneIndexOffset = 0;

	ID3D11DeviceContext* context = myFrameworkPtr->GetContext();
	assert(context && "Hol' up, wtf");
	D3D11_MAPPED_SUBRESOURCE mappedResource;
	context->Map(myBoneBufferTexture, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);

	char* targetBuffer = static_cast<char*>(mappedResource.pData);
	for (auto& model : aModelList)
	{
		if (boneIndexOffset >= MAXNUMBEROFANIMATIONSONSCREEN)
		{
			break;
		}
		if (model->GetModel()->ShouldRender() && model->GetModel()->GetModelData()->myshaderTypeFlags & ShaderFlags::HasBones)
		{
			for (size_t i = 0; i < NUMBEROFANIMATIONBONES; i++)
			{
				matrixes[i] = CommonUtilities::Matrix4x4<float>(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);
			}
			model->SetupanimationMatrixes(matrixes);
			memcpy(targetBuffer + boneIndexOffset * sizeof(matrixes), &matrixes[0], sizeof(matrixes));
			myBoneOffsetMap[model] = boneIndexOffset;
			boneIndexOffset++;
		}
	}
	context->Unmap(myBoneBufferTexture, 0);
}

SpriteRenderer* RenderManager::GetSpriteRenderer()
{
	return &mySpriteRenderer;
}

#if USEIMGUI
void RenderManager::Imgui()
{
	ImGui::Checkbox("Wireframe", &myShouldRenderWireFrame);
	ImGui::BeginGroup();
	ImGui::Checkbox("SSAO", &myDoSSAO);
#if ENABLERENDERPASSITERATIONS
	if (ImGui::TreeNode("Render passes"))
	{
		for (size_t i = 0; i < ENUM_CAST(RenderStep::Count); i++)
		{
			bool selected = i == ENUM_CAST(myCurrentRenderStep);
			if (ImGui::Selectable(names[ENUM_CAST(i)], &selected))
			{
				myCurrentRenderStep = static_cast<RenderStep>(i);
			}
		}
		ImGui::TreePop();
	}
	if (ImGui::TreeNode("Perlin"))
	{
		static CommonUtilities::Vector2<float> scale = { 1,1 };
		static CommonUtilities::Vector2<size_t> size = { 256,256 };
		static int seed = 0;
		bool update = false;
		update |= ImGui::SliderFloat("XScale", &scale.x, 0.01f, 200.f);
		update |= ImGui::SliderFloat("YScale", &scale.y, 0.01f, 200.f);
		update |= ImGui::InputInt("Seed", &seed);

		if (ImGui::Button("X+"))
		{
			if (size.x < 1 << 13)
			{
				size.x *= 2;
				update |= true;
			}
		}
		ImGui::SameLine();
		if (ImGui::Button("X-"))
		{
			if (size.x > 64)
			{
				size.x /= 2;
				update |= true;
			}
		}
		ImGui::SameLine();
		ImGui::Text(("width: " + std::to_string(size.x)).c_str());
		if (ImGui::Button("Y+"))
		{
			if (size.y < 1 << 13)
			{
				size.y *= 2;
				update |= true;
			}
		}
		ImGui::SameLine();
		if (ImGui::Button("Y-"))
		{
			if (size.y > 64)
			{
				size.y /= 2;
				update |= true;
			}
		}
		ImGui::SameLine();
		ImGui::Text(("height: " + std::to_string(size.y)).c_str());
		if (update)
		{
			Texture* newperlin = GeneratePerlin(myFrameworkPtr->GetDevice(), size, scale, seed);
			if (newperlin)
			{
				myPerlinView->Release();
				myPerlinView = newperlin;
			}
		}
		ImGui::TreePop();
	}
#endif // ENABLERENDERPASSITERATIONS


	ImGui::EndGroup();

}
#endif // !_RETAIL


void RenderManager::RenderSelection(const std::vector<ModelInstance*>& aModelsToHighlight, Camera* aCamera)
{
	myFrameworkPtr->WaitForFrameBuffer();
	//return;
	myTextures[static_cast<int>(Textures::Selection)].SetAsActiveTarget();
	myHighlightRenderer.Render(aModelsToHighlight, aCamera, myBoneOffsetMap);

	myTextures[static_cast<int>(Textures::SelectionScaleDown1)].SetAsActiveTarget();
	myTextures[static_cast<int>(Textures::Selection)].SetAsResourceOnSlot(0);
	myFullscreenRenderer.Render(FullscreenRenderer::Shader::COPY);

	myTextures[static_cast<int>(Textures::SelEdgesHalf)].SetAsActiveTarget();
	myTextures[static_cast<int>(Textures::SelectionScaleDown1)].SetAsResourceOnSlot(0);
	myFullscreenRenderer.Render(FullscreenRenderer::Shader::GAUSSIANVERTICAL);

	myTextures[static_cast<int>(Textures::SelEdges)].SetAsActiveTarget();
	myTextures[static_cast<int>(Textures::SelEdgesHalf)].SetAsResourceOnSlot(0);
	myFullscreenRenderer.Render(FullscreenRenderer::Shader::GAUSSIANHORIZONTAL);
	
	myTextures[static_cast<int>(Textures::SelectionScaleDown1)].SetAsActiveTarget();
	myTextures[static_cast<int>(Textures::SelEdges)].SetAsResourceOnSlot(0);
	myFullscreenRenderer.Render(FullscreenRenderer::Shader::COPY);

	myTextures[static_cast<int>(Textures::Selection2)].SetAsActiveTarget();
	myTextures[static_cast<int>(Textures::Selection)].SetAsResourceOnSlot(0);
	myTextures[static_cast<int>(Textures::SelectionScaleDown1)].SetAsResourceOnSlot(1);
	myFullscreenRenderer.Render(FullscreenRenderer::Shader::MERGE);

	myStateManerger.SetBlendState(RenderStateManager::BlendState::AlphaBlend);
	myTextures[static_cast<int>(Textures::BackBuffer)].SetAsActiveTarget();
	myTextures[static_cast<int>(Textures::Selection2)].SetAsResourceOnSlot(0);
	myFullscreenRenderer.Render(FullscreenRenderer::Shader::DiscardFull);
	myStateManerger.SetBlendState(RenderStateManager::BlendState::Disable);
}

bool RenderManager::CreateTextures(const unsigned int aWidth, const unsigned int aHeight)
{
	ID3D11Texture2D* backBufferTexture = myFrameworkPtr->GetBackbufferTexture();
	if (!backBufferTexture)
	{
		SYSERROR("Could not get back buffer texture","");
		return false;
	}

	myTextures[static_cast<int>(Textures::BackBuffer)] = myFullscreenFactory.CreateTexture(backBufferTexture);
	myTextures[static_cast<int>(Textures::IntermediateDepth)] = myFullscreenFactory.CreateDepth({ aWidth, aHeight });

	myTextures[static_cast<int>(Textures::IntermediateTexture)] = myFullscreenFactory.CreateTexture({ aWidth, aHeight }, DXGI_FORMAT::DXGI_FORMAT_R8G8B8A8_UNORM);
	myTextures[static_cast<int>(Textures::HalfSize)] = myFullscreenFactory.CreateTexture({ aWidth / 2U, aHeight / 2U }, DXGI_FORMAT::DXGI_FORMAT_R8G8B8A8_UNORM);
	myTextures[static_cast<int>(Textures::QuaterSize)] = myFullscreenFactory.CreateTexture({ aWidth / 4U, aHeight / 4U }, DXGI_FORMAT::DXGI_FORMAT_R8G8B8A8_UNORM);
	myTextures[static_cast<int>(Textures::HalfQuaterSize)] = myFullscreenFactory.CreateTexture({ aWidth / 8U, aHeight / 8U }, DXGI_FORMAT::DXGI_FORMAT_R8G8B8A8_UNORM);

	myTextures[static_cast<int>(Textures::SSAOBuffer)] = myFullscreenFactory.CreateTexture({ aWidth, aHeight }, DXGI_FORMAT::DXGI_FORMAT_R8_UNORM);
	myTextures[static_cast<int>(Textures::BackFaceBuffer)] = myFullscreenFactory.CreateTexture({ aWidth, aHeight }, DXGI_FORMAT::DXGI_FORMAT_R8G8B8A8_UNORM);

	myTextures[static_cast<int>(Textures::Guassian1)] = myFullscreenFactory.CreateTexture({ aWidth / 8U, aHeight / 8U }, DXGI_FORMAT::DXGI_FORMAT_R8G8B8A8_UNORM);
	myTextures[static_cast<int>(Textures::Guassian2)] = myFullscreenFactory.CreateTexture({ aWidth / 8U, aHeight / 8U }, DXGI_FORMAT::DXGI_FORMAT_R8G8B8A8_UNORM);

	myTextures[static_cast<int>(Textures::Luminance)] = myFullscreenFactory.CreateTexture({ aWidth, aHeight }, DXGI_FORMAT::DXGI_FORMAT_R8G8B8A8_UNORM);


	myTextures[static_cast<int>(Textures::SelectionScaleDown1)] = myFullscreenFactory.CreateTexture({ aWidth / 2U, aHeight / 2U }, DXGI_FORMAT::DXGI_FORMAT_R8G8B8A8_UNORM);
	myTextures[static_cast<int>(Textures::SelectionScaleDown2)] = myFullscreenFactory.CreateTexture({ aWidth / 4U, aHeight / 4U }, DXGI_FORMAT::DXGI_FORMAT_R8G8B8A8_UNORM);

	myTextures[static_cast<int>(Textures::Selection)] = myFullscreenFactory.CreateTexture({ aWidth / 2U, aHeight / 2U }, DXGI_FORMAT::DXGI_FORMAT_R8G8B8A8_UNORM);
	myTextures[static_cast<int>(Textures::SelEdgesHalf)] = myFullscreenFactory.CreateTexture({ aWidth / 2U, aHeight / 2U }, DXGI_FORMAT::DXGI_FORMAT_R8G8B8A8_UNORM);
	myTextures[static_cast<int>(Textures::SelEdges)] = myFullscreenFactory.CreateTexture({ aWidth / 2U, aHeight / 2U }, DXGI_FORMAT::DXGI_FORMAT_R8G8B8A8_UNORM);
	myTextures[static_cast<int>(Textures::Selection2)] = myFullscreenFactory.CreateTexture({ aWidth, aHeight }, DXGI_FORMAT::DXGI_FORMAT_R8G8B8A8_UNORM);

	myRandomNormalview = LoadTexture(myFrameworkPtr->GetDevice(), "Data/Textures/SSAONormal.dds");
	myGBuffer = myFullscreenFactory.CreateGBuffer({ aWidth,aHeight });
	myBufferGBuffer = myFullscreenFactory.CreateGBuffer({ aWidth,aHeight });

	for (auto& i : myTextures)
	{
		if (!i)
		{
			SYSERROR("Could not initialize all fullscreen textures","");
			return false;
		}
	}


	ID3D11Device* device = myFrameworkPtr->GetDevice();

	CD3D11_TEXTURE2D_DESC desc;
	WIPE(desc);
	desc.Width = sizeof(CommonUtilities::Matrix4x4<float>) * NUMBEROFANIMATIONBONES / 16;
	desc.Height = MAXNUMBEROFANIMATIONSONSCREEN;
	desc.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
	desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	desc.Usage = D3D11_USAGE_DYNAMIC;
	desc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
	desc.MipLevels = 1;
	desc.ArraySize = 1;
	desc.SampleDesc.Count = 1;
	desc.SampleDesc.Quality = 0;
	desc.MiscFlags = 0;

	HRESULT result = device->CreateTexture2D(&desc, nullptr, &myBoneBufferTexture);
	if (FAILED(result))
	{
		SYSERROR("Could not create bone buffer texture","");
		return false;
	}


	result = device->CreateShaderResourceView(myBoneBufferTexture, nullptr, &myBoneTextureView);
	if (FAILED(result))
	{
		SYSERROR("Could not create bone shader resource view","");
		return false;
	}

	return true;
}

void RenderManager::Resize(const unsigned int aWidth, const unsigned int aHeight)
{
	if (myScreenSize.x != aWidth || myScreenSize.y != aHeight)
	{
		myFrameworkPtr->GetContext()->OMSetRenderTargets(0, nullptr, nullptr);

		for (auto& tex : myTextures)
		{
			tex.Release();
		}
		myGBuffer.Release();
		myBufferGBuffer.Release();
		ClearScreendumpTexture();


		HRESULT result;
		IDXGISwapChain* chain = myFrameworkPtr->GetSwapChain();
		if (chain)
		{
			result = chain->ResizeBuffers(0, 0, 0, DXGI_FORMAT_UNKNOWN, 0);
			if (FAILED(result))
			{
				SYSERROR("Could not resize swap chain buffer.","");
			}
		}

		CreateTextures(aWidth, aHeight);
		myFrameworkPtr->WaitForFrameBuffer();
		myTextures[static_cast<int>(Textures::BackBuffer)].SetAsActiveTarget();

		SetScreendumpTexture(myFrameworkPtr->GetContext(), myFrameworkPtr->GetBackbufferTexture());

		myScreenSize = { aWidth,  aHeight };
	}
}

#if ENABLERENDERPASSITERATIONS
bool RenderManager::EarlyReturn(RenderStep aStep, Textures aTexture)
{

	if (myCurrentRenderStep == aStep)
	{
		myTextures[ENUM_CAST(Textures::BackBuffer)].SetAsActiveTarget();
		myTextures[static_cast<int>(aTexture)].SetAsResourceOnSlot(0);
		myFullscreenRenderer.Render(FullscreenRenderer::Shader::COPY);
		return true;
	}
	return false;
}
#endif // ENABLERENDERPASSITERATIONS
