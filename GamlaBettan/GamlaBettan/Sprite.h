#pragma once
#include <d3dcommon.h>
#include "Vector2.hpp"
#include "Observer.hpp"

struct ID3D11Buffer;
struct ID3D11VertexShader;
struct ID3D11PixelShader;
struct ID3D11InputLayout;
struct ID3D11ShaderResourceView;

class Sprite : public Observer
{
public:
	struct SpriteData
	{
		UINT myVertexCount = 0;
		UINT myIndexCount = 0;
		UINT myStride = 0;
		UINT myOffset = 0;
		UINT myPrimitiveTopology = D3D11_PRIMITIVE_TOPOLOGY_UNDEFINED;

		ID3D11Buffer*				myIndexBuffer = nullptr;
		ID3D11Buffer*				myVertexBuffer = nullptr;
		class VertexShader*			myVertexShader = nullptr;
		class PixelShader*			myPixelShader = nullptr;
		ID3D11InputLayout*			myInputLayout = nullptr;
		ID3D11ShaderResourceView*	myTexture = nullptr;
		CommonUtilities::Vector2<unsigned int> mySize = { 0, 0 };
	};

	Sprite();
	~Sprite();

	void Init(const SpriteData& aSpriteData, const V2F& aWindowSize);
	SpriteData& GetSpriteData();
	const SpriteData& GetSpriteData() const;
	const CommonUtilities::Vector2<float>& GetWindowSize() const;

	void RecieveMessage(const Message& aMessage) override;

private:
	SpriteData mySpriteData;
	CommonUtilities::Vector2<float> myWindowSize;
	
};