#pragma once
#include <Matrix3x3.hpp>
#include <Matrix4x4.hpp>
#include <Vector3.hpp>
#include <Vector4.hpp>
#include <Sphere.hpp>

#if USEIMGUI
#include <map>
#include <vector>
#include <string>
#endif

class Model;
class Camera;
class Animator;
class GBPhysXActor;

class ModelInstance
{
public:
	ModelInstance(Model* aModel);
	Model* GetModel();
	CommonUtilities::Matrix4x4<float> GetModelToWorldTransform();
	void SetPosition(CommonUtilities::Vector4<float> aPosition);
	void Rotate(CommonUtilities::Vector3<float> aRotation);
	void Rotate(const CommonUtilities::Matrix4x4<float>& aRotationMatrix);
	void SetRotation(CommonUtilities::Vector3<float> aRotation);
	void SetRotation(const CommonUtilities::Matrix4x4<float>& aTargetRotation);
	void SetScale(CommonUtilities::Vector3<float> aScale);
	void SetShouldBeDrawnThroughWalls(const bool aFlag);
	void SetUsePlayerThroughWallShader(const bool aFlag);
	void SetShouldRender(const bool aFlag);
	void AttachAnimator(Animator* aAnimator);
	void SetAnimation(int aAnimation);
	void StepAnimation(float aDeltaTime);
	void ResetSpawnTime();
	//TODO RAGDOLL
	/*
	void DetachAnimator();
	void SetGBPhysXActor(GBPhysXActor* aGBPhysXActor);
	*/
	bool HasAnimations();

	void SetTint(V4F aTint);
	V4F GetTint();

	bool ShouldBeDrawnThroughWalls() const;
	bool IsUsingPlayerThroughWallShader() const;
	bool ShouldRender() const;

	CommonUtilities::Vector3<float> GetPosition() const;

	const std::string GetFriendlyName();

	void SetupanimationMatrixes(std::array<CommonUtilities::Matrix4x4<float>,NUMBEROFANIMATIONBONES>& aMatrixes);

#if USEIMGUI
	virtual bool ImGuiNode(std::map<std::string, std::vector<std::string>>& aFiles, Camera* aCamera);
#endif // !_RETAIL

	CommonUtilities::Sphere<float> GetGraphicBoundingSphere(float aRangeModifier = 1.f);
	const float GetSpawnTime();
	void SetExpectedLifeTime(float aLifeTime);
	const float GetExpectedLifeTime();

	void SetCustomPixelData(float aData[MODELSAMOUNTOFCUSTOMDATA]);
	void SetCustomVertexData(float aData[MODELSAMOUNTOFCUSTOMDATA]);

	void SetIsHighlighted(bool aState);
	bool GetIsHighlighted();

	void SetUsingSecondaryFov(bool aState);
	bool GetIsUsingSecondaryFov();
private:

	bool myIsHighlighted = false;
	bool myIsUsingSecondaryFov = false;

	float myCustomData[MODELSAMOUNTOFCUSTOMDATA*2];

	std::string myFriendlyName;
	//CommonUtilities::Matrix4x4<float> myScaleAndRotate;
	CommonUtilities::Matrix4x4<float> myRotation;

	CommonUtilities::Vector4<float> myPosition;
	V4F myTint;

	CommonUtilities::Vector3<float> myScale;

	class Animator* myAnimator;
	Model* myModel;

	float myGaphicBoundsModifier;

	bool myShouldBeDrawnThroughWalls;
	bool myUsePlayerThroughWallShader;
	bool myShouldRender;
	float mySpawnTime;
	float myExpectedLifeTime;
	//TODO RAGDOLL
	/*
	GBPhysXActor* myGBPhysXActor = nullptr;
	*/
};

