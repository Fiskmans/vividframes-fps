#include "pch.h"
#include "ParticleFactory.h"
#include "DirectX11Framework.h"
#include "ParticleInstance.h"
#include "TextureLoader.h"
#include <d3d11.h>
#include "ShaderCompiler.h"
#include <fstream>
#include "WindSystem.h"

#if USEIMGUI
#include <imgui.h>
#include "Scene.h"
#include "Camera.h"
#define _SILENCE_EXPERIMENTAL_FILESYSTEM_DEPRECATION_WARNING
#include <experimental/filesystem>
#endif
#ifdef _DEBUG
#include "DebugTools.h"
#endif

#pragma region ParticleSaveVersionHandling
namespace SavingAndLoading
{
	void LoadInto(Particle::Data::Customizable& aParticle,const std::string& aFilepath);
	void SaveFrom(Particle::Data::Customizable& aParticle,const std::string& aFilepath);
	void Default(Particle::Data::Customizable& aParticle);
	void Reconstruct(ID3D11Device* aDevice, Particle& aParticle);
}
#pragma endregion

#pragma region Constructing
namespace Constructing
{
	ID3D11Buffer* CreateBufferFor(ID3D11Device* aDevice, Particle::Data& aParticle);
}
#pragma endregion



ParticleFactory::ParticleFactory() :
	myDevice(nullptr),
	myVertexShader(nullptr),
	myGeometryShader(nullptr),
	myPixelShader(nullptr),
	myInputLayout(nullptr)
{
}
ParticleFactory::~ParticleFactory()
{
	for (auto& i : myParticles)
	{
		SAFE_DELETE(i.second);
	}
}

bool ParticleFactory::Init(DirectX11Framework* aFramework)
{
	myDevice = aFramework->GetDevice();

	myPixelShader = GetPixelShader(myDevice, "Data/Shaders/Particle/ParticlePixelShader.hlsl");
	if (!myPixelShader)
	{
		SYSERROR("Could not compile particle pixelshader","");
		return false;
	}

	if (!LoadGeometryShader(myDevice, "Data/Shaders/Particle/ParticleGeometryShader.hlsl", myGeometryShader))
	{
		SYSERROR("Could not compile particle geometryshader","");
		return false;
	}

	std::vector<char> vertexBlob;
	myVertexShader = GetVertexShader(myDevice, "Data/Shaders/Particle/ParticleVertexShader.hlsl", vertexBlob);
	if (!myVertexShader)
	{
		SYSERROR("Could not Compile particle vertexshader","");
		return false;
	}

	D3D11_INPUT_ELEMENT_DESC layout[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 }, 
		{ "MOVEMENT", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 }, 
		{ "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "SIZE", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "DISTANCE", 0, DXGI_FORMAT_R32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "LIFETIME", 0, DXGI_FORMAT_R32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "UVMIN", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 }, 
		{ "UVMAX", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "FBTIMER", 0, DXGI_FORMAT_R32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};

	HRESULT result = myDevice->CreateInputLayout(layout, sizeof(layout) / sizeof(layout[0]), vertexBlob.data(), vertexBlob.size(), &myInputLayout);
	if (FAILED(result))
	{
		SYSERROR("could not create input layout for particle","");
		return false;
	}

	static bool once = true;
	if (once)
	{
		WindSystem::GetInstance().Init(V3F(2.f, 0.f, 3.f).GetNormalized());
		once = false;
	}

	return true;
}

ParticleInstance* ParticleFactory::InstantiateParticle(const std::string& aFilePath)
{
	if (!myDevice)
	{
		SYSERROR("Yo wtf, init pls","");
	}

	Particle* baseParticle = nullptr;
	if (myParticles.count(aFilePath) == 0)
	{
		baseParticle = LoadParticle(aFilePath);
		myParticles[aFilePath] = baseParticle;
	}
	else
	{
		baseParticle = myParticles[aFilePath];
	}
	ParticleInstance* instance = new ParticleInstance();
	instance->SetDirection(V4F(0, 1, 0, 0));
	instance->Init(baseParticle);
	return instance;
}

#if USEIMGUI
void EditOverTime(Particle::Data::Customizable::OverTime& aOverTime)
{
	ImGui::DragFloat("Size", &aOverTime.mySize,0.01f);
	ImGui::ColorPicker4("Color", &aOverTime.myParticleColor.x);
}

void ParticleFactory::EditParticles(Scene* aScene)
{
	static V4F direction;
	if (ImGui::Button("Check For Particles"))
	{
		std::experimental::filesystem::directory_iterator it(std::experimental::filesystem::canonical("Data/Particles/"));
		while (it != std::experimental::filesystem::directory_iterator())
		{
			if (it->path().has_extension())
			{
				if (it->path().extension() == ".part")
				{
					delete InstantiateParticle(it->path().filename().string());
				}
			}
			it++;
		}
	}

	static std::unordered_map<std::string, Particle*>::iterator editing = myParticles.end();
	static ParticleInstance* instance = nullptr;
	if (!aScene->Contains(instance))
	{
		instance = nullptr;
	}
	if (instance)
	{
		instance->RefreshTimeout(1.f);
	}
	if (ImGui::DragFloat3("Direction", &direction.x, 0.01f, -2, 2))
	{
		if (instance)
		{
			instance->SetDirection(direction);
		}
	}
	ImGui::Separator();
	auto it = myParticles.begin();
	while(it != myParticles.end())
	{
		if (ImGui::Selectable(it->first.c_str()))
		{
			editing = it;
			if (instance)
			{
				aScene->RemoveFrom(instance);
				SAFE_DELETE(instance);
			}
			instance = InstantiateParticle(it->first);
			instance->SetDirection(direction);
			aScene->AddInstance(instance);
		}
		it++;
	}
	ImGui::Separator();
	if (editing != myParticles.end())
	{
		ImGui::Text(editing->first.c_str());
		Particle::Data::Customizable& customizable = editing->second->GetData().myCustomizable;

		if (ImGui::Button("Save"))
		{
			SavingAndLoading::SaveFrom(customizable, "Data/Particles/" + editing->first);
		}
		ImGui::SameLine();
		if (ImGui::Button("Load"))
		{
			SavingAndLoading::LoadInto(customizable, "Data/Particles/" + editing->first);

			std::string after = customizable.myFilePath;
			Texture* newTexture = LoadTexture(myDevice, after);
			if (!IsErrorTexture(newTexture))
			{
				if (!IsErrorTexture(editing->second->GetData().myTexture))
				{
					editing->second->GetData().myTexture->Release();
				}
				editing->second->GetData().myTexture = newTexture;
			}
		}
		{
			std::string before = customizable.myFilePath;
			ImGui::InputText("Texture", customizable.myFilePath, 96);
#ifdef _DEBUG
			ImGui::SameLine();
			if (ImGui::Button("Select"))
			{
				ImGui::OpenPopup("textureSelector");
			}
			if (ImGui::BeginPopup("textureSelector"))
			{
				ImGui::Text("Textures");
				ImGui::Separator();
				for (auto& i : DebugTools::FileList->operator[](".dds"))
				{
					if (ImGui::Selectable(i.c_str()))
					{
						strcpy_s<sizeof(customizable.myFilePath)>(customizable.myFilePath, i.c_str());
					}
				}
				ImGui::EndPopup();
			}
#endif // _DEBUG


			if (before != customizable.myFilePath)
			{
				std::string after = customizable.myFilePath;
				Texture* newTexture = LoadTexture(myDevice, after);
				if (!IsErrorTexture(newTexture))
				{
					if (!IsErrorTexture(editing->second->GetData().myTexture))
					{
						editing->second->GetData().myTexture->Release();
					}
					editing->second->GetData().myTexture = newTexture;
				}
			}
		}

		float before = customizable.mySpawnRate;
		if (ImGui::DragFloat("Emission Rate",&customizable.mySpawnRate,0.1f,0.001f,10000.f))
		{
			if (before > customizable.myParticleLifetime)
			{
				SavingAndLoading::Reconstruct(myDevice, *editing->second);
			}
		}
		before = customizable.myParticleLifetime;
		if (ImGui::DragFloat("Lifetime", &customizable.myParticleLifetime,0.1f,0.01f,1000.f))
		{
			if (before < customizable.myParticleLifetime)
			{
				SavingAndLoading::Reconstruct(myDevice, *editing->second);
			}
		}
		ImGui::DragFloat("Seperation", &customizable.mySeperation, 0.01f, 0.0f, 3.0f);
		ImGui::SameLine();
		if (ImGui::Button("Default"))
		{
			customizable.mySeperation = 1.f;
		}
		ImGui::DragFloat3("Gravity xyz", &customizable.myGravity.x,0.1f, -20.f, 20.f);
		ImGui::SameLine();
		if (ImGui::Button("Zero"))
		{
			customizable.myGravity = V3F(0, 0, 0);
		}

		ImGui::DragFloat("Speed", &customizable.myParticleSpeed);
		ImGui::DragFloat("Drag", &customizable.myDrag,0.01f,0.0f,100.0f);

		ImGui::DragFloat("Time Per Frame", &customizable.myFlipBook.myTime);
		ImGui::DragInt("Pages", &customizable.myFlipBook.myPages);
		ImGui::DragInt("SizeX", &customizable.myFlipBook.mySizeX);
		ImGui::DragInt("SizeY", &customizable.myFlipBook.mySizeY);

		ImGui::Columns(2);
		ImGui::Text("Start");
		ImGui::PushID("Start");
		EditOverTime(customizable.myStart);
		ImGui::PopID();
		ImGui::NextColumn();
		ImGui::Text("End");
		ImGui::PushID("End");
		EditOverTime(customizable.myEnd);
		ImGui::PopID();
		ImGui::NextColumn();
	}

}
#endif

Particle* ParticleFactory::LoadParticle(const std::string& aFilePath)
{

	Particle::Data data;


	SavingAndLoading::LoadInto(data.myCustomizable, "Data/Particles/" + aFilePath);


	data.myOffset = 0;
	data.myPrimitiveTopology = D3D11_PRIMITIVE_TOPOLOGY_POINTLIST;
	data.myInputLayout = myInputLayout;
	data.myVertexShader = myVertexShader;
	data.myGeometryShader = myGeometryShader;
	data.myPixelShader = myPixelShader;
	data.myStride = sizeof(Particle::Vertex);

	std::string path = data.myCustomizable.myFilePath;
	data.myTexture = LoadTexture(myDevice, path);

	data.myParticleVertexBuffer = Constructing::CreateBufferFor(myDevice, data);

	Particle* particle = new Particle();
	particle->Init(data);
	return particle;
}



namespace SavingAndLoading
{
	const static unsigned short CurrentVersion = 2;
	struct Header
	{
		unsigned short myVersionNumber;
		char unused[30];
	};

	namespace Version0
	{
		void LoadInto(LegacyParticleTypes::Customizable_V1& aParticle, Header& aHeader,std::ifstream& aFileStream)
		{
			LegacyParticleTypes::Customizable_V0 buffer;
			aFileStream.read(reinterpret_cast<char*>(&buffer), sizeof(LegacyParticleTypes::Customizable_V0));
			aParticle.myEnd.myParticleColor = buffer.myEnd.myParticleColor;
			aParticle.myEnd.mySize = buffer.myEnd.mySize;
			memcpy(aParticle.myFilePath,buffer.myFilePath, sizeof(buffer.myFilePath));
			aParticle.myParticleLifetime = buffer.myParticleLifetime;
			aParticle.myParticleSpeed = buffer.myParticleSpeed;

			aParticle.mySeperation = buffer.mySpawnAgnle;
			aParticle.mySpawnRate = buffer.mySpawnRate;
			aParticle.myStart.myParticleColor = buffer.myStart.myParticleColor;
			aParticle.myStart.mySize = buffer.myStart.mySize;
			aParticle.myGravity = V3F(0,0,0);
			aParticle.myDrag = 0.0f;
		}
	}

	namespace Version1
	{
		void LoadInto(Particle::Data::Customizable& aParticle, Header& aHeader, std::ifstream& aFileStream)
		{
			LegacyParticleTypes::Customizable_V1 buffer;
			if (aHeader.myVersionNumber < 1)
			{
				Version0::LoadInto(buffer, aHeader, aFileStream);
			}
			else
			{
				aFileStream.read(reinterpret_cast<char*>(&buffer), sizeof(LegacyParticleTypes::Customizable_V1));
			}

			aParticle.myEnd.myParticleColor = buffer.myEnd.myParticleColor;
			aParticle.myEnd.mySize = buffer.myEnd.mySize;
			memcpy(aParticle.myFilePath, buffer.myFilePath, sizeof(buffer.myFilePath));
			aParticle.myParticleLifetime = buffer.myParticleLifetime;
			aParticle.myParticleSpeed = buffer.myParticleSpeed;

			aParticle.mySeperation = buffer.mySeperation;
			aParticle.mySpawnRate = buffer.mySpawnRate;
			aParticle.myStart.myParticleColor = buffer.myStart.myParticleColor;
			aParticle.myStart.mySize = buffer.myStart.mySize;
			aParticle.myGravity = V3F(0, 0, 0);
			aParticle.myDrag = 0.0f;

			aParticle.myFlipBook.myTime = 0;
			aParticle.myFlipBook.myPages = 0;
			aParticle.myFlipBook.mySizeX = 0;
			aParticle.myFlipBook.mySizeY = 0;
		}
	}
	void LoadInto(Particle::Data::Customizable& aParticle, Header& aHeader, std::ifstream& aFileStream)
	{
		aFileStream.read(reinterpret_cast<char*>(&aParticle), sizeof(Particle::Data::Customizable));
	}


	void LoadInto(Particle::Data::Customizable& aParticle, const std::string& aFilepath)
	{
		static_assert(CHAR_BIT == 8, "Assumtion wrong");
		static_assert(sizeof(Header) == 32, "Header struct is invalid");
		Header header;
		std::ifstream infile;
		infile.open(aFilepath,std::ios::binary | std::ios::in);			//Open file
		infile.read(reinterpret_cast<char*>(&header), sizeof(Header));	//Read header
		if (infile)														//Load using appropriate loader
		{
			if (header.myVersionNumber == CurrentVersion)
			{
				LoadInto(aParticle, header, infile);
			}
			else
			{
				if (header.myVersionNumber > CurrentVersion)
				{
					SYSERROR("Particle is not compatible with this version", aFilepath);
					Default(aParticle);
				}
				else
				{
					LOGWARNING("Particle is outdated and will be loaded using a legacy loader. file version: " + std::to_string(header.myVersionNumber) + " code version: " + std::to_string(CurrentVersion), aFilepath);
					switch (header.myVersionNumber)
					{
					case 0:
					case 1:
						Version1::LoadInto(aParticle, header, infile);
						break;

					default:
						SYSERROR("Trying to load particle with a unsupported version", aFilepath);
						break;
					}
				}
			}
		}
		else
		{
			Default(aParticle);
		}
	}
	void SaveFrom(Particle::Data::Customizable& aParticle, const std::string& aFilepath)
	{
		static_assert(CHAR_BIT == 8, "Assumtion wrong");
		static_assert(sizeof(Header) == 32, "Header struct is invalid");
		
		Header header;
		header.myVersionNumber = CurrentVersion;

		std::ofstream outfile;
		outfile.open(aFilepath, std::ios::out | std::ios::binary);
		outfile.write(reinterpret_cast<char*>(&header), sizeof(Header));
		outfile.write(reinterpret_cast<char*>(&aParticle), sizeof(Particle::Data::Customizable));
		

	}
	void Reconstruct(ID3D11Device* aDevice, Particle& aParticle)
	{
		ID3D11Buffer* buffer = Constructing::CreateBufferFor(aDevice,aParticle.GetData());

		if (buffer)
		{
			aParticle.GetData().myParticleVertexBuffer->Release();
			aParticle.GetData().myParticleVertexBuffer = buffer;
		}
	}
	void Default(Particle::Data::Customizable& aParticle)
	{
		aParticle.mySeperation = 1;
		aParticle.myParticleSpeed = 100;
		aParticle.myParticleLifetime = 3.5f;
		aParticle.mySpawnRate = 40;

		aParticle.myStart.myParticleColor = { 1,1,0,0.8f };
		aParticle.myStart.mySize = 10;

		aParticle.myEnd.myParticleColor = { 1,0,0,0 };
		aParticle.myEnd.mySize = 5;

		
		strcpy_s(aParticle.myFilePath,96, "Data/Textures/gamlaBettan_Alpha.dds");
	}
}

namespace Constructing
{
	ID3D11Buffer* CreateBufferFor(ID3D11Device* aDevice, Particle::Data& aParticle)
	{
		CD3D11_BUFFER_DESC desc;
		ZeroMemory(&desc, sizeof(desc));
		desc.ByteWidth = CAST(UINT, sizeof(Particle::Vertex) * aParticle.myCustomizable.myParticleLifetime * aParticle.myCustomizable.mySpawnRate);
		desc.Usage = D3D11_USAGE_DYNAMIC;
		desc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
		desc.MiscFlags = 0;

		ID3D11Buffer* buffer;

		HRESULT result = aDevice->CreateBuffer(&desc, nullptr, &buffer);
		if (FAILED(result))
		{
			SYSERROR("Could not create vertex buffer for particles","");
			return nullptr;
		}
		return buffer;
	}
}