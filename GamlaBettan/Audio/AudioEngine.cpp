#include <pch.h>
#include "AudioEngine.h"
#include "fmod_studio_guids.hpp"

#include <Logger.h>

Implementation* myImplementation = nullptr;

Implementation::Implementation() :
	myStudioSystem(nullptr),
	mySystem(nullptr)
{
	FMOD_RESULT result;

	result = FMOD::Studio::System::create(&myStudioSystem);
	if (result != FMOD_OK)
	{
		SYSERROR("Failed to create Fmod studiosystem","");
	}

#ifdef _DEBUG
	result = myStudioSystem->initialize(32, FMOD_STUDIO_INIT_LIVEUPDATE, FMOD_INIT_PROFILE_ENABLE, NULL);
#else
	result = myStudioSystem->initialize(32, FMOD_STUDIO_INIT_NORMAL, FMOD_INIT_NORMAL, NULL);
#endif
	if (result != FMOD_OK)
	{
		SYSERROR("Failed to Initialize Fmod studiosystem","");
	}

	result = myStudioSystem->getCoreSystem(&mySystem);
	if (result != FMOD_OK)
	{
		SYSERROR("Failed to Initialize Fmod coresystem","");
	}
}

Implementation::~Implementation()
{
	FMOD_RESULT result;
	
	result = myStudioSystem->unloadAll();
	if (result != FMOD_OK)
	{
		SYSERROR("Failed to unload all Fmod", "");
	}

	result = myStudioSystem->release();
	if (result != FMOD_OK)
	{
		SYSERROR("Failed to release Fmod", "");
	}
}

void Implementation::Update()
{
	FMOD_RESULT result = myStudioSystem->update();
	if (result != FMOD_OK)
	{
		SYSERROR("Failed to Update Fmod studiosystem", "");
	}
}

void AudioEngine::Init()
{
	myImplementation = new Implementation;
}

void AudioEngine::Update()
{
	myImplementation->Update();
}

void AudioEngine::Shutdown()
{
	delete myImplementation;
}

void AudioEngine::LoadBank(const char* aBankName, FMOD_STUDIO_LOAD_BANK_FLAGS flags)
{
	FMOD_RESULT result;

	FMOD::Studio::Bank* pBank;
	result = myImplementation->myStudioSystem->loadBankFile(aBankName, flags, &pBank);
	if (result != FMOD_OK)
	{
		SYSERROR("failed to load bank", "");
	}

	FMOD_STUDIO_LOADING_STATE state;
	pBank->getSampleLoadingState(&state);

	if (state == FMOD_STUDIO_LOADING_STATE::FMOD_STUDIO_LOADING_STATE_UNLOADED)
	{
		pBank->loadSampleData();
	}
	else
	{
		SYSERROR("failed to load sampleData","");
	}
}

void AudioEngine::UnloadBank(const FMOD_GUID& aBankID)
{
	FMOD_RESULT result;

	FMOD::Studio::Bank* bank;
	result = myImplementation->myStudioSystem->getBankByID(&aBankID, &bank);
	if (result != FMOD_OK)
	{
		SYSERROR("failed to get bank by ID","");
	}

	result = bank->unload();
	if (result != FMOD_OK)
	{
		SYSERROR("failed to unload bank","");
	}
}

FMOD::Studio::EventInstance* AudioEngine::CreateEventInstance(const FMOD_GUID& aEventID)
{
	FMOD_RESULT result;

	FMOD::Studio::EventDescription* pEventDescription = nullptr;
	result = myImplementation->myStudioSystem->getEventByID(&aEventID, &pEventDescription);
	if (result != FMOD_OK)
	{
		SYSERROR("failed to get event by ID","");
	}


	FMOD::Studio::EventInstance* pEventInstance = nullptr;
	result = pEventDescription->createInstance(&pEventInstance);
	if (result != FMOD_OK)
	{
		SYSERROR("failed to create eventInstance","");
	}

	return pEventInstance;
}

FMOD::Studio::Bus* AudioEngine::GetBus(const FMOD_GUID& aID)
{
	FMOD_RESULT result;

	FMOD::Studio::Bus* returnBus;
	result = myImplementation->myStudioSystem->getBusByID(&aID, &returnBus);
	if (result != FMOD_OK)
	{
		SYSERROR("failed to get bus by ID","");
	}

	return returnBus;
}

void AudioEngine::SetListenerAttributes(const FMOD_3D_ATTRIBUTES& someAttributes)
{
	FMOD_RESULT result;

	result = myImplementation->myStudioSystem->setListenerAttributes(0, &someAttributes);
	if (result != FMOD_OK)
	{
		SYSERROR("failed to set Listener", "");
	}
}

void AudioEngine::SetMasterVolume(const float& aVolume)
{
	FMOD_RESULT result;

	FMOD::SoundGroup* master;
	result = myImplementation->mySystem->getMasterSoundGroup(&master);
	if (result != FMOD_OK)
	{
		SYSERROR("failed to get master soundgroup", "");
	}

	result = master->setVolume(aVolume);
	if (result != FMOD_OK)
	{
		SYSERROR("failed to set masterVolume", "");
	}
}

FMOD_VECTOR AudioEngine::VectorToFmod(const CU::Vector3<float>& vPosition)
{
	FMOD_VECTOR fVec;
	fVec.x = vPosition.x;
	fVec.y = vPosition.y;
	fVec.z = vPosition.z;
	return fVec;
}