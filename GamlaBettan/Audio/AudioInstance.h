#pragma once
#include "AudioManager.h"
#include "Vector3.hpp"

namespace FMOD
{
	namespace Studio
	{
		class EventInstance;
	}
}

struct Audio3DAttributes
{
	V3F position = { 0 };
	V3F velocity = { 0 };
	V3F forward = { 0 };
	V3F up = { 0 };
};

class AudioInstance
{
public:

	friend AudioManager;

	AudioInstance(bool shouldFinnishPlaying = false);
	~AudioInstance();

	void Set3DAttributes(const Audio3DAttributes& some3DAttributes);
	void KillAudio();

	bool GetisDead();
	bool GetShouldFinnishPlaying();

private:
	FMOD::Studio::EventInstance* myEventInstance;
	bool myIsDead;
	bool myShouldFinnishPlaying;
};