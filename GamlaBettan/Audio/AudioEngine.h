#pragma once

#include "fmod_studio.hpp"
#include "fmod_studio_common.h"
#include "fmod.hpp"

#pragma comment (lib, "fmod.lib")
#pragma comment (lib, "fmodstudio.lib")
#pragma comment (lib, "fmod_vc.lib")
#pragma comment (lib, "fmodstudio_vc.lib")

namespace CommonUtilities
{
	template<class T>
	class Vector3;
}

//#ifdef SHOULDCOMPILEAUDIO

struct Implementation {
	Implementation();
	~Implementation();

	void Update();

	FMOD::Studio::System* myStudioSystem;
	FMOD::System* mySystem;
};


class AudioEngine {
public:
	static void Init();
	static void Update();
	static void Shutdown();

	void LoadBank(const char* aBankName, FMOD_STUDIO_LOAD_BANK_FLAGS flags);
	void UnloadBank(const FMOD_GUID& aBankID);
	
	FMOD::Studio::EventInstance* CreateEventInstance(const FMOD_GUID& aEventID);

	FMOD::Studio::Bus* GetBus(const FMOD_GUID& aID);

	void SetListenerAttributes(const FMOD_3D_ATTRIBUTES& someAttributes);
	void SetMasterVolume(const float& aVolume);

	FMOD_VECTOR VectorToFmod(const  CommonUtilities::Vector3<float>& aPosition);
};