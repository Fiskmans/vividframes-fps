#pragma once
#include <vector>
#include <array>
#include "Observer.hpp"
#include <unordered_map>

class AudioInstance;
class AudioEngine;
class CCamera;
struct Audio3DAttributes;
struct FMOD_VECOTR;
struct FMOD_GUID;

namespace FMOD
{
	namespace Studio
	{
		class Bus;
	}
}

enum class Reverb
{
	Cave,
	City,
	Hallway,
	LivingRoom,
	StoneRoom,

	count
};

class AudioManager : public Observer
{
public:

	AudioManager();
	~AudioManager();

	bool Init();
	void Shutdown();

	void Update(const float& aDeltaTime);

	void RecieveMessage(const Message& aMessage) override;

	void SetMasterVolume(const float& aVolume);
	void SetSFXVolume(const float& aVolume);
	void SetMusicVolume(const float& aVolume);

	void Set3DListenerAttributes(const Audio3DAttributes& someAttributes);
	AudioInstance* CreateAudioInstance(const FMOD_GUID& aEventID, const Audio3DAttributes& someAttributes, bool shouldFinnishPlaying = false);
	void PlayOneShot3D(const FMOD_GUID& aEventID, Audio3DAttributes someAttributes);
	void PlayOneShot2D(const FMOD_GUID& aEventID);

	void ActivateReverb(Reverb aReverb);
	void DeactivateReverb();

	void PlayMusic(const FMOD_GUID& aEventID);

	void KillAllAudio();

private:
	void SubscribeToMessages();
	void UnsubscribeToMessages();

private: 
	AudioEngine* myAudioEngine;

	AudioInstance* myCurrentMusic;
	AudioInstance* myCurrentAmbience;

	std::array<FMOD::Studio::Bus*, static_cast<int>(Reverb::count)> mySFXReverbBuses;
	FMOD::Studio::Bus* mySFXBus;
	FMOD::Studio::Bus* myMusicBus;

	std::vector<AudioInstance*> myAudioInstances;
	std::unordered_map<unsigned int, float> myAudioEventsOnCooldown;

	float myMusicSilenceTimer;
	FMOD_GUID* myCurrentMusicID;
	FMOD_GUID* myCurrentAmbienceID;

	float myBossScreamTimer;
	bool myInGame = false;
};