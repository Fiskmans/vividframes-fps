#include <pch.h>
#include "AudioInstance.h"
#include "fmod_studio.hpp"

AudioInstance::AudioInstance(bool shouldFinnishPlaying) :
	myEventInstance(nullptr),
	myIsDead(false),
	myShouldFinnishPlaying(shouldFinnishPlaying)
{

}

AudioInstance::~AudioInstance()
{
	myEventInstance = nullptr;
}

static FMOD_VECTOR VectorToFmod(const V3F& aPosition)
{
	FMOD_VECTOR fVec;
	fVec.x = aPosition.x;
	fVec.y = aPosition.y;
	fVec.z = aPosition.z;
	return fVec;
}

void AudioInstance::Set3DAttributes(const Audio3DAttributes& someAttributes)
{
	FMOD_3D_ATTRIBUTES attributes;
	attributes.forward = VectorToFmod(someAttributes.forward);
	attributes.position = VectorToFmod(someAttributes.position);
	attributes.up = VectorToFmod(someAttributes.up);
	attributes.velocity = VectorToFmod(someAttributes.velocity);

	myEventInstance->set3DAttributes(&attributes);
}

bool AudioInstance::GetisDead()
{
	return myIsDead;
}

bool AudioInstance::GetShouldFinnishPlaying()
{
	return myShouldFinnishPlaying;
}

void AudioInstance::KillAudio()
{
	FMOD_RESULT result;
	result = myEventInstance->stop(FMOD_STUDIO_STOP_ALLOWFADEOUT);
	myEventInstance->release();
	myIsDead = true;
}