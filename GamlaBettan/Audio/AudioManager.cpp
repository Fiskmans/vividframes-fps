#include <pch.h>
#include "AudioEngine.h"
#include "AudioManager.h"
#include "AudioInstance.h"
#include <assert.h>
#include "fmod_studio.hpp"
#include "../Tools/Logger.h"
#include "PostMaster.hpp"

#include "../Game/Audio.h"

#include "fmod_studio_guids.hpp"

AudioManager::AudioManager() :
	myAudioEngine(nullptr),
	myCurrentMusic(nullptr),
	myCurrentAmbience(nullptr),
	mySFXBus(nullptr),
	myMusicBus(nullptr)
{
}

AudioManager::~AudioManager()
{
	UnsubscribeToMessages();
}

static FMOD_VECTOR VectorToFmod(const V3F& aPosition)
{
	FMOD_VECTOR fVec;
	fVec.x = aPosition.x;
	fVec.y = aPosition.y;
	fVec.z = aPosition.z;
	return fVec;
}

bool AudioManager::Init()
{
	myAudioEngine->Init();

	myAudioEngine->LoadBank("Data/Sound/Desktop/Master.bank", FMOD_STUDIO_LOAD_BANK_NORMAL);
	myAudioEngine->LoadBank("Data/Sound/Desktop/Master.strings.bank", FMOD_STUDIO_LOAD_BANK_NORMAL);

	myAudioEngine->LoadBank("Data/Sound/Desktop/UI.bank", FMOD_STUDIO_LOAD_BANK_NORMAL);
	myAudioEngine->LoadBank("Data/Sound/Desktop/Ambience.bank", FMOD_STUDIO_LOAD_BANK_NORMAL);
	myAudioEngine->LoadBank("Data/Sound/Desktop/Music.bank", FMOD_STUDIO_LOAD_BANK_NORMAL);

	//TODO: Ladda in SFX ljud n�r man startar sj�lva spelet
	myAudioEngine->LoadBank("Data/Sound/Desktop/SFX.bank", FMOD_STUDIO_LOAD_BANK_NORMAL);

	myAudioEngine->SetMasterVolume(0.5f);

	mySFXReverbBuses[static_cast<int>(Reverb::Cave)] = myAudioEngine->GetBus(Bus::SFX_ReverbCave);
	mySFXReverbBuses[static_cast<int>(Reverb::City)] = myAudioEngine->GetBus(Bus::SFX_ReverbCity);
	mySFXReverbBuses[static_cast<int>(Reverb::Hallway)] = myAudioEngine->GetBus(Bus::SFX_ReverbHallway);
	mySFXReverbBuses[static_cast<int>(Reverb::LivingRoom)] = myAudioEngine->GetBus(Bus::SFX_ReverbLivingRoom);
	mySFXReverbBuses[static_cast<int>(Reverb::StoneRoom)] = myAudioEngine->GetBus(Bus::SFX_ReverbStoneRoom);


	mySFXBus = myAudioEngine->GetBus(Bus::SFX);
	myMusicBus = myAudioEngine->GetBus(Bus::Music);
	myMusicBus->setVolume(0.8f);

	DeactivateReverb();

	SubscribeToMessages();

	myCurrentMusicID = new FMOD_GUID(FMODEvent::Music_cold_Theme);
	myCurrentAmbienceID = new FMOD_GUID(FMODEvent::Ambience_Hallow_Wind);
	myMusicSilenceTimer = 5;

	myBossScreamTimer = 0;

	return true;
}

void AudioManager::Shutdown()
{
	myAudioEngine->Shutdown();
}

void AudioManager::Update(const float& aDeltaTime)
{
	myAudioEngine->Update();


	for (auto& element : myAudioEventsOnCooldown)
	{
		element.second -= aDeltaTime;
	}
	std::unordered_map<unsigned int, float>::iterator it = myAudioEventsOnCooldown.begin();
	while (it != myAudioEventsOnCooldown.end())
	{
		myAudioEventsOnCooldown;

		if (it->second <= 0)
		{
			it = myAudioEventsOnCooldown.erase(it);
		}
		else
		{
			it++;
		}
	}

	for (int i = 0; i < myAudioInstances.size(); ++i)
	{
		FMOD_STUDIO_PLAYBACK_STATE state;
		myAudioInstances[i]->myEventInstance->getPlaybackState(&state);
		if (state == FMOD_STUDIO_PLAYBACK_STOPPED)
		{
			myAudioInstances[i]->myEventInstance->release();
			delete myAudioInstances[i];
			myAudioInstances.erase(myAudioInstances.begin() + i);
		}
	}

	if (myInGame)
	{
		if (myMusicSilenceTimer > 0)
		{

			if (myCurrentMusic)
			{
				FMOD_STUDIO_PLAYBACK_STATE state;
				myCurrentMusic->myEventInstance->getPlaybackState(&state);

				if (state == FMOD_STUDIO_PLAYBACK_STOPPED)
				{
					myMusicSilenceTimer -= aDeltaTime;
				}
			}
			else
			{
				myMusicSilenceTimer -= aDeltaTime;
			}
		}
		else
		{
			PlayMusic(*myCurrentMusicID);
			myMusicSilenceTimer = 90.f;
		}

		if (myBossScreamTimer > 0)
		{
			myBossScreamTimer -= aDeltaTime;
			if (myBossScreamTimer <= 0)
			{
				PlayOneShot2D(FMODEvent::InGame_Enemies_BossScream);
			}
		}

	}
}

void AudioManager::RecieveMessage(const Message& aMessage)
{
	switch (aMessage.myMessageType)
	{
	case MessageType::ActivateReverb:
	{
		//ActivateReverb(Reverb::Cave);
		break;
	}
	case MessageType::DeactivateReverb:
	{
		DeactivateReverb();
		break;
	}
	case MessageType::NewLevelLoaded:
	{
		myInGame = true;
		if (aMessage.myIntValue2 == 0)
		{
			PlayOneShot2D(FMODEvent::Ambience_Hallow_Wind);
			*myCurrentMusicID = FMODEvent::Music_cold_Theme;
		}
		else if (aMessage.myIntValue2 > 3)
		{
			*myCurrentMusicID = FMODEvent::Music_dark_Strings;
		}
		//ActivateReverb(static_cast<Reverb>(1));
		break;
	}
	case MessageType::TriggerEvent:
	{
		if (aMessage.myIntValue == 5124)
		{
			PlayMusic(FMODEvent::Music_cold_Theme);
		}
		break;
	}
	case MessageType::MainMenuStateActivated:
	{
		myInGame = false;
		if (myCurrentMusic)
		{
			myCurrentMusic->KillAudio();
		}
		PlayOneShot2D(FMODEvent::Ambience_Hallow_Wind);
		break;
	}
	case MessageType::MenuButtonActive:
	{
		PlayOneShot2D(FMODEvent::Menues_Hover);
		break;
	}
	case MessageType::MenuButtonHit:
	{
		PlayOneShot2D(FMODEvent::Menues_Clicked);
		break;
	}
	default:
		break;
	}
}

void AudioManager::SetMasterVolume(const float& aVolume)
{
	myAudioEngine->SetMasterVolume(aVolume);
}

void AudioManager::SetSFXVolume(const float& aVolume)
{
	mySFXBus->setVolume(aVolume);
}

void AudioManager::SetMusicVolume(const float& aVolume)
{
	myMusicBus->setVolume(aVolume);
}

void AudioManager::Set3DListenerAttributes(const Audio3DAttributes& someAttributes)
{
	FMOD_3D_ATTRIBUTES attributes;
	attributes.forward = VectorToFmod(someAttributes.forward);
	attributes.position = VectorToFmod(someAttributes.position);
	attributes.up = VectorToFmod(someAttributes.up);
	attributes.velocity = VectorToFmod(someAttributes.velocity);

	myAudioEngine->SetListenerAttributes(attributes);
}

AudioInstance* AudioManager::CreateAudioInstance(const FMOD_GUID& aEventID, const Audio3DAttributes& someAttributes, bool ShouldFinnishPlaying)
{
	AudioInstance* instance;
	if (ShouldFinnishPlaying)
	{
		instance = new AudioInstance(ShouldFinnishPlaying);
	}
	else
	{
		instance = new AudioInstance();
	}

	if (myAudioEventsOnCooldown.find(aEventID.Data1) != myAudioEventsOnCooldown.end())
	{
		return nullptr;
	}

	instance->myEventInstance = myAudioEngine->CreateEventInstance(aEventID);

	FMOD_3D_ATTRIBUTES attributes;
	attributes.forward = VectorToFmod(someAttributes.forward);
	attributes.position = VectorToFmod(someAttributes.position);
	attributes.up = VectorToFmod(someAttributes.up);
	attributes.velocity = VectorToFmod(someAttributes.velocity);

	FMOD_RESULT result;
	result = instance->myEventInstance->set3DAttributes(&attributes);
	if (result != FMOD_OK)
	{
		SYSERROR("Failed to set audio 3D attributes", "");
	}

	result = instance->myEventInstance->start();
	if (result != FMOD_OK)
	{
		SYSERROR("Failed to start audio", "");
	}
	else
	{
		myAudioEventsOnCooldown[aEventID.Data1] = 0.1f;
	}

	myAudioInstances.push_back(instance);

	FMOD::Studio::EventDescription* desc;
	instance->myEventInstance->getDescription(&desc);

	bool hej;
	desc->isStream(&hej);

	if (hej)
	{
		SYSERROR("audio is streaming", "");
	}

	return instance;
}

void AudioManager::PlayOneShot3D(const FMOD_GUID& aEventID, Audio3DAttributes someAttributes)
{
	AudioInstance* instance = new AudioInstance();
	instance->myEventInstance = myAudioEngine->CreateEventInstance(aEventID);

	FMOD_3D_ATTRIBUTES attributes;
	attributes.forward = VectorToFmod(someAttributes.forward);
	attributes.position = VectorToFmod(someAttributes.position);
	attributes.up = VectorToFmod(someAttributes.up);
	attributes.velocity = VectorToFmod(someAttributes.velocity);

	FMOD_RESULT result;
	result = instance->myEventInstance->set3DAttributes(&attributes);
	if (result != FMOD_OK)
	{
		SYSERROR("Failed to set audio 3D attributes", "");
	}

	result = instance->myEventInstance->start();
	if (result != FMOD_OK)
	{
		SYSERROR("Failed to start audio", "");
	}
}

void AudioManager::PlayOneShot2D(const FMOD_GUID& aEventID)
{
	AudioInstance* instance = new AudioInstance();
	instance->myEventInstance = myAudioEngine->CreateEventInstance(aEventID);

	FMOD_RESULT result = instance->myEventInstance->start();
	if (result != FMOD_OK)
	{
		SYSERROR("Failed to start audio", "");
	}
}

void AudioManager::ActivateReverb(Reverb aReverb)
{
	for (auto* reverb : mySFXReverbBuses)
	{
		reverb->setMute(true);
	}
	mySFXReverbBuses[static_cast<int>(aReverb)]->setMute(false);
}

void AudioManager::DeactivateReverb()
{
	for (auto* reverb : mySFXReverbBuses)
	{
		reverb->setMute(true);
	}
}

void AudioManager::PlayMusic(const FMOD_GUID& aEventID)
{
	if (myCurrentMusic != nullptr)
	{
		myCurrentMusic->KillAudio();
		myCurrentMusic->myEventInstance->release();
	}
	else
	{
		myCurrentMusic = new AudioInstance();
	}
	myCurrentMusic->myEventInstance = myAudioEngine->CreateEventInstance(aEventID);

	FMOD_RESULT result;
	result = myCurrentMusic->myEventInstance->start();
	if (result != FMOD_OK)
	{
		SYSERROR("Failed to start audio", "");
	}
}

void AudioManager::KillAllAudio()
{
	for (int i = 0; i < myAudioInstances.size(); ++i)
	{
		myAudioInstances[i]->KillAudio();
		delete myAudioInstances[i];
	}
	myAudioInstances.clear();

	if (myCurrentAmbience)
		myCurrentAmbience->KillAudio();

	if (myCurrentMusic)
		myCurrentMusic->KillAudio();

}

void AudioManager::SubscribeToMessages()
{
	SubscribeToMessage(MessageType::ActivateReverb);
	SubscribeToMessage(MessageType::DeactivateReverb);

	SubscribeToMessage(MessageType::NewLevelLoaded);

	SubscribeToMessage(MessageType::TriggerEvent);
	SubscribeToMessage(MessageType::MainMenuStateActivated);
	SubscribeToMessage(MessageType::MenuButtonActive);
	SubscribeToMessage(MessageType::MenuButtonHit);
}

void AudioManager::UnsubscribeToMessages()
{
	UnSubscribeToMessage(MessageType::MainMenuStateActivated);
	UnSubscribeToMessage(MessageType::ActivateReverb);
	UnSubscribeToMessage(MessageType::DeactivateReverb);

	UnSubscribeToMessage(MessageType::NewLevelLoaded);

	UnSubscribeToMessage(MessageType::TriggerEvent);

	UnSubscribeToMessage(MessageType::MenuButtonActive);
	UnSubscribeToMessage(MessageType::MenuButtonHit);
}