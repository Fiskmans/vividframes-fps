#pragma once

#include <random>

namespace CommonUtilities
{
	int RandomInt(int aMin, int aMax);
	float RandomFloat(float aMin, float aMax);
}